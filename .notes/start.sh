#!/usr/bin/env bashsudo

docker build -t soltec-api .

docker run --name soltec-api-instance -i -t soltec-api

# docker run --rm -it soltec-api sh
# pm2 start processes.json --no-daemon
#
# "args": ["--ignore-watch=\.git|node_modules"]

