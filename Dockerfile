# Dockerfile
# using debian:stretch for it's smaller size over ubuntu
FROM debian:stretch

# Replace shell with bash so we can source files
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# Set environment variables
ENV appDir /var/www/app/current

# Add backports to packages
RUN echo "deb http://ftp.debian.org/debian stretch-backports main contrib non-free" >> /etc/apt/sources.list

# Run updates and install deps
RUN apt-get update

RUN apt-get install -y -q --no-install-recommends \
    apt-transport-https \
    build-essential \
    ca-certificates \
    aptitude \
    curl \
    g++ \
    gcc \
    git \
    make \
    sudo \
    wget \
    tar \
    python2.7 \
    default-jre \
    nano

RUN apt-get install -y -q --no-install-recommends \
    libreoffice \
    pdftk \
    ghostscript


 RUN wget -O libreoffice http://downloadarchive.documentfoundation.org/libreoffice/old/5.3.6.1/deb/x86_64/LibreOffice_5.3.6.1_Linux_x86-64_deb.tar.gz
 RUN tar -xzvf libreoffice
 RUN dpkg -i LibreOffice_5.3.6.1_Linux_x86-64_deb/DEBS/*.deb
 RUN rm -r LibreOffice_5.3.6.1_Linux_x86-64_deb/DEBS/
#
#########################################################################
# ^^^ RESULT OF UPPER TRY WAS: ^^^
# root@soltec-dev:/var/www/app/current# libreoffice --version
# bash: libreoffice: command not found
# root@soltec-dev:/var/www/app/current# libreoffice5.3 --version
#  /opt/libreoffice5.3/program/soffice.bin: error while loading shared libraries: libdbus-glib-1.so.2: cannot open shared object file: No such file or directory
#########################################################################

#RUN aptitude -t jessie-backports \
#             -y -f --no-gui --without-recommends \
#             -o Dpkg::Options::='--force-confnew' \
#             -o Debug::pkgProblemResolver=yes \
#             install libreoffice


RUN apt-get remove -y --purge libreoffice*
RUN apt-get clean \
RUN apt-get autoremove

RUN rm -rf /var/lib/apt/lists/* \
    apt-get autoclean \
    aptitude autoclean

RUN mv /usr/bin/libreoffice /usr/bin/libreoffice4.2
RUN mv /usr/bin/libreoffice5.3 /usr/bin/libreoffice

ENV NVM_DIR /usr/local/nvm
ENV NODE_VERSION 8.1.0

# Install nvm with node and npm
RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.29.0/install.sh | bash \
    && source $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use default

# Set up our PATH correctly so we don't have to long-reference npm, node, &c.
ENV NODE_PATH $NVM_DIR/versions/node/v$NODE_VERSION/lib/node_modules
ENV PATH      $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

# Set the work directory
RUN mkdir -p /var/www/app/current
WORKDIR ${appDir}

# Install pm2 so we can run our application
RUN npm i -g yarn pm2 gulp

# Prepare directories
RUN mkdir -p idms
RUN mkdir -p api
RUN mkdir -p soltec

# Add our package.json files
ADD ./base/license/package.json ./idms/
ADD ./base/license/shared/package.json ./idms/shared/
ADD ./base/api/package.json ./api/
ADD ./base/api/shared/package.json ./api/shared/
ADD ./soltec/package.json ./soltec/
ADD ./soltec/shared/package.json ./soltec/shared/

# Install deps
WORKDIR /var/www/app/current/idms
RUN npm i

WORKDIR /var/www/app/current/api
RUN npm i

WORKDIR /var/www/app/current/soltec
RUN npm i --python=python2.7

WORKDIR /var/www/app/current/idms/shared
RUN npm i

WORKDIR /var/www/app/current/soltec/shared
RUN npm i

WORKDIR /var/www/app/current/api/shared
RUN npm i

# Add application files
ADD ./base/license/ /var/www/app/current/idms
ADD ./base/api/ /var/www/app/current/api
ADD ./soltec/ /var/www/app/current/soltec

ADD ./processes.json /var/www/app/current

WORKDIR /var/www/app/current/idms/node_modules/sjcl
RUN ./configure --with-all --compress=none && make

# Reset workdir
WORKDIR ${appDir}

# Build docs
WORKDIR /var/www/app/current/idms
RUN mv gulpfile-prod.js gulpfile.js
RUN gulp apidoc

WORKDIR /var/www/app/current/api
RUN mv gulpfile-prod.js gulpfile.js
RUN gulp apidoc

WORKDIR /var/www/app/current/soltec
RUN mv gulpfile-prod.js gulpfile.js
RUN gulp apidoc

# Reset workdir
WORKDIR ${appDir}

ADD libre_config.xcu /var/www/app/current

#RUN mkdir -p /root/.config/libreoffice/4/user/
#RUN cp libre_config.xcu /root/.config/libreoffice/4/user/registrymodifications.xcu

RUN cp libre_config.xcu /usr/lib/libreoffice/share/registry/registrymodifications.xcu

#Expose the ports
EXPOSE 5000 5001 5002 7000

CMD ["pm2", "start", "processes.json", "--no-daemon"]
#CMD pm2 start idms/app.js --name=idms --no-daemon && pm2 start api/app.js --name=api --no-daemon && pm2 start soltec/app.js --name=soltec --no-daemon
