# Installation

Run following command in project root folder.
```bash
$ git submodule init
$ git submodule update --recursive --remote
```
You should get following submodules (if folders are empty you did it wrong, use power of google to pull git submodules defined it .gitsubmodules file):
- `base/api/shared`
- `base/license/shared`

Check README.md inside each folder (`/base/api`, `/base/license`, `/soltec`) for install & run details.

# Before first run
```bash
# This will create superadmin acc
$ ./init.sh
```

Start license server & then:
```bash
# This will create predefined sales managers
$ cd soltec/scipts
$ node admin.js
$ node public.js

```

Now if you have running soltec, base/api and base/license instances you can open browser and login with email and password defined in `./init.sh` (open file and peek inside it :eyes:).

# Users structure
## Roles
- superadmin: kalmia developers and soltec boss
- admin: sales managers
- user: companies

Each sales manager has own companies. Companies are defined in admins meta filed `children`. Users (companies) have additional meta filed `parent` containing admin (sales manger) id. All users can have own children (other users) in that case you must set `meta.hasNestedChildren` to `true` otherwise children will be ignored. This was implemented for optimization reasons. Since recursive search for nested children needs to make O(children) HTTP calls to IDMS server.

If you are logged in as admin check `soltec.kalmia.si/utree` for graph showing user tree structure (it can also help validate who can see what).

# Sync
For prod env sync with sharepoint is enabled. For manual sync or debug check `soltec/scripts/sync.js` or run directly `node soltec/scripts/sync.js`.

# Other
> KALMIA COMPANY LOGIN EMAIL for dev: "soltec+7095@kalmia.si"
