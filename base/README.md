# Installation

Run following command in project root folder.

```bash
$ git submodule init
$ git submodule update --recursive --remote
```

Check README.md inside each folder (api, license, kalpass).
