var debug       = require('debug')('app'),
    server      = require('./lib/server'),
    log         = require('./lib/log')(module),
    c           = require('./lib/configuration'),
    updatedb    = require('./lib/middlewares/updateLastDBUpdate');

// Set global object for keeping client connections
global.App = {
  licenseDbConn: undefined,
  licenseModels: undefined,
  activeUser   : {},
  activeDb     : {},
  clientDbConn : {},
  clientModels : {}
};

var account     = require('./lib/controllers/account'),
    user        = require('./lib/controllers/users'),
    group       = require('./lib/controllers/groups'),
    nodes       = require('./lib/controllers/nodes'),
    test        = require('./lib/controllers/test'),
    validators  = require('./lib/middlewares/validateRequest'),
    modifiers   = require('./shared/middlewares/requestModifiers')(log);

// All set, start listening!
server.listen( c.get('port') );

log.debug('== ===========================================');
log.debug('== KalBlocks REST API successfully started');
log.debug('== ===========================================');
log.debug('== PORT: %d', c.get('port') );
log.debug('== MODE: %s', process.env.NODE_ENV || 'dev');
log.debug('== ===========================================');

server.get(c.get('apiBase') + 'version', function (req, res) {
  res.send({
    version: c.get('version'),
  });
});

// All routes require auth that can be accessed only by autheticated users
server.all(c.get('apiBase') + '*', [validators.validateRequest, modifiers.addRequestWrapper]);

/* =======================================================
 * == ACCOUNT
 * ======================================================= */
  // Account routes that can be accessed only by authenticated & authorized users
  server.get(c.get('apiBase') + 'account', account.edit);
  server.put(c.get('apiBase') + 'account', account.update);

/* =======================================================
 * == USERS
 * ======================================================= */
  server.get(c.get('apiBase') + 'users', user.list);
  server.get(c.get('apiBase') + 'users/:id', user.get);
  server.get(c.get('apiBase') + 'users/data/:data', user.list);
  server.put(c.get('apiBase') + 'users/:id', user.update);
  server.put(c.get('apiBase') + 'users/lock/:id', user.lock);
  server.put(c.get('apiBase') + 'users/meta/:id', user.meta);
  server.post(c.get('apiBase') + 'users', user.create);
  server.delete(c.get('apiBase') + 'users/:id', user.delete);

/* =======================================================
 * == GROUPS
 * ======================================================= */
  // Group Routes that can be accessed only by authenticated & authorized users
  server.get(c.get('apiBase') + 'groups', group.list);
  server.get(c.get('apiBase') + 'groups/:id', group.get);
  server.put(c.get('apiBase') + 'groups/:id', group.update);
  server.post(c.get('apiBase') + 'groups', group.create);
  server.delete(c.get('apiBase') + 'groups/:id', group.delete);

/* =======================================================
 * == NODES
 * ======================================================= */
  // Nodes routes that can be accessed only by authenticated & authorized users
  server.get(c.get('apiBase') + 'nodes', nodes.list);
  server.get(c.get('apiBase') + 'nodes/:id', nodes.edit);
  server.put(c.get('apiBase') + 'nodes/:id', nodes.update);
  server.put(c.get('apiBase') + 'nodes/meta/:id', nodes.meta);
  server.post(c.get('apiBase') + 'nodes', nodes.save);
  server.delete(c.get('apiBase') + 'nodes/:id', nodes.delete);

  // Sample Error route
  server.get(c.get('apiBase') + 'ErrorExample', function (req, res, next) {
    next(new Error('Random error!'));
  });

  // Connectivity test
  server.get(c.get('apiBase') + 'test', test.connectivity);

// EOF