var nconf = require('nconf'),
    path  = require('path');

var rootDir = path.resolve(__dirname, '..');

nconf.argv()
     .env({separator: '__'})
     .file({file: rootDir + '/config.json'});

module.exports = nconf;
