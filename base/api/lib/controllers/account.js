var debug       = require('debug')('accounts'),
    log         = require('../log')(module),
    c           = require('../configuration'),
    request     = require('request'),
    _           = require('underscore');

// Get one account
/**
 * @api {get} /account Request your account information
 * @apiName Get
 * @apiGroup Account
 *
 * @apiSuccess {Object} account Account information.
 */
exports.edit = function(req, res) {
  var options = {
    url     : c.get('authApi') + '/account/' + req.user.acc,
    headers : {'x-access-token': req.token}
  };
  request(options, function (err, response, account) {
    if (!account) {
      res.statusCode = 404;
      return res.send({error: 'Not found'});
    }
    if (!err) {
      return res.send(account);
    }
    else {
      res.statusCode = 500;
      log.error('Internal error(%d): %s',res.statusCode,err.message);
      return res.send({error: 'Server error'});
    }
  });
};

// Update account
/**
 * @api {put} /account Edit your account
 * @apiName Edit
 * @apiGroup Account
 *
 * @apiParam {String} [company] Account company.
 * @apiParam {String} [address] Company address.
 * @apiParam {String} [city] Company city.
 * @apiParam {String} [post] Company post.
 * @apiParam {String} [country] Company country.
 * @apiParam {String} [vatId] Company VAT identification.
 * @apiParam {String} [phone] Company phone.
 * @apiParam {String} [email] Company email.
 * @apiParam {String="en","sl_SI"} [language] Wanted language.
 * @apiParam {String="D.M.Y",
 *                   "D. M. Y",
 *                   "D. MMMM Y",
 *                   "D/M/Y",
 *                   "D-M-Y",
 *                   "D MMM Y",
 *                   "DD.MM.Y",
 *                   "DD.MM.YY",
 *                   "dddd, D.M.Y",
 *                   "dddd, MMMM D, Y",
 *                   "MMMM D, Y",
 *                   "MMM D, Y",
 *                   "M/D/Y",
 *                   "M-D-Y",
 *                   "DD.MM.YYYY",
 *                   "Y/M/D",
 *                   "Y/MM/DD",
 *                   "Y",
 *                   "YY",
 *                   "MMM",
 *                   "MMMM",
 *                   "D",
 *                   "DD",
 *                   "ddd",
 *                   "dddd",
 *                   "w"} [dateFormat] Wanted date format.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} account Updated account info.
 */
exports.update = function(req, res) {
  var options = {
    method  : 'PUT',
    url     : c.get('authApi') + '/account/' + req.user.acc,
    headers : {'x-access-token': req.token},
    form    : {
      company           : req.body.company,
      address           : req.body.address,
      city              : req.body.city,
      post              : req.body.post,
      country           : req.body.country,
      vadId             : req.body.vatId,
      phone             : req.body.phone,
      email             : req.body.email,
      language          : req.body.language,
      dateFormat        : req.body.dateFormat,
      license           : req.body.license
    }
  };

  request(options, function (err, response, account) {
    if (!err) {
      log.info('user updated');
      return res.send(account);
    }
    else {
      if (err.name === 'ValidationError') {
        res.statusCode = 400;
        res.send({error: 'Validation error'});
      } else {
        res.statusCode = 500;
        res.send({error: 'Server error'});
      }
      log.error('Internal error(%d): %s', res.statusCode, err.message);
    }
  });
};
