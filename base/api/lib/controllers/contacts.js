var debug       = require('debug')('contacts'),
    log         = require('../log')(module),
    config      = require('../configuration'),
    request     = require('request'),
    async       = require('async'),
    md5         = require('blueimp-md5'),
    _           = require('lodash'),
    qs          = require('querystring'),
    q           = require('q');

/* ======================================
 * == Private Methods
 * ====================================== */
function handleError400(res, err) {
  res.statusCode = 404;
  return res.send({error: 'ValidationError', e: err});
}

function handleError404(res) {
  res.statusCode = 404;
  return res.send({error: 'Not found'});
}

function handleError500(res, err) {
  res.statusCode = 500;
  log.error('Internal error(%d): %s',res.statusCode, err.message);
  return res.send({error: 'Server error', e: err});
}

function getFilters(filters) {
  var f = {};
  // Filter by name
  if (filters.name && filters.name !== '') {
    f.name = {'$regex': filters.name};
  }

  // Filter by company
  if (filters.company) { 
    f.company = { '$in': filters.company };
  }

  // Filter by email
  if (filters.email && filters.email !== '') {
    f.email = {'$regex': filters.email};
  }

  // Filter by dateFrom
  if (filters.dateFrom && filters.dateFrom !== '') {
    var start = new Date(filters.dateFrom);
    start.setHours(0,0,0,0);
    f.createdAt = { '$gte': start };
  }

  // Filter by dateTo
  if (filters.dateTo && filters.dateTo !== '') {
    var end = new Date(filters.dateTo);
    end.setHours(23,59,59,999);
    if (!_.has(f, 'originTime')) {
      f.createdAt = {};
    }
    f.createdAt = { '$lte': end };
  }

  if (filters.tags && filters.tags.length > 0) {
    f.tags = { '$in': filters.tags };
  }

  return f;
}

// Update last DB update timestamp
function updateLastDBUpdate(req) {
  var options = {
    method  : 'GET',
    url     : config.get('authApi') + '/account/' + req.user.acc + '/updatedb',
    headers : { 'x-access-token': req.token }
  };

  request(options, function (err, response, account) {
    if (!err) {
      log.info('Updated lastDBUpdate for draft.');
    } else {
      log.error('Error updating lastDBUpdate for draft.');
    }
  });
}

// Fetch User Details
function getUserDetails(users, id) {
  id = id.toString();
  // Check if email
  if (id.indexOf('@') > -1) {
    return id;
  }
  return _.find(users, function(user) {
    return user._id === id;
  });
}

/* ======================================
 * == Public Methods
 * ====================================== */

/* ============================================
 * ================ LIST CONTACTS =============
 * ============================================ */
/**
 * @api {get} /contacts Request contacts list
 * @apiName List
 * @apiGroup Contacts
 *
 * @apiParam {Object} [filters] Filters for limiting contacts.<br/>
 * Filters can include following properties: <br/>
 * &emsp;- name filter by contact name<br/>
 * &emsp;- company id of company for which you want to see contacts<br/>
 * &emsp;- email email address of contact for whom you want to see details<br/>
 * &emsp;- dateFrom date from which you want to see contacts<br/>
 * &emsp;- dateTo date to which you want to see contacts<br/>
 * &emsp;- tags you want to see contacts for<br/>
 * All other properties will be ignored.
 *
 * @apiSuccess {Object[]} nods Filtered contacts list.
 */
exports.list = function(req, res) {

  var f = {};
  if (req.body.filters) {
    f = getFilters(req.body.filters);

    // Once we got all the filters user entered we need to add our own to filter drafts to a user
    f.$and = [
      {
        // We check that node is not deleted
        '$or': [
          { deleted: { '$exists' : false} },
          { deleted: false }
        ]
      }
    ];

  } else {
    f = {
      '$and': [
        {
          '$or':[{ 
            deleted: {
              '$exists' : false
            }
          }, {
            deleted: false
          }]
        }
      ]
    };
  }

  return req.db.contacts
    .find(f)
    .lean()
    .populate({path: 'creator', select: 'name', model: req.db.users})
    .populate({path: 'company', model: req.db.companies})
    .sort({createdAt: 'desc'})
    .exec(function (err, contacts) {
      if (err) { 
        return handleError500(res, err); 
      }
      else { 
        return res.send(contacts); 
      }
    });
};

/* ============================================
 * ============== GET ONE CONTACT =============
 * ============================================ */
/**
 * @api {get} /contacts/:id Request contact information
 * @apiName Get
 * @apiGroup Contacts
 *
 * @apiParam {String} id Unique contact id.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} contact Contact details.
 */
exports.edit = function(req, res) {
  return req.db.contacts
    .findOne({_id: req.params.id})
    .lean()
    .populate({path: 'creator', select: 'name', model: req.db.users})
    .populate({path: 'company', model: req.db.companies})
    .exec(function (err, contacts) {
      if (!contacts) { 
        return handleError404(res); 
      }
      if (err) { 
        return handleError500(res, err); 
      }
      else {
        return res.send({status: 'OK', contacts: contacts});
      }
    });
};

/* ============================================
 * ============== DELETE CONTACT ==============
 * ============================================ */
/**
 * @api {delete} /contacts/:id Delete existing contact
 * @apiName Delete
 * @apiGroup Contacts
 *
 * @apiParam {String} id Unique contact id.
 *
 * @apiSuccess {String} status Response status.
 */
exports.delete = function (req, res) {
  return req.db.contacts
    .find({ '_id': req.params.id })
    .exec(function (err, contacts) {
      if (!contacts) { 
        return handleError404(res); 
      }

      return contacts.softdelete(function (err) {
        if (err) { 
          handleError500(res, err); 
        }
        else {
          return res.send({ status: 'OK' });
        }
      });
  });
};

/* ============================================
 * ============== CREATE CONTACT ==============
 * ============================================ */

/**
 * @api {post} /contacts/save Create new contact
 * @apiName Add
 * @apiGroup Contacts
 *
 * @apiParam {String} creator Id of user creating the contact.
 * @apiParam {String} firstName Contact first name.
 * @apiParam {String} lastName Contact last name.
 * @apiParam {String} email Contact primary email.
 * @apiParam {String} email2 Contact other email.
 * @apiParam {String} company Id of company contact belongs to.
 * @apiParam {String} address Contact address.
 * @apiParam {String} city Contact city.
 * @apiParam {String} state Contact state.
 * @apiParam {String} postcode Contact post code.
 * @apiParam {String} position Contact work position.
 * @apiParam {String} phone Contact phone.
 * @apiParam {String} mobile Contact mobile.
 * @apiParam {String} phone2 Contact secondary phone.
 * @apiParam {String} workphone Contact work phone.
 * @apiParam {String} website Contact website URL.
 * @apiParam {String} linkedin Contact linkedin URL.
 * @apiParam {String} twitter Contact Twitter URL.
 * @apiParam {String} facebook Contact Facebook URL.
 * @apiParam {String} instagram Contact Instagram profile URL.
 * @apiParam {String} source Contact conversion source.
 * @apiParam {String} fax Contact fax number.
 * @apiParam {String} status Contact status (new|hot|active|notactive).
 * @apiParam {Array} tags Array of tags.
 * @apiParam {Object} meta Node meta data.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} node Created node.
 */

exports.save = function (req, res) {

  var n = new req.db.contacts(req.body);

  // Save case
  n.save(function (err) {
    if (err && err.name === 'ValidationError') {
      return handleError400(res, err);
    } 
    
    else if (err) {
      return handleError500(res, err);
    } 
    
    else if (req.user) {
      updateLastDBUpdate(req);
    }
    return res.send({status: 'OK', contact: n});
  });
};

/* ============================================
 * ============== UPDATE CONTACT ==============
 * ============================================ */
/**
 * @api {put} /contacts/:id Edit existing node
 * @apiName Edit
 * @apiGroup Contacts
 *
 * @apiParam {String} id Unique node id.
 * @apiParam {String} firstName Contact first name.
 * @apiParam {String} lastName Contact last name.
 * @apiParam {String} email Contact primary email.
 * @apiParam {String} email2 Contact other email.
 * @apiParam {String} company Id of company contact belongs to.
 * @apiParam {String} address Contact address.
 * @apiParam {String} city Contact city.
 * @apiParam {String} state Contact state.
 * @apiParam {String} postcode Contact post code.
 * @apiParam {String} position Contact work position.
 * @apiParam {String} phone Contact phone.
 * @apiParam {String} mobile Contact mobile.
 * @apiParam {String} phone2 Contact secondary phone.
 * @apiParam {String} workphone Contact work phone.
 * @apiParam {String} website Contact website URL.
 * @apiParam {String} linkedin Contact linkedin URL.
 * @apiParam {String} twitter Contact Twitter URL.
 * @apiParam {String} facebook Contact Facebook URL.
 * @apiParam {String} instagram Contact Instagram profile URL.
 * @apiParam {String} source Contact conversion source.
 * @apiParam {String} fax Contact fax number.
 * @apiParam {String} status Contact status (new|hot|active|notactive).
 * @apiParam {Array} tags Array of tags.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} case Edited case.
 */
exports.update = function (req, res) {
  req.db.contacts.findById(req.params.id, function (err, c) {
    if (!c) {
      return handleError404(res);
    }
    if (err) {
      return handleError500(res, err);
    }

    _.merge(c, req.body);

    c.save(function (err) {
      if (err && err.name === 'ValidationError') {
        return handleError400(res, err);
      } 
      else if (err) {
        return handleError500(res, err);
      }

      return res.send({status: 'OK', contact: c});
    });
    
  });
};

/* ============================================
 * =========== UPDATE CONTACT META ============
 * ============================================ */

/**
 * @api {put} /contacts/meta/:id Add contact meta
 * @apiName Meta
 * @apiGroup Contacts
 *
 * @apiParam {String} id User ID.
 * @apiParam {String} metaKey User meta key.
 * @apiParam {String} metaValue User meta value.
 *
 * @apiSuccess {String} status Request status.
 * @apiSuccess {Object} contact Contact data object.
 */
exports.meta = function(req, res) {

  req.db.contacts.findById(req.params.id, function (err, c) {

    if (req.body.delete) {
      if(c.meta.hasOwnProperty(req.body.metaKey)) {
        delete c.meta[req.body.metaKey];
      }
    } 
    else {
      c.meta[req.body.metaKey] = req.body.metaValue;
    }
    c.save(function(err) {
      if (err) { 
        res.statusCode = 500; 
        return res.send(err);
      }
      return res.send(c);
    });
  });
};