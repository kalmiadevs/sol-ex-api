var debug       = require('debug')('groups'),
    log         = require('../log')(module),
    c           = require('../configuration'),
    request     = require('request'),
    _           = require('underscore');

var $group       = require('../../shared/entities/group')(c, log),
    $user       = require('../../shared/entities/user')(c, log);

"use strict";

// Add group
/**
 * @api {post} /groups Add new group
 * @apiName Add
 * @apiGroup Groups
 *
 * @apiParam {String} name Group name.
 *
 * @apiSuccess {Object} group Created group.
 */
exports.create = function(req, res) {
  let group = req.body.group;
  let addUsers = req.body.addUsers;
  let removeUsers = req.body.removeUsers;

  $group.create(req, group)
    .then(function (gData) {

      $user.batchKeyRingsUpdate(req, gData, {
        add: addUsers,
        remove: removeUsers
      })
        .then(function (results) {
          let rejected = [];
          const fulfilled = [];

          results.forEach(function (result) {
            if (result.state === "fulfilled") {
              fulfilled.push(result.value);
            } else {
              fulfilled.push(rejected.reason);
            }
          });

          rejected = rejected.map((x) => {
            return {
              errorCode: x.errorCode,
              errorMessage: x.errorMessage
            };
          });

          // TODO remove rejected users from group

          res.send({ id: gData.id, group: gData, rejectedUsers: rejected });
        })
        .catch(function (e) {
          e.finish();
        });
    })
    .catch(function (e) {
      e.finish();
    });
};

// List all groups
/**
 * @api {get} /groups Request groups list
 * @apiName List
 * @apiGroup Groups
 *
 * @apiSuccess {Object[]} groups List of user groups
 */
exports.list = function(req, res) {
  $group.getAll(req)
    .then(function (data) {
      res.send({groups: data});
    })
    .catch(function (e) {
      e.finish();
    });
};

// Get one group
/**
 * @api {get} /groups/:id Request group information
 * @apiName Get
 * @apiGroup Groups
 *
 * @apiParam {String} id Unique group id.
 *
 * @apiSuccess {Object} group Group information.
 */
exports.get = function(req, res) {
  $group.get(req, req.params && req.params.id)
    .then(function (data) {
      res.send({group: data});
    })
    .catch(function (e) {
      e.finish();
    });
};

// Update group
/**
 * @api {put} /groups/:id Edit existing group
 * @apiName Edit
 * @apiGroup Groups
 *
 * @apiParam {String} id Unique group id.
 *
 * @apiSuccess {Object} group Updated group information.
 */
exports.update = function(req, res) {
  let group = req.body.group;
  let addUsers = req.body.addUsers;
  let removeUsers = req.body.removeUsers;

  $group.update(req, group)
    .then(function (gData) {
      $user.batchKeyRingsUpdate(req, gData, {
        add: addUsers,
        remove: removeUsers
      })
        .then(function (x) {
          // TODO do same as for create [create fun for handling in $user, don't even try to copy paste!]
          res.send({group: x, id: x.id});
        })
        .catch(function (e) {
          e.finish();
        });
    })
    .catch(function (e) {
      e.finish();
    });
};

// Delete group
/**
 * @api {delete} /groups/:id Delete existing group
 * @apiName Delete
 * @apiGroup Groups
 *
 * @apiParam {String} id Unique group id.
 *
 * @apiSuccess {String} status Response status.
 */
exports.delete = function (req, res) {
  $group.delete(req, req.params.id)
    .then(function (x) {
      // TODO remove key from users

      res.send({id: req.params.id});
    })
    .catch(function (e) {
      e.finish();
    });
};
