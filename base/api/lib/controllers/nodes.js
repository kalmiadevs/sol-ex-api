var debug       = require('debug')('nodes'),
    log         = require('../log')(module),
    config      = require('../configuration'),
    request     = require('request'),
    async       = require('async'),
    md5         = require('blueimp-md5'),
    _           = require('underscore'),
    qs          = require('querystring'),
    q           = require('q'),
    mongoose    = require('mongoose');

/* ======================================
 * == Private Methods
 * ====================================== */
function handleError400(res, err) {
  res.statusCode = 404;
  return res.send({error: 'ValidationError', e: err});
}

function handleError404(res) {
  res.statusCode = 404;
  return res.send({error: 'Not found'});
}

function handleError500(res, err) {
  res.statusCode = 500;
  log.error('Internal error(%d): %s',res.statusCode, err.message);
  return res.send({error: 'Server error', e: err});
}

function getFilters(filters) {
  var f = {};
  // Filter by title
  if (filters.title && filters.title !== '') {
    f.title = {'$regex': filters.title};
  }
  // Filter by groups
  if (filters.groups && filters.groups > 0) { 
    f.groups = { '$in': filters.groups };
  }

  // Filter by owner
  if (filters.owner && filters.owner !== '') {
    f.owner = filters.owner;
  }

  // Filter by dateFrom
  if (filters.dateFrom && filters.dateFrom !== '') {
    var start = new Date(filters.dateFrom);
    start.setHours(0,0,0,0);
    f.createdAt = { '$gte': start };
  }

  // Filter by dateTo
  if (filters.dateTo && filters.dateTo !== '') {
    var end = new Date(filters.dateTo);
    end.setHours(23,59,59,999);
    if (!_.has(f, 'originTime')) {
      f.createdAt = {};
    }
    f.createdAt = { '$lte': end };
  }

  // Filter by meta data
  if (filters.meta && filters.meta.length > 0) {
    f.$and = [];

    for (var i = 0; i < filters.meta.length; i++) {
      f.$and.push({
        ['meta.' + filters.meta[i].name]: filters.meta[i].value
      });
    }
  }

  // Filter by tags
  if (filters.tags && filters.tags.length > 0) {
    f.tags = { '$in': filters.tags };
  }

  return f;
}

// Update last DB update timestamp
function updateLastDBUpdate(req) {
  var options = {
    method  : 'GET',
    url     : config.get('authApi') + '/account/' + req.user.acc + '/updatedb',
    headers : { 'x-access-token': req.token }
  };

  request(options, function (err, response, account) {
    if (!err) {
      log.info('Updated lastDBUpdate for draft.');
    } else {
      log.error('Error updating lastDBUpdate for draft.');
    }
  });
}

// Fetch User Details
function getUserDetails(users, id) {
  id = id.toString();
  // Check if email
  if (id.indexOf('@') > -1) {
    return id;
  }
  return _.find(users, function(user) {
    return user._id === id;
  });
}

/* ======================================
 * == Public Methods
 * ====================================== */

/* ============================================
 * ================ LIST NODES ================
 * ============================================ */
/**
 * @api {get} /nodes Request nodes list
 * @apiName List
 * @apiGroup Nodes
 *
 * @apiParam {Object} [filters] Filters for limiting nodes.<br/>
 * Filters can include following properties: <br/>
 * &emsp;- title filter by title<br/>
 * &emsp;- groups id of group for which you want to see nodes<br/>
 * &emsp;- owner id of user for whom you want to see ndoes<br/>
 * &emsp;- dateFrom date from which you want to see nodes<br/>
 * &emsp;- dateTo date to which you want to see nodes<br/>
 * &emsp;- meta array of objects with key-value pairs which the node must have<br/>
 * &emsp;&emsp;<pre>[ { name: "name", value: "value" }, ... ]</pre><br/>
 * &emsp;- tags you want to see nodes for<br/>
 * All other properties will be ignored.
 *
 * @apiSuccess {Object[]} nods Filtered nodes list.
 */
exports.list = function(req, res) {

  var f = {};
  if (req.headers.filters) {
    f = getFilters(JSON.parse(req.headers.filters));

    // Once we got all the filters user entered we need to add our own to filter drafts to a 
    if (f.$and == undefined)
      f.$and = [];

    f.$and.push(
      {
        // We check that node is not deleted
        '$or': [
          { deleted: { '$exists' : false} },
          { deleted: false }
        ]
      }
    );

  } else {
    f = {
      '$and': [
        {
          '$or':[{ 
            deleted: {
              '$exists' : false
            }
          }, {
            deleted: false
          }]
        }
      ]
    };
  }

  return req.db.nodes
    .find(f)
    .lean()
    .populate('owner._id')
    .populate({path: 'groups', select: 'title', model: req.db.groups})
    .sort({createdAt: 'desc'})
    .exec(function (err, nodes) {
      if (err) { 
        return handleError500(res, err); 
      }
      else { 
        return res.send(nodes); 
      }
    });
};

/* ============================================
 * ============== GET ONE NODE ================
 * ============================================ */
/**
 * @api {get} /nodes/:id Request node information
 * @apiName Get
 * @apiGroup Nodes
 *
 * @apiParam {String} id Unique node id.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} node Node details.
 */
exports.edit = function(req, res) {
  return req.db.nodes
    .findOne({_id: req.params.id})
    .lean()
    .populate('owner._id')
    .populate({path: 'groups', model: req.db.groups})
    .exec(function (err, node) {
      if (!node) { 
        return handleError404(res); 
      }
      if (err) { 
        return handleError500(res, err);
      }
      else {
        return res.send({status: 'OK', node: node});
      }
    });
};

/* ============================================
 * ============== DELETE NODE =================
 * ============================================ */
/**
 * @api {delete} /nodes/:id Delete existing node
 * @apiName Delete
 * @apiGroup Nodes
 *
 * @apiParam {String} id Unique node id.
 *
 * @apiSuccess {String} status Response status.
 */
exports.delete = function (req, res) {
  return req.db.nodes
    .find({ '_id': req.params.id })
    .exec(function (err, nodes) {
      if (!nodes || nodes.length == 0) {
        return handleError404(res, 'Not found');
      }

      return nodes[0].softdelete(function (err) {
        if (err) { 
          handleError500(res, err); 
        }
        else {
          return res.send({ status: 'OK' });
        }
      });
  });
};

/**
 * @api {post} /nodes Create new node
 * @apiName Add
 * @apiGroup Nodes
 *
 * @apiParam {String} user Id of user creating the node.
 * @apiParam {String} title Node title.
 * @apiParam {String} icon Node icon URL.
 * @apiParam {Array} tags Array of tags.
 * @apiParam {Array} hashes Array of hashes.
 * @apiParam {Array} groups Array of groups.
 * @apiParam {String} content Node content.
 * @apiParam {Object} meta Node meta data.
 * @apiParam {String} privileges Node privileges.
 * @apiParam {String} private Wether ndoe is private or not.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} node Created node.
 */

exports.save = function (req, res) {
  try {
    var node = new req.db.nodes({
      owner:	     req.body.owner,
      title:       req.body.title,
      icon:        req.body.icon,
      tags:        req.body.tags,
      hashes:      req.body.hashes,
      groups:      req.body.groups,
      content:     req.body.content,
      privileges:  req.body.privileges,
      meta:        req.body.meta,
      private:     req.body.private,
    });
    
    // Save case
    node.save(function (err) {
      if (err && err.name === 'ValidationError') {
        return handleError400(res, err);
      } 
    
      else if (err) {
        return handleError500(res, err);
      } 
      
      else if (req.user) {
        updateLastDBUpdate(req);
      }
      return res.send({status: 'OK', node: node});
    });
  } catch (err) {
    res.statusCode = 500;
    return res.send({ error: 'Server error'});
  }
};

/**
 * @api {put} /nodes/:id Edit existing node
 * @apiName Edit
 * @apiGroup Nodes
 *
 * @apiParam {String} id Unique node id.
 * @apiParam {String} title Node title.
 * @apiParam {String} icon Node icon URL.
 * @apiParam {Array} tags Array of tags.
 * @apiParam {Array} hashes Array of hashes.
 * @apiParam {Array} groups Array of groups.
 * @apiParam {String} content Node content.
 * @apiParam {Array} meta Node meta data.
 * @apiParam {String} private Wether ndoe is private or not.
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} case Edited case.
 */
exports.update = function (req, res) {
  req.db.nodes.findById(req.params.id, function (err, n) {
    if (!n) {
      return handleError404(res);
    }
    if (err) {
      return handleError500(res, err);
    }

    // Check version
    if (!req.body.version || req.body.version != n.version) {
      return handleError400(res, 'Node version mismatch');
    }

    // TODO: Check for privileges
    n.title  = req.body.title  ? req.body.title  : n.title;
    n.icon   = req.body.icon   ? req.body.icon   : n.icon;
    n.tags   = req.body.tags   ? req.body.tags   : n.tags;
    n.hashes = req.body.hashes ? req.body.hashes : n.hashes;
    n.groups = req.body.groups ? req.body.groups : n.groups;
    n.content = req.body.content ? req.body.content : n.content;
    n.meta = req.body.meta ? req.body.meta : n.meta;
    n.version = n.version + 1; // increment version


    // TODO: Check if user can set private flag
    n.save(function (err) {
      if (err && err.name === 'ValidationError') {
        return handleError400(res, err);
      } 
      else if (err) {
        return handleError500(res, err);
      }

      return res.send({status: 'OK', node: n});
    });
    
  });
};


/**
 * @api {put} /nodes/meta/:id Add node meta
 * @apiName Meta
 * @apiGroup Nodes
 *
 * @apiParam {String} id User ID.
 * @apiParam {String} metaKey User meta key.
 * @apiParam {String} metaValue User meta value.
 *
 * @apiSuccess {String} status Request status.
 * @apiSuccess {Object} user User data object.
 */
exports.meta = function(req, res) {

  req.db.nodes.findById(req.params.id, function (err, n) {

    if (req.body.delete) {
      for (var i = 0; i < n.meta.length; i++) {
        if (n.meta[i].name == req.body.metaKey) {
          n.meta.splice(i, 1);
        }
      }
    } 
    else {
      n.meta.push({ name: req.body.metaKey, value: req.body.metaValue });
    }
    n.save(function(err) {
      if (err) { 
        res.statusCode = 500; 
        return res.send(err);
      }
      return res.send(n);
    });
  });
};