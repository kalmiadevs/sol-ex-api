var debug       = require('debug')('projects'),
    log         = require('../log')(module),
    config      = require('../configuration'),
    request     = require('request'),
    async       = require('async'),
    md5         = require('blueimp-md5'),
    _           = require('lodash'),
    qs          = require('querystring'),
    q           = require('q');

/* ======================================
 * == Private Methods
 * ====================================== */
function handleError400(res, err) {
  res.statusCode = 404;
  return res.send({error: 'ValidationError', e: err});
}

function handleError404(res) {
  res.statusCode = 404;
  return res.send({error: 'Not found'});
}

function handleError500(res, err) {
  res.statusCode = 500;
  log.error('Internal error(%d): %s',res.statusCode, err.message);
  return res.send({error: 'Server error', e: err});
}

function getFilters(filters) {
  var f = {};
  // Filter by name
  if (filters.name && filters.name !== '') {
    f.name = {'$regex': filters.name};
  }

  // Filter by company
  if (filters.company && filters.company !== '') {
    f.company = filters.company;
  }

  // Filter by contact
  if (filters.contact && filters.contact !== '') {
    f.contact = filters.contact;
  }

  // Filter by creator
  if (filters.creator && filters.creator !== '') {
    f.creator = filters.creator;
  }

  // Filter by dateFrom
  if (filters.dateFrom && filters.dateFrom !== '') {
    var start = new Date(filters.dateFrom);
    start.setHours(0,0,0,0);
    f.createdAt = { '$gte': start };
  }

  // Filter by dateTo
  if (filters.dateTo && filters.dateTo !== '') {
    var end = new Date(filters.dateTo);
    end.setHours(23,59,59,999);
    if (!_.has(f, 'originTime')) {
      f.createdAt = {};
    }
    f.createdAt = { '$lte': end };
  }

  if (filters.tags && filters.tags.length > 0) {
    f.tags = { '$in': filters.tags };
  }

  // Filter by status
  if (filters.status && filters.status !== '') {
    f.status = filters.status;
  }

  return f;
}

// Update last DB update timestamp
function updateLastDBUpdate(req) {
  var options = {
    method  : 'GET',
    url     : config.get('authApi') + '/account/' + req.user.acc + '/updatedb',
    headers : { 'x-access-token': req.token }
  };

  request(options, function (err, response, account) {
    if (!err) {
      log.info('Updated lastDBUpdate for draft.');
    } else {
      log.error('Error updating lastDBUpdate for draft.');
    }
  });
}

// Fetch User Details
function getUserDetails(users, id) {
  id = id.toString();
  // Check if email
  if (id.indexOf('@') > -1) {
    return id;
  }
  return _.find(users, function(user) {
    return user._id === id;
  });
}

/* ======================================
 * == Public Methods
 * ====================================== */

/* ============================================
 * ================ LIST PROJECTS =============
 * ============================================ */
/**
 * @api {get} /projects Request projects list
 * @apiName List
 * @apiGroup Projects
 *
 * @apiParam {Object} [filters] Filters for limiting projects.<br/>
 * Filters can include following properties: <br/>
 * &emsp;- name filter by name<br/>
 * &emsp;- company id of company for which you want to see projects<br/>
 * &emsp;- contact id of contact for which you want to see projects<br/>
 * &emsp;- creator id of creator for whom you want to see projects<br/>
 * &emsp;- dateFrom date from which you want to see projects<br/>
 * &emsp;- dateTo date to which you want to see projects<br/>
 * &emsp;- tags tags you want to see projects for<br/>
 * &emsp;- status status you want to see projects for<br/>
 * &emsp;- tags you want to see projects for<br/>
 * All other properties will be ignored.
 *
 * @apiSuccess {Object[]} nods Filtered projects list.
 */
exports.list = function(req, res) {

  var f = {};
  if (req.body.filters) {
    f = getFilters(req.body.filters);

    // Once we got all the filters user entered we need to add our own to filter drafts to a user
    f.$and = [
      {
        // We check that project is not deleted
        '$or': [
          { deleted: { '$exists' : false} },
          { deleted: false }
        ]
      }
    ];

  } else {
    f = {
      '$and': [
        {
          '$or':[{ 
            deleted: {
              '$exists' : false
            }
          }, {
            deleted: false
          }]
        }
      ]
    };
  }

  return req.db.projects
    .find(f)
    .lean()
    .populate({path: 'creator', select: 'name', model: req.db.users})
    .populate({path: 'company', model: req.db.companies})
    .populate({path: 'contact', model: req.db.contacts})
    .sort({createdAt: 'desc'})
    .exec(function (err, projects) {
      if (err) { 
        return handleError500(res, err); 
      }
      else { 
        return res.send(projects); 
      }
    });
};

/* ============================================
 * ============== GET ONE PROJECT =============
 * ============================================ */
/**
 * @api {get} /projects/:id Request project information
 * @apiName Get
 * @apiGroup Projects
 *
 * @apiParam {String} id Unique project id.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} project Project details.
 */
exports.edit = function(req, res) {
  return req.db.projects
    .findOne({_id: req.params.id})
    .lean()
    .populate({path: 'creator', select: 'name', model: req.db.users})
    .populate({path: 'company', model: req.db.companies})
    .populate({path: 'contact', model: req.db.contacts})
    .exec(function (err, project) {
      if (!projects) { 
        return handleError404(res); 
      }
      if (err) { 
        return handleError500(res, err); 
      }
      else {
        return res.send({status: 'OK', project: project});
      }
    });
};

/* ============================================
 * ============== DELETE PROJECT ==============
 * ============================================ */
/**
 * @api {delete} /projects/:id Delete existing project
 * @apiName Delete
 * @apiGroup Projects
 *
 * @apiParam {String} id Unique project id.
 *
 * @apiSuccess {String} status Response status.
 */
exports.delete = function (req, res) {
  return req.db.projects
    .find({ '_id': req.params.id })
    .exec(function (err, project) {
      if (!project) { 
        return handleError404(res); 
      }

      return project.softdelete(function (err) {
        if (err) { 
          handleError500(res, err); 
        }
        else {
          return res.send({ status: 'OK' });
        }
      });
  });
};

/**
 * @api {post} /projects/save Create new project
 * @apiName Add
 * @apiGroup Projects
 *
 * @apiParam {String} creator Id of user creating the project.
 * @apiParam {String} contact Id of the project contact person.
 * @apiParam {String} company Id of company the project is for.
 * @apiParam {String} name Project name.
 * @apiParam {String} description Project description.
 * @apiParam {String} startDate Project start date.
 * @apiParam {String} endDate Project end date.
 * @apiParam {Array} tags Array of tags.
 * @apiParam {Object} meta Project meta data.
 * @apiParam {Number} progress Project progress.
 * @apiParam {String} status Project status.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} project Created project.
 */

exports.save = function (req, res) {

  var project = new req.db.projects(req.body);

  // Save case
  project.save(function (err) {
    if (err && err.name === 'ValidationError') { return handleError400(res, err); } 
    else if (err) { return handleError500(res, err); }
    else if (req.user) { updateLastDBUpdate(req); }
    return res.send({status: 'OK', project: project});
  });
};

/**
 * @api {put} /projects/:id Edit existing project
 * @apiName Edit
 * @apiGroup Projects
 *
 * @apiParam {String} id Unique project id.
 * @apiParam {String} creator Id of user creating the project.
 * @apiParam {String} contact Id of the project contact person.
 * @apiParam {String} company Id of company the project is for.
 * @apiParam {String} name Project name.
 * @apiParam {String} description Project description.
 * @apiParam {String} startDate Project start date.
 * @apiParam {String} endDate Project end date.
 * @apiParam {Array} tags Array of tags.
 * @apiParam {Object} meta Project meta data.
 * @apiParam {Number} progress Project progress.
 * @apiParam {String} status Project status.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} case Edited case.
 */
exports.update = function (req, res) {
  req.db.projects.findById(req.params.id, function (err, p) {
    if (!p) { return handleError404(res); }
    if (err) { return handleError500(res, err); }

    _.merge(p, req.body);

    p.save(function (err) {
      if (err && err.name === 'ValidationError') { return handleError400(res, err); } 
      else if (err) { return handleError500(res, err); }
      return res.send({status: 'OK', project: p});
    });
  });
};


/**
 * @api {put} /projects/meta/:id Add project meta
 * @apiName Meta
 * @apiGroup Projects
 *
 * @apiParam {String} id User ID.
 * @apiParam {String} metaKey User meta key.
 * @apiParam {String} metaValue User meta value.
 *
 * @apiSuccess {String} status Request status.
 * @apiSuccess {Object} user User data object.
 */
exports.meta = function(req, res) {

  req.db.projects.findById(req.params.id, function (err, p) {

    if (req.body.delete) {
      if(p.meta.hasOwnProperty(req.body.metaKey)) {
        delete p.meta[req.body.metaKey];
      }
    } 
    else {
      p.meta[req.body.metaKey] = req.body.metaValue;
    }
    p.save(function(err) {
      if (err) { 
        res.statusCode = 500; 
        return res.send(err);
      }
      return res.send(n);
    });
  });
};