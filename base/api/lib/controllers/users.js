var debug       = require('debug')('users'),
    log         = require('../log')(module),
    c           = require('../configuration'),
    request     = require('request'),
    _           = require('underscore');

var $user       = require('../../shared/entities/user')(c, log);

/**
 * @api {get} /users/ Request users list
 * @apiName List
 * @apiGroup Users
 *
 * @apiSuccess {Object[]} users List of users.
 */
exports.list = function(req, res) {
  $user.getAll(req, req.params && req.params.data)
    .then(function (x) {
      res.send({users: x});
    })
    .catch(function (e) {
      e.finish();
    });
};

/**
 * @api {get} /users/:id Request user information
 * @apiName Get
 * @apiGroup Users
 *
 * @apiParam {String} id Unique user id.
 *
 * @apiSuccess {Object} user User info object.
 */
exports.get = function(req, res) {
  $user.get(req, req.params.id)
    .then(function (x) {
      res.send(x);
    })
    .catch(function (e) {
      e.finish();
    });
};

/**
 * @api {put} /users/:id Edit existing user
 * @apiName Edit
 * @apiGroup Users
 *
 * @apiParam {String} id User ID.
 * @apiParam {String} firstName User first name.
 * @apiParam {String} lastName User last name.
 * @apiParam {String} email User email.
 * @apiParam {String[]} groups User groups.
 * @apiParam {String="superadmin","admin","user"} [role] User role.
 * @apiParam {String} language ISO 639-1 language code.
 * @apiParam {String} [newPassword] New password.
 * (This will only be considered if user id match with tokens user id)
 * @apiParam {String} [confirmNewPassword] New password confirmation.
 * (This will only be considered if user id match with tokens user id)
 *
 * @apiSuccess {String} status Request status.
 * @apiSuccess {Object} user User data object.
 */
exports.update = function(req, res) {
  $user.update(req, req.body)
    .then(function (x) {
      res.send({user: x, id: x.id});
    })
    .catch(function (e) {
      e.finish();
    });
};

/**
 * @api {put} /users/meta/:id Add user meta
 * @apiName Meta
 * @apiGroup Users
 *
 * @apiParam {String} id User ID.
 * @apiParam {String} metaKey User meta key.
 * @apiParam {String} metaValue User meta value.
 *
 * @apiSuccess {String} status Request status.
 * @apiSuccess {Object} user User data object.
 */
exports.meta = function(req, res) {
  var options = {
    method  : 'PUT',
    url     : c.get('authApi') + '/users/meta/' + req.user.acc + '/' + req.params.id,
    headers : {'x-access-token': req.token},
    form    : {
      metaKey:    req.body.metaKey,
      metaValue:  req.body.metaValue,
      delete:     req.body.delete || false
    }
  };
  request(options, function (err, response, user) {

    user = JSON.parse(user);

    if (user && user.error) {
      if (user.code === 11000) {
        res.statusCode = 400;
      } 
      else {
        res.statusCode = 500;
      }
    }
    return res.send(user);
  });
};

/**
 * @api {post} /users Add new user
 * @apiName Add
 * @apiGroup Users
 *
 * @apiParam {String} firstName User first name.
 * @apiParam {String} lastName User last name.
 * @apiParam {String} email User email.
 * @apiParam {String[]} [groups] User groups.
 * @apiParam {String="superadmin","admin","user"} [role="admin"] User role.
 * @apiParam {String} language ISO 639-1 language code.
 *
 * @apiSuccess {String} status Request status.
 * @apiSuccess {Object} user User data object.
 */
exports.create = function(req, res) {
  $user.create(req, req.body)
    .then(function (x) {
      delete x.password;
      delete x.privateKey;
      res.send({user: x, id: x.id});
    })
    .catch(function (e) {
      e.finish();
    });
};

/**
 * @api {put} /users/lock/:id Lock/unlock current user
 * @apiName Lock
 * @apiGroup Users
 *
 * @apiParam {String} id Unique user id.
 *
 * @apiSuccess {String} status Request status.
 */
exports.lock = function (req, res) {
  var options = {
    method  : 'PUT',
    url     : c.get('authApi') + '/users/lock/' + req.user.acc + '/' + req.params.id,
    headers : {'x-access-token': req.token}
  };
  request(options, function (err, response, user) {
    user = JSON.parse(user);
    if (!err) {
      log.info('user lock');
      return res.send(user);
    }
    else {
      if (err.name === 'ValidationError') {
        res.statusCode = 400;
        res.send({error: 'Validation error'});
      } else {
        res.statusCode = 500;
        res.send({error: 'Server error'});
      }
      log.error('Internal error(%d): %s', res.statusCode, err.message);
    }
  });
};

/**
 * @api {delete} /users/:id Delete existing user
 * @apiName Delete
 * @apiGroup Users
 *
 * @apiParam {String} id Unique user id.
 *
 * @apiSuccess {String} status Request status.
 */
exports.delete = function (req, res) {
  $user.delete(req, req.params.id)
    .then(function (x) {
      res.send({id: req.params.id});
    })
    .catch(function (e) {
      e.finish();
    });
};
