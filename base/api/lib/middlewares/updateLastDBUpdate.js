var jwt          = require('jwt-simple'),
    glob         = require('glob'),
    log          = require('../log')(module),
    c            = require('../configuration'),
    path         = require('path'),
    request     = require('request'),
    q            = require('q');

module.exports = function(req, res, next) {
  var options = {
    method  : 'GET',
    url     : c.get('authApi') + '/account/' + req.user.acc + '/updatedb',
    headers : {'x-access-token': req.token}
  };

  request(options, function (err, response, account) {
    if (!account) {
      res.statusCode = 404;
      return res.send({error: 'Not found'});
    }
    if (!err) {
      next();
    }
    else {
      res.statusCode = 500;
      log.error('Internal error(%d): %s',res.statusCode,err.message);
      return res.send({error: 'Server error'});
    }
  });

};
