module.exports = function (mongoose, dbconn, plugins) {

    var schema = new mongoose.Schema({
      creator:	   {type: mongoose.Schema.Types.ObjectId, ref: 'Users'},
      name:        {type: String, trim: true},
      firstName:   {type: String, trim: true},
      lastName:    {type: String, trim: true},
      email:       {type: String, trim: true},
      email2:      {type: String, trim: true},
      company:     {type: mongoose.Schema.Types.ObjectId, ref: 'Companies'},
      address:     {type: String, trim: true},
      city:        {type: String, trim: true},
      state:       {type: String, trim: true},
      postcode:    {type: String, trim: true},
      position:    {type: String, trim: true},
      phone:       {type: String, trim: true},
      phone2:      {type: String, trim: true},
      mobile:      {type: String, trim: true},
      workphone:   {type: String, trim: true},
      website:     {type: String, trim: true},
      linkedin:    {type: String, trim: true},
      twitter:     {type: String, trim: true},
      facebook:    {type: String, trim: true},
      instagram:   {type: String, trim: true},
      source:      {type: String, trim: true},
      fax:         {type: String, trim: true},
      status:      {type: String, trim: true},
      tags:        [],
      meta:        {},
    }, {
      timestamps: true
    });

    schema.pre('save', function(next) {
      this.nameLower = this.name.toLowerCase();

      this.name = this.firstName + ' ' + this.lastName; 
      var current = this;
    });

    // Apply plugins
    for (var i = 0; i < plugins.length; i++) {
      schema.plugin(plugins[i]);
    }

    return dbconn.model('Contacts', schema, 'contacts');
  };
