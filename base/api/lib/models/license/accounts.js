var payment  = 'invoice,creditcard,paypal'.split(',');
var products = 'kalpass,kalwatch,kaldocs,kalsales,kalcash,kaldesk,kalboard,kalforms,kalworks,kalpost,kalendar,kalchat,kalpeople,kalmotion'.split(',');

module.exports = function (mongoose, dbconn, plugins) {

  var schema = new mongoose.Schema({
    company   : {type: String, unique: true, required: true, trim: true},
    address   : {type: String, trim: true},
    city      : {type: String},
    post      : {type: String},
    country   : {type: String, required: true},
    vadId     : {type: String},
    payment   : {type: String, enum: payment, default: 'invoice'},
    license   : {
      users           : {type: Number},
      admins          : {type: Number},
      active          : {type: Number},
      monthlyPrice    : {type: Number},
      apps            : {type: Array, default: products},
    },
    phone             : {type: String},
    email             : {type: String},
    database          : {type: String, unique: true, required: true, trim: true},
    language          : {type: String, default: 'sl_SI'},
    dateFormat        : {type: String},
    currency          : {type: String},
    currencySimbol    : {type: String},
    currencyPosition  : {type: String},
    currencyThousands : {type: String},
    currencyDecimal   : {type: String},
    currencySeparator : {type: String},
    active            : {type: Boolean, default: true},
    locked            : {type: Boolean, default: false},
    lastDBUpdate      : {type: Date, default: Date.now}
  }, {
    timestamps: true
  });

  // Apply plugins
  for (var i = 0; i < plugins.length; i++) {
    schema.plugin(plugins[i]);
  }

  return dbconn.model('Accounts', schema, 'accounts');
};
