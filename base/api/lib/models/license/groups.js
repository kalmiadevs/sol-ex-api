module.exports = function (mongoose,  dbconn, plugins) {

  var schema = new mongoose.Schema({
    title:       {type: String, required: true, trim: true},
    /**
     * Example:
     * kp_node: 110
     * kp_dash: 111
     */
    permissions: {type: mongoose.Schema.Types.Mixed, required: true},
    users:   	 [{type: mongoose.Schema.Types.ObjectId, ref: 'Users'}],
    nodes:   	 [{type: mongoose.Schema.Types.ObjectId, ref: 'Nodes'}],
    owner:     {type: mongoose.Schema.Types.ObjectId, ref: 'Users', required: true},
    publicKey:    {type: String},
    privateKey:   {type: String},
    keyRing:      {},
    meta:      {}
  }, {
    timestamps: true
  });

  // Apply plugins
  for (var i = 0; i < plugins.length; i++) {
    schema.plugin(plugins[i]);
  }

  return dbconn.model('Groups', schema, 'groups');
};
