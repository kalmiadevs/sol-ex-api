module.exports = function (mongoose, dbconn, plugins) {

    var schema = new mongoose.Schema({
      owner:	     {type: mongoose.Schema.Types.ObjectId, ref: 'Users'},
      title:       {type: String, required: true, trim: true},
      icon:        {type: String},
      tags:        [],
      hashes:      [],
      groups:      [{type: mongoose.Schema.Types.ObjectId, ref: 'Groups'}],
      content:     {type: String, required: true},
      privileges:  {type: Number},
      meta:        {},
      private:     {type: Boolean, default: false},
      version:     {type: Number, default: 0}
    }, {
      timestamps: true
    });

    /*
    schema.pre('save', function(next) {
      this.nameLower = this.name.toLowerCase();
      var current = this;
      dbconn.model('Procedures').findById(current.procedure, function(err, proc) {
        current.procedureLower = proc.label.toLowerCase();
        next();
      });
    });
    */

    // Apply plugins
    for (var i = 0; i < plugins.length; i++) {
      schema.plugin(plugins[i]);
    }

    return dbconn.model('Nodes', schema, 'nodes');
  };
