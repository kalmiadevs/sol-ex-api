module.exports = function (mongoose,  dbconn, plugins) {

  var schema = new mongoose.Schema({
    key    : {type: String, required: true},
    value    : {type: Number, required: true},
    description: {type: String, required: true},
  }, {
    timestamps: true
  });

  // Apply plugins
  for (var i = 0; i < plugins.length; i++) {
    schema.plugin(plugins[i]);
  }

  return dbconn.model('Permissions', schema, 'permissions');
};
