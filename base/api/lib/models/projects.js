module.exports = function (mongoose, dbconn, plugins) {

    var schema = new mongoose.Schema({
      creator:	     {type: mongoose.Schema.Types.ObjectId, ref: 'Users'},
      contact:	     {type: mongoose.Schema.Types.ObjectId, ref: 'Contacts'},
      company:	     {type: mongoose.Schema.Types.ObjectId, ref: 'Companies'},
      name:          {type: String, required: true, trim: true},
      description:   {type: String},
      startDate:     {type: Date},
      endDate:       {type: Date},
      progress:      {type: Number},
      status:        {type: String},
      tags:          [],
      meta:          {},
    }, {
      timestamps: true
    });

    // Apply plugins
    for (var i = 0; i < plugins.length; i++) {
      schema.plugin(plugins[i]);
    }

    return dbconn.model('Projects', schema, 'projects');
  };
