module.exports = function (mongoose, dbconn, plugins) {
    /*
     *
     */
    var schema = new mongoose.Schema({
      user               : {type: mongoose.Schema.Types.ObjectId, ref: 'Users'},
      procedure          : {type: mongoose.Schema.Types.ObjectId, ref: 'Procedures'},
      procedureLower     : {type: String},
      label              : {type: String, required: true, unique: true},
      labelLower         : {type: String},
      description        : {type: String},
      descriptionLower   : {type: String},
      type               : {type: String},
      occurency          : {type: String}, // pre, post
      content            : {}
    }, {
      timestamps: true
    });

    schema.pre('save', function(next) {
      this.labelLower = this.label.toLowerCase();
      if (this.description) {
        this.descriptionLower = this.description.toLowerCase();
      }
      var current = this;
      dbconn.model('Procedures').findById(current.procedure, function(err, proc) {
        current.procedureLower = proc.label.toLowerCase();
        next();
      });
    });

    // Apply plugins
    for (var i = 0; i < plugins.length; i++) {
      schema.plugin(plugins[i]);
    }

    return dbconn.model('Tasks', schema, 'tasks');
  };
