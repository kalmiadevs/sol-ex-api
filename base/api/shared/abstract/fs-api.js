'use strict';

const sanitizeFilename = require('sanitize-filename'),
      normalize        = require('normalize-path'),
      fs               = require('fs-extra'),
      path             = require('path'),
      q                = require('q'),
      find             = require('find'),
      BaseApi          = require('./base-api'),
      _                = require('lodash');

/**
 * Notes:
 * - if you are adding custom logic alwasy make sure you validate path
 * - if path is invalid don't clean it but reject request!
 *
 * Filesystem service.
 */
module.exports = class FileSystemApi extends BaseApi {

  /**
   * Bucket name should comply with DNS naming conventions.
   * Each bucket has an associated access control policy.
   * @param bucket {string} Bucket relative path. Based on fs storage dir.
   * @param options
   */
  constructor(bucket, options = {
    provideFileHash: false,
    baseApi: undefined
  }) {
    super(options.baseApi || {
      module     : module,
      serviceName: 'FileSystemApi',
      entityName : 'file'
    });

    if (!bucket) {
      throw new Error('FileSystemApi: bucket can\'t be empty.');
    }

    /**
     * Expose to parent.
     */
    this._fs    = fs;
    this._path  = path;

    /**
     * Filesystem storage path (absolute).
     * @type {string}
     */
    this.basePath = path.resolve(process.cwd(), 'storage/');

    /**
     * Bucket relative path. Based on fs storage dir.
     */
    this.bucket = bucket;

    /**
     * Bucket absolute path.
     */
    this.bucketPath = path.resolve(this.basePath, bucket);

    fs.ensureDirSync(this.basePath);

    this.errors = {
      invalidPath: {
        resStatusCode: 400,
        resCode      : 'invalid_path',
        resMessage   : 'Path is not valid.'
      },
      noFilesUploaded: {
        resStatusCode: 400,
        resCode      : 'no_files_uploaded',
        resMessage   : 'No file uploaded.'
      },
      emptyPath: {
        resStatusCode: 400,
        resCode      : 'empty_path',
        resMessage   : 'Path can\'t be empty.'
      }
    };
  };

  /**
   * If you wan't to store reference in DB.
   * @param qPath {string | string[]}
   * @return {string}
   */
  encodePath(qPath) {
    return qPath; // TODO check for proper mapping
  }

  /**
   * If you you for example renaming of file and orig name is in DB.
   * @param qPath {string | string[]}
   * @return {string}
   */
  decodePath(qPath) {
    return qPath; // TODO check for proper mapping
  }

  /**
   * Check if path is valid (it's inside bucket)
   * @param aPath
   * @return {boolean}
   */
  validAbsolutePath(aPath) {
    aPath = path.normalize(aPath);
    if (aPath === this.bucketPath) {
      return true;
    }
    const x = path.parse(aPath);
    return x.dir.startsWith(this.bucketPath);
  }

  validPath(qPath) {
    return _.every(qPath.split('/'), x => sanitizeFilename(x) === x);
  }

  /**
   * Convert query path from client to absolute bucket path.
   * WARNING: this is not validating!
   * @param qPath
   */
  queryPathToBucketPath(qPath) {
    qPath = path.normalize(qPath);
    if (qPath.startsWith(path.sep)) {
      qPath = qPath.substring(1);
    }
    return path.resolve(this.bucketPath, qPath);
  }

  /**
   * Convert query path from client to valid absolute bucket path.
   * If path is not valid it will throw exception.
   * @param qPath
   */
  queryPathToValidBucketPath(qPath) {
    const p = this.queryPathToBucketPath(qPath);
    if (!this.validAbsolutePath(p)) {
     throw this.errors.invalidPath;
    }
    return p;
  }

  /**
   * Convert absolute path into bucket relative.
   * Example: /home/tilen/soltec/storage/media/a/image.png => a/image.png
   * @param aPath
   */
  absolutePathToBucketRelative(aPath) {
    return aPath.slice(this.bucketPath.length)
  }

  /**
   * Convert file path to suitable response for client.
   * @param name
   * @param absPath
   * @param path
   * @return {{name: *, type: string, size, btime: *, atime: *, ctime: *, mtime: *}}
   */
  makeFileRes(name, absPath, path) {
    const stat = fs.lstatSync(absPath);
    return {
      name : name,
      path : path,
      type : stat.isDirectory() ? 'dir' : 'file',
      size : stat.size,
      btime: stat.birthtime,
      atime: stat.atime,
      ctime: stat.ctime,
      mtime: stat.mtime,
    };
  }

  /**
   * Create dir.
   * @param rPath
   * @return {Promise}
   */
  $createDir(rPath) {
    return new Promise((resolve, reject) => {
      const p = this.queryPathToValidBucketPath(rPath);
      fs.ensureDir(p, err => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      })
    });
  }

  /**
   * Validate and copy.
   * Note: you should validate sourcePath yourself!
   * @param sourcePath
   * @param destinationPath
   * @param options
   * @param options.errorOnExist
   * @param options.overwrite
   * @param options.preserveTimestamps
   * @param options.filter
   * @return {Promise}
   */
  $copy(sourcePath, destinationPath, options) {
    const self = this;
    destinationPath = this.encodePath(destinationPath);

    return new Promise((resolve, reject) => {
      destinationPath = self.queryPathToValidBucketPath(destinationPath);

      fs.copy(sourcePath, destinationPath, options, err => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }

  /**
   * Validate and write.
   * Note: you should validate sourcePath yourself!
   * @param destinationPath
   * @param data
   * @param options
   * @param options.encoding
   * @param options.mode
   * @param options.flag
   * @return {Promise}
   */
  $write(destinationPath, data, options) {
    const self = this;
    destinationPath = this.encodePath(destinationPath);

    return new Promise((resolve, reject) => {
      destinationPath = self.queryPathToValidBucketPath(destinationPath);

      fs.outputFile(destinationPath, data, options, err => {
        if (err) {
          reject(err);
        } else {
          resolve(destinationPath);
        }
      })
    })
  }

  /**
   * Save file sto bucket.
   * @param files
   * @param key multipart form data key
   * @param dest destination
   * @return {Promise}
   */
  $upload(files, key, dest) {
    return new Promise((resolve, reject) => {
      if (_.isEmpty(files) || _.isEmpty(files[key])) {
        return reject(this.errors.noFilesUploaded);
      }

      const tmpFile = files[key].path;
      this.$copy(tmpFile, dest).then(resolve).catch(reject);
    });
  }

  /**
   * Upload file
   *
   * @param req
   * @param req.files
   * @param req.query
   * @param req.query.filePath
   * @param res
   */
  upload(req, res) {
    this.$upload(req.files, 'file', req.query.filePath)
      .then(x => {
        res.send({status: 'ok'});
      })
      .catch(e => {
        this.handleError(req, res, e);
      });
  }

  /**
   * Download file
   *
   * @param req
   * @param req.query
   * @param req.query.filePath
   * @param res
   */
  get(req, res) {
    try {
      const absPath = this.queryPathToValidBucketPath(req.query.filePath);
      res.download(absPath, path.basename(absPath)); // error handling middleware should catch 404
    } catch (e) {
      this.handleError(req, res, e);
    }
  }

  /**
   * List dir.
   * @param qPath Dir to list.
   * @return {Promise}
   */
  $list(qPath = '') {
    const self = this;

    return new Promise((resolve, reject) => {
      const p = this.queryPathToValidBucketPath(qPath);

      fs.readdir(p, (err, files) => {
        if (err) {
          reject(err);
        } else {
          resolve(files.map(f => {
            return self.makeFileRes(f, path.join(p, f), undefined);
          }));
        }
      });
    });
  }

  /**
   * List files
   *
   * @param req
   * @param req.query
   * @param req.query.filePath
   * @param res
   */
  list(req, res) {
    this.$list(req.query.filePath)
      .then(x => {
        res.send({ files: x });
      })
      .catch(e => {
        this.handleError(req, res, e);
      });
  }

  /**
   * Search for file inside dir.
   *
   * @param qDir Dir to search inside it.
   * @param search Regex string.
   * @return {Promise}
   */
  $search(qDir, search) {
    const self = this;

    return new Promise((resolve, reject) => {
      const dirPath = self.queryPathToValidBucketPath(qDir);
      find.file(new RegExp(search), dirPath, function(files) {
        resolve(files.map(f => {
          const rPath = self.absolutePathToBucketRelative(f);
          return self.makeFileRes(path.basename(rPath), f, rPath);
        }));
      });
    });
  }

  /**
   * Search for file inside dir.
   * @param req
   * @param res
   */
  search(req, res) {
    this.$search(req.query.dir, req.query.name)
      .then(x => {
        res.send({ files: x });
      })
      .catch(e => {
        this.handleError(req, res, e);
      });
  }

  /**
   * Update file
   *
   * @param req
   * @param req.db
   * @param req.query
   * @param req.query.filePath
   * @param res
   */
  update(req, res) {
    return this.create(req, res);
  }

  /**
   * Remove file or dir.
   * @param qPath
   * @return {Promise}
   */
  $remove(qPath) {
    const self = this;
    qPath =  this.encodePath(qPath);

    return new Promise((resolve, reject) => {
      const p = self.queryPathToValidBucketPath(qPath);
      fs.remove(p, err => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      })
    });
  }

  /**
   * Delete file
   *
   * @param req
   * @param req.db
   * @param req.query
   * @param req.query.path
   * @param res
   */
  remove(req, res) {
    const removePath = req.query.path;
    if (!removePath) {
      this.handleError(req, res, this.errors.emptyPath);
    }

    this.$remove(removePath)
      .then(x => {
        res.send({status: 'ok'});
      })
      .catch(e => {
        this.handleError(req, res, e);
      });
  }
};
