const request = require('request');
const _       = require('lodash');

/**
 * LicenseApi
 * @type {LicenseApi}
 */
module.exports = class LicenseApi {
  constructor(authApi) {
    this.token    = undefined;
    this.email    = undefined;
    this.password = undefined;

    this.authData = undefined;

    this.baseApi  = authApi;
  }

  get isAuth() {
    return !!this.authData && !!this.token;
  }

  get accId() {
    return _.get(this.authData, 'account._id', '');
  }

  get userId() {
    return _.get(this.authData, '._id', '');
  }

  /**
   * Req.
   * @param reqOptions
   */
  req(reqOptions) {
    // reqOptions = method: 'GET'
    // reqOptions = uri: 'http://www.google.com'
    reqOptions.headers                   = reqOptions.headers || {};
    reqOptions.headers['x-access-token'] = this.token;

    return new Promise((resolve, reject) => {
      request(reqOptions, (error, response, body) => {
        if (error) {
          return reject(error);
        }

        if (response.statusCode !== 200) {
          return reject({statusCode: response.statusCode, response, body});
        }

        try {
          resolve(JSON.parse(body));
        } catch (e) {
          return reject(e);
        }
      });
    });
  }

  /**
   * Req. Check if token valid and make request. If 401 it will auto auth and retry.
   * @param options
   * @return {Promise.<*>}
   */
  async safeReq(options) {
    const self = this;
    await this.checkIfAuth();

    return new Promise((resolve, reject) => {
      this.req(options)
        .then(resolve)
        .catch(async (e) => {
          if (e && e.statusCode === 401) {
            await self.auth();
            return resolve(self.req(options));
          }

          reject(e);
        })
    });
  }

  setAuth(email, password) {
    this.email    = email;
    this.password = password;
  }

  /**
   * Auth and save credentials.
   * @param email
   * @param password
   * @return {Promise}
   */
  auth(email = this.email, password = this.password) {
    const self = this;
    this.setAuth(email, password);

    return new Promise((resolve, reject) => {
      this.login(email, password)
        .then((body) => {
          self.token = body.token;
          self.authData = body.user;
          resolve(body);
        })
        .catch((e) => {
          reject(e);
        })
    });
  }


  /**
   * Login with given credentials and return user and token
   * @param email
   * @param password
   * @param meta Any additional custom data you want in the token.
   * @return {Promise}
   */
  async login(email = this.email, password = this.password, meta = {}) {
    const self = this;
    return new Promise((resolve, reject) => {
      request.post({
          url : `${ self.baseApi }/login`,
          form: {email, password, meta}
        },
        (err, httpResponse, body) => {
          if (httpResponse.statusCode !== 200) {
            return reject({statusCode: httpResponse.statusCode, httpResponse, body});
          }

          try {
            body          = JSON.parse(body);
            resolve(body);
          } catch (e) {
            return reject(e);
          }
        });
    });
  }

  async checkIfAuth() {
    if (!this.isAuth) {
      try {
        await this.auth();
      } catch (e) { return Promise.reject(e) }
    }
    return Promise.resolve(true);
  }

  /**
   * Get list of users.
   * @return {Promise.<*>}
   */
  async getUsers() {
    await this.checkIfAuth();

    return this.safeReq({
      method: 'GET',
      uri: `${ this.baseApi }/users/${ this.accId }`,
    })
  }

  /**
   * Get info of many of users.
   * @return {Promise.<*>}
   */
  async getUsersInfo(ids) {
    await this.checkIfAuth();

    return this.safeReq({
      method: 'POST',
      uri: `${ this.baseApi }/userlist`,
      form: {'users' : JSON.stringify(ids) }
    })
  }

  /**
   * Get info of all users.
   * @return {Promise.<*>}
   */
  async getAllUsersInfo() {
    await this.checkIfAuth();

    return this.safeReq({
      method: 'POST',
      uri: `${ this.baseApi }/userlist`,
      form: {'users' : JSON.stringify({}), 'fetchAll': true }
    })
  }

  /**
   * Get user.
   * @param userId
   * @return {Promise.<*>}
   */
  async getUser(userId) {
    await this.checkIfAuth();

    if (!this.accId) throw Error('this.accId is undefined');

    return this.safeReq({
      method: 'GET',
      uri: `${ this.baseApi }/users/${ this.accId }/${ userId }`,
    });
  }

  /**
   * Get by meta value.
   * @param key
   * @param value
   * @return {Promise.<*>}
   */
  async getUserByMeta(key, value) {
    await this.checkIfAuth();

    return this.safeReq({
      method: 'GET',
      uri: `${ this.baseApi }/users/meta/${ this.accId }/${ key }/${ encodeURIComponent(value) }`,
    }).catch(x => {
      if (x.statusCode === 404) return null;
      throw x;
    })
  }

  /**
   * List by meta value.
   * @param key
   * @param value
   * @return {Promise.<*>}
   */
  async listUsersByMeta(key, value) {
    await this.checkIfAuth();

    return this.safeReq({
      method: 'GET',
      uri: `${ this.baseApi }/usersList/meta/${ this.accId }/${ key }/${ encodeURIComponent(value) }`,
    }).catch(x => {
      if (x.statusCode === 404) return null;
      throw x;
    })
  }

  /**
   * Create user.
   * @param user
   * @param user.firstName
   * @param user.lastName
   * @param user.email
   * @param user.groups
   * @param user.role
   * @param user.language
   * @param options
   * @return {Promise.<*>}
   */
  async createUser(user, options = {}) {
    await this.checkIfAuth();

    return this.safeReq({
      method: 'POST',
      uri: `${ this.baseApi }/users/${ this.accId }${options.preventEmail ? '?preventEmail=1':''}`,
      form: user
    }).then(x => x.user)
  }

  /**
   * Update user.
   * @param user
   * @param user.id
   * @param user.firstName
   * @param user.lastName
   * @param user.email
   * @param user.groups
   * @param user.role
   * @param user.language
   * @param user.preventEmail boolean preventEmail if pass reset
   * @return {Promise.<*>}
   */
  async updateUser(user, preventEmail) {
    await this.checkIfAuth();
    user.uid = user.uid || user.id || user._id;
    user.aid = this.accId;

    return this.safeReq({
      method: 'PUT',
      uri: `${ this.baseApi }/users/${ this.accId }/${ user.uid }${preventEmail ? '?preventEmail=1':''}`,
      form: user
    }).then(x => {
      return x.user;
    })
  }

  /**
   *
   * @param userId
   * @param key
   * @param val
   * @param remove
   * @return {Promise.<TResult>}
   */
  async updateUserMetaValue(userId, key, val, remove = undefined) {
    await this.checkIfAuth();

    return this.safeReq({
      method: 'PUT',
      uri: `${ this.baseApi }/users/meta/${ this.accId }/${ userId }`,
      form: {
        id       : userId,
        metaKey  : key,
        metaValue: val,
        delete   : remove
      }
    }).then(x => x.user)
  }
};
