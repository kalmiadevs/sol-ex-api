var debug       = require('debug')('app'),
    server      = require('./lib/server'),
    log         = require('./lib/log')(module),
    c           = require('./lib/configuration');

var nodes       = require('./lib/controllers/nodes'),
    test        = require('./lib/controllers/test'),
    validators  = require('./lib/middlewares/validateRequest'),
    modifiers   = require('./shared/middlewares/requestModifiers')(log);

// All set, start listening! 
server.listen( c.get('port') );

log.debug('== ===========================================');
log.debug('== KalPass REST API successfully started');
log.debug('== ===========================================');
log.debug('== PORT: %d', c.get('port') );
log.debug('== MODE: %s', process.env.NODE_ENV || 'dev');
log.debug('== ===========================================');

server.get(c.get('apiBase') + 'version', function (req, res) {
  res.send({
    version: c.get('version'),
  });
});

// All routes require auth that can be accessed only by autheticated users
server.all(c.get('apiBase') + '*', [validators.validateRequest, modifiers.addRequestWrapper]);

/* =======================================================
 * == NODES
 * ======================================================= */
  // Nodes routes that can be accessed only by authenticated & authorized users
  server.get(c.get('apiBase') + 'nodes', nodes.list);
  server.post(c.get('apiBase') + 'nodes', nodes.create);
  server.get(c.get('apiBase') + 'nodes/:id', nodes.get);
  server.put(c.get('apiBase') + 'nodes/:id', nodes.update);
  server.delete(c.get('apiBase') + 'nodes/:id', nodes.delete);

  // Sample Error route
  server.get(c.get('apiBase') + 'ErrorExample', function (req, res, next) {
    next(new Error('Random error!'));
  });

  // Connectivity test
  server.get(c.get('apiBase') + 'test', test.connectivity);

// EOF