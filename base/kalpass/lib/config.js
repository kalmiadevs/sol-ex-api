var debug   = require('debug')('config'),
    path    = require('path'),
    log     = require('./log')(module),
    express = require('express');

// Export method to be compliant with Express 3.0
var applyConfiguration = function (server) {
  var app = server,
      rootDir = path.resolve(__dirname, '..');

  app.configure(function () {
    log.debug('setting up common configuration...');

    app.use(express.favicon());

    // Parse the body
    // Warning: http://andrewkelley.me/post/do-not-use-bodyparser-with-express-js.html
    app.use(express.bodyParser({limit: '200mb', parameterLimit: 10000}));
    app.use(express.urlencoded({limit: '200mb', parameterLimit: 10000}));

    // Use the method override so that PUT and DELETE can be simulated with a POST
    app.use(express.methodOverride());

    // Parse the cookies
    app.use(express.cookieParser());

    // Session support, in normal use, put secret in environment var:
    // app.use(express.session({ secret: process.env.MY_SESSION_SECRET }));
    app.use(express.session({secret: 'hypermegatop really!'}));  // TODO: put secret in env variable

    // Enable the router
    app.use(app.router);

    // Serve static content from "public" directory
    app.use(express.static(rootDir + '/documentation'));
  });

  app.configure('dev', function () {
    log.debug('setting up "dev" configuration...');
    app.use(express.logger('tiny'));
    app.use(express.errorHandler({dumpExceptions: true, showStack: true}));
  });

  app.configure('production', function () {
    log.debug('setting up "production" configuration...');
    app.use(express.errorHandler());
  });

  app.all('/*', function(req, res, next) {
    // CORS headers
    res.header('Access-Control-Allow-Origin', '*'); // restrict it to the required domain
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    // Set custom headers for CORS
    res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
    if (req.method === 'OPTIONS') {
      res.status(200).end();
    } else {
      next();
    }
  });

  // 404 error definition
  app.use(function(req, res, next) {
      res.status(404);
      log.debug('Not found URL: %s',req.url);
      res.send({error: 'Not found'});
      return;
    });

  // 500 error definition
  app.use(function(err, req, res, next) {
      res.status(err.status || 500);
      log.error('Internal error(%d): %s', res.statusCode, err.message, err);
      res.send({error: err.message});
      return;
    });

};

exports.applyConfiguration = applyConfiguration;
