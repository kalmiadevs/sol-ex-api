let debug   = require('debug')('nodes'),
    log     = require('../log')(module),
    request = require('request'),
    async   = require('async'),
    md5     = require('blueimp-md5'),
    c       = require('../configuration'),
    _       = require('lodash'),
    Q       = require('q');

let $group       = require('../../shared/entities/group')(c, log),
    $node        = require('../../shared/entities/node')(c, log),
    handleReject = require('../../shared/errorHandler'),
    errors       = require('../../shared/error');

"use strict";

/* ======================================
 * == Private Methods
 * ====================================== */

/**
 * Add node to group (if group has permission) and node key to users
 * @param req
 * @param groupId
 * @param nodeId
 * @param keysForGroups
 * @param keysForGroups.sec
 * @param keysForGroups.pub
 * @param keysForGroups.sym
 */
var addNodeToGroup = function (req, groupId, nodeId, keysForGroup) {
  var def      = Q.defer();

  $group.get(req, groupId)
    .then(function (group) {

      // add to group
      if (group.nodes.indexOf(nodeId) === -1) {
        group.nodes.push(nodeId);
      }

      group.keyRing = group.keyRing || {};
      group.keyRing[nodeId] = keysForGroup;

      $group.update(req, group)
        .then(function (x) {
          def.resolve(x);
        })
        .catch(def.reject);
    })
    .catch(def.reject);

  return def.promise;
};

/**
 * Remove node from group and node key from users
 * @param req
 * @param groupId
 * @param nodeId
 */
var removeNodeFromGroup = function (req, groupId, nodeId) {
  var def = Q.defer();

  $group.get(req, groupId)
    .then(function (group) {

      // remove from group
      group.nodes = _.without(group.nodes, nodeId);
      delete group.keyRing[nodeId];

      $group.update(req, group)
        .then(function () {
          def.resolve();
        })
        .catch(function (e) {
          def.reject(e);
        });
    })
    .catch(function () {
      def.reject(e);
    });

  return def.promise;
};

/**
 * Apply all group changes for node
 * @param req
 * @param node
 * @param addGroups string[]
 * @param removeGroups string[]
 * @param keysForGroups {{id: string, key: string}[]}
 * @return {Q.Promise}
 */
var applyNodeGroupChanges = function (req, node, addGroups, removeGroups, keysForGroups) {

  var promisesRemove = removeGroups.map(function (gid) {
    return removeNodeFromGroup(req, gid, node.id);
  });

  var promisesAdd = addGroups.map(function (gid) {
    return addNodeToGroup(req, gid, node.id, keysForGroups[gid]);
  });

  var promisesUpdate = _.map(keysForGroups, function (val, gid) {
    return addNodeToGroup(req, gid, node.id, val);
  });

  var promises = promisesRemove.concat(promisesAdd).concat(promisesUpdate);

  var def = Q.defer();
  Q.allSettled(promises)
    .then(def.resolve)
    .catch(function (e) {
      handleReject(req, def, errors.node_handle_groups_fail);
    });

  return def.promise;
};


/* ======================================
 * == Public Methods
 * ====================================== */


/* ============================================
 * ============== CREATE NODE =================
 * ============================================ */
/**
 * @api {post} /nodes Create new node
 * @apiName Add
 * @apiGroup Nodes
 *
 * @apiParam {String} title Node title.
 * @apiParam {String} icon Node icon URL.
 * @apiParam {Array} tags Array of tags.
 * @apiParam {Array} hashes Array of hashes.
 * @apiParam {Array} groups Array of groups.
 * @apiParam {String} content Node content.
 * @apiParam {String} privileges Node privileges.
 * @apiParam {String} private Whether the node is private or not.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} node Created node.
 */
exports.create = function (req, res) {
  let reqNode       = req.body.node;
  let removeGroups  = req.body.removeGroups || [];
  let addGroups     = req.body.addGroups || [];
  let keysForGroups = req.body.keysForGroups || {};
  reqNode.owner     = req.user.uid;

  $node.create(req, reqNode)
    .then(function (node) {
      applyNodeGroupChanges(
        req,
        node,
        addGroups,
        removeGroups,
        keysForGroups
      ).then(function (x) {
        res.send({id: node.id, node: node});
      })
        .catch(function (e) {
          // rollback silently
          $node.delete(req, node.id);
          e.finish();
        });
    })
    .catch(function (e) {
      e.finish();
    });
};

/* ============================================
 * ================ LIST NODES ================
 * ============================================ */
/**
 * @api {get} /nodes Request Kalpass nodes list
 * @apiName List
 * @apiGroup Nodes
 *
 * @apiParam {Object} [filters] Filters for limiting nodes.<br/>
 * Filters can include following properties: <br/>
 * &emsp;- title filter by title<br/>
 * &emsp;- groups id of group for which you want to see nodes<br/>
 * &emsp;- owner id of user for whom you want to see ndoes<br/>
 * &emsp;- dateFrom date from which you want to see nodes<br/>
 * &emsp;- dateTo date to which you want to see nodes<br/>
 * &emsp;- tags you want to see nodes for<br/>
 * All other properties will be ignored.
 *
 * @apiSuccess {Object[]} nods Filtered nodes list.
 */
exports.list = function (req, res) {
  $node.getAll(req)
    .then(function (x) {
      res.send({nodes:x});
    })
    .catch(function (e) {
      e.finish();
    });
};

/* ============================================
 * ============== GET ONE NODE ================
 * ============================================ */
/**
 * @api {get} /nodes/:id Request node information
 * @apiName Get
 * @apiGroup Nodes
 *
 * @apiParam {String} id Unique node id.
 *
 * @apiSuccess {Object} node Node data.
 */
exports.get = function (req, res) {
  $node.get(req, req.params.id)
    .then(function (x) {
      res.send({node: x});
    })
    .catch(function (e) {
      e.finish();
    });
};

/* ============================================
 * ============== UPDATE NODE =================
 * ============================================ */
/**
 * @api {put} /nodes/:id Edit existing node
 * @apiName Edit
 * @apiGroup Nodes
 *
 * @apiParam {String} title Node title.
 * @apiParam {String} icon Node icon URL.
 * @apiParam {Array} tags Array of tags.
 * @apiParam {Array} hashes Array of hashes.
 * @apiParam {String} content Node content.
 * @apiParam {String} private Whether node is private or not.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} case Edited case.
 */
exports.update = function (req, res) {
  var newNode = req.body.node;
  newNode.owner =  newNode.owner || req.user.uid;

  let removeGroups  = req.body.removeGroups || [];
  let addGroups     = req.body.addGroups || [];
  let keysForGroups = req.body.keysForGroups || {};

  $node.get(req, newNode.id)
    .then(function (oldNode) {

      $node.update(req, newNode)
        .then(function (node) {

          applyNodeGroupChanges(
            req,
            node,
            addGroups,
            removeGroups,
            keysForGroups
          ).then(function (x) {
            res.send({id: node.id, node: node});
          })
            .catch(function (e) {
              // rollback silently
              $node.update(req, oldNode);
              e.finish();
            });
        })
        .catch(function (e) {
          // rollback silently
          $node.update(req, oldNode);
          e.finish();
        });
    })
    .catch(function (e) {
      e.finish();
    });
};

/* ============================================
 * ============== DELETE NODE =================
 * ============================================ */
/**
 * @api {delete} /nodes/:id Delete existing node
 * @apiName Delete
 * @apiGroup Nodes
 *
 * @apiParam {String} id Unique node id.
 *
 * @apiSuccess {String} status Response status.
 */
exports.delete = function (req, res) {
  $node.delete(req, req.params.id)
    .then(function (x) {
      // TODO DELETE FROM GROUPS
      res.send({id: req.params.id});
    })
    .catch(function (e) {
      e.finish();
    });
};
