var mongoose = require('mongoose');
var config = require('../lib/configuration');
var _ = require('underscore');
var request = require('request');
var async = require('async');

var token = '';

// Print instructions
console.log('This script fix sorting by adding a normalized property in object.');

var license = mongoose.createConnection(config.get('mongo:license') + '?authSource=admin');
license.once('open', function() {
  license.accounts  = require('../lib/models/license/accounts')(mongoose, license, []);
  license.accounts.find().exec(function(err, accounts) {
    _.each(accounts, function(account) {
      console.log('Updating case number for', account.company);
      updateForDB(account.database);
    });
  });
});

function updateForDB(databaseName) {
  // Open db connection
  var db = mongoose.createConnection(config.get('mongo:clients') + databaseName + '?authSource=admin');
  db.once('open', function () {
    // Load models
    var f = {
        $or:[{deleted: {'$exists' : false}}, {deleted: false}]
      };

    db.cases = require('../lib/models/cases')(mongoose, db, []);
    db.procedure = require('../lib/models/procedure')(mongoose, db, []);
    db.categories = require('../lib/models/categories')(mongoose, db, []);
    db.cases.find(f).populate({path: 'procedure'}).exec(function (err, cases) {

      async.reduce(cases, 0, function(memo, c, callback) {
        var options = {
          url     : config.get('authApi') + '/user/updateCase',
          method  : 'PUT',
          form    : {id: c.user.toString()},
          headers : {'x-access-token': token}
        };

        request(options, function (err, res) {
          if (err) {
            console.log('Error: ', err);
          } else {
            console.log('Finished counting case: ', c.name);
          }
          callback(null, 0);
        });
        c.procedure.meta.cases += 1;
        c.procedure.save();
      }, function (err) {
        console.log('Finished');
      });

    });

  });
}
