var mongoose = require('mongoose');
var c = require('../lib/configuration');
var _ = require('underscore');

// Print instructions
console.log('This script fix sorting by adding a normalized property in object.');

var license = mongoose.createConnection(c.get('mongo:license') + '?authSource=admin');
license.once('open', function() {
  license.accounts  = require('../lib/models/license/accounts')(mongoose, license, []);
  license.accounts.find().exec(function(err, accounts) {
    _.each(accounts, function(account) {
      console.log('Updating sorting for', account.company);
      updateForDB(account.database);
    });
  });
});

function updateForDB(databaseName) {
  // Open db connection
  var db = mongoose.createConnection(c.get('mongo:clients') + databaseName + '?authSource=admin');
  db.once('open', function () {
    // Load models
    db.categories = require('../lib/models/categories')(mongoose, db, []);
    db.procedure = require('../lib/models/procedure')(mongoose, db, []);
    db.procedure.procedure.find().exec(function (err, procedures) {
      _.each(procedures, function(p) {
        p.save(function(err) {
          if (err) {
            console.log('Error: ',err);
          } else {
            console.log('Finished modifiying procedure: ', p.label);
          }
        });
      });
    });
    db.cases = require('../lib/models/cases')(mongoose, db, []);
    db.cases.find().exec(function (err, cases) {
      _.each(cases, function(c) {
        c.save(function(err) {
          if (err) {
            console.log('Error: ',err);
          } else {
            console.log('Finished modifiying case: ', c.name);
          }
        });
      });
    });
    db.codelists = require('../lib/models/codelists')(mongoose, db, []);
    db.codelists.find().exec(function (err, codelists) {
      _.each(codelists, function(c) {
        c.save(function(err) {
          if (err) {
            console.log('Error: ',err);
          } else {
            console.log('Finished modifiying codelist: ', c.label);
          }
        });
      });
    });
    db.documents = require('../lib/models/documents')(mongoose, db, []);
    db.documents.find().exec(function (err, documents) {
      _.each(documents, function(d) {
        d.save(function(err) {
          if (err) {
            console.log('Error: ',err);
          } else {
            console.log('Finished modifiying document: ', d.label);
          }
        });
      });
    });
    db.tasks = require('../lib/models/tasks')(mongoose, db, []);
    db.tasks.find().exec(function (err, tasks) {
      _.each(tasks, function(t) {
        t.save(function(err) {
          if (err) {
            console.log('Error: ',err);
          } else {
            console.log('Finished modifiying task: ', t.label);
          }
        });
      });
    });
  });
}
