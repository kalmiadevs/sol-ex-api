var mongoose = require('mongoose');
var c = require('../lib/configuration');
var _ = require('underscore');

// Print instructions
console.log('This script will fix procedure versions.');

var license = mongoose.createConnection(c.get('mongo:license') + '?authSource=admin');
license.once('open', function() {
  license.accounts  = require('../lib/models/license/accounts')(mongoose, license, []);
  license.accounts.find().exec(function(err, accounts) {
    _.each(accounts, function(account) {
      console.log('Updating procedure version numbers for', account.company);
      updateForDB(account.database);
    });
  });
});

function updateForDB(databaseName) {
  // Open db connection
  var db = mongoose.createConnection(c.get('mongo:clients') + databaseName + '?authSource=admin');
  db.once('open', function () {
    // Load models
    db.procedure = require('../lib/models/procedure')(mongoose, db, []);
    db.procedure.version
      .find()
      .sort({'procedure' : -1, 'createdAt' : 1})
      .exec(function (err, versions) {
        var currentProcedure = '';
        var currentVersion = 1;
        _.each(versions, function(v) {
          if (v.procedure.toString() != currentProcedure) {
            currentProcedure = v.procedure.toString();
            currentVersion = 1;
          } else {
            currentVersion++;
          }
          if (v.version != currentVersion) {
            console.log('Changing', databaseName, ':', v._id, 'version from', v.version, 'to', currentVersion);
            v.version = currentVersion;
            v.save();
          }
        });
      });
  });
}
