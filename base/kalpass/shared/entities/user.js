var debug        = require('debug')('group_svc'),
    _            = require('lodash'),
    errors       = require('./../error'),
    handleReject = require('./../errorHandler'),
    Q            = require('q');

"use strict";

/**
 * CRUD for groups.
 * @param c
 * @param log
 * @return {{}}
 */
module.exports = function (c, log) {
  var module = {};

  /**
   * Key used for this api calls
   * @type {string}
   */
  var apiKey = 'users';

  /**
   *
   * @param req
   * @param group
   * @param actions
   * @param actions.add {[{id, groupKey}]}
   * @param actions.remove
   */
  module.batchKeyRingsUpdate = function(req, group, actions) {
    actions.add = actions.add || [];
    actions.remove = actions.remove || [];

    let addUsersPromises = actions.add.map(function (x) {
      let uDef = Q.defer();

      module.get(req, x.id)
        .then(function (u) {
          u.keyRing = u.keyRing || {};
          if (!x.keys) {
            return uDef.reject('No keys');
          }
          u.keyRing[group.id] = x.keys;

          module.update(req, u)
            .then(uDef.resolve)
            .catch(uDef.reject);
        })
        .catch(uDef.reject);

      return uDef.promise;
    });

    let removeUsersPromises = actions.remove.map(function (x) {
      let uDef = Q.defer();

      module.get(req, x.id)
        .then(function (u) {
          u.keyRing = u.keyRing || {};
          delete u.keyRing[group.id];

          module.update(req, u)
            .then(uDef.resolve)
            .catch(uDef.reject);
        })
        .catch(uDef.reject);

      return uDef.promise;
    });

    let def = Q.defer();

    // TODO use promise-throttle what if we make 100+ HTTP calls? its gonna break
    Q.allSettled(addUsersPromises.concat(removeUsersPromises))
      .then(function (x) {
        def.resolve(x);
      })
      .catch(function (e) {
        // TODO we need proper rollback
        handleReject(req, def, errors.users_batch_keyrings_update)(e);
      });

    return def.promise;
  };

  /**
   * Create user
   * @param req
   * @param user {Object}
   * @return {Q.Promise}
   */
  module.create = function (req, user) {
    var def = Q.defer();

    var options = {
      url    : c.get('authApi') + '/users/' + req.user.acc,
      method : 'POST',
      form   : user
    };

    req.authRequestJson(options)
      .then(function (x) {
        x.user.id = x.user._id;
        def.resolve(x.user);
      })
      .catch(function (e) {
        if (e.json) {
          if (e.json.code === 11000) {
            return handleReject(req, def, errors.user_already_exists)(e, 400);
          }
        }
        
        handleReject(req, def, errors.users_batch_keyrings_update)(e);
      });

    return def.promise;
  };

  /**
   * Get user
   * @param req
   * @param uid  {string}
   * @return {Q.Promise}
   */
  module.get = function (req, uid) {
    var def = Q.defer();

    var options = {
      url: c.get('authApi') + '/' + apiKey + '/' + req.user.acc + '/' + uid,
    };

    req.authRequestJson(options)
      .then(function (x) {
        if (!x) {
          return handleReject(req, def, errors.user_get_fail)(x, 502)
        }
        x.id = x._id;
        def.resolve(x);
      })
      .catch(e => {
        handleReject(req, def, errors.user_get_fail)(e);
      });

    return def.promise;
  };

  /**
   * Get users
   * @param req
   * @param filter
   * @return {Q.Promise}
   */
  module.getAll = function (req, filter) {
    var def = Q.defer();

    var options = {
      url: c.get('authApi') + '/' + apiKey + '/' + req.user.acc,
    };

    if (filter) {
      options.url = c.get('authApi') + '/users/data/' + filter + '/' + req.user.acc;
    }

    req.authRequestJson(options)
      .then(function (x) {
        _.forEach(x, function (y) {
          y.id = y._id;
        });
        def.resolve(x);
      })
      .catch(handleReject(req, def, errors.user_get_all_fail));

    return def.promise;
  };

  /**
   * Update user
   * @param req
   * @param user {Object}
   * @param user.id
   * @param user.firstName
   * @param user.lastName
   * @param user.email
   * @param user.groups
   * @param user.role
   * @param user.language
   * @param user.newPassword
   * @param user.confirmNewPassword
   * @return {Q.Promise}
   */
  module.update = function (req, user) {
    var def = Q.defer();

    var options = {
      url     : c.get('authApi') + '/' + apiKey + '/' + req.user.acc + '/' + user.id,
      method  : 'PUT',
      form    : user
    };

    req.authRequestJson(options, { forwardError: true })
      .then(function (x) {
        if (!x || !x.user) {
          return handleReject(req, def, errors.user_get_fail)(x, 502)
        }
        x.user.id = x.user._id;
        def.resolve(x.user);
      })
      .catch(function (e) {
        handleReject(req, def, errors.user_get_all_fail)(e);
      });

    return def.promise;
  };

  /**
   * Delete user
   * @param req
   * @param uid {string}
   * @return {Q.Promise}
   */
  module.delete = function (req, uid) {
    var def = Q.defer();

    var options = {
      url     : c.get('authApi') + '/' + apiKey +'/' + req.user.acc + '/' + uid,
      method  : 'DELETE'
    };

    req.authRequestJson(options, { forwardError: true })
      .then(function (x) {
        def.resolve(x);
      })
      .catch(handleReject(req, def, errors.user_delete_fail));
    
    return def.promise;
  };

  return module;
};