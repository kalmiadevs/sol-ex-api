# KalSales API #

This is the KalSales API for KalSales v1.x.x

## Installing

Obviously you need node.js and npm. Run `npm install` in the license directory to setup the dependencies. Once installed, you can run the super exciting API with `make`. By default the API is accessible at `http://localhost:5003/`.

## Structure

    kalsales
			documentation [templates for documentation]
    	lib [server-side business logic code]
    		controller [application logic, actual implementation of the routes]
    		middlewares [route middlewares, that handle authentication and dynamic model parsing]
    	Makefile [use "make" to run, "make test" to test, options available, see source]
    	app.js [dumb - no app logic - master file to assemble dependencies and start the app]
    	config.json [global app configuration]