var debug       = require('debug')('app'),
    server      = require('./lib/server'),
    log         = require('./lib/log')(module),
    c           = require('./lib/configuration');

var pipelines   = require('./lib/controllers/pipelines'),
    deals       = require('./lib/controllers/deals'),
    contacts    = require('./lib/controllers/contacts'),
    companies   = require('./lib/controllers/companies'),
    activities  = require('./lib/controllers/activities'),
    tasks       = require('./lib/controllers/tasks'),
    test        = require('./lib/controllers/test'),
    validators  = require('./lib/middlewares/validateRequest'),
    modifiers   = require('./shared/middlewares/requestModifiers')(log);

// All set, start listening! 
server.listen( c.get('port') );

log.debug('== ===========================================');
log.debug('== KalSales REST API successfully started');
log.debug('== ===========================================');
log.debug('== PORT: %d', c.get('port') );
log.debug('== MODE: %s', process.env.NODE_ENV || 'dev');
log.debug('== ===========================================');

server.get(c.get('apiBase') + 'version', function (req, res) {
  res.send({
    version: c.get('version'),
  });
});

// All routes require auth that can be accessed only by autheticated users
server.all(c.get('apiBase') + '*', [validators.validateRequest, modifiers.addRequestWrapper]);

/* =======================================================
 * == SALES PIPELINES
 * ======================================================= */
  // Can be accessed only by authenticated & authorized users
  server.get(c.get('apiBase') + 'pipelines', pipelines.list);
  server.post(c.get('apiBase') + 'pipelines', pipelines.create);
  server.get(c.get('apiBase') + 'pipelines/:id', pipelines.get);
  server.put(c.get('apiBase') + 'pipelines/:id', pipelines.update);
  server.delete(c.get('apiBase') + 'pipelines/:id', pipelines.delete);

/* =======================================================
 * == SALES DEALS
 * ======================================================= */
  // Can be accessed only by authenticated & authorized users
  server.get(c.get('apiBase') + 'deals', deals.list);
  server.post(c.get('apiBase') + 'deals', deals.create);
  server.get(c.get('apiBase') + 'deals/:id', deals.get);
  server.put(c.get('apiBase') + 'deals/:id', deals.update);
  server.delete(c.get('apiBase') + 'deals/:id', deals.delete);

/* =======================================================
 * == SALES CONTACTS
 * ======================================================= */
  // Can be accessed only by authenticated & authorized users
  server.get(c.get('apiBase') + 'contacts', contacts.list);
  server.post(c.get('apiBase') + 'contacts', contacts.create);
  server.get(c.get('apiBase') + 'contacts/:id', contacts.get);
  server.put(c.get('apiBase') + 'contacts/:id', contacts.update);
  server.delete(c.get('apiBase') + 'contacts/:id', contacts.delete);

/* =======================================================
 * == COMPANIES
 * ======================================================= */
  // Can be accessed only by authenticated & authorized users
  server.get(c.get('apiBase') + 'companies', companies.list);
  server.post(c.get('apiBase') + 'companies', companies.create);
  server.get(c.get('apiBase') + 'companies/:id', companies.get);
  server.put(c.get('apiBase') + 'companies/:id', companies.update);
  server.delete(c.get('apiBase') + 'companies/:id', companies.delete);

/* =======================================================
 * == DEAL TASKS
 * ======================================================= */
  // Can be accessed only by authenticated & authorized users
  server.get(c.get('apiBase') + 'tasks', tasks.list);
  server.post(c.get('apiBase') + 'tasks', tasks.create);
  server.get(c.get('apiBase') + 'tasks/:id', tasks.get);
  server.put(c.get('apiBase') + 'tasks/:id', tasks.update);
  server.delete(c.get('apiBase') + 'tasks/:id', tasks.delete);

/* =======================================================
 * == DEAL ACTIVITIES
 * ======================================================= */
  // Can be accessed only by authenticated & authorized users
  server.get(c.get('apiBase') + 'activities', activities.list);
  server.post(c.get('apiBase') + 'activities', activities.create);
  server.get(c.get('apiBase') + 'activities/:id', activities.get);
  server.put(c.get('apiBase') + 'activities/:id', activities.update);
  server.delete(c.get('apiBase') + 'activities/:id', activities.delete);

  // Sample Error route
  server.get(c.get('apiBase') + 'ErrorExample', function (req, res, next) {
    next(new Error('Random error!'));
  });

  // Connectivity test
  server.get(c.get('apiBase') + 'test', test.connectivity);

// EOF