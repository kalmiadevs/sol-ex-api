let debug   = require('debug')('activities'),
    log     = require('../log')(module),
    request = require('request'),
    async   = require('async'),
    md5     = require('blueimp-md5'),
    c       = require('../configuration'),
    _       = require('lodash'),
    Q       = require('q');

let $node        = require('../../shared/entities/node')(c, log),
    handleReject = require('../../shared/errorHandler'),
    errors       = require('../../shared/error');

"use strict";

/* ======================================
 * == Private Methods
 * ====================================== */


/* ======================================
 * == Public Methods
 * ====================================== */


/* ============================================
 * ============== CREATE ACTIVITY =============
 * ============================================ */
/**
 * @api {post} /activities Create new activity
 * @apiName Add
 * @apiGroup Activities
 *
 * @apiParam {String} title Activity title.
 * @apiParam {String} description Activity description.
 * @apiParam {String} startDate Activity start date.
 * @apiParam {String} endDate Activity end date.
 * @apiParam {String} executor Activity executor.
 * @apiParam {String} creator Activity creator.
 * @apiParam {String} deal Activity deal.
 * @apiParam {String} contact Activity contact.
 * @apiParam {String} company Activity company.
 * @apiParam {String} type Activity type.
 * @apiParam {String} status Activity pipeline status.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} activity Created activity.
 */
exports.create = function (req, res) {
  let reqNode         = req.body.node;
      reqNode.owner   = req.user.uid;

  $node.create(req, reqNode)
    .then(function (node) {
      res.send({id: node.id, node: node});
    })
    .catch(function (e) {
      e.finish();
    });
};

/* ============================================
 * ================ LIST ACTIVITIES ===========
 * ============================================ */
/**
 * @api {get} /activities Request activities list
 * @apiName List
 * @apiGroup Activities
 *
 * @apiParam {Object} [filters] Filters for limiting activities.<br/>
 * Filters can include following properties: <br/>
 * &emsp;- executor<br/>
 * &emsp;- creator<br/>
 * &emsp;- status<br/>
 * &emsp;- deal<br/>
 * &emsp;- contact<br/>
 * &emsp;- company<br/>
 * &emsp;- type<br/>
 * 
 * All other properties will be ignored.
 *
 * @apiSuccess {Object[]} activities Filtered activities list.
 */
exports.list = function (req, res) {
  $node.getAll(req)
    .then(function (x) {
      res.send({nodes:x});
    })
    .catch(function (e) {
      e.finish();
    });
};

/* ============================================
 * ============== GET ONE ACTIVITY ============
 * ============================================ */
/**
 * @api {get} /activities/:id Request activity information
 * @apiName Get
 * @apiGroup Activities
 *
 * @apiParam {String} id Unique activity id.
 *
 * @apiSuccess {Object} activity Activity data.
 */
exports.get = function (req, res) {
  $node.get(req, req.params.id)
    .then(function (x) {
      res.send({node: x});
    })
    .catch(function (e) {
      e.finish();
    });
};

/* ============================================
 * ============== UPDATE ACTIVITY =============
 * ============================================ */
/**
 * @api {put} /activities/:id Edit existing activity
 * @apiName Edit
 * @apiGroup Activities
 *
 * @apiParam {String} title Activity title.
 * @apiParam {String} description Activity description.
 * @apiParam {String} startDate Activity start date.
 * @apiParam {String} endDate Activity end date.
 * @apiParam {String} executor Activity executor.
 * @apiParam {String} creator Activity creator.
 * @apiParam {String} deal Activity deal.
 * @apiParam {String} contact Activity contact.
 * @apiParam {String} company Activity company.
 * @apiParam {String} type Activity type.
 * @apiParam {String} status Activity pipeline status.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} activity Edited activity.
 */
exports.update = function (req, res) {
  var newNode       = req.body.node;
      newNode.owner = newNode.owner || req.user.uid;

  $node.get(req, newNode.id)
    .then(function (oldNode) {

      $node.update(req, newNode)
        .then(function (node) {
          res.send({id: node.id, node: node});
        })
        .catch(function (e) {
          // rollback silently
          $node.update(req, oldNode);
          e.finish();
        });
    })
    .catch(function (e) {
      e.finish();
    });
};

/* ============================================
 * ============== DELETE ACTIVITY =============
 * ============================================ */
/**
 * @api {delete} /activities/:id Delete existing activity
 * @apiName Delete
 * @apiGroup Activities
 *
 * @apiParam {String} id Unique activity id.
 *
 * @apiSuccess {String} status Response status.
 */
exports.delete = function (req, res) {
  $node.delete(req, req.params.id)
    .then(function (x) {
      res.send({id: req.params.id});
    })
    .catch(function (e) {
      e.finish();
    });
};
