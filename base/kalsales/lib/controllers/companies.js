let debug   = require('debug')('companies'),
    log     = require('../log')(module),
    request = require('request'),
    async   = require('async'),
    md5     = require('blueimp-md5'),
    c       = require('../configuration'),
    _       = require('lodash'),
    Q       = require('q');

let $node        = require('../../shared/entities/node')(c, log),
    handleReject = require('../../shared/errorHandler'),
    errors       = require('../../shared/error');

"use strict";

/* ======================================
 * == Private Methods
 * ====================================== */


/* ======================================
 * == Public Methods
 * ====================================== */


/* ============================================
 * ============== CREATE COMPANY ==============
 * ============================================ */
/**
 * @api {post} /companies Create new company
 * @apiName Add
 * @apiGroup Companies
 *
 * @apiParam {String} name Company name.
 * @apiParam {String} description Company description.
 * @apiParam {String} email Company email.
 * @apiParam {String} address Company address.
 * @apiParam {String} city Company city.
 * @apiParam {String} state Company state.
 * @apiParam {String} postcode Company postcode.
 * @apiParam {String} position Company position.
 * @apiParam {String} phone Company phone.
 * @apiParam {String} mobile Company mobile.
 * @apiParam {String} website Company website.
 * @apiParam {String} postcode Company postcode.
 * @apiParam {String} linkedin Company linkedin.
 * @apiParam {String} twitter Company twitter.
 * @apiParam {String} facebook Company facebook.
 * @apiParam {String} instagram Company instagram.
 * @apiParam {Array} tags Array of company tags.
 * @apiParam {Object} meta Company metadata.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} company Created company.
 */
exports.create = function (req, res) {
  let reqNode         = req.body.node;
      reqNode.owner   = req.user.uid;

  $node.create(req, reqNode)
    .then(function (node) {
      res.send({id: node.id, node: node});
    })
    .catch(function (e) {
      e.finish();
    });
};

/* ============================================
 * ================ LIST COMPANIES ============
 * ============================================ */
/**
 * @api {get} /companies Request companies list
 * @apiName List
 * @apiGroup Companies
 *
 * @apiParam {Object} [filters] Filters for limiting companies.<br/>
 * Filters can include following properties: <br/>
 * &emsp;- name filter by name<br/>
 * &emsp;- email filter by email<br/>
 * &emsp;- city<br/>
 * &emsp;- state<br/>
 * &emsp;- postcode<br/>
 * &emsp;- tags you want to see contacts for<br/>
 * &emsp;- status for contact status<br/>
 * 
 * All other properties will be ignored.
 *
 * @apiSuccess {Object[]} companies Filtered companies list.
 */
exports.list = function (req, res) {
  $node.getAll(req)
    .then(function (x) {
      res.send({nodes:x});
    })
    .catch(function (e) {
      e.finish();
    });
};

/* ============================================
 * ============== GET ONE COMPANY =============
 * ============================================ */
/**
 * @api {get} /companies/:id Request company information
 * @apiName Get
 * @apiGroup Companies
 *
 * @apiParam {String} id Unique company id.
 *
 * @apiSuccess {Object} pipeline Company data.
 */
exports.get = function (req, res) {
  $node.get(req, req.params.id)
    .then(function (x) {
      res.send({node: x});
    })
    .catch(function (e) {
      e.finish();
    });
};

/* ============================================
 * ============== UPDATE COMPANY ==============
 * ============================================ */
/**
 * @api {put} /companies/:id Edit existing company
 * @apiName Edit
 * @apiGroup Companies
 *
 * @apiParam {String} name Company name.
 * @apiParam {String} description Company description.
 * @apiParam {String} email Company email.
 * @apiParam {String} address Company address.
 * @apiParam {String} city Company city.
 * @apiParam {String} state Company state.
 * @apiParam {String} postcode Company postcode.
 * @apiParam {String} position Company position.
 * @apiParam {String} phone Company phone.
 * @apiParam {String} mobile Company mobile.
 * @apiParam {String} website Company website.
 * @apiParam {String} postcode Company postcode.
 * @apiParam {String} linkedin Company linkedin.
 * @apiParam {String} twitter Company twitter.
 * @apiParam {String} facebook Company facebook.
 * @apiParam {String} instagram Company instagram.
 * @apiParam {Array} tags Array of company tags.
 * @apiParam {Object} meta Company metadata.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} pipeline Edited company.
 */
exports.update = function (req, res) {
  var newNode       = req.body.node;
      newNode.owner = newNode.owner || req.user.uid;

  $node.get(req, newNode.id)
    .then(function (oldNode) {

      $node.update(req, newNode)
        .then(function (node) {
          res.send({id: node.id, node: node});
        })
        .catch(function (e) {
          // rollback silently
          $node.update(req, oldNode);
          e.finish();
        });
    })
    .catch(function (e) {
      e.finish();
    });
};

/* ============================================
 * ============== DELETE COMPANY ==============
 * ============================================ */
/**
 * @api {delete} /companies/:id Delete existing company
 * @apiName Delete
 * @apiGroup Companies
 *
 * @apiParam {String} id Unique company id.
 *
 * @apiSuccess {String} status Response status.
 */
exports.delete = function (req, res) {
  $node.delete(req, req.params.id)
    .then(function (x) {
      // TODO DELETE CONTACTS, DEALS, ACTIVITIES AND TASKS
      res.send({id: req.params.id});
    })
    .catch(function (e) {
      e.finish();
    });
};
