let debug   = require('debug')('contacts'),
    log     = require('../log')(module),
    request = require('request'),
    async   = require('async'),
    md5     = require('blueimp-md5'),
    c       = require('../configuration'),
    _       = require('lodash'),
    Q       = require('q');

let $node        = require('../../shared/entities/node')(c, log),
    handleReject = require('../../shared/errorHandler'),
    errors       = require('../../shared/error');

"use strict";

/* ======================================
 * == Private Methods
 * ====================================== */


/* ======================================
 * == Public Methods
 * ====================================== */


/* ============================================
 * ============== CREATE CONTACT ==============
 * ============================================ */
/**
 * @api {post} /contacts Create new contact
 * @apiName Add
 * @apiGroup Contacts
 *
 * @apiParam {String} name Contact name.
 * @apiParam {String} firstName Contact first name.
 * @apiParam {String} lastName Contact last name.
 * @apiParam {String} email Contact email.
 * @apiParam {String} company Contact link to company.
 * @apiParam {String} address Contact address.
 * @apiParam {String} city Contact city.
 * @apiParam {String} state Contact state.
 * @apiParam {String} postcode Contact postcode.
 * @apiParam {String} position Contact position.
 * @apiParam {String} phone Contact phone.
 * @apiParam {String} mobile Contact mobile.
 * @apiParam {String} website Contact website.
 * @apiParam {String} postcode Contact postcode.
 * @apiParam {String} linkedin Contact linkedin.
 * @apiParam {String} twitter Contact twitter.
 * @apiParam {String} facebook Contact facebook.
 * @apiParam {String} instagram Contact instagram.
 * @apiParam {String} source Contact source.
 * @apiParam {Array} tags Array of contact tags.
 * @apiParam {Object} meta Contact metadata.
 * 
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} contact Created contact.
 */
exports.create = function (req, res) {
  let reqNode         = req.body.node;
      reqNode.owner   = req.user.uid;

  $node.create(req, reqNode)
    .then(function (node) {
      res.send({id: node.id, node: node});
    })
    .catch(function (e) {
      e.finish();
    });
};

/* ============================================
 * ================ LIST CONTACTS=============
 * ============================================ */
/**
 * @api {get} /contacts Request contacts list
 * @apiName List
 * @apiGroup Contacts
 *
 * @apiParam {Object} [filters] Filters for limiting contacts.<br/>
 * Filters can include following properties: <br/>
 * &emsp;- name filter by name<br/>
 * &emsp;- email filter by email<br/>
 * &emsp;- company<br/>
 * &emsp;- city<br/>
 * &emsp;- state<br/>
 * &emsp;- postcode<br/>
 * &emsp;- position<br/>
 * &emsp;- source<br/>
 * &emsp;- tags you want to see contacts for<br/>
 * &emsp;- status for contact status<br/>
 * 
 * All other properties will be ignored.
 *
 * @apiSuccess {Object[]} contacts Filtered contacts list.
 */
exports.list = function (req, res) {
  $node.getAll(req)
    .then(function (x) {
      res.send({nodes:x});
    })
    .catch(function (e) {
      e.finish();
    });
};

/* ============================================
 * ============== GET ONE CONTACT =============
 * ============================================ */
/**
 * @api {get} /contacts/:id Request contact information
 * @apiName Get
 * @apiGroup Contacts
 *
 * @apiParam {String} id Unique contact id.
 *
 * @apiSuccess {Object} pipeline Contact data.
 */
exports.get = function (req, res) {
  $node.get(req, req.params.id)
    .then(function (x) {
      res.send({node: x});
    })
    .catch(function (e) {
      e.finish();
    });
};

/* ============================================
 * ============== UPDATE CONTACT ==============
 * ============================================ */
/**
 * @api {put} /contacts/:id Edit existing contact
 * @apiName Edit
 * @apiGroup Contacts
 *
 * @apiParam {String} name Contact name.
 * @apiParam {String} firstName Contact first name.
 * @apiParam {String} lastName Contact last name.
 * @apiParam {String} email Contact email.
 * @apiParam {String} company Contact link to company.
 * @apiParam {String} address Contact address.
 * @apiParam {String} city Contact city.
 * @apiParam {String} state Contact state.
 * @apiParam {String} postcode Contact postcode.
 * @apiParam {String} position Contact position.
 * @apiParam {String} phone Contact phone.
 * @apiParam {String} mobile Contact mobile.
 * @apiParam {String} website Contact website.
 * @apiParam {String} postcode Contact postcode.
 * @apiParam {String} linkedin Contact linkedin.
 * @apiParam {String} twitter Contact twitter.
 * @apiParam {String} facebook Contact facebook.
 * @apiParam {String} instagram Contact instagram.
 * @apiParam {String} source Contact source.
 * @apiParam {Array} tags Array of contact tags.
 * @apiParam {Object} meta Contact metadata.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} pipeline Edited contact.
 */
exports.update = function (req, res) {
  var newNode       = req.body.node;
      newNode.owner = newNode.owner || req.user.uid;

  $node.get(req, newNode.id)
    .then(function (oldNode) {

      $node.update(req, newNode)
        .then(function (node) {
          res.send({id: node.id, node: node});
        })
        .catch(function (e) {
          // rollback silently
          $node.update(req, oldNode);
          e.finish();
        });
    })
    .catch(function (e) {
      e.finish();
    });
};

/* ============================================
 * ============== DELETE CONTACT ==============
 * ============================================ */
/**
 * @api {delete} /contacts/:id Delete existing contact
 * @apiName Delete
 * @apiGroup Contacts
 *
 * @apiParam {String} id Unique contact id.
 *
 * @apiSuccess {String} status Response status.
 */
exports.delete = function (req, res) {
  $node.delete(req, req.params.id)
    .then(function (x) {
      // TODO DELETE DEALS, ACTIVITIES AND TASKS
      res.send({id: req.params.id});
    })
    .catch(function (e) {
      e.finish();
    });
};
