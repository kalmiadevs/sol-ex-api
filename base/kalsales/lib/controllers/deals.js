let debug   = require('debug')('deals'),
    log     = require('../log')(module),
    request = require('request'),
    async   = require('async'),
    md5     = require('blueimp-md5'),
    c       = require('../configuration'),
    _       = require('lodash'),
    Q       = require('q');

let $node        = require('../../shared/entities/node')(c, log),
    handleReject = require('../../shared/errorHandler'),
    errors       = require('../../shared/error');

"use strict";

/* ======================================
 * == Private Methods
 * ====================================== */


/* ======================================
 * == Public Methods
 * ====================================== */


/* ============================================
 * ============== CREATE DEAL =================
 * ============================================ */
/**
 * @api {post} /deals Create new deal
 * @apiName Add
 * @apiGroup Deals
 *
 * @apiParam {String} title Deal title.
 * @apiParam {String} description Deal description.
 * @apiParam {String} amount Deal amount.
 * @apiParam {String} closeDate Deal expected close date.
 * @apiParam {String} pipeline Deal pipeline.
 * @apiParam {String} creator Deal creator.
 * @apiParam {String} contact Deal contact.
 * @apiParam {String} company Deal company.
 * @apiParam {String} status Deal pipeline status.
 * @apiParam {Array} tags Array of deal tags.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} deal Created deal.
 */
exports.create = function (req, res) {
  let reqNode         = req.body.node;
      reqNode.owner   = req.user.uid;

  $node.create(req, reqNode)
    .then(function (node) {
      res.send({id: node.id, node: node});
    })
    .catch(function (e) {
      e.finish();
    });
};

/* ============================================
 * ================ LIST DEALS ================
 * ============================================ */
/**
 * @api {get} /deals Request pipeline deals list
 * @apiName List
 * @apiGroup Deals
 *
 * @apiParam {Object} [filters] Filters for limiting deals.<br/>
 * Filters can include following properties: <br/>
 * &emsp;- title filter by title<br/>
 * &emsp;- tags you want to see pipelines for<br/>
 * &emsp;- starDate<br/>
 * &emsp;- endDate<br/>
 * &emsp;- status for deal status inside pipeline<br/>
 * &emsp;- contact<br/>
 * &emsp;- company<br/>
 * 
 * All other properties will be ignored.
 *
 * @apiSuccess {Object[]} deals Filtered deals list.
 */
exports.list = function (req, res) {
  $node.getAll(req)
    .then(function (x) {
      res.send({nodes:x});
    })
    .catch(function (e) {
      e.finish();
    });
};

/* ============================================
 * ============== GET ONE DEAL ================
 * ============================================ */
/**
 * @api {get} /deals/:id Request deal information
 * @apiName Get
 * @apiGroup Deals
 *
 * @apiParam {String} id Unique deal id.
 *
 * @apiSuccess {Object} pipeline Deal data.
 */
exports.get = function (req, res) {
  $node.get(req, req.params.id)
    .then(function (x) {
      res.send({node: x});
    })
    .catch(function (e) {
      e.finish();
    });
};

/* ============================================
 * ============== UPDATE DEAL =================
 * ============================================ */
/**
 * @api {put} /deals/:id Edit existing deal
 * @apiName Edit
 * @apiGroup Deals
 *
 * @apiParam {String} title Deal title.
 * @apiParam {String} description Deal description.
 * @apiParam {String} amount Deal amount.
 * @apiParam {String} closeDate Deal expected close date.
 * @apiParam {String} pipeline Deal pipeline.
 * @apiParam {String} creator Deal creator.
 * @apiParam {String} contact Deal contact.
 * @apiParam {String} company Deal company.
 * @apiParam {String} status Deal pipeline status.
 * @apiParam {Array} tags Array of deal tags.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} pipeline Edited deal.
 */
exports.update = function (req, res) {
  var newNode       = req.body.node;
      newNode.owner = newNode.owner || req.user.uid;

  $node.get(req, newNode.id)
    .then(function (oldNode) {

      $node.update(req, newNode)
        .then(function (node) {
          res.send({id: node.id, node: node});
        })
        .catch(function (e) {
          // rollback silently
          $node.update(req, oldNode);
          e.finish();
        });
    })
    .catch(function (e) {
      e.finish();
    });
};

/* ============================================
 * ============== DELETE DEAL =================
 * ============================================ */
/**
 * @api {delete} /deals/:id Delete existing deal
 * @apiName Delete
 * @apiGroup Deals
 *
 * @apiParam {String} id Unique deal id.
 *
 * @apiSuccess {String} status Response status.
 */
exports.delete = function (req, res) {
  $node.delete(req, req.params.id)
    .then(function (x) {
      // TODO DELETE FROM GROUPS
      res.send({id: req.params.id});
    })
    .catch(function (e) {
      e.finish();
    });
};
