var debug       = require('debug')('nodes'),
    log         = require('../log')(module),
    config      = require('../configuration'),
    request     = require('request'),
    async       = require('async'),
    md5         = require('blueimp-md5'),
    c           = require('../configuration'),
    _           = require('underscore'),
    qs          = require('querystring'),
    q           = require('q');


/* ======================================
 * == TODO should refactor into metadata!
 * == let's not make nodes messy
 * ====================================== */


/* ======================================
 * == Private Methods
 * ====================================== */
function handleError400(res, err) {
  res.statusCode = 404;
  return res.send({error: 'ValidationError', e: err});
}

function handleError404(res) {
  res.statusCode = 404;
  return res.send({error: 'Not found'});
}

function handleError500(res, err) {
  res.statusCode = 500;
  log.error('Internal error(%d): %s',res.statusCode, err.message);
  return res.send({error: 'Server error', e: err});
}

// Get favorites for a given user id.
exports.fetchFavoritesNode = function(token, id) {
  return new Promise(function(resolve, reject) {
    var options = {
      url     : c.get('kbApi') + '/nodes',
      headers : {'x-access-token': token, 'filters' : JSON.stringify({
        'meta': [
            { name: 'project', value: c.get('nodeProjectId') },
            { name: 'favorites', value: 'true' },
          ],
        'owner': id
        })
      }
    };

    request(options, function (err, response, nlist) {
      if (!nlist) {
        reject({ code: 404, msg: 'Not found'});
        return;
      }
      if (err || nlist.error !== undefined) {
        reject({ code: 500, msg: 'Server error'});
        log.error('Internal error(%d): %s', res.statusCode, err.message);
      } else {
        nlist = JSON.parse(nlist);
        resolve(nlist.length < 1 ? {} : nlist[0]);
      }
    });
  });
}

/* ======================================
 * == Public Methods
 * ====================================== */

/* ============================================
 * ============== GET FAVORITES ===============
 * ============================================ */
/**
 * @api {get} /favorites Request favorites for user
 * @apiName Get
 * @apiGroup Favorites
 *
 * @apiSuccess {Object} favorites Favorites data.
 */
exports.get = function(req, res) {
  exports.fetchFavoritesNode(req.token, req.user.uid)
  .then(function(node) {
    if (node.content) {
      return res.send(JSON.parse(node.content));
    } else {
      return res.send({ favorites: [] });
    }
  })
  .catch(function(err) {
    res.statusCode = err.code;
    return res.send({error: err.msg});
  })
};

/* ============================================
 * ============= UPDATE FAVORITES =============
 * ============================================ */
/**
 * @api {post} /favorites Update or create favorites for user.
 * @apiName Create
 * @apiGroup Favorites
 *
 * @apiParam {Object} favorites Object containing an array of nodes named 'favorites'
 * @apiSuccess {Object} favorites New favorites data.
 */
exports.update = function(req, res) {
  exports.fetchFavoritesNode(req.token, req.user.uid)
  .then(function(node) {
    if (!node.content) {
      // Favorites node does not exist yet, create it.
      try {
        var options = {
          url     : c.get('kbApi') + '/nodes',
          method  : 'POST',
          headers : {'x-access-token': req.token, 'content-type': 'application/x-www-form-urlencoded'},
          form    : {
            owner: req.user.uid,
            title: 'Favorites',
            icon: '',
            meta: {
                'project': c.get('nodeProjectId'),
                'favorites': true,
              },
            content: JSON.stringify({ favorites: req.body.favorites })
          }
        };

        request(options, function (err, _, n) {
          if (!n || n.error) {
            res.statusCode = 500;
            return res.send({error: 'Server error'});
          }
          try {
            n = JSON.parse(n);

            if (err || !n || !n.node) {
              res.statusCode = 500;
              log.error('Internal error(%d): %s', res.statusCode, (err ? err.message : n.error));
              return res.send({error: 'Server error'});
            } else {
              return res.send({ status: 'OK', favorites: JSON.parse(n.node.content).favorites });
            }
          } catch (ex) {
            res.statusCode = 500;
            return res.send({error: ex});
          }
        });
      } catch (err) {
        res.statusCode = 500;
        return res.send({error: err});
      }

    } else {
      // update favorites node
      try {
        var options = {
          url     : c.get('kbApi') + '/nodes/' + node._id,
          headers : {'x-access-token': req.token, 'content-type': 'application/x-www-form-urlencoded'},
          method  : 'PUT',
          form    : {
            content : JSON.stringify({ favorites: req.body.favorites }),
            version : node.version
          }
        }

        request(options, function (err, _, favStatus) {
          if (!favStatus || err) {
            res.statusCode = 500;
            return res.send({error: 'Server error'});
          }
          try {
            favStatus = JSON.parse(favStatus);

            if (err || !favStatus || !favStatus.status || favStatus.status != 'OK') {
              res.statusCode = 500;
              log.error('Internal error(%d): %s', res.statusCode, (err ? err.message : favStatus));
              return res.send({error: 'Server error'});
            } else {
              res.statusCode = 200;
              return res.send({ status: favStatus.status, favorites: JSON.parse(favStatus.node.content).favorites });
            }
          } catch (err) {
            res.statusCode = 500;
            log.error('Internal error(%d): %s', res.statusCode, (err ? err.message : favStatus));
            return res.send({error: 'Server error'});
          }
        });
      } catch (err) {
        res.statusCode = 500;
        return res.send({error: err});
      }
    }
  })
  .catch(function(err) {
    res.statusCode = err.code;
    return res.send({error: err.msg});
  });
};
