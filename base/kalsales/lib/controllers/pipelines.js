let debug   = require('debug')('pipelines'),
    log     = require('../log')(module),
    request = require('request'),
    async   = require('async'),
    md5     = require('blueimp-md5'),
    c       = require('../configuration'),
    _       = require('lodash'),
    Q       = require('q');

let $node        = require('../../shared/entities/node')(c, log),
    handleReject = require('../../shared/errorHandler'),
    errors       = require('../../shared/error');

"use strict";

/* ======================================
 * == Private Methods
 * ====================================== */


/* ======================================
 * == Public Methods
 * ====================================== */


/* ============================================
 * ============ CREATE PIPELINE ===============
 * ============================================ */
/**
 * @api {post} /pipelines Create new pipeline
 * @apiName Add
 * @apiGroup Pipelines
 *
 * @apiParam {String} title Pipeline title.
 * @apiParam {Array} tags Array of Pipeline tags.
 * @apiParam {Object} stages Pipeline stages.
 * @apiParam {String} privileges Pipeline privileges.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} pipeline Created pipeline.
 */
exports.create = function (req, res) {
  let reqNode       = req.body.node;
      reqNode.owner = req.user.uid;

  $node.create(req, reqNode)
    .then(function (node) {
      res.send({id: node.id, node: node});
    })
    .catch(function (e) {
      e.finish();
    });
};

/* ============================================
 * ============== LIST PIPELINES ==============
 * ============================================ */
/**
 * @api {get} /pipelines Request KalSales pipelines list
 * @apiName List
 * @apiGroup Pipelines
 *
 * @apiParam {Object} [filters] Filters for limiting Pipelines.<br/>
 * Filters can include following properties: <br/>
 * &emsp;- title filter by title<br/>
 * &emsp;- tags you want to see pipelines for<br/>
 * All other properties will be ignored.
 *
 * @apiSuccess {Object[]} pipelines Filtered pipelines list.
 */
exports.list = function (req, res) {
  $node.getAll(req)
    .then(function (x) {
      res.send({nodes:x});
    })
    .catch(function (e) {
      e.finish();
    });
};

/* ============================================
 * ============ GET ONE PIPELINE ==============
 * ============================================ */
/**
 * @api {get} /pipelines/:id Request pipeline information
 * @apiName Get
 * @apiGroup Pipelines
 *
 * @apiParam {String} id Unique pipelines id.
 *
 * @apiSuccess {Object} pipeline Pipelines data.
 */
exports.get = function (req, res) {
  $node.get(req, req.params.id)
    .then(function (x) {
      res.send({node: x});
    })
    .catch(function (e) {
      e.finish();
    });
};

/* ============================================
 * ============ UPDATE PIPELINE ===============
 * ============================================ */
/**
 * @api {put} /pipelines/:id Edit existing Pipeline
 * @apiName Edit
 * @apiGroup Pipelines
 *
 * @apiParam {String} title Pipeline title.
 * @apiParam {Array} tags Array of Pipeline tags.
 * @apiParam {Object} stages Pipeline stages.
 * @apiParam {String} privileges Pipeline privileges.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} pipeline Edited pipeline.
 */
exports.update = function (req, res) {
  var newNode = req.body.node;
  newNode.owner =  newNode.owner || req.user.uid;

  $node.get(req, newNode.id)
    .then(function (oldNode) {

      $node.update(req, newNode)
        .then(function (node) {
          res.send({id: node.id, node: node});
        })
        .catch(function (e) {
          // rollback silently
          $node.update(req, oldNode);
          e.finish();
        });
    })
    .catch(function (e) {
      e.finish();
    });
};

/* ============================================
 * ============== DELETE NODE =================
 * ============================================ */
/**
 * @api {delete} /pipelines/:id Delete existing pipeline
 * @apiName Delete
 * @apiGroup Pipelines
 *
 * @apiParam {String} id Unique pipeline id.
 *
 * @apiSuccess {String} status Response status.
 */
exports.delete = function (req, res) {
  $node.delete(req, req.params.id)
    .then(function (x) {
      // TODO DELETE ALL DEALS
      res.send({id: req.params.id});
    })
    .catch(function (e) {
      e.finish();
    });
};
