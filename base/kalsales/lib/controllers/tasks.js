let debug   = require('debug')('tasks'),
    log     = require('../log')(module),
    request = require('request'),
    async   = require('async'),
    md5     = require('blueimp-md5'),
    c       = require('../configuration'),
    _       = require('lodash'),
    Q       = require('q');

let $node        = require('../../shared/entities/node')(c, log),
    handleReject = require('../../shared/errorHandler'),
    errors       = require('../../shared/error');

"use strict";

/* ======================================
 * == Private Methods
 * ====================================== */


/* ======================================
 * == Public Methods
 * ====================================== */


/* ============================================
 * ============== CREATE TASK =================
 * ============================================ */
/**
 * @api {post} /tasks Create new task
 * @apiName Add
 * @apiGroup Tasks
 *
 * @apiParam {String} title Task title.
 * @apiParam {String} description Task description.
 * @apiParam {String} startDate Task start date.
 * @apiParam {String} endDate Task end date.
 * @apiParam {String} executor Task executor.
 * @apiParam {String} creator Task creator.
 * @apiParam {String} deal Task deal.
 * @apiParam {String} contact Task contact.
 * @apiParam {String} company Task company.
 * @apiParam {String} type Task type.
 * @apiParam {String} status Task pipeline status.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} task Created task.
 */
exports.create = function (req, res) {
  let reqNode         = req.body.node;
      reqNode.owner   = req.user.uid;

  $node.create(req, reqNode)
    .then(function (node) {
      res.send({id: node.id, node: node});
    })
    .catch(function (e) {
      e.finish();
    });
};

/* ============================================
 * ================ LIST TASKS ================
 * ============================================ */
/**
 * @api {get} /tasks Request tasks list
 * @apiName List
 * @apiGroup Tasks
 *
 * @apiParam {Object} [filters] Filters for limiting tasks.<br/>
 * Filters can include following properties: <br/>
 * &emsp;- title<br/>
 * &emsp;- executor<br/>
 * &emsp;- creator<br/>
 * &emsp;- starDate<br/>
 * &emsp;- endDate<br/>
 * &emsp;- status<br/>
 * &emsp;- deal<br/>
 * &emsp;- contact<br/>
 * &emsp;- company<br/>
 * &emsp;- type<br/>
 * 
 * All other properties will be ignored.
 *
 * @apiSuccess {Object[]} tasks Filtered tasks list.
 */
exports.list = function (req, res) {
  $node.getAll(req)
    .then(function (x) {
      res.send({nodes:x});
    })
    .catch(function (e) {
      e.finish();
    });
};

/* ============================================
 * ============== GET ONE TASK ================
 * ============================================ */
/**
 * @api {get} /tasks/:id Request task information
 * @apiName Get
 * @apiGroup Tasks
 *
 * @apiParam {String} id Unique task id.
 *
 * @apiSuccess {Object} pipeline Task data.
 */
exports.get = function (req, res) {
  $node.get(req, req.params.id)
    .then(function (x) {
      res.send({node: x});
    })
    .catch(function (e) {
      e.finish();
    });
};

/* ============================================
 * ============== UPDATE TASK =================
 * ============================================ */
/**
 * @api {put} /tasks/:id Edit existing task
 * @apiName Edit
 * @apiGroup Tasks
 *
 * @apiParam {String} title Task title.
 * @apiParam {String} description Task description.
 * @apiParam {String} startDate Task start date.
 * @apiParam {String} endDate Task end date.
 * @apiParam {String} executor Task executor.
 * @apiParam {String} creator Task creator.
 * @apiParam {String} deal Task deal.
 * @apiParam {String} contact Task contact.
 * @apiParam {String} company Task company.
 * @apiParam {String} type Task type.
 * @apiParam {String} status Task pipeline status.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} task Edited task.
 */
exports.update = function (req, res) {
  var newNode       = req.body.node;
      newNode.owner = newNode.owner || req.user.uid;

  $node.get(req, newNode.id)
    .then(function (oldNode) {

      $node.update(req, newNode)
        .then(function (node) {
          res.send({id: node.id, node: node});
        })
        .catch(function (e) {
          // rollback silently
          $node.update(req, oldNode);
          e.finish();
        });
    })
    .catch(function (e) {
      e.finish();
    });
};

/* ============================================
 * ============== DELETE TASK =================
 * ============================================ */
/**
 * @api {delete} /tasks/:id Delete existing task
 * @apiName Delete
 * @apiGroup Tasks
 *
 * @apiParam {String} id Unique task id.
 *
 * @apiSuccess {String} status Response status.
 */
exports.delete = function (req, res) {
  $node.delete(req, req.params.id)
    .then(function (x) {
      res.send({id: req.params.id});
    })
    .catch(function (e) {
      e.finish();
    });
};
