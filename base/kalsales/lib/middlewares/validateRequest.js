var jwt          = require('jwt-simple'),
    mongoose     = require('mongoose'),
    softDelete   = require('mongoose-softdelete'),
    DataTable    = require('mongoose-datatable'),
    glob         = require('glob'),
    log          = require('../log')(module),
    c            = require('../configuration'),
    path         = require('path'),
    q            = require('q'),
    request     = require('request');

mongoose.set('debug', true);

exports.validatePublicRequest = function(req, res, next) {
  // Public requests need to have aid (account id) query parameter
  if (!req.query.aid) {
    res.status(401);
    res.json({
      'status'  : 401,
      'message' : 'Invalid account id'
    });
    return;
  }
  next();
};

exports.validateRequest = function(req, res, next) {

  // We skip the token outh for [OPTIONS] requests.
  if (req.method === 'OPTIONS') {
    next();
  }

  var apiCall = req.url.split('/')[3];
  // We skip the token auth for test request.
  if (apiCall === 'test') {
    return next();
  }
  // We skip the token auth for public requests
  if (apiCall === 'public') {
    return next();
  }

  // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
  var token = (req.body && req.body.access_token) ||
               (req.query && req.query.access_token) ||
               req.headers['x-access-token'];
  // jscs:enable requireCamelCaseOrUpperCaseIdentifiers
  if (token) {
    try {
      var decoded = jwt.decode(token, c.get('secret'));

      if (decoded.exp <= Date.now()) {
        res.status(401);
        res.json({
          'status'  : 401,
          'message' : 'Token Expired'
        });
        return;
      }

      // Check if token from another api (unstable, stable)
      // TODO(Marko): Remove decoded.iss !== c.get('secret') this was to prevent forcing users to logout
      // because of our typo where iss was secret instead of api
      /*
      if (decoded.iss !== c.get('secret') && decoded.iss !== c.get('api')) {
        res.status(401);
        res.json({
          'status'  : 401,
          'message' : 'Invalid token'
        });
        return;
      }
      */

      req.user  = decoded;
      req.token = token;

      var options = {
        method: 'GET',
        url     : c.get('authApi') + '/authenticated/' + req.user.uid,
        headers : {'x-access-token': req.token}
      };

      request(options, function (err, response, authenticated) {
        authenticated = JSON.parse(authenticated); // BUG if server offline this crashes whole server
        if (!err) {
          if (!authenticated.auth) {
            if (response.statusCode === 401) {
              res.status(401);
              res.json({
                'status': 401,
                'message': 'Token expired'
              });
              return;
            } else {
              res.status(500);
              res.json({
                'status': 500,
                'message': 'Oops something went wrong',
                'error': err
              });
              return;
            }
          } else {
            next();
          }
        } else {
          log.error(err);
          res.status(500);
          res.json({
            'status': 500,
            'message': 'Oops something went wrong',
            'error': err
          });
          return;
        }
      });
    }
    catch (err) {
      res.status(500);
      res.json({
        'status': 500,
        'message': 'Oops something went wrong',
        'error': err
      });
      return;
    }
  }
  else {
    res.status(401);
    res.json({
      'status': 401,
      'message': 'Invalid Token or Key'
    });
    return;
  }
};
