# MightyFields AUTH #

This is the authentication and license API for KalBlocks version 1

## Installing

Obviously you need node.js and npm. Run `npm install` in the license directory to setup the dependencies. Once installed, you can run the super exciting API with `make`. By default the API is accessible at `http://localhost:5001/`. To add new customer run `make customer`.

## Structure

    license
    	lib [server-side business logic code]
    		controller [application logic, actual implementation of the routes]
    		middlewares [route middlewares, that handle authentication and dynamic model parsing]
    		models [DB models that access MongoDB database]
    		templates [holds the email templates]
    	public [unrestricted area]
    	  images [holds images that need to be accessible for emails]
    	test [automated unit test files]
    	Makefile [use "make" to run, "make test" to test, options available, see source]
    	app.js [dumb - no app logic - master file to assemble dependencies and start the app]
    	config.json [global app configuration]
