var debug       = require('debug')('app'),
    server      = require('./lib/server'),
    log         = require('./lib/log')(module),
    c           = require('./lib/configuration'),
    auth        = require('./lib/controllers/auth'),
    account     = require('./lib/controllers/accounts'),
    user        = require('./lib/controllers/users'),
    role        = require('./lib/controllers/roles'),
    permissions = require('./lib/controllers/permissions'),
    cron        = require('./lib/controllers/cron'),
    test        = require('./lib/controllers/test'),
    ravenStore  = require('./shared/store/raven'),
    validate    = require('./lib/middlewares/validateRequest'),
    connect     = require('./lib/middlewares/connectRequest');

ravenStore.config(c.get('raven'), {
  environment: c.get('production') ? 'production' : 'development',
  tags: {
    api: 'license'
  }
});

global.App = {
  db      : undefined,
  license : undefined
};

// All set, start listening!
var port = c.get('port');
server.listen(port);
log.debug('Express server listening on port %d in %s mode', port, process.env.NODE_ENV);

server.get(c.get('apiBase') + 'version', function (req, res) {
  res.send({
    version: c.get('version')
  });
});

// Routes that can be accessed by any one
server.get(c.get('apiBase') + 'user-info', validate, auth.userInfo);
server.post(c.get('apiBase') + 'login', validate, auth.login);
server.post(c.get('apiBase') + 'login-long', validate, auth.loginLong);
server.post(c.get('apiBase') + 'logout', validate, auth.logout);
// server.post(c.get('apiBase') + 'register', validate, auth.createAccount);
server.post(c.get('apiBase') + 'set', validate, auth.setPassword);
server.get(c.get('apiBase') + 'db/:aid', validate, auth.accountDB);
server.post(c.get('apiBase') + 'reset', validate, auth.resetPassword);
// server.post(c.get('apiBase') + 'new', auth.newPassword); !! NOT YET IMLEMENTED
server.get(c.get('apiBase') + 'authenticated/:uid', validate, auth.check);
server.get(c.get('apiBase') + 'authenticatedlong/:uid', validate, auth.checkLong);

// Accounts
// server.get(c.get('apiBase') + 'account', validate, account.list);
server.post(c.get('apiBase') + 'account', connect, account.add); // public!

server.get(c.get('apiBase') + 'account/:aid', validate, account.edit);
server.put(c.get('apiBase') + 'account/:aid', validate, account.update);
server.get(c.get('apiBase') + 'account/:aid/activate', validate, account.activate);
server.get(c.get('apiBase') + 'account/:aid/block', validate, account.block);
// server.delete(c.get('apiBase') + 'account/:id', validate, account.delete); !! NOT YET IMLEMENTED
server.get(c.get('apiBase') + 'account/:aid/lastupdate', validate, account.lastUpdate);
server.get(c.get('apiBase') + 'account/:aid/updatedb', validate, account.updateLastUpdate);

// Users
server.get(c.get('apiBase') + 'users/:aid', validate, user.list);
server.get(c.get('apiBase') + 'users/data/:data/:aid', validate, user.list);
server.get(c.get('apiBase') + 'users/:aid/:uid', validate, user.get);
server.get(c.get('apiBase') + 'users/meta/:aid/:mkey/:mval', validate, user.getByMeta);
server.get(c.get('apiBase') + 'usersList/meta/:aid/:mkey/:mval', validate, user.listByMeta);
server.put(c.get('apiBase') + 'users/:aid/:uid', validate, user.update);
server.put(c.get('apiBase') + 'users/lock/:aid/:uid', validate, user.lock);
server.put(c.get('apiBase') + 'users/meta/:aid/:uid', validate, user.meta);
server.post(c.get('apiBase') + 'users/:aid', validate, user.add);
// server.get(c.get('apiBase') + 'users/:aid/:id/activate/:key', user.activate); !! NOT YET IMLEMENTED
server.delete(c.get('apiBase') + 'users/:aid/:uid', validate, user.delete);
server.post(c.get('apiBase') + 'users/:aid/:uid/invite', validate, user.invite);

server.get(c.get('apiBase') + 'avatar/:aid/:uid', connect, user.avatar);

server.post(c.get('apiBase') + 'userlist', validate, user.getMany);


// Roles
server.get(c.get('apiBase') + 'role/:aid', validate, role.list);
server.post(c.get('apiBase') + 'role/:aid', validate, role.add);
server.put(c.get('apiBase') + 'role/:aid/:id', validate, role.update);
server.delete(c.get('apiBase') + 'role/:aid/:id', validate, role.delete);
server.post(c.get('apiBase') + 'role/:aid/:id/user', validate, role.addUser);
server.delete(c.get('apiBase') + 'role/:aid/:id/user', validate, role.removeUser);

// Permissions
server.post(c.get('apiBase') + 'permissions/:permissionId', validate, permissions.create);
// server.get(c.get('apiBase') + 'permissions/:aid', validate, permissions.list);

// Connectivity test
server.get(c.get('apiBase') + 'test', test.connectivity);

// Cron & other dev tasks
server.get(c.get('apiBase') + 'cron/post-install', cron.createPermissions);
