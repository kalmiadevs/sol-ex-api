var jwt = require('jwt-simple'),
  log = require('../log')(module),
  pw = require('secure-password'),
  c = require('../configuration'),
  crypto = require('crypto'),
  mail = require('./mailer'),
  permissions = require('./permissions'),
  getSlug = require('speakingurl'),
  generator = require('password-generator'),
  _ = require('underscore'),
  q = require('q');

'use strict';

/**
 * @api {post} /login Login
 * @apiName Login
 * @apiGroup Authentication
 * @apiDescription This request can be used without token.
 * It generates you one for all future requests.
 *
 * @apiParam {String} email Your email.
 * @apiParam {String} password Your password.
 *
 * @apiSuccess {Object} user Your user data.
 */
exports.login = function (req, res) {

  var email = req.body.email || '';
  var password = req.body.password || '';

  if (email === '' || password === '') {
    res.status(401);
    res.json({
      'status' : 401,
      'message': 'Invalid credentials'
    });
    return;
  }

  // Fire a query to your DB and check if the credentials are valid
  var f = {
    '$and': [
      {
        '$or': [
          {'email': email},
          {'meta.vatId': email},
        ]
      },
      {'$or': [{deleted: {'$exists': false}}, {deleted: false}]}, //if user is deleted he shouldn't be able to login
      {'$and': [{active: true}, {locked: false}]}, //if user is locked or unactive he shouldn't be able to login
    ]
  };
  req.db.users
    .findOne(f)
    .populate({path: 'account', model: req.db.accounts})
    .select('-devices')
    .exec(function (err, user) {
      if (err) {
        return log.error(err);
      }
      // If authentication fails, we send a 401 back
      if (!user || !pw.verifyPassword(password, user.password)) {
        res.status(401);
        res.json({
          'status' : 401,
          'message': 'Invalid credentials'
        });
        return;
      }

      if (user) {
        if (!user.account) {
          res.status(500);
          res.json({
            'status' : 500,
            'message': 'User not linked to account.'
          });
          return;
        }
        permissions.getPermissionsForUser(req, user.account._id, user._id)
          .then((pData) => {
            user.permissions = pData;

            if (user.role === 'admin' || user.role === 'superadmin') {
              user.permissions[permissions.permissionKeys.admin] = 0b111;
            }

            user.lastLoginAt = new Date;
            user.save();

            res.json(genToken(user, req.body.meta));
          })
          .catch(function (e) {
            res.status(500);
            res.json({
              'status' : 500,
              'message': 'Oops something went wrong'
            });
          })
      }

      return res;
    });
};

/**
 * @api {post} /login-long Login mobile
 * @apiName LoginLong
 * @apiGroup Authentication
 * @apiDescription This request can be used without token.
 * It generares you one for all future requests.
 *
 * @apiParam {String} email Your email.
 * @apiParam {String} password Your password.
 *
 * @apiSuccess {Object} user Your user data.
 */
exports.loginLong = function (req, res) {

  var email = req.body.email || '';
  var password = req.body.password || '';
  var deviceToken = req.body.deviceToken || '';

  if (email === '' || password === '') {
    res.status(401);
    res.json({
      'status' : 401,
      'message': 'Invalid credentials'
    });
    return;
  }

  // Fire a query to your DB and check if the credentials are valid
  var f = {
    'email': email,
    '$or'  : [{deleted: {'$exists': false}}, {deleted: false}], //if user is deleted he shouldn't be able to login
    '$and' : [{active: true}, {locked: false}], //if user is locked or unactive he shouldn't be able to login
  };
  req.db.users
    .findOne(f)
    .populate({path: 'account', model: req.db.accounts})
    .populate({path: 'groups', model: req.db.groups})
    .exec(function (err, user) {
      if (err) {
        return log.error(err);
      }
      // If authentication fails, we send a 401 back
      if (!user || !pw.verifyPassword(password, user.password)) {
        res.status(401);
        res.json({
          'status' : 401,
          'message': 'Invalid credentials'
        });
        return;
      }

      if (user) {
        // Add a device token to user
        if (deviceToken && !_.contains(user.devices, deviceToken)) {
          user.devices.push(deviceToken);
          user.meta.devices += 1;
          user.save();
        }

        res.json(genLongToken(user));
      }

      return res;
    });
};

/**
 * @api {get} /user-info Request user information mobile
 * @apiName UserInfo
 * @apiGroup Authentication
 *
 * @apiSuccess {Object} user User information.
 */
exports.userInfo = function (req, res) {
  console.log(req.user, req.token);
  if (req.user === undefined) {
    res.status(401);
    res.json({
      'status' : 401,
      'message': 'User not found'
    });
    return;
  }

  return req.db.users
    .findById(req.user.uid)
    .populate({path: 'account', model: req.db.accounts})
    .exec(function (err, user) {
      if (err) {
        return log.error(err);
      }
      if (user) {
        res.send(user);
      }
      return res;
    });
};

/**
 * @api {get} /db/:aid Get account's database name
 * @apiName GetDB
 * @apiGroup Authentication
 *
 * @apiParam {String} aid Unique account id.
 *
 * @apiSuccess {String} database Account's database name.
 */
exports.accountDB = function (req, res) {

  return req.db.accounts
    .findById(req.params.aid)
    .select('database')
    .exec(function (err, account) {
      if (err) {
        return log.error(err);
      }
      if (account) {
        res.send(account.database);
      }
      return res;
    });
};

exports.logout = function (req, res) {
  // Set activation code
  req.body.active = false;
};

/**
 * @api {post} /reset Request password reset mail
 * @apiName ResetMail
 * @apiGroup Authentication
 * @apiDescription This request can be used without token.
 * It sends password reset link on requested email.
 *
 * @apiParam {String} email Your email.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {String} message Response description.
 */
exports.resetPassword = function (req, res) {
  exports.generateResetToken(req)
    .then(function (user) {
      var config = {
        template: 'password-reset-mail',
        email   : user.email,
        subject : c.get('branding:productName') + ' password reset',
        data    : {
          user: {
            firstName    : user.firstName,
            lastName     : user.lastName,
            email        : user.email,
            resetPwdToken: user.resetPwdToken
          },
          link: c.get('branding:productWebsite')
        }
      };

      mail.send(config);

      res.send({status: 'OK', message: 'Password mail sent.'});
    })
    .catch(function (code, err) {
      res.statusCode = code;
      res.send({error: err});
    });
}


exports.generateResetToken = function (req) {
  var deferred = q.defer();

  req.db.users
    .findOne({'email': req.body.email, 'meta.public': {$not: {$exists: true}}})
    .populate({path: 'account', model: req.db.accounts})
    .exec(function (err, user) {
      if (err) {
        return log.error(err);
      }

      // If no user we send normal reply otherwise users can check if other users
      // user MF.
      if (!user) {
        deferred.reject({code: 404, err: 'Not found'})
      }

      if (user) {
        crypto.randomBytes(20, function (err, buf) {
          var token = buf.toString('hex');

          user.resetPwdToken = token;
          user.resetPwdExpires = Date.now() + 3600000; // 1 hour

          user.save(function (err) {
            if (!err) {
              log.info('user password token updated');

              deferred.resolve(user);
            } else {
              if (err.name === 'ValidationError') {
                deferred.reject({code: 400, err: 'Validation error'});
              } else {
                deferred.reject({code: 500, err: 'Server error'});
              }
              log.error('Internal error(%d): %s', res.statusCode, err.message);
            }
          });
        });
      }
    });

  return deferred.promise;
}


/**
 * @api {post} /set/:token Reset password
 * @apiName ResetPwd
 * @apiGroup Authentication
 *
 * @apiParam {String} token Password reset token recieved in mail by
 * <a href="#api-Authentication-ResetMail">Request password reset mail</a>
 * @apiParam {String} password New password.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {String} message Response description.
 */
exports.setPassword = function (req, res) {
  // Fire a query to your DB and check if the user exists
  req.db.users
    .findOne({
      'resetPwdToken'  : req.body.token,
      'resetPwdExpires': {$gt: Date.now()},
      'meta.public'    : {$not: {$exists: true}}
    })
    .populate({path: 'account', model: req.db.accounts})
    .exec(function (err, user) {
      if (err) {
        return log.error(err);
      }

      // If no user we send a 401 back
      if (!user) {
        res.status(400);
        res.json({
          'status' : 400,
          'message': 'Password reset token is invalid or has expired.'
        });
        return;
      }

      if (user) {
        user.password = pw.makePassword(req.body.newPassword);
        user.resetPwdToken = undefined;
        user.resetPwdExpires = undefined;

        user.save(function (err) {
          if (!err) {
            log.info('user password changed');

            var config = {
              template: 'password-reset-confirm-mail',
              email   : user.email,
              subject : 'Your ' + c.get('branding:productName') + ' password has been changed',
              data    : {
                user: {
                  firstName: user.firstName,
                  lastName : user.lastName,
                  email    : user.email,
                },
                link: c.get('branding:productWebsite')
              }
            };
            mail.send(config);

            res.send({status: 'OK', message: 'Password successfully changed.'});
          }
          else {
            if (err.name === 'ValidationError') {
              res.statusCode = 400;
              res.send({error: 'Validation error'});
            } else {
              res.statusCode = 500;
              res.send({error: 'Server error'});
            }
            log.error('Internal error(%d): %s', res.statusCode, err.message);
          }
        });
      }

      return res;
    });
};

exports.activate = function (req, res) {
  req.body.active = false;
};

/**
 * @api {get} /authenticated/:uid Check if user has rights to access studio
 * @apiName Check
 * @apiGroup Authentication
 *
 * @apiParam {String} uid Unique user id.
 *
 * @apiSuccess {Number} status Response status code.
 * @apiSuccess {String} message Response description.
 * @apiSuccess {Boolean} auth Is user authenticated.
 */
exports.check = function (req, res) {
  return req.db.users.findById(req.params.uid, function (err, user) {
    if (!user) {
      res.status(401);
      res.json({
        'status' : 401,
        'message': 'User does not exist',
        'auth'   : false
      });
      return;
    }
    if (err) {
      res.status(500);
      res.json({
        'status' : 500,
        'message': 'Oops something went wrong',
        'error'  : err,
        'auth'   : false
      });
      return;
    }
    if (!user.active || user.deleted) {
      res.status(401);
      res.json({
        'status' : 401,
        'message': 'Token expired',
        'auth'   : false
      });
      return;
    }

    res.status(200);
    res.json({
      'status' : 200,
      'message': 'OK',
      'auth'   : true
    });
    return;
  });
};

/**
 * @api {get} /authenticatedlong/:uid Check if user has rights to access mobile
 * @apiName CheckMobile
 * @apiGroup Authentication
 *
 * @apiParam {String} uid Unique user id.
 *
 * @apiSuccess {Number} status Response status code.
 * @apiSuccess {String} message Response description.
 * @apiSuccess {Boolean} auth Is user authenticated.
 */
exports.checkLong = function (req, res) {
  return req.db.users.findById(req.params.uid, function (err, user) {
    if (!user) {
      res.status(404);
      res.json({
        'status' : 404,
        'message': 'Not found',
        'auth'   : false
      });
      return;
    }
    if (err) {
      res.status(500);
      res.json({
        'status' : 500,
        'message': 'Oops something went wrong',
        'error'  : err,
        'auth'   : false
      });
      return;
    }

    if (!user.active || user.deleted) {
      res.status(401);
      res.json({
        'status' : 401,
        'message': 'Token expired',
        'auth'   : false
      });
      return;
    }

    res.status(200);
    res.json({
      'status' : 200,
      'message': 'OK',
      'auth'   : true
    });
    return;
  });
};

// private method
function genToken(user, meta) {
  var created = new Date();
  var expires = expiresIn(created, 7); // 7 days
  var token = jwt.encode({
    exp : expires,
    acc : user.account._id,
    sub : user.email,
    uid : user._id,
    db  : user.account.database, //'mf_client'
    grp : user.role,
    per : user.permissions,
    iss : c.get('api'),
    meta: meta || {}
  }, c.get('secret'));

  return {
    token  : token,
    created: created,
    expires: expires,
    user   : user
  };
}

// Generate a unique database name for company
function generteDbName(company) {
  var opt = {
    separator: '_',
    titleCase: true,
  };

  return 'mf_' + getSlug(company, opt);
}

// private method
function genLongToken(user) {
  var created = new Date();
  var expires = expiresIn(created, 120); // 120 days
  var token = jwt.encode({
    exp: expires,
    acc: user.account._id,
    sub: user.email,
    uid: user._id,
    db : user.account.database,
    grp: user.role,
    iss: c.get('api')
  }, c.get('secret'));

  return {
    token  : token,
    created: created,
    expires: expires,
    user   : user
  };
}

function expiresIn(created, numDays) {
  return created.setDate(created.getDate() + numDays);
}
