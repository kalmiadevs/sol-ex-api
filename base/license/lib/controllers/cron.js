var debug       = require('debug')('users'),
    log         = require('../log')(module),
    c           = require('../configuration'),
    pw          = require('secure-password'),
    generator   = require('password-generator'),
    sjcl        = require('sjcl'),
    Q           = require('q'),
    permissions = require('./permissions'),
    mail        = require('./mailer');

"use strict";

exports.createPermissions = function (req, res) {
  Q.allSettled([
    permissions.$create(req, 'kb_admin', 0, 'Admin rights.'),
    permissions.$create(req, 'kp_node', 0, 'Admin rights.'),
  ]).then(x => {
    res.send(x);
  })
    .catch(e => {
      res.statusCode = 500;
      res.send(x);
    });
};
