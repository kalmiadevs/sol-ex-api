var debug           = require('debug')('permissions'),
    log             = require('../log')(module),
    c               = require('../configuration'),
    Q               = require('q'),
    _               = require('underscore'),
    mail            = require('./mailer');

"use strict";

/**
 * List of keys.
 * @type {{[key.string]: string}}
 */
exports.permissionKeys = {
  admin: 'kb_admin'
};

/**
 * Create permission
 * @param req
 * @param key
 * @param value
 * @param description
 * @return {Q.Promise}
 */
var $create = function (req, key, value, description) {
  var def = Q.defer();

  var permission = new req.db.permission({
    key         : key,
    value       : value,
    description : description,
  });

  // Save case
  permission.save(function (err) {
    if (err) {
      return def.reject(err);
    }
    def.resolve();
  });

  return def.promise;
};
exports.$create = $create;

/**
 * Get all permission
 * @return {Q.Promise}
 */
var $getAll = function (req) {
  var def = Q.defer();

  req.db.permission.find({}, function (err, data) {
    if (err) {
      return def.reject(err);
    }
    def.resolve(data);
  });

  return def.promise;
};
exports.$getAll = $getAll;

/**
 * @api {post} /permissions Add new permission
 * @apiName Add
 * @apiGroup Permissions
 *
 * @apiParam {String} key Key.
 * @apiParam {String} value Value (number).
 * @apiParam {String} description Summary.
 *
 * @apiSuccess {String} status Request status.
 * @apiSuccess {Object} permission Permission data object.
 */
exports.create = function(req, res) {
  $create(req)
    .then(function (x) {
      res.send(x);
    })
    .catch(function (e) {
      e.finish();
    });
};


// List all permissions for all accounts
/**
 * @api {get} /permission/:aid Request users list
 * @apiName List
 * @apiGroup Permissions
 *
 * @apiParam {String} aid Unique account id.
 *
 * @apiSuccess {Object[]} permissions List of * acc.
 */
exports.list = function(req, res) {
  $getAll(req)
    .then(allP => {
      let p = {};
      for (const i in allP) {
        if (allP[i].key) {
          p[allP[i].key] = allP[i].value || 0;
        }
      }
      return res.send(p)
    })
    .catch(e => {
      res.statusCode = 500;
      log.error('Internal error(%d): %s', res.statusCode, e.message);
    });
};

exports.getPermissionsForUser = function (req, aid, uid) {
  var def = Q.defer();
  $getAll(req)
    .then(allP => {
      var f = {
        $and: [
          {$or: [{deleted: {'$exists' : false}}, {deleted: false}]},
          {$or: [{owner: aid}]},
          { users: uid }
        ]
      };
      return req.db.groups.find(f, function (err, roles) {
        if (!err) {
          let p = {};

          for (const i in allP) {
            if (allP[i].key) {
              p[allP[i].key] = allP[i].value || 0;
            }
          }

          for (const i in roles) {
            for (const pk in roles[i].permissions) {
              p[pk] = (p[pk] || 0) | roles[i].permissions[pk];
            }
          }

          def.resolve(p);
        }
        else {
          def.reject(err);
        }
      });
    })
    .catch(def.reject);

  return def.promise;
};
