var debug           = require('debug')('roles'),
    log             = require('../log')(module),
    c               = require('../configuration');

// List all roles
/**
 * @api {get} /role/:aid Request roles list
 * @apiName List
 * @apiGroup Roles
 *
 * @apiParam {String} aid Unique account id.
 *
 * @apiSuccess {Object[]} roles Roles list.
 */
exports.list = function(req, res) {
  var f = {
    $and: [
      {$or: [{deleted: {'$exists' : false}}, {deleted: false}]},
      {$or: [{owner: req.params.aid} /*, {owner: 'master'} */]}
    ]
  };
  return req.db.groups.find(f, function (err, roles) {
    if (!err) {
      return res.send(roles);
    }
    else {
      res.statusCode = 500;
      log.error('Internal error(%d): %s',res.statusCode,err.message);
      return res.send({error: 'Server error'});
    }
  });
};

// Update role
/**
 * @api {put} /role/:aid/:id Edit existing role
 * @apiName Edit
 * @apiGroup Roles
 *
 * @apiParam {String} aid Unique account id.
 * @apiParam {String} id Unique role id.
 * @apiParam {String} name Role name.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} role Updated role.
 */
exports.update = function(req, res) {
  return req.db.groups.findById(req.params.id, function (err, role) {
    if (!role) {
      res.statusCode = 404;
      return res.send({error: 'Not found'});
    }

    role.name        = req.body.name;
    role.users       = req.body.users;
    role.permissions = req.body.permissions || {};

    role.publicKey  = req.body.publicKey;
    role.keyRing    = req.body.keyRing;

    role.users = req.body.users;
    role.nodes = req.body.nodes;

    return role.save(function (err) {
      if (!err) {
        log.info('role updated');
        return res.send({status: 'OK', role: role});
      }
      else {
        if (err.name === 'ValidationError') {
          res.statusCode = 400;
          res.send({error: 'Validation error'});
        } else {
          res.statusCode = 500;
          res.send({error: 'Server error'});
        }
        log.error('Internal error(%d): %s', res.statusCode, err.message);
      }
    });
  });
};

// Add role
/**
 * @api {post} /role/:aid Add new role
 * @apiName Add
 * @apiGroup Roles
 *
 * @apiParam {String} aid Unique account id..
 * @apiParam {String} name Role name.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} role Created role.
 */
exports.add = function(req, res) {

  // var permissionSet = new req.db.permissions({
  //   permissions : {},
  // });

  var role = new req.db.groups({
    name       : req.body.name,
    owner      : (req.params.aid /*|| 'master'*/),
    // permissions: req.body.permissions || {},
    permissions: req.body.permissions,

    publicKey : req.body.publicKey,
    keyRing   : req.body.keyRing,

    users: req.body.users,
    nodes: req.body.nodes,
  });

  role.save(function (err) {
    if (!err) {
      log.info('role created');

      res.statusCode = 200;
      return res.send({status: 'OK', role: role});

      /*permissionSet.save(function (err) {
        if (!err) {
          res.statusCode = 200;
          return res.send({status: 'OK', role: role});
        } else {
          if (err.name === 'ValidationError') {
            res.statusCode = 400;
            res.send({error: 'Validation error', e: err});
          } else {
            res.statusCode = 500;
            res.send({error: 'Server error'})
          }
          log.error('Internal error(%d): %s', res.statusCode, err.message);
        }
      })*/
    }
    else {
      if (err.name === 'ValidationError') {
        res.statusCode = 400;
        res.send({error: 'Validation error', e: err});
      }
      else {
        res.statusCode = 500;
        res.send({error: 'Server error', e: err});
      }
      log.error('Internal error(%d): %s', res.statusCode, err.message);
    }
  });
};

// Delete role
/**
 * @api {delete} /role/:aid/:id Delete existing role
 * @apiName Delete
 * @apiGroup Roles
 *
 * @apiParam {String} aid Unique account id.
 * @apiParam {String} id Unique role id.
 *
 * @apiSuccess {String} status Response status.
 */
exports.delete = function (req, res) {
  return req.db.groups.findById(req.params.id, function (err, role) {
    if (!role) {
      res.statusCode = 404;
      return res.send({error: 'Not found'});
    }

    // don't need that?
    // if (!role.deletable) {
    //   res.statusCode = 500;
    //   return res.send({error: 'This role can\'t be deleted.'});
    // }

    return role.softdelete(function (err) {
      if (!err) {
        log.info('role removed');
        return res.send({status: 'OK'});
      }
      else {
        res.statusCode = 500;
        log.error('Internal error(%d): %s', res.statusCode, err.message);
        return res.send({error: 'Server error'});
      }
    });
  });
};

// Add user to role
/**
 * @api {post} /role/:aid/:id/user Add user to role
 * @apiName AddUser
 * @apiGroup Roles
 *
 * @apiParam {String} aid Unique account id.
 * @apiParam {String} id Unique role id.
 * @apiParam {String} userId Id of user you want to add to role.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} role Updated role.
 */
exports.addUser = function(req, res) {
  return req.db.groups.findById(req.params.id, function (err, role) {
    if (!role) {
      res.statusCode = 404;
      return res.send({error: 'Not found'});
    }

    role.users.push(req.body.userId);

    return role.save(function (err) {
      if (!err) {
        log.info('role updated');
        return res.send({status: 'OK', role: role});
      }
      else {
        if (err.name === 'ValidationError') {
          res.statusCode = 400;
          res.send({error: 'Validation error'});
        } else {
          res.statusCode = 500;
          res.send({error: 'Server error'});
        }
        log.error('Internal error(%d): %s', res.statusCode, err.message);
      }
    });
  });
};

// Remove user from role
/**
 * @api {delete} /role/:aid/:id/user Delete user from role
 * @apiName DeleteUser
 * @apiGroup Roles
 *
 * @apiParam {String} aid Unique account id.
 * @apiParam {String} id Unique role id.
 * @apiParam {String} userId Id of user you want to remove from role..
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} role Updated role.
 */
exports.removeUser = function(req, res) {
  return req.db.groups.findById(req.params.id, function (err, role) {
    if (!role) {
      res.statusCode = 404;
      return res.send({error: 'Not found'});
    }

    var idx = -1;
    _.each(role.users, function(item, i) {
      if (item._id === req.params.userId) {
        idx = i;
      }
    });
    role.users.splice(idx, 1);

    return role.save(function (err) {
      if (!err) {
        log.info('role updated');
        return res.send({status: 'OK', role: role});
      }
      else {
        if (err.name === 'ValidationError') {
          res.statusCode = 400;
          res.send({error: 'Validation error'});
        } else {
          res.statusCode = 500;
          res.send({error: 'Server error'});
        }
        log.error('Internal error(%d): %s', res.statusCode, err.message);
      }
    });
  });
};
