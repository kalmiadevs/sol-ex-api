var debug       = require('debug')('procedures'),
    log         = require('../log')(module),
    c           = require('../configuration'),
    q           = require('q'),
    mongoose    = require('mongoose');

// Test connectivity
/**
 * @api {get} /test Test connection
 * @apiName Test
 * @apiGroup Test
 *
 * @apiSuccess {String} db Database connection status.
 */
exports.connectivity = function(req, res) {
  var result = {};
  // DB connectivity
  var dbPromise = q.defer();
  var db = mongoose.createConnection(c.get('mongo:license') + '?authSource=admin');
  db.on('error', function() {
    result.db = 'error';
    dbPromise.reject();
    db.close();
  });
  db.once('open', function() {
    result.db = 'ok';
    dbPromise.resolve();
    db.close();
  });
  // Check if all connectivity test passed
  q.all([dbPromise.promise]).then(function() {
    res.send(result);
  }).catch(function() {
    res.statusCode = 404;
    res.send(result);
  });
};
