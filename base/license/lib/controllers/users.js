var debug       = require('debug')('users'),
    log         = require('../log')(module),
    c           = require('../configuration'),
    pw          = require('secure-password'),
    generator   = require('password-generator'),
    sjcl        = require('sjcl'),
    _           = require('underscore'),
    mail        = require('./mailer'),
    auth        = require('./auth'),
    jdenticon   = require('jdenticon'),
    Jimp        = require('jimp'),
    permissions = require('./permissions');

exports.avatar = function (req, res) {
  var fallbackImg = function () {
    var out = jdenticon.toPng(req.params.uid, req.params.size || 256);
    res.set('Content-Type', 'image/png');
    res.end(out, 'binary');
  };

  if (!req.params.aid) {
    return fallbackImg();
  }

  return req.db.users.find({ 'account': req.params.aid, '_id': req.params.uid }, function (err, user) {
    if (!user || !user[0]) {
      return fallbackImg();
    }
    if (!err) {
      if (user[0].avatarBase64) {
        res.set('Content-Type', 'image/png');
        res.end(new Buffer(user[0].avatarBase64.split(',')[1], 'base64'), 'binary');
      } else {
        fallbackImg();
      }
    }
    else {
      return fallbackImg();
    }
  });
};

// List all users for account
/**
 * @api {get} /users/:aid Request users list
 * @apiName List
 * @apiGroup Users
 *
 * @apiParam {String} aid Unique account id.
 *
 * @apiSuccess {Object[]} users List of users.
 */
exports.list = function (req, res) {

  var options = {
    conditions: {
      '$or'        : [{ deleted: { '$exists': false } }, { deleted: false }],
      account      : req.params.aid,
      'meta.public': { $not: { $exists: true } }
    },
    select    : [
      'active',
      'groups'
    ]
  };

  if (req.params.data) {
    req.db.users.dataTable(JSON.parse(req.params.data), options, function (err, data) {
      if (err) {
        res.statusCode = 500;
        log.error('Internal error(%d): %s', res.statusCode, err.message);
      } else {
        data.password = undefined;
        return res.send(data);
      }
    });
  } else {
    var f = {
      'account'    : req.params.aid,
      '$or'        : [{ deleted: { '$exists': false } }, { deleted: false }],
      'meta.public': { $not: { $exists: true } }
    };
    return req.db.users
      .find(f)
      .lean()
      .exec(function (err, users) {
        if (!err) {
          users.password = undefined;
          return res.send(users);
        }
        else {
          res.statusCode = 500;
          log.error('Internal error(%d): %s', res.statusCode, err.message);
          return res.send({ error: 'Server error' });
        }
      });
  }
};

// Get one user
/**
 * @api {get} /users/:aid/:uid Request user information
 * @apiName Get
 * @apiGroup Users
 *
 * @apiParam {String} aid Unique account id.
 * @apiParam {String} uid Unique user id.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} user User information.
 */
exports.get = function (req, res) {
  return req.db.users.find({ 'account': req.params.aid, '_id': req.params.uid }, function (err, user) {
    if (!user || !user[0]) {
      res.statusCode = 404;
      return res.send({ error: 'Not found' });
    }
    if (!err) {
      user[0].password = undefined;
      return res.send(user[0]);
    }
    else {
      res.statusCode = 500;
      log.error('Internal error(%d): %s', res.statusCode, err.message);
      return res.send({ error: 'Server error' });
    }
  });
};


// Get many users by IDs
/**
 * @api {get} /userList/ Request info for many users.
 * @apiName GetMany
 * @apiGroup Users
 *
 * @apiParam {String[]} users Array of user IDs.
 * @apiParam {Boolean} fetchAll True to fetch all users instead.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} users Requested users information.
 */
exports.getMany = function (req, res) {
  let users;
  try {
    users = JSON.parse(req.body.users);
  } catch (err) {
    res.statusCode = 500;
    log.error('Internal error(%d): %s', res.statusCode, err.message);
    return res.send({ error: 'Server error' });
  }

  let options = {
    '$or': [{ deleted: { '$exists': false } }, { deleted: false }]
  };
  if (!req.body.fetchAll) {
    options['_id'] = { '$in': users };
  }

  return req.db.users.find(options, function (err, users) {
    if (!users) {
      res.statusCode = 404;
      return res.send({ error: 'Not found' });
    }
    if (!err) {
      return res.send(_.map(users, (v) => _.omit(v.toObject(), [
        'password', 'privateKey', 'publicKey', 'deleted', 'deletedAt', 'createdAt', 'locked', 'updatedAt', '__v'
      ])));
    } else {
      res.statusCode = 500;
      log.error('Internal error(%d): %s', res.statusCode, err.message);
      return res.send({ error: 'Server error' });
    }
  });
};

// Get one user by meta key
/**
 * @api {get} /users/:aid/:mkey/:mval Request user information
 * @apiName Get
 * @apiGroup Users
 *
 * @apiParam {String} aid Unique account id.
 * @apiParam {String} uid Unique user id.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} user User information.
 */
exports.getByMeta = function (req, res) {
  return req.db.users.find({
    'account'                  : req.params.aid,
    ['meta.' + req.params.mkey]: req.params.mval
  }, function (err, user) {
    if (!user || !user[0]) {
      res.statusCode = 404;
      return res.send({ error: 'Not found' });
    }
    if (!err) {
      user[0].password = undefined;
      return res.send(user[0]);
    }
    else {
      res.statusCode = 500;
      log.error('Internal error(%d): %s', res.statusCode, err.message);
      return res.send({ error: 'Server error' });
    }
  });
};

// Get all users by meta key
/**
 * @api {get} /usersList/:aid/:mkey/:mval Request user information
 * @apiName Get
 * @apiGroup Users
 *
 * @apiParam {String} aid Unique account id.
 * @apiParam {String} uid Unique user id.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} user User information.
 */
exports.listByMeta = function (req, res) {
  return req.db.users.find({
    'account'                  : req.params.aid,
    ['meta.' + req.params.mkey]: req.params.mval
  }, function (err, user) {
    if (!user || !user.length) {
      res.statusCode = 404;
      return res.send({ error: 'Not found' });
    }
    if (!err) {
      return res.send(user.map(x => {
        x.password = undefined;
        return x;
      }));
    }
    else {
      res.statusCode = 500;
      log.error('Internal error(%d): %s', res.statusCode, err.message);
      return res.send({ error: 'Server error' });
    }
  });
};

// Update user
/**
 * @api {put} /users/:aid/:uid Edit existing user
 * @apiName Edit
 * @apiGroup Users
 *
 * @apiParam {String} aid Unique account id.
 * @apiParam {String} uid Unique user id.
 * @apiParam {String} [firstName] User's first name.
 * @apiParam {String} [lastName] User's last name.
 * @apiParam {String} [email] User's email.
 * @apiParam {String} language ISO 639-1 language code
 * @apiParam {String[]} [groups] Id's of groups you want to put the user in.
 * @apiParam {String="superadmin","admin","user"} [role] Role you want the user in.
 * @apiParam {String} [newPassword] New password.
 * (This will only be considered if user id match with tokens user id)
 * @apiParam {String} [confirmNewPassword] New password confirmation.
 * (This will only be considered if user id match with tokens user id)
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} user Updated user.
 */
exports.update = function (req, res) {
  var avatarBase64 = null;

  var update = function () {
    return req.db.users.findOne({ 'account': req.params.aid, '_id': req.params.uid }, function (err, user) {
      var passChanged = false;
      if (!user) {
        res.statusCode = 404;
        return res.send({ error: 'Not found' });
      }

      // Only allow updating of specific properties for public users
      if (user.meta.public) {
        if (req.body.meta) {
          if (!user.meta) {
            user.meta = {};
          }

          if (req.body.meta.orderCount) {
            user.meta.orderCount = req.body.meta.orderCount;
          }
        }
      } else {
        user.firstName = req.body.firstName ? req.body.firstName : user.firstName;
        user.lastName  = req.body.lastName ? req.body.lastName : user.lastName;
        user.email     = req.body.email ? req.body.email : user.email;
        user.language  = req.body.language ? req.body.language : user.language;

        user.privateKey = req.body.privateKey ? req.body.privateKey : user.privateKey;
        user.publicKey  = req.body.publicKey ? req.body.publicKey : user.publicKey;
        user.keyRing    = req.body.keyRing ? req.body.keyRing : user.keyRing;

        if (avatarBase64) {
          user.avatarBase64 = avatarBase64;
        }

        if (req.body.meta) { user.meta = req.body.meta; }

        if (req.body.groups && req.body.groups.length > 0) {
          user.groups = req.body.groups;
        }

        if (req.user.uid !== req.params.uid &&
          req.user.grp === 'superadmin' &&
          user.email.search('@mightyfields.com') === -1 && // TODO ???
          req.body.role &&
          req.body.role !== user.role) {
          user.role = req.body.role;
        }

        if (req.user.grp === 'superadmin' && req.body.active !== undefined) {
          user.active = req.body.active;
        }

        if ((req.user.uid === req.params.uid || req.user.grp === 'superadmin') && req.body.newPassword && req.body.confirmNewPassword) {
          if (req.user.grp !== 'superadmin' || req.query.passc === 'T') {
            if (!pw.verifyPassword(req.body.oldPass, user.password)) {
              res.statusCode = 400;
              res.send({ error: 'Wrong password.', errCode: 'wrongPass' });
              return;
            }
          }

          if (req.body.newPassword.length >= 5 && req.body.newPassword === req.body.confirmNewPassword) {
            user.password = pw.makePassword(req.body.newPassword);
            passChanged   = true;
          } else {
            res.statusCode = 400;
            res.send({ error: 'Bad password.', errCode: 'badPass' });
            return;
          }
        }
      }

      user.save(function (err) {
        if (!err) {
          log.info('user updated');

          if (passChanged) {
            if (!req.query || req.query.preventEmail === undefined) {
              // Send password reset email
              var config = {
                template: 'password-reset-confirm-mail',
                email   : user.email,
                subject : 'Your Soltec Portal password has been changed',
                data    : {
                  user: {
                    firstName: user.firstName,
                    lastName : user.lastName,
                    email    : user.email
                  },
                  link: c.get('branding:productWebsite')
                }
              };
              mail.send(config);
            }
          }
          user.password = undefined;
          return res.send({ status: 'OK', user: user });
        }
        else {
          if (err.name === 'ValidationError') {
            res.statusCode = 400;
            res.send({ error: 'Validation error' });
          } else if (err.code === 11000) {
            res.statusCode = 400;
            res.send({ error: 'User already exists', code: 11000 });
          } else {
            res.statusCode = 500;
            res.send({ error: 'Server error' });
          }
          log.error('Internal error(%d): %s', res.statusCode, err.message);
        }
      });
    });
  };

  if (req.body.avatarBase64) {
    var b64 = req.body.avatarBase64.split(',')[1];
    Jimp.read(Buffer.from(b64, 'base64'), function (err, img) {
      if (!err) {
        img.scaleToFit(256, 256)
          .getBase64(Jimp.MIME_PNG, function (err, imgBase64) {
            avatarBase64 = imgBase64;
            update();
          });
      } else {
        update();
      }
    });
  } else {
    update();
  }
};

function getUserByCode(db, uKey) {
  return new Promise((resolve, reject) => {
    db.users.findOne({ 'uniqueCode': uKey }, function (err, user) {
      if (err) {
        return reject(err);
      }
      resolve(user);
    });
  });
}

var genUniqueUserCode = async (db, firstName, lastName, options) => {
  options      = options || {};
  options.take = options.take || 1;

  let uKey = ((firstName.substring(0, options.take) || '') + (lastName.substring(0, options.take) || '')).toUpperCase();

  if (!uKey) {
    uKey = (uKey + Math.random().toString(36).substr(2, 5)).toUpperCase();
  }

  try {
    const u = await getUserByCode(db, uKey);
    if (u) {
      options.take++;
      return genUniqueUserCode(db, firstName, lastName, options);
    }
    return uKey;
  }
  catch (e) {
    throw e;
  }
};

// Add user
/**
 * @api {post} /users/:aid Add new user
 * @apiName Add
 * @apiGroup Users
 *
 * @apiParam {String} firstName User first name.
 * @apiParam {String} lastName User last name.
 * @apiParam {String} email User email.
 * @apiParam {String[]} [groups] User groups.
 * @apiParam {String="superadmin","admin","user"} [role="admin"] User role.
 * @apiParam {String} language ISO 639-1 language code.
 * @apiParam {String} password Plaintext password for the user.
 *
 * @apiSuccess {String} status Request status.
 * @apiSuccess {Object} user Created object.
 */
exports.add = function (req, res) {
  let password = req.body.password || generator(12, false);

  // Get account info first to check license details
  req.db.accounts.findById(req.params.aid, function (err, account) {
    if (account) {

      if (account.license.active >= account.license.users) {
        res.statusCode = 500;
        return res.send({ code: 9000, error: 'License exceeded. Please buy more licenses to add more users.' });
      }

      req.db.users.findOne({ 'email': req.body.email }, function (err, user) {
        //Check if user already exists
        if (user) {
          account.license.active = account.license.active + 1;
          if (account.license.active > account.license.users) {
            account.license.active = account.license.users;
            res.statusCode         = 500;
            return res.send({ code: 9000, error: 'License exceeded. Please buy more licenses to add more users.' });
          }

          if (user.deleted) {
            // 1. Restore user
            return user.restore(function (err) {
              if (!err) {
                account.save(function () {
                  log.info('user restored');
                  user.password = undefined;
                  return res.send({ status: 'OK', user: user, code: 2000 });
                });
              }
              else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s', res.statusCode, err.message);
                return res.send({ error: 'Server error' });
              }
            });
          } else {
            res.statusCode = 400;
            log.error('User already exists');
            return res.send({ error: 'User already exists', code: 11000 });
          }

        } else {
          // Fetch info about creating user.
          req.db.users.find({ 'account': req.params.aid, '_id': req.user.uid }, function (err, supervisor) {
            if (!supervisor || !supervisor[0]) {
              res.statusCode = 404;
              return res.send({ error: 'Not found' });
            }

            if (!err) {
              supervisor = supervisor[0];

              var keys = sjcl.ecc.elGamal.generateKeys(256);
              var pub  = keys.pub.get(), sec = keys.sec.get();

              // Serialized public key:
              pub = sjcl.codec.base64.fromBits(pub.x.concat(pub.y));

              // Serialized private key:
              sec = sjcl.codec.base64.fromBits(sec);

              genUniqueUserCode(req.db, req.body.firstName, req.body.lastName)
                .then(uniqueCode => {
                  // 1. Create user
                  user = new req.db.users({
                    account   : req.params.aid,
                    firstName : req.body.firstName,
                    lastName  : req.body.lastName,
                    uniqueCode: uniqueCode,
                    email     : req.body.email,
                    language  : req.body.language ? req.body.language : 'en',
                    active    : true,
                    locked    : false,
                    privateKey: sjcl.encrypt(password, sec),
                    publicKey : pub,
                    meta      : _.extend(req.body.meta || {}, {
                      cases  : 0,
                      devices: 0
                    }),
                    password  : pw.makePassword(password)
                  });

                  if (req.body.groups && req.body.groups.length > 0) {
                    user.groups = req.body.groups;
                  }

                  if (req.user.grp === 'superadmin' &&
                    req.body.role &&
                    req.body.role !== user.role) {
                    user.role = req.body.role;
                  } else if (req.user.grp === 'admin') {
                    user.role = 'user';
                  }

                  user.save(function (err) {
                    if (!err) {
                      log.info('user created');

                      // 3. Send emails
                      var config = {
                        template: 'welcome-mail-user',
                        email   : user.email,
                        subject : 'Welcome to SoltecPortal',
                        data    : {
                          account   : {
                            company: account.company
                          },
                          password  : password,
                          supervisor: {
                            firstName: supervisor.firstName,
                            lastName : supervisor.lastName
                          },
                          user      : {
                            firstName: user.firstName,
                            lastName : user.lastName,
                            email    : user.email
                          },
                          link      : c.get('branding:productWebsite')
                        }
                      };
                      if (!req.query || req.query.preventEmail === undefined) {
                        mail.send(config);
                      }

                      // Update license
                      account.license.active = account.license.active + 1;
                      account.save(function (err) {
                        user.password = undefined;
                        return res.send({ status: 'OK', user: user });
                      });
                    }
                    else {
                      if (err.name === 'ValidationError') {
                        res.statusCode = 400;
                        res.send({ error: 'Validation error', e: err });
                      }
                      else {
                        res.statusCode = 500;
                        if (err.code === 11000) {
                          res.send({ code: err.code, error: 'User with this email address already exists.' });
                        }
                        else { res.send({ error: 'Server error' }); }
                      }
                      log.error('Internal error(%d): %s', res.statusCode, err.message);
                    }
                  });
                })
                .catch(err => {
                  res.statusCode = 500;
                  log.error('Internal error(%d): %s', res.statusCode, err.message);
                  return res.send({ error: 'Server error' });
                });
            } else {
              res.statusCode = 500;
              log.error('Internal error(%d): %s', res.statusCode, err.message);
              return res.send({ error: 'Server error' });
            }
          });
        }
      });
    }
    else {
      res.statusCode = 500;
      log.error('Internal error(%d): %s', res.statusCode, err.message);
      return res.send({ error: 'Server error' });
    }
  });
};


/**
 * @api {post} users/:aid/:uid/invite Send invitation mail to a user with a link to change their password
 * @apiName Invite
 * @apiGroup Users
 * @apiDescription Sends an invitation mail to the user with the given email address.
 *
 * @apiParam {String} email Target user's email address.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {String} message Response description.
 */
exports.invite = function (req, res) {
  if (req.user.grp === 'admin' || req.user.grp === 'superadmin' || req.user.per[permissions.permissionKeys.admin] === 0b111) {
    var promise = auth.generateResetToken(req);
    promise.then(function (user) {
      var config = {
        template: 'invitation-mail',
        email   : user.email,
        subject : 'You are invited to join ' + c.get('branding:productName'),
        data    : {
          user   : {
            firstName    : user.firstName,
            lastName     : user.lastName,
            email        : user.email,
            resetPwdToken: user.resetPwdToken
          },
          product: c.get('branding:productName'),
          link   : c.get('branding:productWebsite')
        }
      };

      mail.send(config);

      res.send({ status: 'OK', message: 'Invitation mail sent.' });
    })
      .catch(function (response) {
        res.statusCode = response.code;
        res.send({ error: response.err });
      });
  }
};


// Delete user
/**
 * @api {delete} /users/:aid/:uid Delete existing user
 * @apiName Delete
 * @apiGroup Users
 *
 * @apiParam {String} aid Unique account id.
 * @apiParam {String} uid Unique user id.
 *
 * @apiSuccess {String} status Response status.
 */
exports.delete = function (req, res) {
  return req.db.users.findOne({
    'account'    : req.params.aid,
    '_id'        : req.params.uid,
    'meta.public': { $not: { $exists: true } }
  }, function (err, user) {
    if (req.user.uid === req.params.uid) {
      res.statusCode = 500;
      return res.send({ code: 9001, error: 'Can not delete this user.' });
    }
    if (!user) {
      res.statusCode = 404;
      return res.send({ error: 'Not found' });
    }
    if (user.email.substr('@mightyfields.com') > -1) {
      res.statusCode = 500;
      return res.send({ code: 9001, error: 'Can not delete this user.' });
    }

    req.db.accounts.findById(req.params.aid, function (err, account) {
      // 1. Remove user
      return user.softdelete(function (err) {
        if (!err) {
          account.license.active = account.license.active - 1;
          account.save(function () {
            log.info('user removed');
            return res.send({ status: 'OK' });
          });
        }
        else {
          res.statusCode = 500;
          log.error('Internal error(%d): %s', res.statusCode, err.message);
          return res.send({ error: 'Server error' });
        }
      });

    });
  });
};

// Lock/unlock user
/**
 * @api {put} /users/lock/:aid/:uid Lock/unlock current user
 * @apiName Lock
 * @apiGroup Users
 *
 * @apiParam {String} aid Unique account id.
 * @apiParam {String} uid Unique user id.
 *
 * @apiSuccess {String} status Request status.
 */
exports.lock = function (req, res) {
  var f = {
    'account'    : req.params.aid,
    '_id'        : req.params.uid,
    'meta.public': { $not: { $exists: true } }
  };
  return req.db.users.findOne(f, function (err, user) {
    if (req.user.uid === req.params.uid) {
      res.statusCode = 500;
      return res.send({ code: 9001, error: 'Can not lock this user.' });
    }
    if (!user) {
      res.statusCode = 404;
      return res.send({ error: 'Not found' });
    }

    req.db.accounts.findById(req.params.aid, function (err, account) {
      if (user.email.substr('@mightyfields.com') === -1) {
        if (user.active) {
          if (account.license.active === 1) {
            res.statusCode = 500;
            return res.send({ code: 9001, error: 'Can not lock this user.' });
          }
          account.license.active = account.license.active - 1;
        } else {
          account.license.active = account.license.active + 1;
        }
        if (account.license.active > account.license.users) {
          account.license.active = account.license.users;
          res.statusCode         = 500;
          return res.send({ code: 9000, error: 'License exceeded. Please buy more licenses to add more users.' });
        }
      }

      // lock/unlock user
      user.active = !user.active;
      log.info(user.active);

      return user.save(function (err) {
        if (!err) {
          account.save(function () {
            log.info('user lock');
            user.password = undefined;
            return res.send({ status: 'OK', user: user });
          });
        }
        else {
          res.statusCode = 500;
          log.error('Internal error(%d): %s', res.statusCode, err.message);
          return res.send({ error: 'Server error' });
        }
      });

    });
  });
};

/**
 * @api {put} /users/meta/:aid/:uid Update user's meta
 * @apiName UpdateMeta
 * @apiGroup Users
 *
 * @apiParam {String} id Id of user for whom you want to update meta.
 * @apiParam {String} metaKey Meta key.
 * @apiParam {String} metaValue Meta value.
 * @apiParam {String} delete If we are in delete mode.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} user Updated user.
 */
exports.meta = function (req, res) {
  return req.db.users.findOne({ _id: req.body.id, 'meta.public': { $not: { $exists: true } } }, function (err, user) {
    if (!user) {
      res.statusCode = 404;
      return res.send({ error: 'Not found' });
    }

    if (req.body.delete) {
      if (user.meta.hasOwnProperty(req.body.metaKey)) {
        delete user.meta[req.body.metaKey];
      }
    }
    else {
      user.meta[req.body.metaKey] = req.body.metaValue;
    }

    user.save(function (err) {
      if (!err) {
        user.password = undefined;
        return res.send({ status: 'OK', user: user });
      }
    });
  });
};
