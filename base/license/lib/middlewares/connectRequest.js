var jwt          = require('jwt-simple'),
    mongoose     = require('mongoose'),
    softDelete   = require('mongoose-softdelete'),
    DataTable    = require('mongoose-datatable'),
    glob         = require('glob'),
    log          = require('../log')(module),
    c            = require('../configuration'),
    Raven        = require('../../shared/store/raven').Raven,
    dbTools      = require('../../shared/tools/db'),
    validateUser = require('../controllers/auth').validateUser,
    path         = require('path');

"use strict";

mongoose.set('debug', true);

module.exports = function(req, res, next) {
  if (req.method === 'OPTIONS') { next(); }
  setConnection(req, res, next);
};


// Create a dynamic DB connection and save it to global variable
function setConnection(req, res, next) {
  if (global.App.db === undefined) {
    dbTools.getConnection(c.get('mongo:license'))
      .then(db => {
        log.info('Connection to DB ');
        global.App.db = db;
        mongoose.connection = global.App.db;
        modelsInit(req, res, next, db);
      })
      .catch(e => {
        console.error(e);
        Raven.captureException(e);
        res.status(503);
        res.send({ message: 'Service Unavailable' });
      });
  } else {
    mongoose.connection = global.App.db;
    modelsInit(req, res, next, global.App.db);
  }
}


// Init models if not already
function modelsInit(req, res, next, db) {
  log.debug('Check if models for this user are already compiled');

  if (global.App.license === undefined) {

    dbTools.modelsInit(db)
      .then(resDB => {
        global.App.license = resDB;
        req.db = global.App.license;

        next();
      })
      .catch(e => {
        console.error(e);
        Raven.captureException(e);
        res.status(503);
        res.send({ message: 'Service Unavailable' });
      });

  } else {
    log.debug('Already compiled. Just setting the active connection.');
    req.db = global.App.license;

    next();
  }
}