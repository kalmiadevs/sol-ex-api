var jwt          = require('jwt-simple'),
    mongoose     = require('mongoose'),
    softDelete   = require('mongoose-softdelete'),
    DataTable    = require('mongoose-datatable'),
    glob         = require('glob'),
    log          = require('../log')(module),
    c            = require('../configuration'),
    Raven        = require('../../shared/store/raven').Raven,
    dbTools      = require('../../shared/tools/db'),
    validateUser = require('../controllers/auth').validateUser,
    path         = require('path');

"use strict";

mongoose.set('debug', true);

module.exports = function(req, res, next) {
  if (req.method === 'OPTIONS') { next(); }
  var apiCall = req.url.split(c.get('apiBase'))[1];
  if (['login', 'login-long', 'logout', 'reset', 'set', 'db', 'test'].indexOf(apiCall) > -1) {
    setConnection(req, res, next);
  } else {
    // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
    var token = (req.body && req.body.access_token) ||
                 (req.query && req.query.access_token) ||
                 req.headers['x-access-token'];
    // jscs:enable requireCamelCaseOrUpperCaseIdentifiers

    if (token) {
      try {
        var decoded = jwt.decode(token, c.get('secret'));

        if (decoded.exp <= Date.now()) {
          res.status(401);
          res.json({
            'status'  : 401,
            'message' : 'Token Expired'
          });
          return;
        }

        req.token = token;
        req.user  = decoded;

        // Check if potential :aid parameter is equal to the account id saved in token
        if (req.route.params.aid && req.route.params.aid !== req.user.acc) {
          res.status(401);
          res.json({
            'status'  : 401,
            'message' : 'Unauthorized'
          });
          return;
        }

        // Check if potential :uid parameter is equal to user id saved in token,
        // or user has admin/superadmin role.
        if (req.route.params.uid && req.route.params.uid !== req.user.uid) {
          if (req.user.grp !== 'superadmin' && req.user.grp !== 'admin') {
            res.status(401);
            res.json({
              'status'  : 401,
              'message' : 'Unauthorized'
            });
            return;
          }
        }

        // Create database connection
        setConnection(req, res, next);

        //next();
      }
      catch (err) {
        res.status(500);
        res.json({
          'status': 500,
          'message': 'Oops something went wrong',
          'error': err
        });
      }
    }
    else {
      res.status(401);
      res.json({
        'status': 401,
        'message': 'Invalid Token or Key'
      });
      return;
    }
  }
};

// Create a dynamic DB connection and save it to global variable
function setConnection(req, res, next) {
  if (global.App.db === undefined) {
    dbTools.getConnection(c.get('mongo:license'))
      .then(db => {
        log.info('Connection to DB ');
        global.App.db = db;
        mongoose.connection = global.App.db;
        modelsInit(req, res, next, db);
      })
      .catch(e => {
        console.error(e);
        Raven.captureException(e);
        res.status(503);
        res.send({ message: 'Service Unavailable' });
      });
  } else {
    mongoose.connection = global.App.db;
    modelsInit(req, res, next, global.App.db);
  }
}

// Init models if not already
function modelsInit(req, res, next, db) {
  log.debug('Check if models for this user are already compiled');

  if (global.App.license === undefined) {

    dbTools.modelsInit(db)
      .then(resDB => {
        global.App.license = resDB;
        req.db = global.App.license;

        setPermissions(req, res, next);
      })
      .catch(e => {
        console.error(e);
        Raven.captureException(e);
        res.status(503);
        res.send({ message: 'Service Unavailable' });
      });

  } else {
    log.debug('Already compiled. Just setting the active connection.');
    req.db = global.App.license;

    setPermissions(req, res, next);
  }
}

// Set permissions
function setPermissions(req, res, next) {

  if (req.user) {
    // TODO check for 429 token version change
  }

  next();
}
