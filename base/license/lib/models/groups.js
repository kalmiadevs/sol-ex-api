module.exports = function (mongoose,  dbconn, plugins) {

  var schema = new mongoose.Schema({
    name:       {type: String, required: true, trim: true},
    // permissions: {type: mongoose.Schema.Types.ObjectId, ref: 'Permissions'},
    /**
     * Example:
     * kp_node: 110
     * kp_dash: 111
     */
    permissions: {type: mongoose.Schema.Types.Mixed, required: true},
    users:   	 [{type: mongoose.Schema.Types.ObjectId, ref: 'Users'}],
    nodes:   	 [{type: mongoose.Schema.Types.ObjectId, ref: 'Nodes'}],
    owner:     {type: mongoose.Schema.Types.ObjectId, ref: 'Users', required: true},
    publicKey:    {type: String},
    keyRing:      {},
    meta:      {}
  }, {
    timestamps: true
  });

  // Apply plugins
  for (var i = 0; i < plugins.length; i++) {
    schema.plugin(plugins[i]);
  }

  // Middleware
  schema.pre('remove', function(next) {
    this.model('Permissions').remove({ _id: this._id }, next);
  });

  return dbconn.model('Groups', schema, 'groups');
};
