var userRole = ['user', 'admin', 'superadmin'];

function toTitleCase(str) {
  return str.replace(/š*đ*č*ć*ž*Š*Đ*Č*Ć*Ž*\w\S*/g, function(txt) {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
}

module.exports = function (mongoose,  dbconn, plugins) {

  var schema = new mongoose.Schema({
    account:         {type: mongoose.Schema.Types.ObjectId, ref: 'Accounts'},
    firstName:       {type: String, required: true, trim: true},
    lastName:        {type: String, required: true, trim: true},
    email:           {type: String, unique: true, required: true, lowercase: true, trim: true},
    password:        {type: String},
    uniqueCode:      {type: String}, // For example Janez Novak would be JN.
    position:        {type: String},
    language:        {type: String, default: 'en'},
    resetPwdToken:   {type: String},
    avatarBase64:    {type: String},
    resetPwdExpires: {type: Date},
    publicKey:       {type: String},
    privateKey:      {type: String},
    keyRing:          {},
    active:          {type: Boolean, default: false},
    locked:          {type: Boolean, default: false},
    groups:          [{type: mongoose.Schema.Types.ObjectId, ref: 'Groups'}],
    meta:            {},
    role:            {type: String, required: true, enum: userRole, default: 'admin'},
    lastLoginAt:     {type: Date}
  }, {
    timestamps: true
  });

  schema.pre('save', function(next) {
    this.firstName = toTitleCase(this.firstName);
    this.lastName = toTitleCase(this.lastName);
    next();
  });

  // Apply plugins
  for (var i = 0; i < plugins.length; i++) {
    schema.plugin(plugins[i]);
  }

  return dbconn.model('Users', schema, 'users');
};
