var mongoose  = require('mongoose');
var c         = require('../lib/configuration');
var getSlug   = require('speakingurl');
var prompt    = require('prompt');
var generator = require('password-generator');
var pw        = require('secure-password');
var q         = require('q');
var _         = require('underscore');
var sjcl      = require('../node_modules/sjcl');
var mail      = require('../lib/controllers/mailer');

var autoObj = {
  'company': 'Kalmia LTD',
  'address': '',
  'city'   : '',
  'post'   : '',
  'country': 'unknown',
  'vatId'  : ''
};
var auto = false;
var kalmiaUserPassword = generator(12, false);
var kalmiaUserEmail    = undefined;

process.argv.slice(2).forEach(x => {
  if (x === '--auto') {
    auto = true;
  }
  if (x.startsWith('--email=')) {
    kalmiaUserEmail = x.slice('--email='.length);
  }
  if (x.startsWith('--pass=')) {
    kalmiaUserPassword = x.slice('--pass='.length);
  }
  if (x.startsWith('--company=')) {
    autoObj.company = x.slice('--company='.length);
  }
});
// return console.log({auto, kalmiaUserPassword, kalmiaUserEmail});

// Print instructions
if (!auto) {
  console.log('This script will create customer and one supperadmin support user. Please follow instructions.');
} else {
  console.log('This script will create customer and one supperadmin support user. No need for interaction.');
}

// Open db connection
var db = mongoose.createConnection(c.get('mongo:license') + '?authSource=admin');
db.once('open', function () {
  // Load models
  db.accounts = require('../lib/models/accounts')(mongoose, db, []);
  db.users    = require('../lib/models/users')(mongoose, db, []);

  var gen = function (err, accountData) {
    var opt = {
      separator: '_',
      symbols: false
    };

    var userData = {};
    userData.email = kalmiaUserEmail || 'support+' + getSlug(accountData.company, opt) + '@kalblocks.com';

    // Create account
    accountData.email = userData.email;
    createAccount(accountData).then(function(accountResult) {

      // Customer created
      console.log('Result', accountResult);
      userData.accountId = accountResult.account._id;

      // Create support user
      createKalmiaUser(userData, accountResult.account.company)
        .then(function (userResult) {
          // Support user created
          console.log(userResult);

          // Send email
          /*
          sendWelcomeEmail(userResult.user,
                           userResult.password,
                           accountResult.account
          )
          .then(function() {
            console.log('Support welcome email sent');
            process.exit();
          })
          .fail(function() {
            console.log('There was a problem sending welcome email');
            process.exit();
          });
          */
          process.exit();
        })
        .fail (function (err) {
          console.log(err);
          process.exit();
        });
    }).fail(function(accountError) {
      // Error creating customer
      console.log(accountError.msg);
      console.log(accountError.error);
      process.exit();
    });
  };

  if (!auto) {
    // Gather data about customer
    console.log('Provide company data:');
    prompt.start();
    prompt.get(['company', 'address', 'city', 'post', 'country', 'vatId'], gen);
  } else {
    gen(null, autoObj);
  }
});

function sendWelcomeEmail(user, password, account) {
  console.log(user.email);
  // 4. Send welcome email
  var config = {
    template    : 'welcome-mail',
    email       : user.email,
    subject     : 'Welcome to SoltecPortal',
    data        : {
      user      : user,
      account   : account,
      password  : password
    }
  };
  return mail.send(config);
}

function createKalmiaUser(data, company) {
  var deferred = q.defer();
  var password = kalmiaUserPassword;

  console.log('====================================');
  console.log('Support user username: ' + data.email);
  console.log('Support user password: ' + password);
  console.log('====================================');

  console.log('Creating superuser for support');
  var keys = sjcl.ecc.elGamal.generateKeys(256, 6);
  var pub = keys.pub.get(), sec = keys.sec.get();

  // Serialized public key:
  pub = sjcl.codec.base64.fromBits(pub.x.concat(pub.y));

  // Serialized private key:
  sec = sjcl.codec.base64.fromBits(sec);

  try {

    var user = db.users({
      account       : data.accountId,
      firstName     : 'KalBlocks',
      lastName      : 'Support',
      name          : 'KalBlocks Support',
      email         : data.email,
      password      : pw.makePassword(password),
      language      : data.language || 'en_GB',
      privateKey    : sjcl.encrypt(password, sec),
      publicKey     : pub,
      active        : true,
      uniqueCode    : "AA",
      locked        : false,
      role          : 'superadmin',
      accessTokens  : [ ],
      groups        : [ ],
      meta          : {}
    });

  } 
  catch(e){
    console.log(e);
  }

  user.save(function (err) {
    var result = {};
    result.user = user;
    result.password = password;
    if (!err) {
      result.msg = 'Support user successfully created';
      deferred.resolve(result);
    } else {
      result.msg = 'Error creating user';
      result.error = err;
      deferred.reject(result);
    }
  });
  return deferred.promise;
}

function createAccount(data) {
  var deferred = q.defer();
  var account = new db.accounts({
    company   : data.company,
    address   : data.address,
    city      : data.city,
    post      : data.post,
    country   : data.country,
    vadId     : data.vatId,
    payment   : 'invoice',
    license   : {
      users         : 999999,
      active        : 0,
      admins        : 2,
      monthlyPrice  : 100,
      apps          : 'kalpass'
    },
    phone               : '',
    email               : data.email,
    database            : generateDbName(data.company),
    language            : 'en_GB',
    dateFormat          : 'dd.mm.YYYY',
    active              : true,
    locked              : false
  });
  account.save(function (err) {
    var result = {};
    result.account = account;
    if (!err) {
      result.msg = 'Account successfully created';
      deferred.resolve(result);
    } 
    else {
      result.error = err;
      result.msg = 'Error creating account';
      deferred.reject(result);
    }
  });
  return deferred.promise;
}

function generateDbName(company) {
  var opt = {
    separator: '_',
    symbols: false
  };
  return 'kal_customer_' + getSlug(company, opt);
}