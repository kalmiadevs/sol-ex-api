# Abstract api's

Here are abstract classes that you should use in your development in order to reduce code repetition and keep consistent with coding style guides. If you don't know what style guides to follow ask Tilen Tomakić to coffe ;).

Some guides:
 - function starting with $ is intend for direct manipulation, for example with DB, FS, ...
   It's never called directly by express router!
 - function order inside class tries to follow CRUD keyword.
   So reader can expect first create functions, then read, get, getAll, list functions, and so on.
 - Extra functionality shall be possible via hook_ ideology.