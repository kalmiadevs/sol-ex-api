'use strict';

const nodemailer = require('nodemailer'),
  Joi = require('joi'),
  EmailTemplate = require('email-templates').EmailTemplate,
  htmlToText = require('html-to-text'),
  q = require('q'),
  c = require('../../lib/configuration'),
  FsLogApi = require('../../shared/abstract/fs-log-api'),
  queue = require("async-delay-queue");


const SEND_DELAY_MS = 500;

const mailOptionsSchema = Joi.object().keys({
  from: Joi.string().email().required(),
  to: Joi.string().email().required(),
  cc: Joi.string().allow('').optional(),
  subject: Joi.string().required(),

  content: Joi.string().invalid(''),

  template: Joi.string(),
  data: Joi.object(),

  attachments: Joi.array(),
}).with('template', 'data');

/**
 * Mail sending service.
 */
module.exports = class MailService {


  /**
   * Constructor. Prepares a mail transport object.
   *
   * @param transportOptions Mail transport options.
   */
  constructor(transportOptions = c.get('mailService')) {
    this.transport = nodemailer.createTransport(transportOptions);
    if (!this.transport) {
      throw new Error('Failed to create mail transport.');
    }
  };

  /**
   * Send email message.
   *
   * @param options
   * @param options.from Email address we're sending from.
   * @param options.to Comma separated list or an array of recipients email addresses that will appear on the To: field
   * @param options.cc Comma separated list or an array of recipients email addresses that will appear on the Cc: field
   * @param options.subject Subject string.
   * @param options.content Content of the email message. Only used if no template is provided.
   * @param options.template Path to the template to use.
   * @param options.data Template data. Required if using a template.
   * @param options.attachments Attachment data. See https://nodemailer.com/message/attachments/ for more details.
   */
  async sendMail(options) {
    return await queue.delay(() => this.$sendMail(options), SEND_DELAY_MS);
  }

  $sendMail(options) {
    const fsLog = new FsLogApi([], {name: 'mailService.log'});

    let deferred = q.defer();

    const validationResult = Joi.validate(options, mailOptionsSchema);
    if (validationResult.error) {
      deferred.reject({errorCode: 400, errorMessage: 'Schema validation failed (' + validationResult.error + ')'});
      return deferred.promise;
    }

    let self = this;

    function send(content) {
      let mailOptions = {
        from: c.get('appName') + ' <' + options.from + '>',
        to: options.to,
        cc: options.cc,
        subject: options.subject,
        text: htmlToText.fromString(content, {}),
        html: content,
        attachments: options.attachments
      };

      self.transport.sendMail(mailOptions, function (error, info) {
        if (error) {
          fsLog.alertError('Email error: ' + error + ' | Data: ' + JSON.stringify(mailOptions));
          deferred.reject({errorCode: 500, errorMessage: error});
          console.log(error);
          queue.delay(() => self.$sendMail(options), SEND_DELAY_MS);
        } else {
          fsLog.alertOk('Email sent | Data: ' + JSON.stringify(mailOptions));
          deferred.resolve({message: info.response});
          console.log('Email sent: ' + info.response);
        }
      });
    }


    if (options.template) {
      let tpl = new EmailTemplate(options.template);

      tpl.render(options.data, function (err, results) {
        if (results === undefined) {
          deferred.reject({errorCode: 500, errorMessage: 'Template not found'});
        } else {
          send(results.html);
        }
      });
    } else {
      send(options.content);
    }

    return deferred.promise;
  }
};
