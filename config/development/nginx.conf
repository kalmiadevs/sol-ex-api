# Redirect to HTTPS
server {
	listen 80 default_server;
	listen [::]:80 default_server;
	server_name _;
	return 301 https://$host$request_uri;
}

# Soltec API Config
server {
    listen	443 ssl;
    server_name	sol-api.kalmia.si;
    
    ssl_certificate "/etc/letsencrypt/live/sol-api.kalmia.si/fullchain.pem";
    ssl_certificate_key "/etc/letsencrypt/live/sol-api.kalmia.si/privkey.pem";
    ssl_session_cache shared:SSL:1m;
    ssl_session_timeout  10m;
    ssl_ciphers HIGH:SEED:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!RSAPSK:!aDH:!aECDH:!EDH-DSS-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA:!SRP;
    ssl_prefer_server_ciphers on;
    ssl_stapling on;
    ssl_stapling_verify on;
    ssl_protocols SSLv3 TLSv1.2 TLSv1.1 SSLv2;
    
    location / {
  		proxy_pass http://127.0.0.1:5001;
  		proxy_redirect off;
  		proxy_set_header X-Real-IP $remote_addr;
  		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
  		proxy_set_header X-Forwarded-Proto $scheme;
  		proxy_set_header Host $host;
  		proxy_set_header X-NginX-Proxy true;
  		proxy_set_header Connection "";
  		proxy_http_version 1.1;
    }
}

# Soltec IDMS Config
server {
    listen	443 ssl;
    server_name	sol-idms.kalmia.si;
    
    ssl_certificate "/etc/letsencrypt/live/sol-api.kalmia.si/fullchain.pem";
    ssl_certificate_key "/etc/letsencrypt/live/sol-api.kalmia.si/privkey.pem";
    ssl_session_cache shared:SSL:1m;
    ssl_session_timeout  10m;
    ssl_ciphers HIGH:SEED:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!RSAPSK:!aDH:!aECDH:!EDH-DSS-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA:!SRP;
    ssl_prefer_server_ciphers on;
    ssl_stapling on;
    ssl_stapling_verify on;
    ssl_protocols SSLv3 TLSv1.2 TLSv1.1 SSLv2;
    
    location / {
  		proxy_pass http://127.0.0.1:5000;
  		proxy_redirect off;
  		proxy_set_header X-Real-IP $remote_addr;
  		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
  		proxy_set_header X-Forwarded-Proto $scheme;
  		proxy_set_header Host $host;
  		proxy_set_header X-NginX-Proxy true;
  		proxy_set_header Connection "";
  		proxy_http_version 1.1;
    }
}

# Soltec Portal API Config
server {
    listen      443 ssl;
    server_name portal-api.kalmia.si;

    ssl_certificate "/etc/letsencrypt/live/sol-api.kalmia.si/fullchain.pem";
    ssl_certificate_key "/etc/letsencrypt/live/sol-api.kalmia.si/privkey.pem";
    ssl_session_cache shared:SSL:1m;
    ssl_session_timeout  10m;
    ssl_ciphers HIGH:SEED:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!RSAPSK:!aDH:!aECDH:!EDH-DSS-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA:!SRP;
    ssl_prefer_server_ciphers on;
    ssl_stapling on;
    ssl_stapling_verify on;
    ssl_protocols SSLv3 TLSv1.2 TLSv1.1 SSLv2;

    location / {
        proxy_pass http://127.0.0.1:5002;
        proxy_redirect off;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header Host $host;
        proxy_set_header X-NginX-Proxy true;
        proxy_set_header Connection "";
        proxy_http_version 1.1;
    }
}

# Soltec Portal Config
server {
    listen      443 ssl;
    server_name soltec.kalmia.si;

    ssl_certificate "/etc/letsencrypt/live/sol-api.kalmia.si/fullchain.pem";
    ssl_certificate_key "/etc/letsencrypt/live/sol-api.kalmia.si/privkey.pem";
    ssl_session_cache shared:SSL:1m;
    ssl_session_timeout  10m;
    ssl_ciphers HIGH:SEED:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!RSAPSK:!aDH:!aECDH:!EDH-DSS-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA:!SRP;
    ssl_prefer_server_ciphers on;
    ssl_stapling on;
    ssl_stapling_verify on;
    ssl_protocols SSLv3 TLSv1.2 TLSv1.1 SSLv2;

    location / {
        proxy_pass http://127.0.0.1:5020;
        proxy_redirect off;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header Host $host;
        proxy_set_header X-NginX-Proxy true;
        proxy_set_header Connection "";
        proxy_http_version 1.1;
    }
}
