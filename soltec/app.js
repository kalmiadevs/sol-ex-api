const debug      = require('debug')('app'),
      server     = require('./lib/server'),
      log        = require('./shared/log')(module),
      c          = require('./lib/configuration'),
      ravenStore = require('./shared/store/raven'),
      FsLogApi   = require('./shared/abstract/fs-log-api'),
      updatedb   = require('./lib/middlewares/updateLastDBUpdate');

ravenStore.config(c.get('raven'), {
  environment: c.get('production') ? 'production' : 'development',
  tags       : {
    api: 'soltec'
  }
});

new FsLogApi([], { name: 'app.log' }).info(`started`);

// Set global object for keeping client connections
global.App = {
  licenseDbConn: undefined,
  licenseModels: undefined,
  activeUser   : {},
  activeDb     : {},
  clientDbConn : {},
  clientModels : {}
};

const orders     = require('./lib/controllers/order').orderApi,
      sync       = require('./lib/controllers/order').syncApi,
      excel      = require('./lib/controllers/order').excelApi,
      docx       = require('./lib/controllers/order').docxApi,
      articles   = require('./lib/controllers/article').articleApi,
      pub        = require('./lib/controllers/public').publicApi,
      mail       = require('./lib/controllers/mail'),
      media      = require('./lib/controllers/media'),
      admin      = require('./lib/controllers/admin').adminApi,
      logs       = require('./lib/controllers/log'),
      test       = require('./lib/controllers/test'),
      validators = require('./lib/middlewares/validateRequest'),
      modifiers  = require('./shared/middlewares/requestModifiers')(log);

// START SYNC CRON
if (c.get('enableCron')) {
  orders.syncApi.syncAllCron.start();
}


// All set, start listening!
server.listen(c.get('port'));

log.debug('== ===========================================');
log.debug('== Soltec REST API successfully started');
log.debug('== ===========================================');
log.debug('== PORT: %d', c.get('port'));
log.debug('== MODE: %s', process.env.NODE_ENV || 'dev');
log.debug('== ===========================================');

server.get(c.get('apiBase') + 'version', function (req, res) {
  res.send({
    version: c.get('version')
  });
});

// All routes require auth that can be accessed only by autheticated users
server.all(c.get('apiBase') + '*', [validators.validateRequest, modifiers.addRequestWrapper]);


/* =======================================================
 * == ORDERS
 * ======================================================= */
// Nodes routes that can be accessed only by authenticated & authorized users
server.get(c.get('apiBase') + 'orders/listSalesManagers', orders.listSalesManagers.bind(orders));
server.get(c.get('apiBase') + 'orders/listDistributors', orders.listDistributors.bind(orders));
server.get(c.get('apiBase') + 'orders/exportSearch', orders.exportSearch.bind(orders));

server.get(c.get('apiBase') + 'orders', orders.list.bind(orders));
server.get(c.get('apiBase') + 'orders/:id', orders.get.bind(orders));
server.put(c.get('apiBase') + 'orders/:id/confirm', orders.confirm.bind(orders));
server.put(c.get('apiBase') + 'orders/:id/cancel', orders.cancel.bind(orders));
server.put(c.get('apiBase') + 'orders/:id/transfer', orders.transfer.bind(orders));
server.put(c.get('apiBase') + 'orders/:id', orders.update.bind(orders));
server.post(c.get('apiBase') + 'orders', orders.create.bind(orders));
// server.get(c.get('apiBase') + 'stats/orders', orders.aggregate.bind(orders));
server.delete(c.get('apiBase') + 'orders/:id', orders.remove.bind(orders));

server.get(c.get('apiBase') + 'orders/:id/documents', orders.getDoc.bind(orders));
server.get(c.get('apiBase') + 'orders/:id/spDocuments', orders.getSpFile.bind(orders));
server.post(c.get('apiBase') + 'orders/:id/documents', orders.uploadDoc.bind(orders));
server.delete(c.get('apiBase') + 'orders/:id/documents', orders.deleteDoc.bind(orders));

server.get(c.get('apiBase') + 'orders/suggestions/clients', orders.suggestClients.bind(orders));
server.get(c.get('apiBase') + 'orders/suggestions/distributors', orders.suggestDistributors.bind(orders));
server.get(c.get('apiBase') + 'orders/suggestions/salesManagers', orders.suggestSalesManagers.bind(orders));

/*=======================================================
 * == NEWS
 * ======================================================= */
// Nodes routes that can be accessed only by authenticated & authorized users
server.get(c.get('apiBase') + 'articles', articles.list.bind(articles));
server.get(c.get('apiBase') + 'articles/:id', articles.get.bind(articles));
server.put(c.get('apiBase') + 'articles/:id', articles.update.bind(articles));
server.post(c.get('apiBase') + 'articles', articles.create.bind(articles));
server.delete(c.get('apiBase') + 'articles/:id', articles.remove.bind(articles));


/* =======================================================
 * == MAIL
 * ======================================================= */
// Nodes routes that can be accessed only by authenticated & authorized users
server.post(c.get('apiBase') + 'mail', mail.send.bind(mail));


/* =======================================================
 * == MEDIA
 * ======================================================= */
// Nodes routes that can be accessed only by authenticated & authorized users
server.get(c.get('apiBase') + 'media/list', media.list.bind(media));
server.get(c.get('apiBase') + 'media/search', media.search.bind(media));
server.get(c.get('apiBase') + 'media', media.get.bind(media));
server.post(c.get('apiBase') + 'media', media.upload.bind(media));
server.delete(c.get('apiBase') + 'media', media.remove.bind(media));

/* =======================================================
 * == LOGS
 * ======================================================= */
// Nodes routes that can be accessed only by authenticated & authorized users
server.get(c.get('apiBase') + 'logs/list', logs.list.bind(logs));
server.get(c.get('apiBase') + 'logs', logs.get.bind(logs));
server.get(c.get('apiBase') + 'logs/search', logs.search.bind(logs));


/* =======================================================
 * == EXCEL
 * ======================================================= */
// Nodes routes that can be accessed only by authenticated & authorized users
server.get(c.get('apiBase') + 'excel/:id', excel.genExcel.bind(excel));


/* =======================================================
 * == DOCX
 * ======================================================= */
// Nodes routes that can be accessed only by authenticated & authorized users
server.get(c.get('apiBase') + 'docx/:id', docx.genDocx.bind(docx));


/* =======================================================
 * == SYNC
 * ======================================================= */
// Sync triggers
server.get(c.get('apiBase') + 'sync/:action/:data', sync.manualSync.bind(sync));
server.get(c.get('apiBase') + 'sync/:action', sync.manualSync.bind(sync));
server.get(c.get('apiBase') + 'sync', sync.manualSyncAll.bind(sync));

/* =======================================================
 * == ADMIN
 * ======================================================= */
server.get(c.get('apiBase') + 'admin/create-admins', admin.createSalesManagers.bind(admin));
server.get(c.get('apiBase') + 'admin/delete-orders', admin.deleteOrders.bind(admin));
server.get(c.get('apiBase') + 'admin/delete-companies', admin.deleteCompanies.bind(admin));
server.get(c.get('apiBase') + 'admin/delete-admins', admin.deleteAdmins.bind(admin));
server.post(c.get('apiBase') + 'admin/reset-user-password', admin.resetUserPasswordAndSendMail.bind(admin));

/* =======================================================
 * == PUBLIC
 * ======================================================= */
server.post(c.get('apiBase') + 'public/login', pub.loginPublic.bind(pub));

/* =======================================================
 * == OTHER
 * ======================================================= */
// Sample Error route
server.get(c.get('apiBase') + 'ErrorExample', function (req, res, next) {
  next(new Error('Random error!'));
});

// Connectivity test
server.get(c.get('apiBase') + 'test', test.connectivity);

// EOF
