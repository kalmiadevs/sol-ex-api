// Include gulp utils
var gulp              = require('gulp'),
    jshint            = require('gulp-jshint'),
    jshintXMLReporter = require('gulp-jshint-xml-file-reporter'),
    jscs              = require('gulp-jscs'),
    stylish           = require('gulp-jscs-stylish'),
    apidoc            = require('gulp-apidoc'),
    swagger           = require('gulp-apidoc-swagger');

// Paths
var src = 'lib/**/*.js';

// --------- CODESTYLE ---------------
// Generate codestyle report: uses project wise JSHint and JSCS
gulp.task('lint', function() {
  return gulp.src(src)
    .pipe(jshint('../.jshintrc'))
    .pipe(jscs({configPath: '../.jscsrc'}))
    .pipe(stylish.combineWithHintResults())
    .pipe(jshint.reporter('gulp-jshint-html-reporter', {
      filename: __dirname + '/lint-report.html',
      createMissingFolders : true
    }));
});

// Generate codestyle report for jenkins: uses project wise JSHint and JSCS
gulp.task('jenkins:lint', function() {
  return gulp.src(src)
    .pipe(jshint('../.jshintrc'))
    .pipe(jscs({configPath: '../.jscsrc'}))
    .pipe(stylish.combineWithHintResults())
    .pipe(jshint.reporter(jshintXMLReporter))
    .on('end', jshintXMLReporter.writeFile({
      format: 'checkstyle',
      filePath: './lint-report.xml'
    }));
});

gulp.task('apidoc', function (done) {
  return apidoc({
    src: 'lib/controllers/',
    dest: 'documentation/',
    template: 'documentation/template/',
    config: '',
    includeFilters: ['.*\\.js$']
  }, done);
});

// gulp.task('swagger', function() {
//   return swagger.exec({
//     src: 'lib/controllers/',
//     dest: 'doc/'
//   });
// });
