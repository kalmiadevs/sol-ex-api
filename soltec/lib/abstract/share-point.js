'use strict';

const _          = require('lodash'),
      BaseApi    = require('../../shared/abstract/base-api'),
      to         = require('../../shared/tools/to'),
      fs         = require('fs-extra'),
      SpDownload = require('sp-download').Download,
      FsLogApi   = require('../../shared/abstract/fs-log-api'),
      Q          = require('q'),
      SpRequest  = require('sp-request');

function encodeSharePointComponent(x) {
  return encodeURIComponent(x).replace(/'/g, '\'\'');
}

/*=====SYNC API=====*/
/**
 * Api for share point.
 */
class SharePointApi extends BaseApi {
  /**
   * Constructor
   */
  constructor(baseOpt, sharePointConf = {
    organization: undefined,
    clientId    : undefined,
    clientSecret: undefined,
    realm       : undefined,

    username: undefined,
    password: undefined
  }, options                          = {
    useAddIn: false
  }) {
    super(baseOpt);

    this.sharePointConf = sharePointConf;
    this.options        = options;

    this.authContext = options.useAddIn ?
      {
        clientId    : sharePointConf.clientId,
        clientSecret: sharePointConf.clientSecret,
        realm       : sharePointConf.realm
      } :
      {
        username: sharePointConf.username,
        password: sharePointConf.password
      };

    /**
     * Share point req instance.
     * @type {ISPRequest}
     */
    this.spReq = SpRequest.create(this.authContext);
  }

  get baseApi() {
    return `https://${this.sharePointConf.organization}.sharepoint.com/solutions365/_api`;
  }

  $spGetDigest() {
    return this.spReq.requestDigest(`https://${this.sharePointConf.organization}.sharepoint.com/solutions365`);
  }

  /**
   * Share point POST req.
   * @param uri
   * @param body
   * @param headers
   * @return {Promise}
   */
  async $spPost(uri, body, headers = {}, update) {
    headers['X-RequestDigest'] = await this.$spGetDigest();
    // if update
    // headers['X-HTTP-Method']   = 'MERGE';
    // headers['IF-MATCH']        = '*';

    return this.spReq.post(uri, {
      body,
      headers
    })
      .then((res) => {
        return res.body.d;  // response.statusCode === 204  list 'vals updated!'
      })
      .catch(e => {
        console.error(e);   // if (err.statusCode === 404) list not found!
        throw e;
      });
  }

  /**
   * Share point GET req.
   * @param uri
   * @return {Promise}
   */
  $spGet(uri) {
    const self = this;
    return new Promise((resolve, reject) => {
      self.spReq.get(uri)
        .then((res) => {
          resolve(res.body.d);
        })
        .catch((e) => {
          reject(e);
        });
    });
  }

  $spDownloadFile(filePathToDownload, saveToPath) {
    const download = new SpDownload(this.authContext);

    return download.downloadFile(filePathToDownload, saveToPath)
      .then(savedToPath => {
        console.log(`${filePathToDownload} has been downloaded to ${savedToPath}`);
        return Promise.resolve(savedToPath);
      })
      .catch(error => {
        console.error(error);
        throw error;
      });
  }

  /**
   * Create item
   * @param listTitle
   * @param listType
   * @param body
   * @param headers
   * @return {Promise.<*>}
   */
  async $spCreateListItem(listTitle, listType, body, headers = {}) {
    const fsLog = new FsLogApi([], { name: 'syncHttpRequests.log' });

    const uri                  = `${this.baseApi}/Web/Lists/getbytitle('${encodeSharePointComponent(listTitle)}')/items`;
    headers['X-RequestDigest'] = await this.$spGetDigest();
    body.__metadata            = body.__metadata || {};
    body.__metadata.type       = listType;

    fsLog.info({
      api: '$spCreateListItem',
      uri,
      body,
      headers
    });

    return this.spReq.post(uri, { body, headers })
      .then((res) => {
        fsLog.info({
          api     : '$spCreateListItem - Success',
          uri,
          body,
          headers,
          response: res
        });

        return res.body.d;
      })
      .catch(e => {
        console.error(e);
        fsLog.error({
          api  : '$spCreateListItem - Failed',
          uri,
          body,
          headers,
          error: e
        });

        throw e;
      });
  }

  async $spCreateListItemFakeOffer() {
    return await Promise.resolve({
      '__metadata'                        : {
        'id'  : 'ccb6de73-a7ab-4f7a-8f85-e134b79a98a3',
        'uri' : 'https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid\'c95025d1-b150-4a66-912c-005661a66591\')/Items(960)',
        'etag': '"1"',
        'type': 'SP.Data.SalesOpportunitiesListItem'
      },
      'FirstUniqueAncestorSecurableObject': {
        '__deferred': {
          'uri': 'https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid\'c95025d1-b150-4a66-912c-005661a66591\')/Items(960)/FirstUniqueAncestorSecurableObject'
        }
      },
      'RoleAssignments'                   : {
        '__deferred': {
          'uri': 'https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid\'c95025d1-b150-4a66-912c-005661a66591\')/Items(960)/RoleAssignments'
        }
      },
      'Activities'                        : {
        '__deferred': {
          'uri': 'https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid\'c95025d1-b150-4a66-912c-005661a66591\')/Items(960)/Activities'
        }
      },
      'AttachmentFiles'                   : {
        '__deferred': {
          'uri': 'https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid\'c95025d1-b150-4a66-912c-005661a66591\')/Items(960)/AttachmentFiles'
        }
      },
      'ContentType'                       : {
        '__deferred': {
          'uri': 'https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid\'c95025d1-b150-4a66-912c-005661a66591\')/Items(960)/ContentType'
        }
      },
      'GetDlpPolicyTip'                   : {
        '__deferred': {
          'uri': 'https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid\'c95025d1-b150-4a66-912c-005661a66591\')/Items(960)/GetDlpPolicyTip'
        }
      },
      'FieldValuesAsHtml'                 : {
        '__deferred': {
          'uri': 'https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid\'c95025d1-b150-4a66-912c-005661a66591\')/Items(960)/FieldValuesAsHtml'
        }
      },
      'FieldValuesAsText'                 : {
        '__deferred': {
          'uri': 'https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid\'c95025d1-b150-4a66-912c-005661a66591\')/Items(960)/FieldValuesAsText'
        }
      },
      'FieldValuesForEdit'                : {
        '__deferred': {
          'uri': 'https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid\'c95025d1-b150-4a66-912c-005661a66591\')/Items(960)/FieldValuesForEdit'
        }
      },
      'File'                              : {
        '__deferred': {
          'uri': 'https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid\'c95025d1-b150-4a66-912c-005661a66591\')/Items(960)/File'
        }
      },
      'Folder'                            : {
        '__deferred': {
          'uri': 'https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid\'c95025d1-b150-4a66-912c-005661a66591\')/Items(960)/Folder'
        }
      },
      'ParentList'                        : {
        '__deferred': {
          'uri': 'https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid\'c95025d1-b150-4a66-912c-005661a66591\')/Items(960)/ParentList'
        }
      },
      'Properties'                        : {
        '__deferred': {
          'uri': 'https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid\'c95025d1-b150-4a66-912c-005661a66591\')/Items(960)/Properties'
        }
      },
      'Versions'                          : {
        '__deferred': {
          'uri': 'https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid\'c95025d1-b150-4a66-912c-005661a66591\')/Items(960)/Versions'
        }
      },
      'FileSystemObjectType'              : 0,
      'Id'                                : 960,
      'ServerRedirectedEmbedUri'          : null,
      'ServerRedirectedEmbedUrl'          : '',
      'ContentTypeId'                     : '0x01006EF774FE86D6CB4CA8C19D623B09080D',
      'Title'                             : 'KALMIA DEV TEST PROJECT #0 (PA-AG-0013)',
      'Company'                           : 'Kalmia TEST DEV COMPANY',
      'Contact'                           : '1234567890',
      'Value'                             : 42,
      'OpportunityDate'                   : '2017-09-08T08:45:48Z',
      'MaturityDate'                      : '2017-10-07T22:00:00Z',
      'Description'                       : '<div class="ExternalClass00F5D564685A4F478189F11839BBD104">No comment.</div>',
      'OpportunityLeaderId'               : 36,
      'OpportunityLeaderStringId'         : '36',
      'Status'                            : 'Open',
      'PhaseOfSalesOpportunity'           : 'S0',
      'ProductGroupId'                    : 3,
      'ProjectCreated'                    : false,
      'SalesOpWF'                         : null,
      'ProjectTypeId'                     : null,
      'OfferNumber'                       : 'PA-AG-0013',
      'Installments'                      : null,
      'Receiver'                          : null,
      'AutoCreateProject'                 : false,
      'ID'                                : 960,
      'Modified'                          : '2017-09-08T08:45:48Z',
      'Created'                           : '2017-09-08T08:45:48Z',
      'AuthorId'                          : 51,
      'EditorId'                          : 51,
      'OData__UIVersionString'            : '1.0',
      'Attachments'                       : false,
      'GUID'                              : 'e5319afa-0e46-4677-b757-c340197f243e'
    });
  }

  /**
   * Update item
   * @param listTitle
   * @param listType
   * @param itemId
   * @param body
   * @param headers
   * @return {Promise.<*>}
   */
  async $spUpdateListItem(listTitle, listType, itemId, body, headers = {}) {
    const fsLog = new FsLogApi([], { name: 'syncHttpRequests.log' });

    const uri                  = `${this.baseApi}/Web/Lists/getbytitle('${encodeSharePointComponent(listTitle)}')/items(${encodeURIComponent(itemId)})`;
    headers['X-RequestDigest'] = await this.$spGetDigest();
    headers['IF-MATCH']        = '*';
    headers['X-HTTP-Method']   = 'MERGE';
    body.__metadata            = body.__metadata || {};
    body.__metadata.type       = listType;

    fsLog.info({
      api: '$spUpdateListItem',
      uri,
      body,
      headers
    });

    return this.spReq.post(uri, { body, headers }).then((res) => {
      fsLog.info({
        api     : '$spUpdateListItem - Success',
        uri,
        body,
        headers,
        response: res
      });

      return res.statusCode;
    })
      .catch(e => {
        fsLog.error({
          api  : '$spUpdateListItem - Failed',
          uri,
          body,
          headers,
          error: e
        });

        console.error(e);
        throw e;
      });
  }

  /**
   * Delete item.
   * @param listTitle
   * @param listType
   * @param itemId
   * @param headers
   * @return {<"http".IncomingMessage>}
   */
  async $spDeleteListItem(listTitle, listType, itemId, headers = {}) {
    const uri                  = `${this.baseApi}/Web/Lists/getbytitle('${listTitle}')/items(${itemId})`;
    headers['X-RequestDigest'] = await this.$spGetDigest();
    headers['IF-MATCH']        = '*';
    headers['X-HTTP-Method']   = 'DELETE';

    return await Promise.resolve(); // TODO remove me on prod
    return this.spReq.post(uri, {
      body,
      headers
    }).then((res) => {
      resolve(res.body.d);
    });
  }

  /**
   * Get items by specific filed.
   * @param listName {string}
   * @param key {string}
   * @param value {string}
   * @return {Promise}
   */
  $spGetBy(listName, key, value) {
    const self = this;
    return new Promise((resolve, reject) => {
      self.spReq.get(`${self.baseApi}/Web/Lists/getbytitle('${listName}')/items?$filter= ${encodeSharePointComponent(key)} eq '${encodeSharePointComponent(value)}'`)
        .then((res) => {
          resolve(res.body.d.results);
        })
        .catch((e) => {
          reject(e);
        });
    });
  }

  /**
   * Get via list guid item by filed.
   * @param listTitle
   * @param key
   * @param value
   * @return {Promise.<SpFileDetails[]>}
   */
  $spGetFilesBy(listTitle, key, value) {
    const self = this;
    return new Promise((resolve, reject) => {
      const uri = `${self.baseApi}/Web/Lists/getbytitle('${encodeSharePointComponent(listTitle)}')/items?$filter= ${encodeSharePointComponent(key)} eq '${encodeSharePointComponent(value)}'`;
      self.spReq.get(uri)
        .then((res) => {

          /**
           * @param x {SpFile}
           * @return {Promise<SpFile>}
           */
          const f = x => {
            return self.spReq.get(x.File.__deferred.uri).then(df => {
              const file       = df.body.d;
              x.___downloadUrl = `https://${this.sharePointConf.organization}.sharepoint.com/solutions365/Documents/${encodeURIComponent(file.Name)}`;
              x.Title          = file.Title || file.Name;
              x.__File         = df;
              return x;
            });
          };

          return Q.all((res.body.d.results || []).map(f));
        })
        .then(files => {
          resolve(files);
        })
        .catch((e) => {
          reject(e);
        });
    });
  }

  /**
   * Upload file to document lib & and put meta into document list
   * @param list
   * @param list.name
   * @param list.type
   * @param filePath
   * @param body
   * @param body.Title
   * @param useFolderUrl
   * @return {Promise}
   */
  async $spUploadFile(list, filePath, body, useFolderUrl = false) {
    const digest        = await this.$spGetDigest();
    const fileUploadUri = useFolderUrl ?
      `${this.baseApi}/Web/GetFolderByServerRelativeUrl('${encodeSharePointComponent(list.name)}')/Files/Add(url='${encodeSharePointComponent(body.Title)}', overwrite=true)` :
      `${this.baseApi}/Web/Lists/getByTitle('${encodeSharePointComponent(list.name)}')/RootFolder/Files/Add(url='${encodeSharePointComponent(body.Title)}', overwrite=true)`;
    const opt           = {
      headers: {
        'X-RequestDigest': digest
      },
      body   : fs.readFileSync(filePath),
      json   : false
    };

    // The response from the call that uploads the document will be a SP.File object representing the file that was just created.
    /**
     * @type{FileUploadRes}
     */
    const uploadRes = await this.spReq.post(fileUploadUri, opt)
      .then((res) => {
        return JSON.parse(res.body).d;
      })
      .catch(e => {
        e.extraData = { uploadFileFail: true };
        throw e;
      });

    // This object will have a deffered property called ListItemAllFields, which you can use to get the URL needed to make a call to get the list item associated with the file.
    const fileItem = await this.$spGet(uploadRes.ListItemAllFields.__deferred.uri)
      .catch(e => {
        e.extraData = { uploadRes };
        throw e;
      });

    const updateItemUri                = useFolderUrl ?
      fileItem.__metadata.uri :
      `${this.baseApi}/Web/Lists/getbytitle('${list.name}')/Items(${fileItem.Id})`;
    const updateItemOpt                = {
      headers: {
        'X-RequestDigest': digest,
        'IF-MATCH'       : fileItem.__metadata.etag,
        'X-Http-Method'  : 'MERGE'
      },
      body   : body
    };
    updateItemOpt.body.__metadata      = updateItemOpt.body.__metadata || {};
    updateItemOpt.body.__metadata.type = list.type;

    // Once you have the list item you can then make a call to set the field value.
    const updateItemRes = await this.spReq.post(updateItemUri, updateItemOpt)
      .then((res) => {
        return res.statusCode;
      })
      .catch(e => {
        e.extraData = { uploadRes, fileItem };
        throw e;
      });


    return { fileId: uploadRes.UniqueId, uploadRes, fileItem, updateItemRes };
  }

  /**
   * Get item by ID.
   * @param listName {string}
   * @param id {string}
   * @return {Promise}
   */
  $spGetByID(listName, id) {
    const self = this;
    return new Promise((resolve, reject) => {
      self.spReq.get(`${self.baseApi}/Web/Lists/getbytitle('${encodeSharePointComponent(listName)}')/Items(${encodeURIComponent(id)})/FieldValuesAsText`)
        .then((res) => {
          resolve(res.body.d);
        })
        .catch((e) => {
          reject(e);
        });
    });
  }

  /**
   * Get item based on meta data.
   * @param data
   * @param data.FieldValuesAsText
   * @param data.FieldValuesAsText.__deferred
   * @param data.FieldValuesAsText.__deferred.uri
   * @return {Promise}
   */
  $spGetItemFromMetaData(data) {
    return this.$spGet(data.FieldValuesAsText.__deferred.uri);
  }

  /**
   * Get list from share point.
   * @param key
   * @return {Promise.<GetListResponse>}
   */
  $spGetList(key) {
    const self = this;
    return this.$spGet(`${self.baseApi}/Web/Lists/${encodeURIComponent(key)}`)
      .then(res => {
        // const lastItemModifiedDate     = res.LastItemModifiedDate;
        // const lastItemUserModifiedDate = res.LastItemUserModifiedDate;
        // res.Items.__deferred.uri
        // res.Id
        return res;
      });
  }

  /**
   * Iterate over items in share point.
   * @param listProvider {listProvider | string}
   * @param onItem {onItem}
   * @param accumulator {*}
   * @param options
   * @param options.gtModified dateISOString
   * @return {Promise.<*>}
   */
  async $spGetListReduce(listProvider, onItem, accumulator, options = {}) {
    const self = this;
    const res  = await (_.isString(listProvider) ? () => this.$spGetList(listProvider) : listProvider)();

    const $spGetAllItems = async (uri) => {
      return self.$spGet(uri)
        .then(async results => {
          const items = results.results || [];

          for (let i = 0, len = items.length; i < len; i++) {
            const metaData = items[i];

            // ONLY KALMIA
            // if (metaData.ID != 1667) {continue;}
            // if (metaData.ID != 7095) {continue;}
            // if (metaData.EmailAddress1 !== 'info@kalmia.si') {
            //   continue;
            // }

            const item = await this.$spGetItemFromMetaData(metaData);
            await onItem(accumulator, item, metaData, res);
          }

          if (results.__next) {
            return await $spGetAllItems(results.__next);
          }
          return true;
        });
    };


    try {
      let urlPostfix = '';
      if (options.gtModified) {
        urlPostfix = `?$filter=Modified gt datetime'${encodeSharePointComponent(options.gtModified)}'`;
      }
      // Filters should already be encoded for sharepoint
      if (options.filters) {
        if (!options.gtModified) {
          urlPostfix = `?$filter=`;
        } else {
          urlPostfix += ' and ';
        }
        urlPostfix += options.filters.map(x => encodeURIComponent(x)).join(' and ');
      }

      await $spGetAllItems(`${self.baseApi}/Web/Lists(guid'${encodeSharePointComponent(res.Id)}')/Items${urlPostfix}`);
    } catch (e) {
      this.reportError(e);
      throw e;
    }
    return { acc: accumulator, res };
  }
}

module.exports = SharePointApi;

// JSDOC
/**
 * List Provider
 * @callback listProvider
 * @return {Promise}
 */

/**
 * On item callback
 * @callback onItem
 * @param {Object} accumulator
 * @param {ProjectType|SalesOpportunity|Project|Company|*} item
 * @param {Object} metaData
 * @param {Object} res List Response
 * @return {Promise}
 */

/**
 * @typedef {{lastItemModified: number, items: Object[], rawRes: Object, rawItemsRes: ListResponse }} GetListResponse
 */

/**
 * @typedef  {Object} __metadata
 * @property {String} id
 * @property {String} uri
 * @property {String} type
 */

/**
 * @typedef  {Object} SpFile
 * @property {Object} __metadataFILE
 * @property {Object} FirstUniqueAncestorSecurableObject
 * @property {Object} RoleAssignments
 * @property {Object} Activities
 * @property {Object} AttachmentFiles
 * @property {Object} ContentType
 * @property {Object} GetDlpPolicyTip
 * @property {Object} FieldValuesAsHtml
 * @property {Object} FieldValuesAsText
 * @property {Object} FieldValuesForEdit
 * @property {Object} File
 * @property {Object} Folder
 * @property {Object} ParentList
 * @property {Object} Properties
 * @property {Object} Versions
 * @property {Number} FileSystemObjectType
 * @property {Number} Id
 * @property {String} ServerRedirectedEmbedUri
 * @property {String} ServerRedirectedEmbedUrl
 * @property {String} ContentTypeId
 * @property {String} Title
 * @property {String} Company
 * @property {Object} Contact
 * @property {String} SalesOpportunity
 * @property {Object} SharedWithUsersId
 * @property {Object} SharedWithDetails
 * @property {Object} Project
 * @property {Object} MediaServiceAutoTags
 * @property {Object} ComplianceAssetId
 * @property {Number} ID
 * @property {String} Created
 * @property {Number} AuthorId
 * @property {String} Modified
 * @property {Number} EditorId
 * @property {Object} OData__CopySource
 * @property {Object} CheckoutUserId
 * @property {String} OData__UIVersionString
 * @property {String} GUID
 * @property {String} ___downloadUrl
 */

/**
 * @typedef  {Object} SpFileDetails
 * @property {Object} Author
 * @property {String} CheckInComment
 * @property {Number} CheckOutType
 * @property {Object} CheckedOutByUser
 * @property {String} ContentTag
 * @property {Number} CustomizedPageStatus
 * @property {String} ETag
 * @property {Object} EffectiveInformationRightsManagementSettings
 * @property {Boolean} Exists
 * @property {Object} InformationRightsManagementSettings
 * @property {Boolean} IrmEnabled
 * @property {String} Length
 * @property {Number} Level
 * @property {Null} LinkingUri
 * @property {String} LinkingUrl
 * @property {Object} ListItemAllFields
 * @property {Object} LockedByUser
 * @property {Number} MajorVersion
 * @property {Number} MinorVersion
 * @property {Object} ModifiedBy
 * @property {String} Name
 * @property {Object} Properties
 * @property {String} ServerRelativeUrl
 * @property {String} TimeCreated
 * @property {String} TimeLastModified
 * @property {String} Title
 * @property {Number} UIVersion
 * @property {String} UIVersionLabel
 * @property {String} UniqueId
 * @property {Object} VersionEvents
 * @property {Object} Versions
 * @property {Object} __metadata
 */


/**
 * @typedef  {Object} __metadataFILE
 * @property {String} id
 * @property {String} uri
 * @property {String} etag
 * @property {String} type
 */

/**
 * @typedef  {Object} FileUploadRes
 * @property {Object} __metadata
 * @property {Object} Author
 * @property {Object} CheckedOutByUser
 * @property {Object} EffectiveInformationRightsManagementSettings
 * @property {Object} InformationRightsManagementSettings
 * @property {Object} ListItemAllFields
 * @property {Object} LockedByUser
 * @property {Object} ModifiedBy
 * @property {Object} Properties
 * @property {Object} VersionEvents
 * @property {Object} Versions
 * @property {String} CheckInComment
 * @property {Number} CheckOutType
 * @property {String} ContentTag
 * @property {Number} CustomizedPageStatus
 * @property {String} ETag
 * @property {Boolean} Exists
 * @property {Boolean} IrmEnabled
 * @property {String} Length
 * @property {Number} Level
 * @property {Object} LinkingUri
 * @property {String} LinkingUrl
 * @property {Number} MajorVersion
 * @property {Number} MinorVersion
 * @property {String} Name
 * @property {String} ServerRelativeUrl
 * @property {String} TimeCreated
 * @property {String} TimeLastModified
 * @property {Object} Title
 * @property {Number} UIVersion
 * @property {String} UIVersionLabel
 * @property {String} UniqueId
 */
