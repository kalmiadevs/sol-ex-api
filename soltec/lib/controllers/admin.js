'use strict';

const _ = require('lodash'),
  LicenseApi = require('../../shared/sdk/license'),
  c = require('../configuration'),
  SALES_MANAGERS = require('../const').SALES_MANAGERS,
  LICENSE_CONF = require('../const').LICENSE_CONF,
  SOLTEC_LOGO = require('../const').SOLTEC_LOGO,
  generator = require('password-generator'),
  getEmailSubject = require('../const').getEmailSubject,
  getEmailTemplate = require('../const').getEmailTemplate,
  ms = require('../../shared/abstract/mail-api'),
  moment = require('moment');


const licenseConf = LICENSE_CONF;

class AdminApi {
  /**
   * Constructor
   */
  constructor() {

    /**
     * @type {LicenseApi}
     */
    this.license = new LicenseApi(c.get('authApi'));
    this.license.setAuth(licenseConf.email, licenseConf.password);

    this.mailService = new ms();
  }

  async $createSalesManagers(SM) {
    const out = [];
    for (let i = 0; i < SM.length; i++) {
      const x = SM[i];
      const accountManager = x.firstName + ' ' + x.lastName;
      console.log('starting ... ' + accountManager);

      if (await this.license.getUserByMeta('accountManager', accountManager)) {
        out.push('already created ' + accountManager);
        console.log('already created ' + accountManager);
      } else {
        const user = {
          role: 'admin',
          firstName: x.firstName,
          lastName: x.lastName,
          email: x.email,
          uniqueCode: x.uniqueCode,
          language: x.language,
          meta: x.meta
        };
        const res = await this.license.createUser(user, {preventEmail: true});
        console.log(res);
        out.push(res);
      }
    }
    return out;
  }

  createSalesManagers(req, res) {
    if (req.user.grp !== 'superadmin') {
      return res.send(403);
    }

    this.$createSalesManagers(SALES_MANAGERS)
      .then(x => res.send(x))
      .catch(x => res.send(x));
  };

  deleteOrders(req, res) {
    if (req.user.grp !== 'superadmin') {
      return res.send(403);
    }

    // req.db.orders.remove({});
    res.send({exec: true});
  };

  deleteCompanies(req, res) {
    if (req.user.grp !== 'superadmin') {
      return res.send(403);
    }

    res.send({error: 'not_implemented'});
  };

  deleteAdmins(req, res) {
    if (req.user.grp !== 'superadmin') {
      return res.send(403);
    }

    res.send({error: 'not_implemented'});
  };

  async resetUserPasswordAndSendMail(req, res) {
    if (req.user.grp !== 'superadmin') {
      return res.send(403);
    }

    const uid = req.body.id;


    let user;
    try {
      user = await this.license.getUser(uid);
    } catch (e) {
      return res.send(404);
    }

    let accountManager;
    try {
      accountManager = await this.license.getUser(user.meta.parent);
    } catch (e) {
      return res.send(404);
    }

    const newPassword = generator(12, false);

    user.password = newPassword;
    user.newPassword = user.password;
    user.confirmNewPassword = user.password;


    const newUserData = await this.license.updateUser(user, true);

    if (newUserData) {
      user.mailTemplate = {
        color: '#3e454d',
        projectName: 'Soltec Portal',
        logo: SOLTEC_LOGO
      };

      const lang = user.language;

      const welcomeMailData = {
        from: c.get('appMail'),
        to: user.email,
        cc: accountManager.email,
        subject: getEmailSubject('welcomeMail', lang)(),
        template: getEmailTemplate('lib/templates/welcome-mail', lang),
        data: {
          user: user,
          password: newPassword,
          link: c.get('webApp'),
        }
      };

      let mailSend;
      let mailError;
      if (user.active) {
        try {
          mailSend = await this.mailService.sendMail(welcomeMailData);
        } catch (e) {
          mailError = e;
        }
      }

      if (!mailError) {
        return res.send(200);
      } else {
        return res.send(500);
      }
    } else {
      return res.send(500);
    }
  }
}

module.exports.adminApi = new AdminApi();
