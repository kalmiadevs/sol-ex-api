'use strict';

const _               = require('lodash'),
      /**
       * @type {*}
       */
      Joi             = require('joi'),
      EntityApi       = require('../../shared/abstract/entity-api'),
      LicenseApi      = require('../../shared/sdk/license'),
      c               = require('../configuration'),
      ms              = require('../../shared/abstract/mail-api');

const $group       = require('../../shared/entities/group')(c),
      $user        = require('../../shared/entities/user')(c);


/*=====ARTICLE SCHEMA=====*/
// Note: you can also automate things further with https://github.com/yoitsro/joigoose but be careful not
// to put 'id' and things like that to schema.
const schema = Joi.object().keys({
  id    : [Joi.string(), Joi.object()], // our internal id

  owner : [Joi.string(), Joi.object()],

  title     : Joi.string().required(),
  content   : Joi.string().required(),

  birth     : Joi.date().optional(),
});


/*=====ARTICLE API=====*/
class ArticleApi extends EntityApi {
  constructor() {
    super({
      entityCollectionName: 'articles',
      entityName          : 'article',
      entitiesName        : 'articles',
      entitySchema        : schema
    });
  }

  canUserManageNews(req) {
    return (req.user.grp === 'superadmin');
  }

  /**
   * @api {post} /articles Add new article
   * @apiName Add
   * @apiOrder Articles
   **
   * @apiSuccess {Object} article Created article.
   */
  create(req, res) {
    if (!this.canUserManageNews(req)) return res.send(401);
    super.create(req, res);
  };

  async hook_beforeCreate(req, res, article) {
    article.owner  = req.user.uid;
    article.birth  = new Date();

    return article;
  }


  /**
   * @api {put} /articles/:id Edit existing article
   * @apiName Edit
   * @apiOrder Articles
   *
   * @apiParam {String} id Unique article id.
   *
   * @apiSuccess {Object} article Updated article information.
   */
  update(req, res) {
    if (!this.canUserManageNews(req)) return res.send(401);
    super.update(req, res);
  }


  /**
   * @api {delete} /articles/:id Delete existing article
   * @apiName Delete
   * @apiOrder Articles
   *
   * @apiParam {String} id Unique article id.
   *
   * @apiSuccess {String} status Response status.
   */
  remove(req, res) {
    if (!this.canUserManageNews(req)) return res.send(401);
    super.remove(req, res);
  }
}

const articleApi = new ArticleApi();
module.exports.articleApi = articleApi;
module.exports.ArticleApiCls = ArticleApi;
