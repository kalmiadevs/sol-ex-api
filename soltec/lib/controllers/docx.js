'use strict';

const _           = require('lodash'),
  BaseApi         = require('../../shared/abstract/base-api'),
  Docxtemplater   = require('docxtemplater'),
  JSZip           = require('jszip'),
  expressions     = require('angular-expressions'),
  ORDER_DOC_TYPES = require('../const').ORDER_DOC_TYPES,
  fs              = require('fs-extra'),
  path            = require('path');


/*=====DOCX API=====*/
class DocxApi extends BaseApi {
  /**
   * Constructor
   */
  constructor(orderApi) {
    super({
      module     : module,
      serviceName: 'docx',
      entityName : 'docx'
    });

    this.orderApi = orderApi;

    /**
     * @type {LicenseApi}
     */
    this.license = this.orderApi.license;

    this.templatePath = 'data/templates/order-form.docx';
  }

  genDocx(req, res) {
    let self = this;
    this.$genDocx(req.db, req.params.id)
      .then((fn) => {
        self.hook_genDocxSuccess(req, res, fn);
      })
      .catch((err) => {
        self.hook_genDocxFail(req, res, err);
      });
  }

  hook_genDocxSuccess(req, res, fn) {
    res.send({ filename: fn });
  }

  hook_genDocxFail(req, res, err) {
    let e = {
      resCode: 'gen_docx_fail',
      resMessage: 'Can\'t generate docx file',
      message: err,
    };
    super.handleError(req, res, e);
  }


  /**
   * Creates docx file (populated with specified order) in bucket orders/:id.
   * @param db
   * @param orderId
   */
  async $genDocx(db, orderId) {
    console.log(`Gen. docx template for ${ orderId } started.`);
    const orderModel = await this.orderApi.$get(db, orderId);
    const order      = orderModel.toObject();
    
    // Load the docx file
    const docxFile = fs.readFileSync(this.templatePath, 'binary');
    const docxZip = new JSZip(docxFile);

    // Define expression filter functions here, for example, to be able to write {clientname | upper}
    expressions.filters.upper = function(input) {
      if (!input) return input;
      return input.toString().toUpperCase();
    };
    const angularParser = function(tag) {
      return {
        get: tag === '.' ? function(s) { return s;} : function(s) {
          return expressions.compile(tag.replace(/’/g, "'"))(s);
        }
      };
    };

    // Create templater instance
    const templater = new Docxtemplater();
    templater.loadZip(docxZip);
    templater.setOptions({
      parser: angularParser,
      nullGetter: function(part) {
        if (part.module === 'rawxml') {
          return '';
        }
        return '';
      }
    });

    // Feed data into templater
    templater.setData(order);

    // Render template
    try {
      // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
      templater.render()
    }
    catch (error) {
      this.reportError(new Error(`Can not render docx template.`), { error });
      throw error;
    }

    const outputData = templater.getZip().generate({ type: 'nodebuffer' });

    const outputName       = `${ order.number }_sheet.docx`;
    const outputBucketPath = `${ orderId }${ path.sep }order_sheet${ path.sep }${ outputName }`;

    await this.orderApi.fs.$write(outputBucketPath, outputData, {encoding: 'binary'});

    // Bind docx to order
    order.documents = order.documents || [];
    const docI = _.findIndex(order.documents, { path: outputBucketPath });
    if (docI > -1) { order.documents.splice(docI, 1); }
    order.documents.push({
      path: outputBucketPath,
      type: ORDER_DOC_TYPES.docxExport,
      name: outputName,
      meta: {
        birth: new Date()
      }
    });

    await this.orderApi.$update(db, order, {ignoreState:true});

    console.log(`Gen. docx template for ${ orderId } finished.`);
    return outputBucketPath;
  }
}

module.exports.DocxApiCls = DocxApi;
