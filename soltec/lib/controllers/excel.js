'use strict';

const _                  = require('lodash'),
      BaseApi            = require('../../shared/abstract/base-api'),
      XlsxTemplate       = require('xlsx-template'),
      XlsxPopulate       = require('xlsx-populate'),
      fs                 = require('fs-extra'),
      ORDER_DOC_TYPES    = require('../const').ORDER_DOC_TYPES,
      ORDER_STATE        = require('../const').ORDER_STATE,
      EXCEL_LANGUAGES    = require('../const').EXCEL_LANGUAGES,
      EXCEL_TYPES        = require('../const').EXCEL_TYPES,
      SHEET_IMAGE_LAYOUT = require('../const').SHEET_IMAGE_LAYOUT,
      XLSX               = require('xlsx'),
      ExcelJs            = require('exceljs'),
      ImageSize          = require('image-size'),
      path               = require('path'),
      FsLogApi           = require('../../shared/abstract/fs-log-api'),
      os                 = require('os'),
      moment             = require('moment'),
      webOrderToPdf      = require('../print/print').webOrderToPdf,
      scissors           = require('scissors'),
      streamToPromise    = require('stream-to-promise'),
      Q                  = require('q');

const util = require('util');
const exec = util.promisify(require('child_process').exec);

/*=====EXCEL API=====*/
/**
 * Tip: We always leave generated files in bucket so we can reuse them, or
 * if something is not ok inspect them.
 */
class ExcelApi extends BaseApi {
  /**
   * Constructor
   */
  constructor(orderApi) {
    super({
      module     : module,
      serviceName: 'excel',
      entityName : 'excel'
    });

    this.orderApi = orderApi;

    /**
     * @type {LicenseApi}
     */
    this.license = this.orderApi.license;

    this.templatePath            = 'data/templates/order.xlsx';
    this.orderFormTemplatePath   = 'data/templates/soltec-order-form.xlsx';
    this.sheetTemplatePath       = 'data/templates/order';
    this.searchExcelTemplatePath = 'data/templates/';
    this.sheetName               = 'TEMPLATE';
  }

  genExcel(req, res) {
    let self = this;
    this.$genExcel(req.db, req.params.id, EXCEL_TYPES.excelPrivate, req)
      .then((fn) => {
        self.hook_genExcelSuccess(req, res, fn);
      })
      .catch((err) => {
        self.hook_genExcelFail(req, res, err);
      });
  }

  hook_genExcelSuccess(req, res, fn) {
    res.send({ filename: fn });
  }

  hook_genExcelFail(req, res, err) {
    let e = {
      resCode   : 'gen_excel_fail',
      resMessage: 'Can\'t generate excel file',
      message   : err
    };
    super.handleError(req, res, e);
  }

  /**
   * Get sheet id from name
   */
  getSheetIDFromName(workbook, name) {
    for (let x of workbook.sheets) {
      if (x.name === name) {
        return x.id;
      }
    }
  }

  /**
   * Creates excel file (populated with specified order) in bucket orders/:id.
   * @param db
   * @param orderId
   * @param excelType
   */
  async $genExcel(db, orderId, excelType = EXCEL_TYPES.excelPrivate, req) {
    console.log(`Gen. excel template for ${ orderId } started.`);
    const orderModel = await this.orderApi.$get(db, orderId);
    const order      = orderModel.toObject();

    const orderFolder    = `${ this.orderApi.fs.bucketPath }${ path.sep }${ orderId }`;
    const owner          = await this.license.getUser(order.owner);
    owner.meta           = owner.meta || {};
    const spExportFormat = owner.meta.spExportFormat || 'pdf';

    const pergolaTypeArray = order.product.pergolaType.split('/');
    const sheetName        = `DEVIS ${ pergolaTypeArray[0] } ${ pergolaTypeArray[1] }`;

    const excelTpl = fs.readFileSync(this.templatePath);
    const template = new XlsxTemplate(excelTpl);

    const sheetNumber = this.getSheetIDFromName(template, this.sheetName);
    if (!sheetNumber) {
      throw Error('Sheet "' + this.sheetName + '" not found');
    }

    // Don't wait for response :D
    // const orderWebPdfName           = `${ order.number }_order_sheet.pdf`;
    // const orderWebPdfNameBucketPath = `${ orderId }${ path.sep }sync_offer${ path.sep }${ orderWebPdfName }`;
    // const webToPdfRes = webOrderToPdf(orderId, `${ this.orderApi.fs.bucketPath }${ path.sep }${orderWebPdfNameBucketPath}`);

    // ADD ADDITIONAL VALUES TO ORDER OBJECT
    if (owner.role === 'admin') {
      const children = await this.license.getUsersInfo(owner.meta.children || []) || [];
      let distributor;
      if (children) {
        distributor = children.find((x) => {
          return x.meta.spTitle && x.meta.spTitle.toLowerCase() === order.client.name.toLowerCase();
        });

        if (distributor) {
          order.address = {
            address   : distributor.meta.spAddress,
            postalCode: distributor.meta.spPostNumber,
            city      : distributor.meta.spCity,
            country   : distributor.meta.spCountry
          };
          order.company = distributor.meta.spTitle;
          order.vat     = order.client.vat || distributor.meta.vatId;

        } else {
          const addressTokens = order.client.address.split('\n');
          order.address       = {
            address   : addressTokens[0],
            postalCode: addressTokens[1],
            city      : addressTokens[2],
            country   : addressTokens[3]
          };
          order.vat           = order.client.vat;
        }
      } else {
        console.warn(`Sales manager '${ owner.name } has no distributors`);
      }
    } else {
      const addressTokens = order.client.address.split('\n');
      order.address       = {
        address   : addressTokens[0],
        postalCode: addressTokens[1],
        city      : addressTokens[2],
        country   : addressTokens[3]
      };
      order.vat           = order.client.vat;
    }
    order.language = order.languageOverride || owner.language || 'en';
    if (!_.some(EXCEL_LANGUAGES, (x) => x === order.language)) {
      order.language = 'en';
    }

    order.public = order._public;

    order.fromNewConfigurator = order.fromNewConfigurator ? 1 : 0; // Convert to number value for excel.

    order.discountCalc = (excelType === EXCEL_TYPES.excelPublic && !order.discountOverride) ? 0 : order.discountVal / 100;

    order.product.specifications.ledQuantity        = order.product.specifications.ledPositions.length;
    order.product.specifications.ledPositionsString = order.product.specifications.ledPositions.sort((a, b) => a - b).map((x) => x + 1).join(', ');

    // PRICE TBD
    if ((order.product.options.slidingPanelPosition.p1p2 && order.product.additionalPoleData.find((x) => x.position === 'p1p2')) ||
      (order.product.options.slidingPanelPosition.p3p4 && order.product.additionalPoleData.find((x) => x.position === 'p3p4')) ||
      (order.product.options.slidingPanelPosition.p1p3 && order.product.additionalPoleData.find((x) => x.position === 'p1p3')) ||
      (order.product.options.slidingPanelPosition.p2p4 && order.product.additionalPoleData.find((x) => x.position === 'p2p4')) ||

      (order.product.options.glassPanelsPosition.p1p2 && order.product.additionalPoleData.find((x) => x.position === 'p1p2')) ||
      (order.product.options.glassPanelsPosition.p3p4 && order.product.additionalPoleData.find((x) => x.position === 'p3p4')) ||
      (order.product.options.glassPanelsPosition.p1p3 && order.product.additionalPoleData.find((x) => x.position === 'p1p3')) ||
      (order.product.options.glassPanelsPosition.p2p4 && order.product.additionalPoleData.find((x) => x.position === 'p2p4'))) {

      order.priceTbd = true;
    }

    // CALCULATE ZIP BLINDS LENGTH AND WIDTH
    const poleSize = 120;

    let lengthp1p2 = order.product.options.blindsPosition.p1p2 ? order.product.length - (poleSize * 2) : 0;
    let lengthp3p4 = order.product.options.blindsPosition.p3p4 ? order.product.length - (poleSize * 2) : 0;
    let widthp1p3  = order.product.options.blindsPosition.p1p3 ? order.product.width - (poleSize * 2) : 0;
    let widthp2p4  = order.product.options.blindsPosition.p2p4 ? order.product.width - (poleSize * 2) : 0;
    if (order.product.installationType === 6) {
      lengthp1p2 -= order.product.poleData.p1y;
      lengthp1p2 -= order.product.poleData.p2y;
      lengthp3p4 -= order.product.poleData.p3y;
      lengthp3p4 -= order.product.poleData.p4y;
    }


    if (order.product.installationType === 2 && lengthp3p4) lengthp3p4 += (poleSize * 2);
    if (order.product.installationType === 2 && widthp1p3) widthp1p3 += (poleSize * 1);
    if (order.product.installationType === 2 && widthp2p4) widthp2p4 += (poleSize * 1);

    if (order.product.installationType === 3 && lengthp3p4) lengthp3p4 += (poleSize * 2);
    if (order.product.installationType === 3 && widthp1p3) widthp1p3 += (poleSize * 1);
    if (order.product.installationType === 3 && widthp2p4) widthp2p4 += (poleSize * 2);
    if (order.product.installationType === 3 && lengthp1p2) lengthp1p2 += (poleSize * 1);

    if (order.product.installationType === 4 && lengthp3p4) lengthp3p4 += (poleSize * 2);
    if (order.product.installationType === 4 && widthp1p3) widthp1p3 += (poleSize * 1);
    if (order.product.installationType === 4 && widthp2p4) widthp2p4 += (poleSize * 1);

    if (order.product.installationType === 5 && lengthp3p4) lengthp3p4 += (poleSize * 2);
    if (order.product.installationType === 5 && widthp1p3) widthp1p3 += (poleSize * 1);
    if (order.product.installationType === 5 && widthp2p4) widthp2p4 += (poleSize * 2);
    if (order.product.installationType === 5 && lengthp1p2) lengthp1p2 += (poleSize * 1);

    let blindsSizeWidth  = order.product.width;
    let blindsSizeLength = order.product.length;

    let zipQuantityWidth = 0;
    let zipQuantityLength = 0;

    if (order.product.length >= 6000) {
      zipQuantityLength++;
    }

    for (const additionalPole of order.product.additionalPoleData) {

      switch (additionalPole.position) {
        case 'p1p2':
          lengthp1p2 -= poleSize;
          blindsSizeLength -= poleSize;
          if (lengthp1p2 > 0) {
            zipQuantityLength++;
          }
          break;
        case 'p3p4':
          lengthp3p4 -= poleSize;
          blindsSizeLength -= poleSize;
          if (lengthp3p4 > 0) {
            zipQuantityLength++;
          }
          break;
        case 'p1p3':
          widthp1p3 -= poleSize;
          blindsSizeWidth -= poleSize;
          if (widthp1p3 > 0) {
            zipQuantityWidth++;
          }
          break;
        case 'p2p4':
          widthp2p4 -= poleSize;
          blindsSizeWidth -= poleSize;
          if (widthp2p4 > 0) {
            zipQuantityWidth++;
          }
          break;
      }
    }

    zipQuantityLength += lengthp1p2 > 0 ? 1 : 0;
    zipQuantityLength += lengthp3p4 > 0 ? 1 : 0;

    zipQuantityWidth += widthp1p3 > 0 ? 1 : 0;
    zipQuantityWidth += widthp2p4 > 0 ? 1 : 0;

    const totalLength = Math.max(0, lengthp1p2) + Math.max(0, lengthp3p4);
    const totalWidth  = Math.max(0, widthp1p3) + Math.max(0, widthp2p4);

    order.blindsTotalLength = totalLength;
    order.blindsTotalWidth  = totalWidth;

    order.blindsSizeP1P3 = widthp1p3 > 0 ? blindsSizeWidth : 0;
    order.blindsSizeP2P4 = widthp2p4 > 0 ? blindsSizeWidth : 0;
    order.blindsSizeP1P2 = lengthp1p2 > 0 ? blindsSizeLength : 0;
    order.blindsSizeP3P4 = lengthp3p4 > 0 ? blindsSizeLength : 0;

    order.blindsQuantityLength = zipQuantityLength;
    order.blindsQuantityWidth = zipQuantityWidth;


    // FILL WITH ORDER VALS
    template.substitute(sheetNumber, order);
    const outputData = template.generate();

    // CREATE EXCEL ALL
    const excelAllName       = `${ order.number }_all.xlsx`;
    const excelAllBucketPath = `${ orderId }${ path.sep }sync_project${ path.sep }${ excelAllName }`;
    await this.orderApi.fs.$write(excelAllBucketPath, outputData, { encoding: 'binary' });
    // BIND EXCEL ALL TO ORDER
    order.documents = order.documents || [];
    const docI      = _.findIndex(order.documents, { path: excelAllBucketPath });
    if (docI > -1) { order.documents.splice(docI, 1); }
    order.documents.push({
      path: excelAllBucketPath,
      type: ORDER_DOC_TYPES.excelAll,
      name: excelAllName,
      meta: {
        birth: new Date()
      }
    });

    // hack to trigger recalculate once opened
    // TODO we can optimize this by setting a global flag in the XLSX file to say that all formulas and functions should be recalculated
    await this.$hideExcelSheetAndSetActive(`${ this.orderApi.fs.bucketPath }/${ excelAllBucketPath }`, 'TEMPLATE', 'INFO');

    // CALCULATE TOTAL COST
    // we need tmp folder
    fs.ensureDirSync(orderFolder + '/tmp');

    const recalcWb = await this.recalculateExcel(`${ this.orderApi.fs.bucketPath }/${ excelAllBucketPath }`, orderFolder + path.sep + 'tmp', `${ order.number }_all`);
    const cost     = recalcWb.Sheets[sheetName][{
      'DEVIS SL 160 28': 'K61',
      'DEVIS SL 170 28': 'K61',
      'DEVIS SL 170 36': 'K61'
    }[sheetName]].v;

    if (cost && _.isNumber(cost)) {
      order.cost    = cost + ' €';
      order.costVal = cost;

      if (cost < 50) {
        this.reportWarning({ message: `Order ${ orderId } has strange cost.`, order });
        this.reportError(new Error(`Order ${ orderId } has strange cost.`), {
          cost,
          sheetName,
          orderNumber: order.number
        });
      }
    } else {
      this.reportWarning({ message: `Order ${ orderId } has no cost.`, order });
      this.reportError(new Error(`Order ${ orderId } has no cost.`), { cost, sheetName, orderNumber: order.number });
    }

    // EXPORT SPECIFIC SHEET
    const exportFilename  = `${ order.number }_${ owner.language }${ excelType === EXCEL_TYPES.excelPublic ? '_public' : ''}.xlsx`;
    const exportSheetPath = `${ orderId }${ path.sep }sync_offer${ path.sep }${ exportFilename }`;
    fs.ensureDirSync(orderFolder + path.sep + 'sync_offer');
    await this.createOrderSheet(order, recalcWb, sheetName, `${ this.orderApi.fs.bucketPath }/${ exportSheetPath }`, owner.avatarBase64);

    // BIND EXPORTED SHEET FILE TO ORDER
    if (spExportFormat === 'excel') {
      const docXI = _.findIndex(order.documents, { path: exportSheetPath });
      if (docXI > -1) {
        order.documents.splice(docXI, 1);
      }
      order.documents.push({
        path: exportSheetPath,
        type: ORDER_DOC_TYPES.excelExportOneSheet,
        name: exportFilename,
        meta: {
          birth: new Date()
        }
      });
    }

    // EXPORT SPECIFIC SHEET TO PDF
    if (spExportFormat === 'pdf') {
      const pdfPath = `${ orderId }${ path.sep }sync_offer${ path.sep }${ order.number }_${ owner.language }${ excelType === EXCEL_TYPES.excelPublic ? '_public' : ''}.pdf`;
      await this.convertToPdf(`${ this.orderApi.fs.bucketPath }${ path.sep }${ exportSheetPath }`, `${ this.orderApi.fs.bucketPath }${ path.sep }${ orderId }${ path.sep }sync_offer`);

      // If excel type is not public, strip the last two pages from the PDF.
      if (!order._public) {
        console.log('Removing non-public pages from PDF');
        const pdfFullPath = `${ this.orderApi.fs.bucketPath }${ path.sep }${ pdfPath }`.replace(/\\/g, '/');
        const pdfFullPathTemp = `${pdfFullPath }.pdftk.tmp`;

        await streamToPromise(
          scissors(pdfFullPath)
            .pages(1)
            .pdfStream().pipe(fs.createWriteStream(pdfFullPathTemp))
        );

        await fs.move(pdfFullPathTemp, pdfFullPath, { overwrite: true });
      }

      // BIND EXPORTED SHEET PDF TO ORDER
      const docPDFI = _.findIndex(order.documents, { path: pdfPath });
      if (docPDFI > -1) {
        order.documents.splice(docPDFI, 1);
      }
      order.documents.push({
        path: pdfPath,
        type: ORDER_DOC_TYPES.excelExportOneSheet,
        name: `${ order.number }_${ owner.language }${ excelType === EXCEL_TYPES.excelPublic ? '_public' : ''}.pdf`,
        meta: {
          birth: new Date()
        }
      });
    }

    // FILL ORDER SHEET
    let authorUser = owner;
    if (req) {
      // If we have request object available then set the author to whoever requested the excel to generate.
      authorUser = await this.license.getUser(req.user.uid);
    }
    const authorName = authorUser.role === 'user' ? authorUser.firstName : `${ authorUser.firstName } ${ authorUser.lastName }`;

    const orderWebPdfName                  = `${ order.number }_order_sheet.pdf`;
    const orderWebPdfNameNoExt             = `${ order.number }_order_sheet`;
    const orderWebPdfNameBucketPath        = `${ orderId }${ path.sep }sync_offer${ path.sep }${ orderWebPdfName }`;
    const orderWebPdfNameBucketPath2       = `${ orderId }${ path.sep }sync_offer${ path.sep }${ orderWebPdfNameNoExt }`;
    const orderWebPdfNameBucketPathProject = `${ orderId }${ path.sep }sync_project${ path.sep }${ orderWebPdfName }`;
    const webToPdfRes                      = await webOrderToPdf(orderId, `${ this.orderApi.fs.bucketPath }${ path.sep }${orderWebPdfNameBucketPath2}`, authorName);
    this.orderApi.fs.$copy(webToPdfRes, orderWebPdfNameBucketPathProject, { overwrite: true }); // Make a copy of the PDF in project folder to be synced
    order.documents = order.documents || [];
    const docII     = _.findIndex(order.documents, { path: orderWebPdfNameBucketPath });
    if (docII > -1) { order.documents.splice(docII, 1); }
    order.documents.push({
      path    : orderWebPdfNameBucketPath,
      type    : ORDER_DOC_TYPES.excelExportOneSheet,
      noPublic: true,
      name    : orderWebPdfName,
      meta    : {
        birth: new Date()
      }
    });

    /*
     const orderFormExcelTpl = fs.readFileSync(this.orderFormTemplatePath);
     const orderFormTemplate = new XlsxTemplate(orderFormExcelTpl);

     const orderFormSheetNumber = this.getSheetIDFromName(orderFormTemplate, 'Sheet1');
     if (!orderFormSheetNumber) {
     throw Error('Sheet "' + this.sheetName + '" not found');
     }

     orderFormTemplate.substitute(orderFormSheetNumber, order);
     const orderFormOutputData = orderFormTemplate.generate();

     const orderSheetExcelAllName       = `${ order.number }_order_sheet.xlsx`;
     const orderSheetExcelAllBucketPath = `${ orderId }${ path.sep }sync_offer${ path.sep }${ orderSheetExcelAllName }`;
     await this.orderApi.fs.$write(orderSheetExcelAllBucketPath, orderFormOutputData, {encoding: 'binary'});

     order.documents = order.documents || [];
     const docII = _.findIndex(order.documents, { path: orderSheetExcelAllBucketPath });
     if (docII > -1) { order.documents.splice(docII, 1); }
     order.documents.push({
     path: orderSheetExcelAllBucketPath,
     type: ORDER_DOC_TYPES.excelExportOneSheet,
     name: orderSheetExcelAllName,
     meta: {
     birth: new Date()
     }
     });
     */

    // // ONLY FOR DEBUG
    // order.documents.push({
    //   path: `${ orderId }/sync/${ order.number }_all` + '.xlsx',
    //   type: 'excel_orig',
    //   name: 'tmp.xlsx',
    //   meta: {
    //     birth: new Date()
    //   }
    // });
    await this.orderApi.$update(db, order, { ignoreState: true });

    // remove exported excel sheet if not needed
    if (spExportFormat !== 'excel') {
      fs.removeSync(`${ this.orderApi.fs.bucketPath }/${ exportSheetPath }`);
    }

    // clean TMP
    fs.removeSync(`${ orderFolder }/tmp`);

    return excelAllBucketPath;
  }

  /**
   * Creates excel file with search results and returns the populated excel data.
   * @param db
   * @param orderId
   * @param excelType
   */
  async $genSearchResultsExport(user, resultsList) {
    const excelPath = path.join(this.searchExcelTemplatePath, 'search-export-' + user.role + '.xlsx');
    const excelTpl  = fs.readFileSync(excelPath);
    const template  = new XlsxTemplate(excelTpl);

    // Create template data
    const templateData = {
      data: []
    };

    for (const r of resultsList) {
      templateData.data.push({
        id                : r.number,
        salesManager      : r.salesManager,
        distributor       : r.distributor,
        projectName       : r.project.name,
        projectDescription: r.project.description,
        endDate           : r.project.date ? moment(r.project.date).format('DD.MM.YYYY') : '',
        status            : r.step,
        amount            : r.costVal,
        clientName        : r.client.name,
        clientVat         : r.client.vat,
        clientAddress     : r.client.address,
        clientPhone       : r.client.phone,
        clientEmail       : r.client.email
      });
    }

    // Generate excel
    template.substitute(1, templateData);
    const outputData = template.generate();

    return outputData;
  }

  async createOrderSheet(order, workbookAll, sheetName, destPath, avatarBase64) {
    const srcPath = `${ this.sheetTemplatePath }-${ sheetName.replace(/ /g, '_') }.xlsx`;

    const workbook = new ExcelJs.Workbook();
    await workbook.xlsx.readFile(srcPath);
    const worksheet = workbook.getWorksheet(sheetName);

    // Add images to offer
    for (const key of Object.keys(SHEET_IMAGE_LAYOUT)) {
      const filepath = path.resolve('data/images/' + key + '.png');

      const imageId = workbook.addImage({
        filename : filepath,
        extension: 'png'
      });

      worksheet.addImage(imageId, SHEET_IMAGE_LAYOUT[key]);
    }

    // Fix borders
    const cols = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K'];
    for (const col of cols) {
      worksheet.getCell(col + '144').border = {
        top   : { style: 'medium', color: { argb: 'ff674ea7' } },
        bottom: { style: 'medium', color: { argb: 'ff674ea7' } },
        left  : { style: 'medium', color: { argb: 'ff674ea7' } },
        right : { style: 'medium', color: { argb: 'ff674ea7' } }
      };
    }
    for (let i = 144; i < 156; i++) {
      worksheet.getCell('K' + i).border = {
        top   : { style: 'medium', color: { argb: 'ff674ea7' } },
        bottom: { style: 'medium', color: { argb: 'ff674ea7' } },
        right : { style: 'medium', color: { argb: 'ff674ea7' } }
      };
    }

    /*
     if (avatarBase64) {
     // find image in top left corner
     const mediaInfo = worksheet._media.filter(x => {
     return x.range.tl.col < 1 && x.range.tl.row < 1 && x.type === 'image';
     })[0];

     if (mediaInfo) {
     const media     = workbook.media[mediaInfo.imageId];
     const mediaSize = ImageSize(media.buffer);

     // replace image
     media.buffer   = Buffer.from(avatarBase64.split(',')[1], 'base64');
     const logoSize = ImageSize(media.buffer);

     // max height
     const cW = mediaInfo.range.br.col - mediaInfo.range.tl.col;
     const cH = mediaInfo.range.br.row - mediaInfo.range.tl.row;

     const k = (logoSize.height * cH) / mediaSize.height;
     //mediaInfo.range.br.col = mediaInfo.range.br.col * k;
     }
     }
     */

    // COPY VALUES FROM calculated excel
    const self = this;
    worksheet._columns.forEach((col, colIndex) => {
      col.eachCell(function (cell, rowNumber) {
        if (cell.value && (cell.value.formula || cell.value.sharedFormula)) {
          const x = workbookAll.Sheets[sheetName][cell._address];
          if (x) {
            cell.value = x.v;
          } else {
            console.warn(`Cell in tmp/all ${ sheetName } ${ cell._address } is undefined, but is marked as formula!`);
          }
        }
      });
    });

    return workbook.xlsx.writeFile(destPath);
  }

  /**
   * Convert excel to PDF
   * @param src
   * @param dst
   * @return {Promise.<void>}
   */
  async convertToPdf(src, dst) {
    const quote = os.platform() === 'win32' ? '"' : '\'';
    const cmd   = `libreoffice --headless --convert-to pdf ${ quote }${ src }${ quote } --outdir ${ quote }${ dst }${ quote }`;
    console.log('Running command: ' + cmd);

    const { stdout, stderr } = await exec(cmd);

    if (stderr) {
      this.reportError(new Error(`Can not convert EXCEL to PDF.`), { stderr, cmd });
      throw new Error(stderr);
    }

    return stdout;
  }

  /**
   * Recalculate excel formulas.
   * @param srcFile
   * @param tmpFolder
   * @param outputFilename
   * @return {Promise.<*>}
   */
  async recalculateExcel(srcFile, tmpFolder, outputFilename) {
    const quote = os.platform() === 'win32' ? '"' : '\'';
    const cmd   = `libreoffice --headless --convert-to xlsx:"Calc MS Excel 2007 XML" ${ quote }${ srcFile }${ quote } --outdir ${ quote }${ tmpFolder }${ (os.platform() === 'win32' ? '' : path.sep) }${ quote }`;
    console.log('Running command: ' + cmd);
    const { stdout, stderr } = await exec(cmd);

    if (stderr) {
      this.reportError(new Error(`Can not convert EXCEL.`), { stderr, cmd });
    }

    return XLSX.readFile(tmpFolder + path.sep + outputFilename + '.xlsx');
  }

  /**
   * Hides specified excel sheet and sets one sheet as active.
   * @param path
   * @param sheetName
   * @param active
   * @return {Promise}
   */
  $hideExcelSheetAndSetActive(path, sheetName, active) {
    return XlsxPopulate.fromFileAsync(path)
      .then(workbook => {
        let s     = workbook.sheets();
        let names = [];
        for (let i = 0; i < s.length; i++) {
          try {
            let name = s[i].name();
            names.push(name);

            if (name === sheetName) {
              s[i].hidden(true);
            } else {
              s[i].hidden(false);
            }
          } catch (e) {
            // ignore
          }
        }
        workbook.activeSheet(active);
        return workbook.toFileAsync(path);
      });
  }
}

module.exports.ExcelApiCls = ExcelApi;
