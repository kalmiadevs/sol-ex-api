'use strict';

const _         = require('lodash'),
      FileSystemApi = require('../../shared/abstract/fs-api');


/*=====LOGS API=====*/
class LogApi extends FileSystemApi {
  /**
   * Constructor
   */
  constructor() {
    super('logs');
  }

  get(req, res) {
    if (req.user.grp !== 'superadmin') {
      return res.send(403);
    }
    super.get(req, res);
  };

  list(req, res) {
    if (req.user.grp !== 'superadmin') {
      return res.send(403);
    }
    super.list(req, res);
  }
}

module.exports = new LogApi();
