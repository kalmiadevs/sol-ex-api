'use strict';

const c             = require('../configuration'),
      _             = require('lodash'),
      Joi           = require('joi'),
      FileSystemApi = require('../../shared/abstract/fs-api'),
      SOLTEC_LOGO   = require('../const').SOLTEC_LOGO,
      ms            = require('../../shared/abstract/mail-api');

// TODO move this logic into  ../../shared/abstract/mail-api

/*===== SCHEMAS ======*/
// let mailAttachmentSchema = Joi.object().keys({
//   filename: Joi.string().required(),
//   content: Joi.string().base64().required()
// });

let mailSchema = Joi.object().keys({
  email: Joi.string().email().required(),
  subject: Joi.string().required(),
  body: Joi.string().required(),

  // attachments: Joi.array().items(mailAttachmentSchema).required()
  attachments: Joi.array().required()
});



/*=====MAIL API=====*/
class MailApi {

  constructor() {
    this.mailService = new ms();

    /**
     * FileSystemApi
     * @type {FileSystemApi}
     */
    this.fs = new FileSystemApi('media');
  };

  sendErrorMessage(res, err) {
    return res.send({
      errorCode: 'mail_send_fail',
      errorMessage: err
    });
  }


  /**
   * @api {post} /mail Send mail with attachments.
   * @apiName Send
   * @apiOrder Mail
   *
   * @apiParam {String} email Email address to send files to.
   * @apiParam {String} subject Optional subject of the email.
   * @apiParam {String} body Content of email message.
   * @apiParam {Array} attachments Array of attachments.
   *    Each object in array must contain the following data:
   *      'filepath': Path to the file.
   *
   * @apiSuccess {Object} email Mail status.
   */
  send(req, res) {
    const self = this;
    const validationResult = Joi.validate(req.body, mailSchema);
    if (validationResult.error) {
      res.statusCode = 400;
      this.sendErrorMessage(res, validationResult.error.message);
      return false;
    }

    req.body.attachments = req.body.attachments.map(x => {
      return {
        path: self.fs.queryPathToBucketPath(x),
        filename: x.split('/').pop(),
      }
    });

    const logo = SOLTEC_LOGO;

    this.mailService.sendMail({
      from: c.get('appMail'),
      to: req.body.email,
      subject: req.body.subject + ' | Soltec Portal',
      template: 'lib/templates/media-files-mail',
      data: {
        user: {
          mailTemplate: {
            color: '#3e454d',
            projectName: 'Soltec Portal',
            logo: logo,
          }
        },
        subject: req.body.subject,
        body: req.body.body,
        link: c.get('webApp'),
      },
      attachments: req.body.attachments,
    })
    .then(() => {
      res.statusCode = 200;
      res.send({ status: 'OK' });
    })
    .catch((err) => {
      res.statusCode = err.errorCode || 500;
      this.sendErrorMessage(res, err);
    });
  };

}

module.exports = new MailApi();
