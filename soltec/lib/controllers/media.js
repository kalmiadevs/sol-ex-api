'use strict';

const _         = require('lodash'),
      FileSystemApi = require('../../shared/abstract/fs-api');


/*=====MEDIA API=====*/
class MediaApi extends FileSystemApi {
  /**
   * Constructor
   */
  constructor() {
    super('media');
    this._fs.ensureDirSync(this._path.join(this.bucketPath, 'en'));
    this._fs.ensureDirSync(this._path.join(this.bucketPath, 'sl'));
    this._fs.ensureDirSync(this._path.join(this.bucketPath, 'fr'));
  }

  /**
   * @api {post} /media/ Save a media file
   * @apiName Create
   * @apiOrder Media
   *
   * @apiParam {String} filePath Filepath.
   *
   * @apiSuccess {String} status Response status.
   */
  upload(req, res) {
    super.upload(req, res);
  };

  /**
   * @api {get} /media/ Get a media file
   * @apiName Get
   * @apiOrder Media
   *
   * @apiParam {String} filepath Filepath.
   *
   * @apiSuccess {String} status Response status.
   */
  get(req, res) {
    super.get(req, res);
  };

  /**
   * @api {get} /media/list List media file.
   * @apiName List
   * @apiOrder Media
   *
   * @apiParam {String} filepath Filepath.
   *
   * @apiSuccess {String[]} fileList List of files.
   */
  list(req, res) {
    super.list(req, res);
  }

  /**
   * @api {get} /media/search Search file.
   * @apiName Search
   * @apiOrder Media
   *
   * @apiParam {String} dir Directory to search in.
   * @apiParam {String} name Search name.
   *
   * @apiSuccess {String[]} fileList List of files (absolute path for bucket not system!).
   */
  search(req, res) {
    super.search(req, res);
  }

  /**
   * @api {delete} /media/ Delete a media file
   * @apiName Delete
   * @apiOrder Media
   *
   * @apiParam {String} path Directory or file path.
   *
   * @apiSuccess {String} status Response status.
   */
  remove(req, res) {
    super.search(req, res);
  }
}

module.exports = new MediaApi();
