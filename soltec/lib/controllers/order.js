'use strict';

const _                       = require('lodash'),
      Joi                     = require('joi'),
      EntityApi               = require('../../shared/abstract/entity-api'),
      FileSystemApi           = require('../../shared/abstract/fs-api'),
      LicenseApi              = require('../../shared/sdk/license'),
      SyncApiCls              = require('./sync').SyncApiCls,
      ExcelApiCls             = require('./excel').ExcelApiCls,
      DocxApiCls              = require('./docx').DocxApiCls,
      c                       = require('../configuration'),
      ms                      = require('../../shared/abstract/mail-api'),
      ORDER_DOC_TYPES         = require('../const').ORDER_DOC_TYPES,
      ORDER_STATE             = require('../const').ORDER_STATE,
      SHAREPOINT_CONF         = require('../const').SHAREPOINT_CONF,
      getEmailSubject         = require('../const').getEmailSubject,
      getEmailTemplate        = require('../const').getEmailTemplate,
      VALID_LANGS             = require('../const').VALID_LANGUAGES,
      EXCEL_TYPES             = require('../const').EXCEL_TYPES,
      SOLTEC_LOGO             = require('../const').SOLTEC_LOGO,
      BLOCKED_EMAIL_ADDRESSES = require('../const').BLOCKED_EMAIL_ADDRESSES,
      FsLogApi                = require('../../shared/abstract/fs-log-api'),
      moment                  = require('moment'),
      Readable                = require('stream').Readable,
      parseCurrency           = require('parsecurrency'),
      path                    = require('path'),
      request                 = require('request-promise');

const $group = require('../../shared/entities/group')(c),
      $user  = require('../../shared/entities/user')(c);

/*=====HANDLED ERRORS=====*/

/**
 * Here should be gathered all possible errors for this controller.
 * So we have easy overview what are possible errors for FE to handle.
 *
 * Note: EntityApi itself can produce some dynamic error codes:
 *  - <entityName>_<action>_fail -> for CRUD operations
 *  - <entityName>_invalid_schema
 *  - unknown_error
 *
 * @type {{[string]: {resStatusCode: number, resCode: string, resMessage: string}}}
 */
const ctrlErrors = {
  wrongStep             : {
    resStatusCode: 400,
    resCode      : 'order_wrong_step',
    resMessage   : 'Order is not in the correct step'
  },
  wrongStepCanNotCancel : {
    resStatusCode: 400,
    resCode      : 'order_wrong_step',
    resMessage   : 'Order can\'t be canceled any more.'
  },
  invalidUser           : {
    resStatusCode: 404,
    resCode      : 'invalid_user',
    resMessage   : 'Invalid user'
  },
  invalidCountry        : {
    resStatusCode: 400,
    resCode      : 'invalid_country',
    resMessage   : 'Invalid country'
  },
  orderNotPublic        : {
    resStatusCode: 400,
    resCode      : 'order_not_public',
    resMessage   : 'Order is not public'
  },
  orderAlreadyTransfered: { // Only for sales managers
    resStatusCode: 400,
    resCode      : 'order_already_transferred',
    resMessage   : 'Order already transferred from original owner'
  },
  accessDenied          : {
    resStatusCode: 401,
    resCode      : 'access_denied',
    resMessage   : 'Access denied'
  },
  invalidTransferType   : {
    resStatusCode: 401,
    resCode      : 'invalid_transfer_type',
    resMessage   : 'Invalid transfer type'
  }
};


/*=====ORDER STEPS=====*/
const step1_new       = 'S1';
const step2_confirmed = 'S2';
const step3           = 'S3';
const step4           = 'S4';

function getOrderStep(s) {
  return {
    'S0'     : 'Cancelled',
    'S1'     : 'Offer',
    'S2'     : 'Order',
    'S3'     : 'Accepted',
    'S4'     : 'Completed',
    undefined: ''
  }[s];
}

/*=====ORDER SCHEMA=====*/
// Note: you can also automate things further with https://github.com/yoitsro/joigoose but be careful not
// to put 'id' and things like that to schema.


const specifications = Joi.object().keys({
  // motorPosition:            Joi.object().keys({
  //   a: Joi.boolean(),
  //   b: Joi.boolean(),
  // }),
  motorPos     : Joi.string(),
  waterExit    : Joi.object().keys({
    a1    : Joi.boolean(),
    b1    : Joi.boolean(),
    a2    : Joi.boolean(),
    b2    : Joi.boolean(),
    a3    : Joi.boolean(),
    b3    : Joi.boolean(),
    a4    : Joi.boolean(),
    b4    : Joi.boolean(),
    custom: Joi.boolean()
  }),
  powerSupply  : Joi.object().keys({
    p1: Joi.boolean(),
    p2: Joi.boolean(),
    p3: Joi.boolean(),
    p4: Joi.boolean()
  }),
  wallFixations: Joi.object().keys({
    p1p3: Joi.boolean(),
    p2p4: Joi.boolean(),
    p1p2: Joi.boolean(),
    p3p4: Joi.boolean()
  }),

  mountingWallType           : Joi.string(),
  mountingGroundType         : Joi.string(),
  mountingGroundQuantity     : Joi.number().min(0),
  mountingGroundColorStandard: Joi.boolean(),

  bladesOpeningDirection: Joi.string(),

  ledLights  : Joi.boolean(),
  ledQuantity: Joi.number().min(0),
  ledLength  : Joi.number().min(0),
  ledType    : Joi.string(),

  ledPositions            : Joi.array().items(Joi.number()),
  customLedPositionEnabled: Joi.boolean(),
  customLedPosition       : Joi.number().min(0)
});

const positions = new Joi.object().keys({
  p1p2: Joi.boolean(),
  p3p4: Joi.boolean(),
  p1p3: Joi.boolean(),
  p2p4: Joi.boolean()
});

const poleData = new Joi.object().keys({
  p1      : Joi.boolean(),
  p2      : Joi.boolean(),
  p3      : Joi.boolean(),
  p4      : Joi.boolean(),
  p1y     : Joi.any().default(0),
  p2y     : Joi.any().default(0),
  p3y     : Joi.any().default(0),
  p4y     : Joi.any().default(0),
  p1height: Joi.number().min(0),
  p2height: Joi.number().min(0),
  p3height: Joi.number().min(0),
  p4height: Joi.number().min(0)
});

const additionalPoleData = new Joi.object().keys({
  position: Joi.string().required(),
  x       : Joi.number().min(0),
  height  : Joi.number().min(0)
});

const slidingPanelGroup = new Joi.object().keys({
  type         : Joi.string(),
  size         : Joi.number(),
  panelQuantity: Joi.number(),
  placement    : Joi.array().items(Joi.number()),
  arrowLeft    : Joi.string(),
  arrowRight   : Joi.string()
});

const glassPanelGroup = new Joi.object().keys({
  guides       : Joi.number(),
  size         : Joi.number(),
  panelQuantity: Joi.number(),
  placement    : Joi.array().items(Joi.number()),
  arrowLeft    : Joi.string(),
  arrowRight   : Joi.string(),
  locker       : Joi.boolean()
});

const options = new Joi.object().keys({
  blindsPosition              : positions,
  blindsFabric                : Joi.string(),
  blindsFabricSoltis          : Joi.boolean(),
  blindsFabricSoltisCode      : Joi.string().allow('').default(''),
  blindsCustomFabric          : Joi.boolean(),
  blindsCustomFabricID        : Joi.string(),
  blindsFullScreen            : Joi.boolean(),
  blindsGuideColorMatchesFrame: Joi.boolean(),
  blindsGuideColorCustomID    : Joi.number(),
  blindsFinishType            : Joi.string(),
  blindsGuideType             : Joi.string(),
  blindsMotorType             : Joi.string(),
  blindsPanelSize1            : Joi.number().min(0),
  blindsPanelSize2            : Joi.number().min(0),

  slidingPanelPosition     : positions,
  slidingPanelOnLength     : slidingPanelGroup,
  slidingPanelOnWidth      : slidingPanelGroup,
  slidingPanelMaterial     : Joi.string(),
  slidingPanelColor        : Joi.number(),
  slidingPanelCustomColor  : Joi.boolean(),
  slidingPanelCustomColorID: Joi.number(),
  slidingPanelFinishType   : Joi.string(),

  glassPanelsPosition: positions,
  glassPanelsOnLength: glassPanelGroup,
  glassPanelsOnWidth : glassPanelGroup
});

const accessories = new Joi.object().keys({
  // LED Lights
  betaQuantity: Joi.number().min(0),
  ledStrip    : Joi.number().min(0).max(1).default(0),

  // Remote controller
  additionalRemoteControl: Joi.number().min(0),

  // Heaters
  heaterQuantity: Joi.number().min(0),

  // Audio
  bluetoothSystem: Joi.number().min(0),
  speakers       : Joi.number().min(0),
  rcAudioSystem  : Joi.number().min(0),

  // Sensors
  windSensorQuantity       : Joi.number().min(0),
  rainSensorQuantity       : Joi.number().min(0),
  temperatureSensorQuantity: Joi.number().min(0),
  presenceSensorQuantity   : Joi.number().min(0),
  externalAntenna          : Joi.number().min(0),

  // Somfy
  somfyMotorQuantity: Joi.number().min(0),

  somfyRTST1TBQuantity : Joi.number().min(0),
  somfyRTST4TBQuantity : Joi.number().min(0),
  somfyRTST16TBQuantity: Joi.number().min(0),

  somfyBridge1Quantity: Joi.number().min(0),
  somfyBridge7Quantity: Joi.number().min(0),

  somfyRTST1Quantity : Joi.number().min(0),
  somfyRTST4Quantity : Joi.number().min(0),
  somfyRTST16Quantity: Joi.number().min(0),

  doubleMotor: Joi.number().min(0),

  // Transport
  transportBox1: Joi.number().min(0),
  transportBox2: Joi.number().min(0),
  transportBox3: Joi.number().min(0),

  // Extra
  sparePartsKitSL         : Joi.number().min(0),
  merbenitGlue            : Joi.number().min(0),
  butylTapeRoll           : Joi.number().min(0),
  heaterAnthraciteQuantity: Joi.number().min(0)
});

const product = Joi.object().keys({
  pergolaType     : Joi.string(),
  installationType: Joi.number().min(1).max(6),

  pergolaQuantity: Joi.number().min(1),

  width : Joi.number().min(0),
  length: Joi.number().min(0),
  height: Joi.number().min(0),

  numberOfPoles          : Joi.number().min(0).max(4),
  numberOfAdditionalPoles: Joi.number().min(0),
  poleData               : poleData,
  customPoleHeight       : Joi.boolean(),
  additionalPoleData     : Joi.array().items(additionalPoleData),

  nonStandardColorEnabled: Joi.boolean(),
  structureColor         : Joi.number(),
  bladesColor            : Joi.number(),
  structureFinishType    : Joi.string(),
  bladesFinishType       : Joi.string(),

  specifications: specifications,
  options       : options,
  accessories   : accessories
});

const schema = Joi.object().keys({
  id: [Joi.string(), Joi.object()], // our internal id

  owner             : [Joi.string(), Joi.object()],
  _originalOwnerId  : [Joi.string(), Joi.object()], // If distributor was changed, store the original owner id here
  _originalOwnerName: Joi.string(), // If distributor was changed, store the original owner name here

  number: Joi.string(), // order number
  cost  : [Joi.string(), Joi.object()],
  step  : Joi.string().alphanum().required(),
  status: Joi.string().allow('').optional(),
  sid   : Joi.number().integer(), // share point id

  client              : Joi.object().keys({
    name       : Joi.string(),
    address    : Joi.string(),
    phone      : Joi.string().allow('').allow(null).optional(),
    email      : Joi.string().email(),
    vat        : Joi.string().allow('').allow(null).optional(),
    countryCode: Joi.string().allow('').allow(null).optional() // Used for new configurator to store country code
  }),
  deliverySameAsClient: Joi.boolean().required(),

  sourceContactInfo: Joi.object().allow(null).optional(), // Source contact info object from new configurator

  delivery: Joi.object().keys({
    name   : Joi.string(),
    address: Joi.string(),
    phone  : Joi.string(),
    email  : Joi.string().email()
  }),

  deliveryCost: Joi.number(),
  vatRate     : Joi.number(),

  product: product,

  project: Joi.object().keys({
    name                 : Joi.string(),
    description          : Joi.string().optional().allow(''),
    date                 : Joi.string().allow('').optional().allow(null),
    requestedDeliveryDate: Joi.string().isoDate().optional().allow(null)
  }),

  comment: Joi.string().allow('').optional(),

  _state                   : Joi.string().allow('').optional(),
  _public                  : Joi.boolean().allow('').optional(),
  _salesManagerOverrideId  : Joi.string().allow('').optional(),
  _originalSalesManagerName: Joi.string().allow('').optional(),
  _originalSalesManagerId  : Joi.string().allow('').optional(),

  terms: Joi.object().keys({
    terms: Joi.string().allow('').optional(),
    agree: Joi.boolean()
  }),

  documents: Joi.array().items(
    Joi.object().keys({
      type    : Joi.string(),
      path    : Joi.string(),
      name    : Joi.string(),
      size    : Joi.number(),
      noPublic: Joi.boolean(),
      meta    : Joi.object().optional(),
      remote  : Joi.string().optional()
    })
  ).optional(),

  languageOverride: Joi.string().optional().allow([null, '']),
  discountOverride: Joi.number().optional().allow([null, '']),

  skipCustomerEmails: Joi.number().optional().allow([null, '']),

  fromNewConfigurator: Joi.boolean().optional().allow([null, '']),
  assemblyFee        : Joi.number().optional().allow([null, '']),

  distributor : Joi.string().optional().allow([null, '']),
  salesManager: Joi.string().optional().allow([null, '']),

  transaction: Joi.object().keys({
    id    : Joi.string().required(),
    date  : Joi.date().required(),
    amount: Joi.number().min(0).required(),
    status: Joi.string().required()
  }).optional()

}).with('number', 'id');


/*=====ORDER API=====*/
class OrderApi extends EntityApi {
  constructor() {
    super({
      entityCollectionName: 'orders',
      entityName          : 'order',
      entitiesName        : 'orders',
      entitySchema        : schema
    });

    /**
     * FileSystemApi
     * @type {FileSystemApi}
     */
    this.fs = new FileSystemApi('order');
    this.syncApi = new SyncApiCls(this);

    /**
     * @type {LicenseApi}
     */
    this.license = this.syncApi.license; // we steal it from sync ;)
    //this.license = new LicenseApi(c.get('authApi'));
    //this.license.setAuth(licenseConf.email, licenseConf.password);

    this.excelApi = new ExcelApiCls(this);
    this.docxApi  = new DocxApiCls(this);

    this.mailService = new ms();
  }

  async getAllUserChildrenAndItself(req) {
    return [req.user.uid, ...await this.getAllUserChildren(req, req.user.uid)];
  }

  async getAllUsersChildren(usersId) {
    let out           = [];
    let usersChildren = [];
    const users       = await this.license.getUsersInfo(usersId);
    for (let i = 0, len = users.length; i < len; i++) {
      const u = users[i];
      u.meta  = u.meta || {};
      const c = u.meta.children || [];

      if (c.length) {
        out = out.concat(c);
        if (u.meta.hasNestedChildren) { // note bug here hasNestedChildren is "false" <-- string  always true
          usersChildren = usersChildren.concat(c); // add to to do list so we can batch pull at the end
        }
      }
    }

    const childrenOfUsersChildren = usersChildren.length ? await this.getAllUsersChildren(usersChildren) : [];
    return out.concat(childrenOfUsersChildren);
  }

  async getAllUserChildren(req, userId) {
    return await this.getAllUsersChildren([userId]);

    // const u = await this.license.getUser(userId);
    // u.meta  =  u.meta || {};
    // let c =  u.meta.children || [];
    // let out = [];
    // if (c.length) {
    //   out = out.concat(c);
    //   if (u.meta.hasNestedChildren) { // only if hasNestedChildren is set we will go and make HTTP call for each user object
    //     out = out.concat(await this.getAllUsersChildren(c));
    //   }
    // }
    // return out;
  }

  async canUserAccessOrderId(req, orderId) {
    const o          = await this.$get(req.db, orderId);
    /**
     * User who created order. NOT sales manager!
     * @type {string}
     */
    const orderOwner = o.owner + '';
    if (req.user.grp === 'superadmin') return true;
    if (req.user.grp === 'user') return o.owner.equals(req.user.uid);
    if (o._salesManagerOverrideId) {
      return o._salesManagerOverrideId.equals(req.user.uid);
    }
    return await this.hasUserChild(req.user.uid, orderOwner);
  }

  async isOrderInProgress(req, orderId) {
    const o = await this.$get(req.db, orderId);
    return o._state === ORDER_STATE.inProgress;
  }

  async hasUserChild(userId, childId) {
    if (!childId) return false;
    if (userId === childId) return true;

    const u = await this.license.getUser(userId);
    // meta.parent => id
    // meta.children => [id's...]

    u.meta          = u.meta || {};
    u.meta.children = u.meta.children || [];

    for (let i = 0, len = u.meta.children.length; i < len; i++) {
      if (await this.hasUserChild(u.meta.children[i], childId)) {
        return true;
      }
    }

    return false;
  }

  /**
   * @api {post} /orders Add new order
   * @apiName Add
   * @apiOrder Orders
   *
   * @apiParam {String} name Order name.
   *
   * @apiSuccess {Object} order Created order.
   */
  async create(req, res) {
    const user          = await this.license.getUser(req.user.uid);
    const isPublicOrder = user.meta.public === 'true';

    if (isPublicOrder) {
      if (req.query.country) {
        req.user.meta.country = req.query.country;
      }
      if (req.query.source) {
        req.user.meta.source = req.query.source;
      }
      if (req.query.discount) {
        req.body.discountOverride = req.query.discount;
      }
      if (req.query.language) {
        req.body.languageOverride = req.query.language;
      }
      if (req.query.skipCustomerEmails) { // 1 to not send emails
        req.body.skipCustomerEmails = req.query.skipCustomerEmails;
      }
      if (req.query.nc) {
        req.body.fromNewConfigurator = true;
      }
      if (req.query.assemblyFee) {
        req.body.assemblyFee = parseFloat(req.query.assemblyFee);
      }
    }

    super.create(req, res);
  };

  /**
   * @param req
   * @param res
   * @param order Order
   * @returns {Promise.<*>}
   */
  async hook_beforeCreate(req, res, order) {
    let owner = await this.license.getUser(req.user.uid);

    order.owner = owner._id;

    const isPublicOrder = owner.meta.public === 'true';

    // Override order owner if this is a public order.
    if (isPublicOrder) {
      if (req.user.meta && req.user.meta.country) {
        let b2cDistributor;
        try {
          b2cDistributor = await this.license.getUserByMeta('b2c', req.user.meta.country);
        } catch (e) {
          return this.handleError(req, res, ctrlErrors.invalidCountry);
        }

        if (b2cDistributor) {
          owner       = b2cDistributor;
          order.owner = b2cDistributor._id;

          const distributorParent         = await this.getUserParent(req, b2cDistributor._id);
          order._originalSalesManagerName = `${distributorParent.firstName} ${distributorParent.lastName}`;
          order._originalSalesManagerId   = distributorParent._id;
        } else {
          return this.handleError(req, res, ctrlErrors.invalidCountry);
        }
      } else {
        return this.handleError(req, res, ctrlErrors.invalidCountry);
      }

      order._public = true;
    }

    // Set source
    if (isPublicOrder) {
      let source = SHAREPOINT_CONF.publicSourceId;
      if (req.user.meta && req.user.meta.source) {
        const sourceVal = parseInt(req.user.meta.source);
        if (!isNaN(sourceVal) && sourceVal >= 0) {
          source = sourceVal;
        }
      }

      order.spSource = source;
    }

    // Set client country if order is from the new configurator.
    if (order.fromNewConfigurator) {
      order.client.countryCode = req.query.country;
    }

    order.number       = await this.genOrderNumber(req, order);
    order.birth        = new Date();
    order.client.email = order.client.email.trim();
    order._state       = ORDER_STATE.inProgress;

    // Add discount, packaging & transport
    let children;
    if (req.user.grp === 'admin') {
      const childrenIds = await this.getAllUserChildren(req, req.user.uid);
      children          = await this.license.getUsersInfo(childrenIds);
    } else if (req.user.grp === 'superadmin') {
      children = await this.license.getUsers();
    } else {
      children = await this.license.getUsersInfo([req.user.uid]);
    }
    const foundClients = children.filter(x => {
      return x.meta && x.meta.spTitle && order.client.name && x.meta.spTitle.toLowerCase().trim() === order.client.name.toLowerCase().trim();
    });
    if (foundClients.length === 1) {
      order.discount    = (foundClients[0].meta.spRabatPergola || 0) + '%';
      order.discountVal = foundClients[0].meta.spRabatPergola ? Number(foundClients[0].meta.spRabatPergola.replace('\%', '').trim()) || 0 : 0;
      order.transport   = foundClients[0].meta.spTransport || 0;
      order.packaging   = foundClients[0].meta.spPackaging || 0;
    } else if (req.user.grp === 'user') {
      order.discount    = (owner.meta.spRabatPergola || 0) + '%';
      order.discountVal = owner.meta.spRabatPergola ? Number(owner.meta.spRabatPergola.replace('\%', '').trim()) || 0 : 0;
      order.transport   = owner.meta.spTransport || 0;
      order.packaging   = owner.meta.spPackaging || 0;
    } else {
      order.discount    = '0%';
      order.discountVal = 0;
      order.transport   = 0;
      order.packaging   = 0;
    }

    // Override disount if set in order
    if (order.discountOverride) {
      order.discount    = order.discountOverride + '%';
      order.discountVal = order.discountOverride;
    }

    await this.processOrderDocuments(req, res, order);

    return order;
  }

  async getUserParent(req, uid) {
    let u = await this.license.getUser(uid);
    if (u.meta && u.meta.parent) {
      return await this.getUserParent(req, u.meta.parent);
    }
    return u;
  }

  async genOrderNumber(req, order) {
    let u = await this.license.getUser(order.owner);

    if (u.meta && u.meta.parent) {
      u = await this.getUserParent(req, u.meta.parent);
    }

    u.meta            = u.meta || {};
    u.meta.orderCount = u.meta.orderCount || 0;
    u.meta.orderCount++;

    await this.license.updateUser(u);

    return `PA-${u.uniqueCode}-${('' + u.meta.orderCount).padStart(4, '0')}`;
  }

  /**
   * Action to take in place after order change.
   *
   */
  async onOrderChange(req, model) {
    const fsLogOrder = new FsLogApi([], { name: 'onChangeOrder.log' });
    const orderId    = model._id + '';
    const self       = this;

    // Sync before generating any documents
    try {
      await self.syncApi.$syncOrder(orderId);
      fsLogOrder.ok({ user: req && req.user, order: model, resSync: resSync });
    } catch (err) {
      fsLogOrder.error({ debugPoint: 0, error: err, user: req.user, order: model, model: model });
    }

    try {
      const [_distributor, _salesManager] = [model.distributor, model.salesManager];
      await this.linkDistributorsAndSalesManagers(model);
      if (model.distributor !== _distributor || model.salesManager !== _salesManager) {
        await super.$update(req.db, model);
      }

      try {
        if (req.user.grp === 'user') {
          await self.excelApi.$genExcel(req.db, orderId, EXCEL_TYPES.excelPublic, req);
          if (model.step !== 'S1') {
            await self.excelApi.$genExcel(req.db, orderId, EXCEL_TYPES.excelPrivate, req);
          }
        } else {
          await self.excelApi.$genExcel(req.db, orderId, EXCEL_TYPES.excelPrivate, req);
        }

        const order  = await self.$get(req.db, orderId);
        order._state = ORDER_STATE.ok;
        order.save((error, model) => {
          if (error) {
            fsLogOrder.error({ debugPoint: 1, user: req && req.user, order: order, error });
            throw error;
          }
        });
      } catch (e) {
        try {
          const order  = await self.$get(req.db, orderId);
          order._state = ORDER_STATE.failed;
          order.save();

          fsLogOrder.error({ debugPoint: 2, error: e, user: req.user, order: model, model: model });
        } catch (e) {
          fsLogOrder.error({ debugPoint: 3, error: e, user: req.user, order: model, model: model });
          self.reportError(e, { orderId });
          throw e;
        }

        fsLogOrder.error({ debugPoint: 4, error: e, user: req.user, order: model, model: model });
        self.reportError(e, { orderId });
        throw e;
      }
    } catch (e) {
      fsLogOrder.error({ debugPoint: 5, error: e, user: req && req.user, order: order });
    }

    // Sync again
    self.syncApi.$syncOrder(orderId).catch(err => {
      fsLogOrder.error({ debugPoint: 6, error: err, user: req.user, order: model, model: model });
    }).then(resSync => {
      fsLogOrder.ok({ user: req && req.user, order: model, resSync: resSync });
    });
  }

  objToResObj(obj) {
    // return this.validateSchema(obj).value;
    return obj;
  }

  async linkDistributorsAndSalesManagers(order) {
    if (order.owner) {
      const users = await this.license.getAllUsersInfo();

      // Find distributor & sales manager.
      const owner = users.find((x) => x._id === order.owner.toString());
      if (owner) {
        if (owner.role === 'user') {
          const sm           = users.find((y) => y._id === (order._salesManagerOverrideId ? order._salesManagerOverrideId.toString() : owner.meta.parent));
          order.distributor  = owner.firstName;
          order.salesManager = sm && (sm.firstName + ' ' + sm.lastName);
        } else if (owner.role === 'admin') {
          const dist         = users.find((y) => ((y.meta && y.meta.spTitle) || y.firstName).toLowerCase() === order.client.name.toLowerCase());
          order.distributor  = dist && ((dist.meta && dist.meta.spTitle) || dist.firstName);
          order.salesManager = owner.firstName + ' ' + owner.lastName;
        }
      }
    }

    order.distributor  = order.distributor || '';
    order.salesManager = order.salesManager || '';

    return order;
  }

  /**
   *
   * @param db
   * @param order
   * @param options
   * @param options.skipCreateOp
   * @return {*|Promise.<Mongoose.model>}
   */
  async $create(db, order) {
    order.modified = +new Date();
    return super.$create(db, order);
  }

  hook_createReqSuccess(req, res, model) {
    this.runPostCreateReqActions(req, res, model);
  }

  async runPostCreateReqActions(req, res, model) {
    const fsLogOrder = new FsLogApi([], { name: 'createOrder.log' });
    fsLogOrder.info({ user: req.user, order: model });

    const self = this;
    super.hook_createReqSuccess(req, res, model);

    // silent sync, no need to notify client
    this.onOrderChange(req, model)
      .catch(e => {
        fsLogOrder.error({ error: e, orderId: model._id, order: model, req });
      })
      .then(x => {
        self.$get(req.db, model._id).then(lastModel => {


          self.license.getUser(lastModel.owner)
            .then((user) => {
              if (!!user) {
                let attachments = [];
                for (let doc of lastModel.documents) {
                  if (doc.type === ORDER_DOC_TYPES.excelExportOneSheet) {
                    if (lastModel._public) {
                      if (!doc.noPublic) {
                        attachments.push({
                          filename: doc.name,
                          path    : 'storage/order/' + doc.path
                        });
                      }
                    } else {
                      attachments.push({
                        filename: doc.name,
                        path    : 'storage/order/' + doc.path
                      });
                    }
                  }
                }
                self.sendOrderStatusUpdateEmail(lastModel, attachments);

                if (lastModel._public) {
                  self.sendPublicOrderCreateEmail(lastModel, attachments);
                }
              }
            });
        })
          .catch(e => {
            super.hook_createReqSuccess(req, res, model);
            self.reportError(e, { orderId: model._id });
          });
      });
  }

  /**
   * @api {get} /orders/:id Request order information
   * @apiName Get
   * @apiOrder Orders
   *
   * @apiParam {String} id Unique order id.
   *
   * @apiSuccess {Object} order Order information.
   */
  get(req, res) {
    const self = this;
    this.canUserAccessOrderId(req, req.params.id)
      .then(access => {
        if (!access) {
          return res.send(403);
        }
        super.get(req, res);
      }).catch(e => {
      self.handleError(req, res, e);
    });
  };

  async hook_getReqSuccess(req, res, model) {
    if (typeof model.salesManager === 'undefined' && typeof model.distributor === 'undefined') {
      await this.linkDistributorsAndSalesManagers(model);
      super.$update(req.db, model);
    }
    super.hook_getReqSuccess(req, res, model);
  }

  /**
   * @api {get} /orders Request orders list
   * @apiName List
   * @apiOrder Orders
   *
   * @apiSuccess {Object[]} orders List of user orders
   */
  list(req, res) {
    try {
      super.list(req, res);
    } catch (e) {
      this.handleError(req, res, e);
    }
  }

  async hook_beforeList(req, res, options) {
    if (req.user.grp !== 'superadmin') {
      const c            = await this.getAllUserChildrenAndItself(req);
      const filterObject = {
        $or: [
          {
            $and: [
              { _salesManagerOverrideId: { $eq: null } },
              { owner: { $in: c } }
            ]
          },
          {
            $and: [
              { _salesManagerOverrideId: { $ne: null } },
              { _salesManagerOverrideId: { $eq: req.user.uid } }
            ]
          }
        ]
      };
      if (req.user.grp === 'user') {
        filterObject['$or'].push({
          owner: { $eq: req.user.uid }
        });
      }
      options.filter      = options.filter || {};
      options.filter.$and = options.filter.$and || [];
      options.filter.$and.push(filterObject);

      req.__userChildrenAndItself = c;
    }
  }

  async hook_listReqSuccess(req, res, result) {
    if (req.query.aggregate) {
      if (!result.data) {
        result.data = {};
      }
      result.data.stats = await this.getAggregateStats(req, res);
    }

    for (let order of result.items) {
      if (typeof order.salesManager === 'undefined' && typeof order.distributor === 'undefined') {
        await this.linkDistributorsAndSalesManagers(order);
        super.$update(req.db, order);
      }
    }

    super.hook_listReqSuccess(req, res, result);
  }

  /**
   * Aggregate order data.
   * @param db
   * @param filter filter object
   * @return {Promise.<Mongoose.model>}
   */
  $aggregate(db, filter) {
    return new Promise((resolve, reject) => {
      super.$list(db, { filter: filter })
        .then((result) => {
          let stats = {
            count  : result.items.length,
            amount : 0,
            step   : {},
            product: {}
          };

          result.items.forEach((x) => {
            stats.step[x.step]                   = (stats.step[x.step] || 0) + 1;
            stats.product[x.product.pergolaType] = (stats.product[x.product.pergolaType] || 0) + 1;

            stats.amount += x.costVal || 0;
          });

          resolve(stats);
        })
        .catch((e) => {
          reject(e);
        });
    });
  }

  async getAggregateStats(req, res, result) {
    let filter = {};
    if (req.query.filter) {
      filter.$and = [];
      super.populateFilterObject(filter.$and, req.query.filter);
    }
    if (req.query.search) {
      filter.$or = [];
      this.populateFilterObject(filter.$or, req.query.search);
    }

    if (req.user.grp !== 'superadmin') {
      filter.$and = filter.$and || [];
      filter.$and.push({
        $or: [
          {
            $and: [
              { _salesManagerOverrideId: { $eq: null } },
              { owner: { $in: req.__userChildrenAndItself } }
            ]
          },
          {
            $and: [
              { _salesManagerOverrideId: { $ne: null } },
              { _salesManagerOverrideId: { $eq: req.user.uid } }
            ]
          }
        ]
      });
    }

    return await this.$aggregate(req.db, filter);
  };


  /**
   * @api {get} /orders/aggregate Get statistics for all orders
   * @apiName Aggregate
   * @apiOrder Orders
   *
   * @apiSuccess {Object} orders Aggregated data
   */
  aggregate(req, res) {
    throw Error('Not implemented!');
  }

  //   const self = this;
  //
  //   let filter = {};
  //   if (req.query.filter) {
  //     try {
  //       filter.$and = []; super.populateFilterObject(filter.$and, req.query.filter);
  //     } catch (e) {
  //       return super.hook_listReqFail(req, res, e);
  //     }
  //   }
  //
  //   if (req.user.grp !== 'superadmin') {
  //     filter.$and = filter.$and || [];
  //     filter.$and.push({
  //       owner: {$in: req.__userChildrenAndItself}
  //     });
  //   }
  //
  //   this.$aggregate(req.db, filter)
  //     .then((stats) => {
  //       self.hook_aggregateReqSuccess(req, res, stats);
  //     })
  //     .catch((e) => {
  //       self.hook_aggregateReqFail(req, res, e);
  //     });
  // }

  /**
   * @api {put} /orders/:id Edit existing order
   * @apiName Edit
   * @apiOrder Orders
   *
   * @apiParam {String} id Unique order id.
   *
   * @apiSuccess {Object} order Updated order information.
   */
  update(req, res) {
    const self = this;
    this.canUserAccessOrderId(req, req.body.id)
      .then(access => {
        if (!access) {
          return res.send(403);
        }

        this.isOrderInProgress(req, req.body.id)
          .then(inProgress => {
            if (inProgress && req.user.grp !== 'superadmin') {
              return res.send(409);
            } else {
              super.update(req, res);
            }
          })
          .catch(e => {
            self.handleError(req, res, e);
          });
      }).catch(e => {
      self.handleError(req, res, e);
    });
  }

  $update(db, order, options = {}) {
    order.modified = +new Date();
    if (!options.ignoreState) {
      order._state = ORDER_STATE.inProgress;
    }
    return super.$update(db, order);
  }

  async hook_beforeUpdate(req, res, order) {
    await this.processOrderDocuments(req, res, order);
    return order;
  }

  hook_updateReqSuccess(req, res, model) {
    const fsLogOrder = new FsLogApi([], { name: 'updateOrder.log' });
    fsLogOrder.info({ user: req.user, order: model });

    const self = this;
    super.hook_updateReqSuccess(req, res, model);

    // silent sync, no need to notify client
    this.onOrderChange(req, model)
      .catch(e => {
        fsLogOrder.error({ error: e, orderId: model._id, order: model });
      })
      .then(x => {
        self.$get(req.db, model._id).then(lastModel => {

        }).catch(e => {
          self.reportError(e, { orderId: model._id });
        });
      });
  }


  /**
   * @api {delete} /orders/:id Delete existing order
   * @apiName Delete
   * @apiOrder Orders
   *
   * @apiParam {String} id Unique order id.
   *
   * @apiSuccess {String} status Response status.
   */
  remove(req, res) {
    if (req.user.grp !== 'superadmin') {
      return res.send(403);
    }
    const self = this;
    this.canUserAccessOrderId(req, req.params.id)
      .then(access => {
        if (!access) {
          return res.send(403);
        }
        const fsLogOrder = new FsLogApi([], { name: 'removeOrder.log' });
        fsLogOrder.info({ user: req.user, order: req.params.id });
        super.remove(req, res);
      }).catch(e => {
      self.handleError(req, res, e);
    });
  }

  hook_removeReqSuccess(req, res, id) {
    super.hook_removeReqSuccess(req, res, id);

    // silent sync, no need to notify client
    this.syncApi.$syncOrder(id);
  }

  /**
   * @api {put} /orders/:id/confirm Confirm existing order
   * @apiName Confirm
   * @apiOrder Orders
   *
   * @apiParam {String} id Unique order id.
   *
   * @apiSuccess {String} status Response status.
   */
  confirm(req, res) {
    const self = this;

    this.canUserAccessOrderId(req, req.params.id)
      .then(access => {
        if (!access) {
          return res.send(403);
        }

        super.$get(req.db, req.params.id)
          .then(model => {
            if (model.step !== step1_new) {
              return self.handleError(req, res, ctrlErrors.wrongStep);
            }
            model.step   = step2_confirmed;
            model._state = ORDER_STATE.inProgress;
            model.save((error, model) => {
              if (error) {
                self.hook_updateReqFail(req, res, e);
              } else {
                self.hook_updateReqSuccess(req, res, model);
              }

              // sync will be handled by updateReqSuccess hook above

              // send email
              let attachments = [];
              for (let doc of model.documents) {
                if (doc.type === ORDER_DOC_TYPES.excelExportOneSheet && !doc.noPublic) {
                  attachments.push({
                    filename: doc.name,
                    path    : 'storage/order/' + doc.path
                  });
                }
              }

              self.sendOrderStatusUpdateEmail(model, attachments);
            });
          })
          .catch(e => {
            super.hook_getReqFail(req, res, e);
          });
      }).catch(e => {
      self.handleError(req, res, e);
    });
  }

  /**
   * @api {put} /orders/:id/cancel Cancel existing order
   * @apiName Cancel
   * @apiOrder Orders
   *
   * @apiParam {String} id Unique order id.
   *
   * @apiSuccess {String} status Response status.
   */
  cancel(req, res) {
    const self = this;

    this.canUserAccessOrderId(req, req.params.id)
      .then(access => {
        if (!access) {
          return res.send(403);
        }

        super.$get(req.db, req.params.id)
          .then(model => {
            if (model.step === 'S0') {
              super.hook_updateReqSuccess(req, res, model);
              return;
            }

            const fsLogOrder = new FsLogApi([], { name: 'cancelOrder.log' });
            fsLogOrder.info({ user: req.user, order: model });

            if (!(model.step === step1_new || model.step === step2_confirmed || model.step === step3)) {
              return self.handleError(req, res, ctrlErrors.wrongStepCanNotCancel);
            }
            model.step = 'S0';
            model.save((error, model) => {
              if (error) {
                self.hook_updateReqFail(req, res, e);
              } else {
                self.hook_updateReqSuccess(req, res, model);

                // sync will be handled by updateReqSuccess hook above

                // send mail
                self.sendOrderStatusUpdateEmail(model);
              }
            });
          })
          .catch(e => {
            super.hook_getReqFail(req, res, e);
          });
      }).catch(e => {
      self.handleError(req, res, e);
    });
  }

  /**
   * @api {put} /orders/:id/transfer Transfer an order to another sales manager or distributor (public orders only)
   * @apiName Transfer
   * @apiOrder Orders
   *
   * @apiParam {String} id Unique order id.
   * @apiParam {String} newUserData New user data
   * @apiParam {String} newUserData.type Type of transfer, 'salesManager' or 'distributor'
   * @apiParam {String} newUserData.userId User id you want to transfer to
   *
   * @apiSuccess {String} status Response status.
   */
  transfer(req, res) {
    const self = this;

    this.canUserAccessOrderId(req, req.params.id)
      .then(access => {
        if (!access) {
          return res.send(403);
        }

        super.$get(req.db, req.params.id)
          .then(async (model) => {
            // Order must be public
            if (!model._public) {
              return self.handleError(req, res, ctrlErrors.orderNotPublic);
            }

            // Distributers are not allowed access
            const thisUser = await this.license.getUser(req.user.uid);
            if (thisUser.role === 'user') {
              return self.handleError(req, res, ctrlErrors.accessDenied);
            }

            // Only allow transfers in S1 and S2
            if (model.step !== step1_new && model.step !== step2_confirmed) {
              return self.handleError(req, res, ctrlErrors.wrongStep);
            }

            const newUser = await this.license.getUser(req.body.userId);
            // Check if new user is valid
            if (!newUser) {
              return self.handleError(req, res, ctrlErrors.invalidUser);
            }

            const fsLogOrder = new FsLogApi([], { name: 'transferOrder.log' });
            fsLogOrder.info({ user: req.user, newUser, order: model });

            // Transfer based on type
            try {
              switch (req.body.type) {
                case 'salesManager':
                  await this.transferSalesManager(req, res, model, thisUser, newUser);
                  break;
                case 'distributor':
                  await this.transferDistributor(req, res, model, thisUser, newUser);
                  break;
                default:
                  return self.handleError(req, res, ctrlErrors.invalidTransferType);
              }
            } catch (e) {
              return self.handleError(req, res, e);
            }

            // Re-link and save
            await this.linkDistributorsAndSalesManagers(model);

            model.save((error, model) => {
              if (error) {
                super.hook_updateReqFail(req, res, error);
              } else {
                super.hook_updateReqSuccess(req, res, model);
              }
            });
          })
          .catch(e => {
            super.hook_getReqFail(req, res, e);
          });
      }).catch(e => {
      self.handleError(req, res, e);
    });
  }

  // Private
  async transferSalesManager(req, res, order, requestingUser, newUser) {
    // Limit to only one transfer, except if requested by super admin
    /*
     if (order._salesManagerOverrideId && requestingUser.role !== 'superadmin') {
     throw ctrlErrors.orderAlreadyTransfered;
     }
     */

    // Transfer the order, or clear the override filed if the new user equals the original user
    //order._salesManagerOverrideId = order._originalSalesManagerId && order._originalSalesManagerId.equals(newUser._id) ? undefined : newUser._id;
    order._salesManagerOverrideId = newUser._id;
  }

  // Private
  async transferDistributor(req, res, order, requestingUser, newUser) {
    // Save original owner
    if (!order._originalOwnerId) {
      const originalOwnerUser  = await this.license.getUser(order.owner);
      const originalOwnerName  = originalOwnerUser.role === 'user' ? originalOwnerUser.firstName : `${originalOwnerUser.firstName} ${originalOwnerUser.lastName}`;
      order._originalOwnerId   = order.owner;
      order._originalOwnerName = originalOwnerName;
    }

    // Transfer the order, or reset the owner back if the new owner matches the original owner
    /*
     if (order._originalOwnerId && order._originalOwnerId.equals(newUser._id)) {
     order._originalOwnerId   = undefined;
     order._originalOwnerName = undefined;
     }
     */
    order.owner = newUser._id;
  }


  /**
   * @api {get} /orders/exportSearch Export orders list as an excel file
   * @apiName Export Search
   * @apiOrder Orders
   */
  async exportSearch(req, res) {
    const requestingUser = await this.license.getUser(req.user.uid);

    if (requestingUser.role !== 'superadmin') {
      return this.handleError(req, res, ctrlErrors.accessDenied);
    }

    try {
      const self = this;

      let filter = {};
      if (req.query.filter) {
        try {
          filter.$and = [];
          this.populateFilterObject(filter.$and, req.query.filter);
        } catch (e) {
          return self.hook_listReqFail(req, res, e);
        }
      }
      if (req.query.search) {
        try {
          filter.$or = [];
          this.populateFilterObject(filter.$or, req.query.search);
        } catch (e) {
          return self.hook_listReqFail(req, res, e);
        }
      }
      const options = {
        filter   : filter,
        offset   : req.query.offset,
        page     : req.query.page,
        limit    : req.query.limit,
        sort     : req.query.sort,
        sortOrder: req.query.sortOrder
      };

      await this.hook_beforeList(req, res, options);

      this.$list(req.db, options)
        .then(async (results) => {
          const data = await this.excelApi.$genSearchResultsExport(requestingUser, results.items);

          self.hook_exportSearchReqSuccess(req, res, data);
        })
        .catch(function (e) {
          self.hook_exportSearchReqFail(req, res, e);
        });
    } catch (e) {
      this.handleError(req, res, e);
    }
  }

  hook_exportSearchReqSuccess(req, res, data) {
    const filename = `SearchExport_${moment().format('YYYY-MM-DD-kkmmss')}.xlsx`;

    const buffer = new Buffer(data, 'binary');
    res.attachment(filename).send(buffer);
  }

  hook_exportSearchReqFail(req, res, e) {
    e.resCode    = e.resCode || `${this.entityName}_export_search_fail`;
    e.resMessage = e.resMessage || `Can't export search for ${this.entityName}.`;
    this.handleError(req, res, e);
  }

  /**
   * @api {get} /orders/suggestions/clients Typeahead - Clients
   * @apiName Suggestions/Clients
   * @apiOrder Orders
   *
   * @apiSuccess {String} status Response status.
   */
  async suggestClients(req, res) {
    try {
      if (req.user.grp === 'admin' || req.user.grp === 'superadmin') {
        // Suggest distributors
        //return this.hook_suggestClientsFail(req, res, 'Not allowed');
        let childrenData;
        if (req.user.grp === 'superadmin') {
          childrenData = await this.license.getAllUsersInfo();
          childrenData = childrenData.filter((x) => x.role === 'user');
        } else {
          const children = await this.getAllUserChildren(req, req.user.uid);
          childrenData   = await this.license.getUsersInfo(children);
        }

        return this.hook_suggestClientsSuccess(req, res, _.map(childrenData, x => {
          let address = (x.meta.spAddress || x.lastName || '').trim();
          if (address.indexOf('\'') === 0) {
            address = address.slice(1);
          }
          if (address.lastIndexOf('\'') === address.length - 1) {
            address = address.slice(0, address.length - 2);
          }
          return {
            name      : x.meta.spTitle, // x.firstName,
            address   : address,
            country   : x.meta.spCountry,
            postNumber: x.meta.spPostNumber,
            city      : x.meta.spCity,
            phone     : x.meta.spPhone,
            spId      : x.meta.spId,
            spTitle   : x.meta.spTitle,
            email     : x.email,
            vat       : x.meta.vatId
          };
        }));
      } else if (req.user.grp === 'user') {
        // Suggest clients
        let clients = [];

        const user = await this.license.getUser(req.user.uid);

        let address = (user.meta.spAddress || user.lastName || '').trim();
        if (address.indexOf('\'') === 0) {
          address = address.slice(1);
        }
        if (address.lastIndexOf('\'') === address.length - 1) {
          address = address.slice(0, address.length - 2);
        }

        clients.push({
          client: {
            name      : user.meta.spTitle,
            address   : address,
            country   : user.meta.spCountry,
            postNumber: user.meta.spPostNumber,
            city      : user.meta.spCity,
            phone     : user.meta.spPhone,
            spId      : user.meta.spId,
            spTitle   : user.meta.spTitle,
            email     : user.email,
            vat       : user.meta.vatId
          }
        });

        const options = {};
        await this.hook_beforeList(req, res, options);

        clients = clients.concat((await this.$list(req.db, options)).items);

        return this.hook_suggestClientsSuccess(req, res, _.uniqBy(clients.map(x => {
          return x.client;
        }), x => x.name.trim()));
      }
    } catch (e) {
      this.hook_suggestClientsFail(req, res, e);
    }
  }

  hook_suggestClientsSuccess(req, res, clients) {
    res.send({ clients: clients || [] });
  }

  hook_suggestClientsFail(req, res, err) {
    let e = {
      resCode   : 'suggest_clients_fail',
      resMessage: `Can't suggest clients`,
      message   : err
    };
    super.handleError(req, res, e);
  }


  /**
   * @api {get} /orders/suggestions/distributors Typeahead - Distributors
   * @apiName Suggestions/Distributors
   * @apiOrder Orders
   *
   * @apiSuccess {String} status Response status.
   */
  async suggestDistributors(req, res) {
    try {
      if (req.user.grp !== 'admin' && req.user.grp !== 'superadmin') {
        //return this.hook_suggestClientsFail(req, res, 'Not allowed');
        return this.hook_suggestDistributorsSuccess(req, res, []); // return success with an empty clients list
      }

      let childrenData;
      if (req.user.grp === 'admin') {
        const children = await this.getAllUserChildren(req, req.user.uid);
        childrenData   = await this.license.getUsersInfo(children);
      } else {
        childrenData = await this.license.getAllUsersInfo();
        childrenData = childrenData.filter((x) => x.role === 'user' && !!x.active);
      }

      return this.hook_suggestDistributorsSuccess(req, res, _.map(childrenData, x => {
        let name = (x.meta.spTitle || x.firstName);
        if (name) {
          name.replace(/[^\s]+/g, function (word) {
            return word.replace(/^./, function (first) {
              return first.toUpperCase();
            });
          });
          name = name.trim();
        }

        return {
          name   : name,
          country: x.meta.spCountry,
          address: x.meta.spAddress,
          phone  : x.meta.spPhone,
          email  : x.meta.spEmailAddress1,
          vat    : x.meta.vatId,
          status : x.meta.spStatus
        };
      }));
    } catch (e) {
      this.hook_suggestDistributorsFail(req, res, e);
    }
  }

  hook_suggestDistributorsSuccess(req, res, distributors) {
    res.send({ distributors: distributors || [] });
  }

  hook_suggestDistributorsFail(req, res, err) {
    let e = {
      resCode   : 'suggest_distributors_fail',
      resMessage: `Can't suggest distributors`,
      message   : err
    };
    super.handleError(req, res, e);
  }

  /**
   * @api {get} /orders/listSalesManagers List sales managers to whom the user can transfer the order to
   * @apiName Suggestions/Sales Managers
   * @apiOrder Orders
   *
   * @apiSuccess {String} status Response status.
   */
  async listSalesManagers(req, res) {
    try {
      if (req.user.grp !== 'admin' && req.user.grp !== 'superadmin') {
        //return this.hook_suggestClientsFail(req, res, 'Not allowed');
        return this.hook_listSalesManagersSuccess(req, res, []); // return success with an empty clients list
      }

      let childrenData = await this.license.getAllUsersInfo();
      childrenData     = childrenData.filter((x) => x.role === 'admin' && !!x.active);

      return this.hook_listSalesManagersSuccess(req, res, _.map(childrenData, x => {
        return {
          id       : x._id,
          firstName: x.firstName,
          lastName : x.lastName
        };
      }));
    } catch (e) {
      this.hook_listSalesManagersFail(req, res, e);
    }
  }

  hook_listSalesManagersSuccess(req, res, salesManagers) {
    res.send({ salesManagers: salesManagers || [] });
  }

  hook_listSalesManagersFail(req, res, err) {
    let e = {
      resCode   : 'list_sales_managers_fail',
      resMessage: `Can't list sales managers`,
      message   : err
    };
    super.handleError(req, res, e);
  }

  /**
   * @api {get} /orders/listDistributors List distributors to whom the user can transfer the order to
   * @apiName Suggestions/Distributors
   * @apiOrder Orders
   *
   * @apiSuccess {String} status Response status.
   */
  async listDistributors(req, res) {
    try {
      if (req.user.grp !== 'admin' && req.user.grp !== 'superadmin') {
        //return this.hook_suggestClientsFail(req, res, 'Not allowed');
        return this.hook_listDistributorsSuccess(req, res, []); // return success with an empty clients list
      }

      let childrenData;
      if (req.user.grp === 'admin') {
        const children = await this.getAllUserChildren(req, req.user.uid);
        childrenData   = await this.license.getUsersInfo(children);
      } else {
        childrenData = await this.license.getAllUsersInfo();
        childrenData = childrenData.filter((x) => x.role === 'user' && !!x.active);
      }
      childrenData = childrenData.filter(x => x.active);

      return this.hook_listDistributorsSuccess(req, res, _.map(childrenData, x => {
        return {
          id       : x._id,
          firstName: x.firstName,
          lastName : x.lastName
        };
      }));
    } catch (e) {
      this.hook_listDistributorsFail(req, res, e);
    }
  }

  hook_listDistributorsSuccess(req, res, distributors) {
    res.send({ distributors: distributors || [] });
  }

  hook_listDistributorsFail(req, res, err) {
    let e = {
      resCode   : 'list_distributors_fail',
      resMessage: `Can't list distributors`,
      message   : err
    };
    super.handleError(req, res, e);
  }


  /**
   * @api {get} /orders/suggestions/salesManagers Typeahead - Sales managers
   * @apiName Suggestions/Sales Managers
   * @apiOrder Orders
   *
   * @apiSuccess {String} status Response status.
   */
  async suggestSalesManagers(req, res) {
    try {
      if (req.user.grp !== 'superadmin') {
        //return this.hook_suggestClientsFail(req, res, 'Not allowed');
        return this.hook_suggestSalesManagersSuccess(req, res, []); // return success with an empty clients list
      }

      let childrenData = await this.license.getAllUsersInfo();
      childrenData     = childrenData.filter((x) => x.role === 'admin' && !!x.active);

      return this.hook_suggestSalesManagersSuccess(req, res, _.map(childrenData, x => {
        return {
          name: x.firstName + ' ' + x.lastName
        };
      }));
    } catch (e) {
      this.hook_suggestSalesManagersFail(req, res, e);
    }
  }

  hook_suggestSalesManagersSuccess(req, res, salesManagers) {
    res.send({ salesManagers: salesManagers || [] });
  }

  hook_suggestSalesManagersFail(req, res, err) {
    let e = {
      resCode   : 'suggest_sales_managers_fail',
      resMessage: `Can't suggest sales managers`,
      message   : err
    };
    super.handleError(req, res, e);
  }

  /**
   * Send public order create email.
   * @param order {Order} The order model.
   */
  async sendPublicOrderCreateEmail(order, attachments = []) {
    if (order.skipCustomerEmails) {
      return;
    }

    const targetEmail = order.client.email;
    if (BLOCKED_EMAIL_ADDRESSES.find(x => x.trim() === targetEmail.trim())) {
      console.log(`Blocked email to '${targetEmail}'`);
      return false;
    }

    const user = await this.license.getUser(order.owner);

    const lang = order.languageOverride || user.language;

    const logo = SOLTEC_LOGO;

    this.mailService.sendMail({
      from       : c.get('appMail'),
      to         : order.client.email,
      subject    : getEmailSubject('orderPublicCreate', lang)(),
      template   : getEmailTemplate('lib/templates/order-public-create-mail', lang),
      data       : {
        user     : {
          mailTemplate: {
            color      : '#3e454d',
            projectName: 'Soltec Portal',
            logo       : logo
          },
          name        : order.client.name
        },
        order    : order,
        stepName : getOrderStep(order.step),
        link     : c.get('webApp'),
        orderLink: c.get('webApp') + '/order/' + order._id.toString()
      },
      attachments: attachments
    });
  }

  /**
   * Send order status update email.
   * @param order {Order} The order model.
   * @param attachments {[ ... ]} Array of attachments, see mail API for more info.
   */
  async sendOrderStatusUpdateEmail(order, attachments = []) {
    const user          = await this.license.getUser(order.owner);
    const isDistributor = user.role === 'user';
    const isPublicUser  = order._public;

    const lang = order.languageOverride || user.language;

    const logo = SOLTEC_LOGO;

    // Send email to order creator
    if (!isPublicUser) {
      this.mailService.sendMail({
        from       : c.get('appMail'),
        to         : user.email,
        subject    : (order.step === 'S1') ?
          getEmailSubject('orderStatusNew', lang)({ orderNumber: order.number, orderStep: getOrderStep(order.step) }) :
          getEmailSubject('orderStatusUpdate', lang)({
            orderNumber: order.number,
            orderStep  : getOrderStep(order.step)
          }),
        template   : getEmailTemplate('lib/templates/order-status-mail', lang),
        data       : {
          user     : {
            mailTemplate: {
              color      : '#3e454d',
              projectName: 'Soltec Portal',
              logo       : logo
            }
          },
          newOrder : (order.step === 'S1'),
          order    : order,
          stepName : getOrderStep(order.step),
          link     : c.get('webApp'),
          orderLink: c.get('webApp') + '/order/' + order._id.toString()
        },
        attachments: attachments
      });
    }

    // Send email to sales manager
    if (isDistributor && (user.meta.parent || order._salesManagerOverrideId) && (order.step === 'S1' || order.step === 'S2' || order.step === 'S0')) {
      try {
        const salesManager = await this.license.getUser(order._salesManagerOverrideId ? order._salesManagerOverrideId.toString() : user.meta.parent);

        const creatorFullName = user.firstName;

        const mailOptions = {
          from       : c.get('appMail'),
          to         : salesManager.email,
          subject    : (order.step === 'S1') ?
            getEmailSubject('orderStatusSMNew', lang)({
              orderNumber: order.number,
              orderStep  : getOrderStep(order.step),
              creatorFullName
            }) :
            getEmailSubject('orderStatusSMUpdate', lang)({
              orderNumber: order.number,
              orderStep  : getOrderStep(order.step),
              creatorFullName
            }),
          template   : getEmailTemplate('lib/templates/order-status-mail-salesmanager', lang),
          data       : {
            user           : {
              mailTemplate: {
                color      : '#3e454d',
                projectName: 'Soltec Portal',
                logo       : logo
              }
            },
            newOrder       : (order.step === 'S1'),
            order          : order,
            stepName       : getOrderStep(order.step),
            link           : c.get('webApp'),
            orderLink      : c.get('webApp') + '/order/' + order._id.toString(),
            creatorFullName: creatorFullName
          },
          attachments: attachments
        };

        if (isPublicUser) {
          mailOptions.cc = 'portal@soltec.si, backoffice@soltec.si';
        }

        this.mailService.sendMail(mailOptions);
      } catch (e) {
        this.reportError(e);
      }
    }
  }

  /**
   * @param req
   * @param orderId {string} if NULL check will be skipped!
   * @param docMeta {{path, type, meta}}
   * @return {Promise.<Boolean>}
   */
  async canAccessFile(req, orderId, docMeta) {
    if (req.user.grp === 'superadmin') return true;

    if (orderId !== null) {
      if (!await this.canUserAccessOrderId(req, orderId)) return false;
    }

    return [ORDER_DOC_TYPES.excelExportOneSheet].indexOf(docMeta.type) > -1;
  }

  getSpFile(req, res) {
    const self     = this;
    const fileName = req.query.file;
    const orderId  = req.params.id;

    this.canUserAccessOrderId(req, orderId)
      .then(() => {
        req.query.filePath = `${orderId}/download/${fileName}`;
        self.fs.get(req, res);
      })
      .catch(e => {
        res.send(403);
      });
  }

  getDoc(req, res) {
    const self = this;
    const path = req.query.file;

    this.$get(req.db, req.params.id)
      .then(async order => {
        const docMeta = _.find(order.documents || [], x => x.path === path);
        if (!docMeta) {
          return res.send(404);
        }

        if (await self.canAccessFile(req, req.params.id, docMeta)) {
          req.query.filePath = `${req.query.file}`;
          self.fs.get(req, res);
        } else {
          res.send(403);
        }
      })
      .catch(e => {
        this.handleError(req, res, e);
      });
  }

  uploadDoc(req, res) {
    const self = this;
    throw Error('Not implemented!');

    this.fs.$upload(req.files, 'file', `${req.params.id}/${req.query.name}`)
      .then(() => {
        self.$get(req.db, req.params.id)
          .then(model => {
            const index = model.documents.indexOf(req.params.file);
            if (index === -1) {
              model.documents.push(req.query.name);
            }

            model.save(err => {
              if (err) {
                super.handleError(req, res, err);
              } else {
                res.send();
              }
            });
          })
          .catch(e => {
            super.handleError(req, res, e);
          });
      })
      .catch(e => {
        this.handleError(req, res, e);
      });
  }

  deleteDoc(req, res) {
    const self = this;
    throw Error('Not implemented!');

    this.fs.$remove(`${req.params.id}/${req.query.name}`)
      .then(() => {
        self.$get(req.db, req.params.id)
          .then(model => {
            model.documents = _.without(model.documents, req.query.name);

            model.save(err => {
              if (err) {
                super.handleError(req, res, err);
              } else {
                res.send();
              }
            });
          })
          .catch(e => {
            super.handleError(req, res, e);
          });
      })
      .catch(e => {
        this.handleError(req, res, e);
      });
  }

  async processOrderDocuments(req, res, order) {
    if (order.documents && Array.isArray(order.documents)) {
      for (const doc of order.documents) {
        if (doc.remote) {
          if (!doc.name) {
            throw new Error('No file name provided!');
          }

          const data = await this.fetchRemoteDocument(order.id, doc.remote, doc.name);
          doc.type   = data.type;
          doc.path   = data.path;
          doc.name   = data.name;
          doc.meta   = data.meta;

          delete doc.remote;
        }
      }
    }
  }

  async fetchRemoteDocument(orderId, remoteUrl, filename) {
    const options = {
      method                 : 'GET',
      url                    : remoteUrl,
      resolveWithFullResponse: true,
      encoding               : null
    };

    console.log(`Fetching remote document: ${remoteUrl}`);
    const res = await request(options);
    if (res.statusCode !== 200) {
      throw new Error(`Can't get document: ${res.statusCode} - ${res.statusMessage}`);
    }
    const filepath = `${orderId}${path.sep}remote_documents${path.sep}${filename}`;

    await this.fs.$write(filepath, res.body, { encoding: 'binary' });

    return {
      path: filepath,
      type: ORDER_DOC_TYPES.generic,
      name: filename,
      meta: {
        birth: new Date()
      }
    };
  }
}


process.on('unhandledRejection', (reason, p) => {
  console.log('Unhandled Rejection at: Promise', p, 'reason:', reason.stack);
  // application specific logging, throwing an error, or other logic here
});

const orderApi             = new OrderApi();
module.exports.orderApi    = orderApi;
module.exports.syncApi     = orderApi.syncApi;
module.exports.excelApi    = orderApi.excelApi;
module.exports.docxApi     = orderApi.docxApi;
module.exports.OrderApiCls = OrderApi;

/**
 * @typedef  {Object} Order
 * @property {String} _id
 * @property {Object} deletedAt
 * @property {Boolean} deleted
 * @property {String} updatedAt
 * @property {String} createdAt
 * @property {String} number
 * @property {String} step
 * @property {Boolean} deliverySameAsClient
 * @property {Boolean} proformaEmailConfirmation
 * @property {Object} product
 * @property {String} comment
 * @property {String} owner
 * @property {String} birth
 * @property {Number} modified
 * @property {Number} __v
 * @property {String} cost
 * @property {Number} costVal
 * @property {Object[]} documents
 * @property {[]} spFiles
 * @property {Object} terms
 * @property {Object} project
 * @property {Object} client
 * @property {String} id
 */

/**
 * @typedef  {Object} deletedAt
 */

/**
 * @typedef  {Object} product
 * @property {String} pergolaType
 * @property {Number} installationType
 * @property {Number} width
 * @property {Number} length
 * @property {Number} height
 * @property {Number} numberOfPoles
 * @property {Number} numberOfAdditionalPoles
 * @property {Object} poleData
 * @property {Boolean} customPoleHeight
 * @property {Boolean} nonStandardColorEnabled
 * @property {String} structureColor
 * @property {String} bladesColor
 * @property {Object} specifications
 * @property {Object} options
 * @property {Object} accessories
 * @property {String} _id
 * @property {[]} additionalPoleData
 */

/**
 * @typedef  {Object} poleData
 * @property {Boolean} p1
 * @property {Boolean} p2
 * @property {Boolean} p3
 * @property {Boolean} p4
 * @property {Number} p1height
 * @property {Number} p2height
 * @property {Number} p3height
 * @property {Number} p4height
 * @property {String} _id
 */

/**
 * @typedef  {Object} specifications
 * @property {String} motorPos
 * @property {String} mountingWallType
 * @property {String} mountingGroundType
 * @property {Number} mountingGroundQuantity
 * @property {String} bladesOpeningDirection
 * @property {Number} ledLength
 * @property {String} ledType
 * @property {String} _id
 * @property {[]} ledPositions
 * @property {Object} powerSupply
 * @property {Object} waterExit
 */

/**
 * @typedef  {Object} powerSupply
 * @property {Boolean} p1
 * @property {Boolean} p2
 * @property {Boolean} p3
 * @property {Boolean} p4
 */

/**
 * @typedef  {Object} waterExit
 * @property {Boolean} a1
 * @property {Boolean} b1
 * @property {Boolean} a2
 * @property {Boolean} b2
 * @property {Boolean} a3
 * @property {Boolean} b3
 * @property {Boolean} a4
 * @property {Boolean} b4
 */

/**
 * @typedef  {Object} options
 * @property {Object} blindsPosition
 * @property {String} blindsFabric
 * @property {Boolean} blindsFabricSoltis
 * @property {Boolean} blindsFullScreen
 * @property {Boolean} blindsGuideColorMatchesFrame
 * @property {String} blindsGuideColorCustomID
 * @property {String} blindsGuideType
 * @property {String} blindsMotorType
 * @property {Object} slidingPanelPosition
 * @property {String} slidingPanelType
 * @property {Number} slidingPanelQuantity
 * @property {Number} slidingPanelDimension
 * @property {String} slidingPanelMaterial
 * @property {String} slidingPanelColor
 * @property {Boolean} slidingPanelCustomColor
 * @property {String} slidingPanelCustomColorID
 * @property {Object} glassPanelsPosition
 * @property {String} glassPanelsSlidingType
 * @property {Number} glassPanelsQuantity
 * @property {Number} glassPanelsDimension
 * @property {String} _id
 */

/**
 * @typedef  {Object} blindsPosition
 * @property {Boolean} p1p2
 * @property {Boolean} p3p4
 * @property {Boolean} p1p3
 * @property {Boolean} p2p4
 * @property {String} _id
 */

/**
 * @typedef  {Object} slidingPanelPosition
 * @property {Boolean} p1p2
 * @property {Boolean} p3p4
 * @property {Boolean} p1p3
 * @property {Boolean} p2p4
 * @property {String} _id
 */

/**
 * @typedef  {Object} glassPanelsPosition
 * @property {Boolean} p1p2
 * @property {Boolean} p3p4
 * @property {Boolean} p1p3
 * @property {Boolean} p2p4
 * @property {String} _id
 */

/**
 * @typedef  {Object} accessories
 * @property {Number} rainSensorQuantity
 * @property {Number} snowSensorQuantity
 * @property {Number} temperatureSensorQuantity
 * @property {Number} presenceSensorQuantity
 * @property {Number} remoteControlStandardQuantity
 * @property {Number} remoteControlEvoQuantity
 * @property {Number} remoteControlSomfyQuantity
 * @property {Number} heaterQuantity
 * @property {String} _id
 */

/**
 * @typedef  {Object} documents instance
 * @property {String} path
 * @property {String} type
 * @property {String} name
 * @property {Object} meta
 * @property {String} _id
 */

/**
 * @typedef  {Object} meta
 * @property {String} birth
 */

/**
 * @typedef  {Object} terms
 * @property {String} terms
 * @property {Boolean} agree
 */

/**
 * @typedef  {Object} project
 * @property {String} name
 * @property {String} date
 */

/**
 * @typedef  {Object} client
 * @property {String} name
 * @property {String} address
 * @property {String} phone
 * @property {String} email
 */
