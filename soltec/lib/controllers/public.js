'use strict';

const _              = require('lodash'),
      LicenseApi     = require('../../shared/sdk/license'),
      c              = require('../configuration'),
      LICENSE_CONF   = require('../const').LICENSE_CONF,
      PUBLIC_CONF    = require('../const').PUBLIC_CONF,
      moment         = require('moment');


const licenseConf = LICENSE_CONF;
const publicConf = PUBLIC_CONF;

class PublicApi {
  /**
   * Constructor
   */
  constructor() {

    /**
     * @type {LicenseApi}
     */
    this.license = new LicenseApi(c.get('authApi'));
    this.license.setAuth(licenseConf.email, licenseConf.password);
  }

  async $createPublicUser() {
    const out = [];
    console.log('creating public user... ');

    const users = await this.license.getAllUsersInfo();
    const sm = users.filter((x) => x.role === 'admin' && x.meta.spAccountManagerId === '19');

    if (!sm || !sm.length) {
      throw new Error('Missing sales manager with id 19!');
    }

    if (sm.length > 1) {
      console.warn('Multiple sales managers found with id 19, assuming first one found:');
      console.warn(sm[1]);
    }

    if (await this.license.getUserByMeta('public', true)) {
      out.push('public user already exists');
      console.log('public user already exists');
    } else {
      const user = {
        role: 'user',
        firstName: 'Public',
        lastName: 'User',
        email:  publicConf.email,
        password: publicConf.password,
        uniqueCode: 'PX',
        meta: {
          public            : true,
          parent            : sm[0]._id,
          vatId             : '',
          spModified        : '',
          spSyncTimestamp   : '',
          spExportFormat    : 'pdf',
          spTitle           : 'Public User',
          spAccountManagerId: 19,
          children          : [],
          hasNestedChildren : false
        }
      };
      const res = await this.license.createUser(user, { preventEmail: true });
      console.log(res);
      out.push(res);
    }
    return out;
  }

  createPublicUser(req, res) {
    if (req.user.grp !== 'superadmin') {
      return res.send(403);
    }

    this.$createPublicUser()
      .then(x => res.send(x))
      .catch(x => res.send(x));
  };

  loginPublic(req, res) {
    this.license.login(publicConf.email, publicConf.password, req.body.meta)
      .then((body) => {
        res.send({ token: body.token });
      })
      .catch((e) => {
        res.status(401).send('Unauthorized');
      });
  }
}

module.exports.publicApi = new PublicApi();
