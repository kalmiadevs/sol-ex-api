'use strict';

const _                   = require('lodash'),
      SharePointApi       = require('../abstract/share-point'),
      dbTools             = require('../../shared/tools/db'),
      appConf             = require('../../shared/tools/db-config'),
      generator           = require('password-generator'),
      to                  = require('../../shared/tools/to'),
      LicenseApi          = require('../../shared/sdk/license'),
      FsLogApi            = require('../../shared/abstract/fs-log-api'),
      fs                  = require('fs-extra'),
      c                   = require('../configuration'),
      Config              = require('../const'),
      SOLTEC_LOGO         = require('../const').SOLTEC_LOGO,
      SP_COUNTRY_CODE_MAP = require('../const').SP_COUNTRY_CODE_MAP,
      getEmailSubject     = require('../const').getEmailSubject,
      getEmailTemplate    = require('../const').getEmailTemplate,
      ms                  = require('../../shared/abstract/mail-api'),
      CronJob             = require('cron').CronJob,
      CircularJSON        = require('circular-json'),
      moment              = require('moment'),
      Q                   = require('q'),
      parseCurrency       = require('parsecurrency');

const spDateFormats = ['D.M.YYYY H:m', 'D.M.YYYY', 'YYYY-MM-DD hh:mm', 'YYYY-MM-DD', 'MM/DD/YYYY'];

/**
 * Disable creation of orders & docs on share point.
 * @type {boolean}
 */
const DISABLE_SYNC_ORDER = !c.get('production');

const sharePointConf       = Config.SHAREPOINT_CONF;
const licenseConf          = Config.LICENSE_CONF;
const dbConf               = Config.CLIENT_DB_CONF;
const allowedCompanyPhases = Config.SP_SYNC_COMPANY_PHASE;
const confKeys             = Config.CONFIG_KEYS;


////////////////////
// SP constants
////////////////////
// NOTES:
// CompaniesList -- partners  (vse od T3 do T4)  ---> CREATE, UPDATE, DELETE
// SalesOpportunitiesList  -- order s1 - s4
// ProjectTypesList -- order s3
/*
 * document libraries:
 * Documents                   https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid'6932561a-db0f-41e4-ac7a-fc93b5d0c173')/Items
 * Documents - Project leaders https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid'ef539e79-791e-40d7-ab20-eca4486b7b02')/Items
 * Documents - Project team    https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid'b46c3eb4-5dc0-40fc-9763-20b465a7b2c2')/Items
 * Site Assets                 https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid'886f0eed-e20e-4306-9ca1-6f0cfd78d643')/Items
 *
 *
 * DOC VIEW:
 * https://mipoo365.sharepoint.com/solutions365/Documents/Forms/AllItems.aspx?RootFolder=%2Fsolutions365%2FDocuments
 *
 * */
const spLists            = {
  companies              : 'Companies', // jsdoc def Company
  projectTypes           : 'ProjectTypes', // jsdoc def ProjectType
  salesOpportunities     : 'SalesOpportunities', // jsdoc def SalesOpportunity
  projects               : 'Projects', // jsdoc def Project
  productGroups          : 'ProductGroups',
  documents              : 'Documents', // '6932561a-db0f-41e4-ac7a-fc93b5d0c173'
  documentsProjectLeaders: 'DocumentsProjectLeaders', // 'ef539e79-791e-40d7-ab20-eca4486b7b02'
  companyDetails         : 'Company details'
};
const spListTypeFullName = {
  companies              : 'SP.Data.CompaniesListItem',
  projectTypes           : 'SP.Data.ProjectTypesListItem',
  salesOpportunities     : 'SP.Data.SalesOpportunitiesListItem',
  projects               : 'SP.Data.ProjectsListItem',
  documents              : 'SP.Data.DocumentsItem',
  documentsProjectLeaders: 'SP.Data.DocumentsProjectLeadersItem',
  companyDetails         : 'SP.Data.CompanyDetailsListItem'
};

/*=====SYNC API=====*/
/**
 * Api for share point.
 *
 * Class organization:
 *  - share point specific tools
 *  - sync functions
 *  - API exposed func.
 */
class SyncApi extends SharePointApi {
  /**
   * Constructor
   */
  constructor(orderApi) {
    super(
      {
        module     : module,
        serviceName: 'sync',
        entityName : 'sync'
      },
      sharePointConf,
      {
        useAddIn: false
      }
    );

    this.mailService = new ms();

    this.skipAccountManagerCheck = false;
    this.skipPhaseCheck          = false;

    this.orderApi = orderApi;
    this.db       = undefined;
    this.dbConnect();

    this.$syncAllInProgress = false;

    /**
     * @type {LicenseApi}
     */
    this.license = new LicenseApi(c.get('authApi'));
    this.license.setAuth(licenseConf.email, licenseConf.password);

    /**
     * Cron jon instance. By default stopped.
     * @type {CronJob}
     */
    this.syncAllCron = this.createNewSyncAllCronJob();

    this.addErrors({
      syncInProgress: {
        resStatusCode: 423,
        resCode      : 'already_in_progress',
        resMessage   : `Already started.`
      }
    });

    // TEST SHARE POINT
    // this.$spSyncCompanies();
    // this.$spSyncOrders();
    // this.test(spLists.projectTypes)
  }

  /**
   * Connect to DB.
   * @return {Promise.<void>}
   */
  async dbConnect() {
    if (!this.db) {
      this.db = await dbTools.getClientConnectionAndInitModels(dbConf.client);
    }
    return this.db;
  }

  /**
   * Basic iterator for dev testing purposes.
   * @param key
   * @return {Promise.<*>}
   */
  test(key) {
    const onItem = async (acc, x, meta) => {
      console.log(x, meta);
    };
    return this.$spGetListReduce(key + 'List', onItem);
  }

  /**
   * Get SyncAll cron job instance. It's stopped!
   * You must call job.start();
   * @return {CronJob}
   */
  createNewSyncAllCronJob() {
    const self = this;
    return new CronJob('00 00 */4 * * *', () => {
        // return new CronJob('00 00 * * * *', () => {
        self.log.info(`Sync all cron job stared at ${new Date()}.`);
        const fsLog = new FsLogApi(['cron']).alertInfo(`Sync all cron job stared.`);
        self.$syncAll()
          .then(x => {
            self.log.info(`Sync all cron job finished.`, x);
            fsLog.alertOk(x);
          })
          .catch(e => {
            self.log.error(`Sync all cron job FAILED.`, e);
            self.reportError(e, { code: 'cron_job' });
            fsLog.alertError(e);
          });
      }, null, false, 'Europe/Berlin'
    );
  }

  ////////////////////////////////////////////
  // FROM SHARE POINT TO LOCAL OPERATIONS
  ////////////////////////////////////////////

  //region ORDER
  /**
   * Add dynamic data from share point.
   * @param order {*}
   * @param salesOp {SalesOpportunity}
   * @param project {Project}
   * @param files {SpFile[]}
   * @param company
   * @param projectMeta {Project}
   */
  orderInjectSpData(order, salesOp, project, files, company, projectMeta) {
    order.spId            = salesOp.Title;
    order.spSyncTimestamp = +new Date();
    order.spModified      = salesOp.Modified; // it's ok to leave in string threat it as version code
    order.spSalesOppID    = salesOp.ID;
    // order.spSource = salesOp.SourceID; TODO SharePoint does not give us the ID, only the text value (salesOp.Source).
    order.spProjectID     = project && project.ID;

    order.spCreated           = salesOp.Created;
    order.spOpportunityLeader = salesOp.OpportunityLeader;
    order.spStatus            = salesOp.Status; // closed
    order.spFiles             = files.map(x => {
      return {
        file                : x.___downloadUrl,
        name                : x.Title,
        modified            : x.Modified,
        id                  : x.ID,
        isOnSalesOpportunity: !!x.SalesOpportunity,
        isOnProject         : !!x.Project,
        salesOpportunity    : x.SalesOpportunity,
        project             : x.Project
      };
    });

    order.step   = salesOp.PhaseOfSalesOpportunity; // S1; ..
    order.status = project && project.Status; // IN PROCESS; COMPLETED; ..
    // In sharepoint you will often see number = 'mail' Example: salesOp ID 46
    order.number        = salesOp.OfferNumber;
    order.projectNumber = project && project.ProjectNumber;
    order.spDueDate     = projectMeta && projectMeta.DueDate;

    order.cost    = project && project.ProjectValue || salesOp.Value;
    order.costVal = (parseCurrency(order.cost) || {}).value;

    order.project      = order.project || {};
    order.project.date = projectMeta && projectMeta.DueDate && moment(projectMeta.DueDate, spDateFormats).toDate(); // project && project.DateOfDispatch || salesOp.MaturityDate;

    if (company) {
      order.owner = company._id;
    }
  }

  /**
   * Called when order step is changed.
   * @param order
   * @param oldStep
   * @param newStep
   */
  onOrderStepChange(order, oldStep, newStep) {
    if (oldStep !== newStep) {
      this.orderApi.sendOrderStatusUpdateEmail(order);
    }
  }

  /**
   * Send pro forma notification email.
   * @param order
   * @param proformaFile {Array.<{name: String, path: String}>}
   * @return {Promise<void>}
   */
  async sendEmailConfirmationProformaAdded(order, proformaFile) {
    order = order.toObject();

    const user = await this.license.getUser(order.owner);

    const lang = order.languageOverride || user.language;

    const logo = SOLTEC_LOGO;

    let attachments = [];
    for (let file of (proformaFile || [])) {
      attachments.push({
        filename: file.name,
        path    : 'storage/order/' + file.path
      });
    }

    // Send email to order creator
    this.mailService.sendMail({
      from       : c.get('appMail'),
      to         : user.email,
      subject    : getEmailSubject('orderProforma', lang)({ orderNumber: order.number }),
      template   : getEmailTemplate('lib/templates/order-proforma-mail', lang),
      data       : {
        user     : {
          mailTemplate: {
            color      : '#3e454d',
            projectName: 'Soltec Portal',
            logo       : logo
          }
        },
        order    : order,
        link     : c.get('webApp'),
        orderLink: c.get('webApp') + '/order/' + order._id.toString()
      },
      attachments: attachments
    });
  }

  /**
   *
   * @param orderOld {Order}
   * @param files {SpFile[]}
   * @returns {Promise<void>}
   */
  async downloadFiles(orderOld, files) {
    if (orderOld.spOrigin) {
      return [];
    }

    const filesOld         = _.keyBy(orderOld.spFiles, 'id');
    const localFiles       = (orderOld.documents || []).map(x => x.name); // it's ok to do that since excel files are not set in sync scope
    const orderDownloadDir = `${this.orderApi.fs.bucketPath}/${orderOld.id}/download`;
    let downloadFiles      = files.filter(x => localFiles.indexOf(x.Title) === -1 && (
      !filesOld[x.ID] ||
      filesOld[x.ID].modified !== x.Modified ||
      !fs.pathExistsSync(`${orderDownloadDir}/${x.ID}/${x.Title}`)
    ));

    fs.ensureDirSync(orderDownloadDir);
    downloadFiles = downloadFiles.map(x => {
      return this.$spDownloadFile(x.___downloadUrl, `${orderDownloadDir}/${x.ID}`);
    });
    const res     = await Q.allSettled(downloadFiles);
    return res;
  }

  /**
   * Order confirmation with Proforma attachment: trigger an email notification that proforma is waiting in Configurator and add URL link
   * @param order {Order}
   * @param files {SpFile[]}
   * @return {Promise}
   */
  async checkForProforma(order, files) {
    if (!order.spOrigin && !order.proformaEmailConfirmation && (order.step === 'S2' || order.step === 'S3')) {
      const localFiles   = (order.documents || []).map(x => x.name);
      const unknownFiles = order.spFiles.filter(x => localFiles.indexOf(x.name) === -1 && x.isOnSalesOpportunity);

      if (unknownFiles.length) {
        await this.sendEmailConfirmationProformaAdded(order, []/*, unknownFiles*/);
        order.proformaEmailConfirmation = true;
        let err;
        [err, order]                    = await to(new Promise((ok, fail) => {
          order.save((error, model) => {
            if (error) {
              fail(error);
            } else {
              ok(model);
            }
          });
        }));
      }
    }
    return true;
  }

  /**
   * Update order DB based on sharePoint obj.
   * @param salesOp {SalesOpportunity}
   * @param project {Project}
   * @param files {SpFile[]}
   * @param salesOpMeta {SalesOpportunity}
   * @param projectMeta {Project}
   * @return {Promise}
   */
  async $spSyncOrder(salesOp, project, files, salesOpMeta, projectMeta) {
    console.log('Sync share point started. SalesOp ID: ' + salesOp.ID);
    let localOrder, err, updated, created, warnings, company;

    try {
      localOrder = await this.orderApi.$getByFilter(this.db, {
        spSalesOppID: salesOp.ID
      });
    } catch (err) {
      throw { error: err, code: 'can_not_get_order', salesOpID: salesOp.ID };
    }


    try {
      if (!salesOpMeta.Company) {
        throw Error(`Parameter 'salesOpMeta.Company' is undefined for salesOp: ${salesOp && salesOp.ID}. Local order found: ${localOrder && localOrder._id}`);
      }

      // company = await this.license.getUserByMeta('spTitle', salesOpMeta.Company);
      company = await this.license.getUserByMeta('spTitleTrimmed', salesOpMeta.Company.trim().toLowerCase());
    } catch (err) {
      throw { error: err, code: 'can_not_get_company', company: salesOpMeta.Company };
    }

    if (!company) {
      return { skipped: true, code: 'no_company_found_in_db', company: salesOpMeta.Company };
    }

    if (files) {
      files = files
      // filter out null
      // .filter(x => {
      //   if (!x.Title) {
      //     warnings = warnings || [];
      //     warnings.push({ nullFile: x.ID });
      //     return false;
      //   }
      //   return true;
      // })
      // only SalesOpportunity
        .filter(x => {
          return !!x.SalesOpportunity;
        });
    }

    if (!localOrder) {
      if (salesOp.ProductGroup === 'Pergola') {
        const _name     = salesOp.Company;
        const _receiver = salesOp.Receiver;

        const obj = {
          birth               : moment(salesOpMeta.Created, spDateFormats).toDate(),
          deliverySameAsClient: _name === _receiver,
          spOrigin            : true,
          client              : {
            name   : _name,
            address: project && project.ClientContact || salesOp.Contact
          },
          delivery            : {
            name: _receiver
          },
          project             : {
            name: project && project.Title || salesOp.Title
          },
          product             : {
            pergolaType: salesOp.ProductGroup
          },
          comment             : salesOp.Description + '\n\n' + (project && project.Description || '')
        };
        this.orderInjectSpData(obj, salesOp, project, files, company, projectMeta);

        // CREATE
        [err, localOrder] = await to(this.orderApi.$create(this.db, obj));
        if (err) {
          throw { error: err, code: 'can_not_create_order', salesOpID: salesOp.ID };
        }
        created = true;
      } else {
        return { skipped: true, code: 'wrong_product_group', ProductGroup: salesOp.ProductGroup };
      }
    } else if (!localOrder.deleted) {
      const oldStep = localOrder.step;
      try {
        await this.downloadFiles(localOrder, files);
      } catch (e) {
        warnings = warnings || [];
        warnings.push(e);
      }
      this.orderInjectSpData(localOrder, salesOp, project, files, company, projectMeta);

      if (project && project.Description) {
        // localOrder.comment = project && project.Description;
      }

      // Update sales manager override if the order is public because it might have been assigned to another person in sharepoint
      /* TODO uncomment this if sync is required
       if (localOrder._public && localOrder.spOpportunityLeader) {
       let accountManager;
       try {
       let users = await this.license.listUsersByMeta('spAccountManagerId', localOrder.spOpportunityLeader);

       accountManager = users.find(x => x.role === 'admin' || x.role === 'superadmin');
       } catch (err) {
       return reject({
       error           : err,
       code            : 'can_not_get_account_manager',
       spCompanyMetaAcc: spCompanyMeta.AccountManager
       });
       }

       localOrder._salesManagerOverrideId = accountManager._id;
       }
       */

      // UPDATE
      [err, localOrder] = await to(new Promise((ok, fail) => {
        localOrder.save((error, model) => {
          if (error) {
            fail(error);
          } else {
            ok(model);
          }
        });
      }));

      if (err) {
        throw { error: err, code: 'can_not_update_order', salesOpID: salesOp.ID };
      }

      if (!localOrder.spOrigin) {
        if (oldStep !== localOrder.step) {
          this.onOrderStepChange(localOrder, oldStep, localOrder.step);
        }

        await this.checkForProforma(localOrder, files);
      }

      updated = true;
    } else {
      return { skipped: true, code: 'local_order_deleted', company: salesOpMeta.Company };
    }

    return { localOrderId: localOrder._id, updated, created, warnings };
  }

  /**
   * Sync Orders.
   * @return {Promise}
   */
  async $spSyncOrders(limit) {
    const self  = this;
    const fsLog = new FsLogApi(['spSyncOrders']);

    await this.dbConnect();
    const modifiedKey = confKeys.spOrdersLastItemModifiedDate;
    const modified    = await appConf.get(this.db, modifiedKey, undefined);
    fsLog.alertInfo('SYNC only orders with modified date larger than: ' + modifiedKey + ' = ' + modified);

    let count = 0;

    /**
     * On company iterator.
     * @param acc
     * @param salesOp {SalesOpportunity}
     * @param meta
     * @return {Promise.<Boolean>}
     */
    const onItem = async (acc, salesOp, meta) => {
      count++;
      if (limit !== undefined && count > limit) {
        throw Error('Limit reached.');
      }
      try {
        let projectMeta = await self.$spGetBy(spLists.projects, 'Title', salesOp.Title);
        projectMeta     = (projectMeta || [])[0];
        let project;
        if (projectMeta) {
          project = await self.$spGetItemFromMetaData(projectMeta);
        }

        let files = await self.$spGetFilesBy(spLists.documents, 'SalesOpportunity', salesOp.Title);
        const res = await self.$spSyncOrder(salesOp, project, files, meta, projectMeta);

        if (res.skipped) {
          // Skipped
          acc.skipped.push({ id: salesOp.ID, code: res.code });
          fsLog.ok(res);
        } else {
          // Success, or with warnings
          res.salesOpID = salesOp.ID;
          if (res.warnings && res.warnings.length) {
            fsLog.warning(res);
          } else {
            fsLog.ok(res);
          }
          acc.success.push({ id: salesOp.ID, code: res.code });
        }
      } catch (e) {
        e.salesOpID = salesOp.ID;
        fsLog.error(e);
        self.reportError(e.error || e, { code: e.code, id: salesOp.ID });
        acc.failed.push({ id: salesOp.ID, error: e });
      }
    };

    fsLog.alertInfo(`Starting iterator`);

    try {
      return this.$spGetListReduce(
        spLists.salesOpportunities + 'List',
        onItem,
        { success: [], skipped: [], failed: [] },
        { gtModified: modified }
      )
        .then(x => {
          const msg        = x.acc;
          msg.successCount = msg.success.length;
          msg.skippedCount = msg.skipped.length;
          msg.failedCount  = msg.failed.length;
          msg.total        = count;
          msg.Modified     = x.res.LastItemModifiedDate;

          if (!x.acc.failed.length) {
            appConf.set(self.db, modifiedKey, x.res.LastItemModifiedDate);
          }

          fsLog.alertSuccess(msg).end();
          return x.acc;
        })
        .catch(e => {
          fsLog.alertError(e).end();
          self.reportError(e, { context: 'Inside $spSyncOrders => $spGetListReduce' });
          return Promise.reject(e);
        });
    } catch (e) {
      try {
        fsLog.alertError(e).end();
        self.reportError(e, { context: 'Wrapping $spSyncOrders => $spGetListReduce' });
      } catch (_e) {
        return Promise.reject(_e.message);
      }
      return Promise.reject(e.message);
    }
  }

  //endregion

  mapLanguage(lang) {
    switch (lang) {
      case 'atu':
      case 'ch':
      case 'de':
        return 'de';
      case 'it':
        return 'it';
      case 'si':
        return 'si';
      case 'fr':
        return 'fr';
      case 'en':
        return 'en';
      default:
        return 'en';
    }
  }

  //region COMPANY
  /**
   * Update company DB based on sharePoint obj.
   * @param spCompany {Company}
   * @param spCompanyMeta {Company}
   * @param details {Array.<{RabatPergola: string}>}
   * @return {Promise}
   */
  $spSyncCompany(spCompany, spCompanyMeta, details) {
    console.log('Sync share point started. Company ID: ' + spCompany.ID);

    const self = this;
    return new Promise(async (resolve, reject) => {
      let localCompany, accountManager;

      if (!this.skipPhaseCheck && allowedCompanyPhases.indexOf(spCompany.Phase) === -1) {
        return resolve({ skipped: true, code: 'wrong_phase' });
      }

      try {
        localCompany = await this.license.getUserByMeta('spId', spCompany.ID);
      } catch (err) {
        return reject({ error: err, code: 'can_not_get_user_from_license' });
      }

      try {
        let users = await this.license.listUsersByMeta('spAccountManagerId', spCompanyMeta.AccountManagerId);
        if (!users || !users.length) {
          users = await this.license.listUsersByMeta('spAccountManager', spCompany.AccountManager);
        }

        accountManager = users.find(x => x.role === 'admin' || x.role === 'superadmin');
      } catch (err) {
        return reject({
          error           : err,
          code            : 'can_not_get_account_manager',
          spCompanyMetaAcc: spCompanyMeta.AccountManager
        });
      }

      if (!this.skipAccountManagerCheck && !accountManager) {
        return resolve({
          skipped           : true,
          logSkip           : true,
          code              : 'no_accountManager_found_in_db',
          spAccountManager  : spCompany.AccountManager,
          spAccountManagerID: spCompanyMeta.AccountManager
        });
      }
      const hasConfiguratorAccount = !!(details && details[0] && details[0].HasConfiguratorAccount);
      const activationChanged      = !!(localCompany && !localCompany.active === hasConfiguratorAccount);
      const hasExcelOffer          = (details && details[0] && details[0].HasExcelOffer) ? 'excel' : 'pdf';
      const hasExcelOfferChange    = !!(localCompany && localCompany.meta.spExportFormat !== hasExcelOffer);

      const user = _.merge(_.cloneDeep(localCompany || {}), {
        firstName: spCompany.Title || '[unknown]',
        lastName : '\'' + (spCompany.Address || '[unknown]') + '\'',
        email    : spCompany.EmailAddress1 || spCompany.EmailAddress2, // 'soltec+' + spCompany.ID + '@kalmia.si'
        groups   : [],
        role     : 'user',
        language : this.mapLanguage((spCompany.VATPrefix || 'en').toLowerCase()),
        active   : hasConfiguratorAccount
      });

      user.meta = _.merge(user.meta, {
        spAccountManager  : spCompany.AccountManager,
        spAccountManagerId: spCompanyMeta.AccountManagerId,

        spId           : spCompany.ID,
        spSyncTimestamp: +new Date(),
        spModified     : spCompany.Modified,
        vatId          : spCompany.VatNumber && (spCompany.VATPrefix + spCompany.VatNumber),

        spCompanyCategory: spCompany.CompanyCategory,
        spCity           : spCompany.City,
        spTitle          : spCompany.Title,
        spTitleTrimmed   : (spCompany.Title || '').trim().toLowerCase(),
        spStatus         : spCompany.Status,
        spPhone          : spCompany.Phone,
        spPostNumber     : spCompany.PostNumber,
        spProvince       : spCompany.Province,
        spAddress        : spCompany.Address,
        spCountry        : spCompany.Country,

        spRabatPergola: details && details[0] && +details[0].RabatPergola,

        spTransport: details && details[0] && details[0].Transport,
        spPackaging: details && details[0] && details[0].Packaging,

        spExportFormat: hasExcelOffer,  // possible values: excel or pdf

        spEmailAddress1: spCompany.EmailAddress1,
        spEmailAddress2: spCompany.EmailAddress2,

        parent: accountManager && accountManager._id
      });

      user.mailTemplate = {
        color      : '#3e454d',
        projectName: 'Soltec Portal',
        logo       : SOLTEC_LOGO
      }; // TODO (these fields should always exist, otherwise the templates won't render correctly)

      const lang = user.language;

      const password        = generator(12, false);
      const welcomeMailData = {
        from    : c.get('appMail'),
        to      : user.email, // accountManager.email
        cc      : accountManager.email, // 'itkalmia@kalmia.si'
        subject : getEmailSubject('welcomeMail', lang)(),
        template: getEmailTemplate('lib/templates/welcome-mail', lang),
        data    : {
          user    : user,
          password: password,
          link    : c.get('webApp')
        }
      };

      try {
        if (!localCompany) {
          user.password = password;
          localCompany  = await self.license.createUser(user, { preventEmail: true });

          accountManager.meta          = accountManager.meta || {};
          accountManager.meta.children = _.union(accountManager.meta.children || [], [localCompany._id]);
          accountManager               = await self.license.updateUser(accountManager);

          let mailSend;
          let mailError;
          if (user.active && c.get('production')) {
            try {
              mailSend = await this.mailService.sendMail(welcomeMailData);
            } catch (e) {
              mailError = e;
            }
          }

          if (!mailError) {
            return resolve({ created: true, userId: localCompany.id, mailSend, activationChanged });
          } else {
            return reject({ created: true, userId: localCompany.id, mailSend, mailError, activationChanged });
          }
        } else {
          if (
            !activationChanged &&
            !hasExcelOfferChange &&
            (localCompany.meta || {}).spModified === spCompany.Modified &&
            (localCompany.meta || {}).parent === user.meta.parent &&
            localCompany.email === user.email &&
            (localCompany.meta || {}).spRabatPergola === user.meta.spRabatPergola &&
            (localCompany.meta || {}).spTransport === user.meta.spTransport &&
            (localCompany.meta || {}).spPackaging === user.meta.spPackaging
          ) {
            return resolve({ skipped: true, code: 'nothing_to_update' });
          }

          if (activationChanged && user.active) {
            user.newPassword        = password;
            user.confirmNewPassword = password;
          }

          localCompany = await self.license.updateUser(user, true);


          console.log('#>>>', !activationChanged);
          console.log('#>>>', !hasExcelOfferChange);
          console.log('#>>>', (localCompany.meta || {}).spModified, spCompany.Modified);
          console.log('#>>>', (localCompany.meta || {}).spRabatPergola, user.meta.spRabatPergola);
          console.log('#>>>', (localCompany.meta || {}).spTransport, user.meta.spTransport);
          console.log('#>>>', (localCompany.meta || {}).spPackaging, user.meta.spPackaging);


          accountManager.meta          = accountManager.meta || {};
          accountManager.meta.children = accountManager.meta.children || [];
          accountManager.meta.children.push(localCompany._id);
          accountManager.meta.children = _.uniq(accountManager.meta.children);

          accountManager = await self.license.updateUser(accountManager);


          let mailSend;
          let mailError;
          if (activationChanged && user.active) {
            try {
              mailSend = await this.mailService.sendMail(welcomeMailData);
            } catch (e) {
              mailError = e;
            }
          }

          if (!mailError) {
            return resolve({ updated: true, userId: localCompany.id, mailSend, activationChanged });
          } else {
            return reject({ updated: true, userId: localCompany.id, mailSend, mailError, activationChanged });
          }
        }
      } catch (e) {
        return reject({ error: e, code: 'can_not_push_to_licence' });
      }
    });
  }

  /**
   * Sync companies.
   * @return {Promise}
   */
  async $spSyncCompanies(limit) {
    const self  = this;
    const fsLog = new FsLogApi(['spSyncCompanies']);

    await this.dbConnect();
    const modifiedKey    = confKeys.spCompaniesLastItemModified;
    const lastDeletedKey = 'spCompaniesLastItemDeletedDate';
    const modified       = await appConf.get(this.db, modifiedKey, undefined);
    const lastDeleted    = await appConf.get(this.db, lastDeletedKey, undefined);
    fsLog.alertInfo('SYNC only companies with modified date larger than: ' + modifiedKey + ' = ' + modified + ', ' + lastDeletedKey + ' = ' + lastDeleted);

    let count = 0;

    /**
     * On company iterator.
     * @param acc
     * @param x {Company}
     * @param meta
     * @return {Promise.<Boolean>}
     */
    const onItem = async (acc, x, meta, listRes) => {
      count++;
      if (limit !== undefined && count > limit) {
        throw Error('Limit reached.');
      }
      try {
        let details     = await self.$spGetBy(spLists.companyDetails, 'Company', meta.Title);
        const res       = await self.$spSyncCompany(x, meta, details);
        res.spCompanyID = x.ID;
        if (!res.skipped) {
          // Synced and not skipped
          fsLog.ok(res);
          acc.success.push({ id: x.ID, res });
          // } else if (res.logSkip) {
        } else {
          fsLog.info(res);
          acc.skipped.push({ id: x.ID, res });
        }

      } catch (e) {
        e.spCompanyID = x.ID;
        fsLog.error(e);
        self.reportError(e.error || e, { code: e.code, id: x.ID });
        acc.failed.push({ id: x.ID, error: e });
      }

      if (lastDeleted !== listRes.LastItemDeletedDate) {
        acc.checked.push(x.ID);
      }
    };

    fsLog.alertInfo(`Starting iterator`);

    return this.$spGetListReduce(
      spLists.companies + 'List',
      onItem,
      { failed: [], success: [], skipped: [], checked: [] }
      // not used Microsoft.SharePoint.SPQueryThrottledException  { gtModified: modified } // BUG http://answers.flyppdevportal.com/MVC/Post/Thread/a79d8b20-8f20-417b-8904-c0e8edfdf38b?category=sharepointdevelopment
    )
      .then(x => {
        if (!x.acc.failed.length) {
          appConf.set(self.db, modifiedKey, x.res.LastItemModifiedDate);
        }

        if (lastDeleted !== x.res.LastItemDeletedDate) {
          // COULD TODO List of all companies id: x.acc.checked
          appConf.set(self.db, lastDeletedKey, x.res.LastItemDeletedDate);
        }

        const msg = {
          failed             : x.acc.failed,
          failedCount        : x.acc.failed.length,
          success            : x.acc.success,
          successCount       : x.acc.success.length,
          skipped            : x.acc.skipped,
          skippedCount       : x.acc.skipped.length,
          total              : count,
          Modified           : x.res.LastItemModifiedDate,
          LastItemDeletedDate: x.res.LastItemModifiedDate
        };
        fsLog.alertSuccess(msg);
        return x.acc;
      })
      .catch(e => {
        fsLog.alertError(e).end();
        self.reportError(e, { context: 'Inside $spSyncCompanies => $spGetListReduce' });
        return Promise.reject(e);
      });
  }

  //endregion
  /**
   * Get SalesOpportunity
   * @param id
   * @return {Promise.<SalesOpportunity>}
   */
  async $spGetSalesOpportunity(id) {
    try {
      return await this.$spGetByID('Sales opportunities', id);
    } catch (e) {
      throw e;
      //return e.statusCode === 404 ? undefined : e;
    }
  }

  /**
   * Get Project
   * @param title
   * @return {Promise.<Project>}
   */
  async $spGetProject(title) {
    try {
      return await this.$spGetBy(spLists.projects, 'Title', title);
    } catch (e) {
      throw e;
      //return e.statusCode === 404 ? undefined : e;
    }
  }

  /**
   * Get files of order.
   * @param title
   * @return {Promise.<SpFile[]>}
   */
  async $spGetOrderFiles(title) {
    try {
      return await this.$spGetFilesBy(spLists.documents, 'SalesOpportunity', title);
    } catch (e) {
      throw e;
      // return e.statusCode === 404 ? undefined : e;
    }
  }

  /**
   * Get share point order instance
   * @param salesOpId
   * @return {Promise.<{salesOp: SalesOpportunity, project: Project, files: SpFile[]}>}
   */
  async $spGetOrder(salesOpId) {
    const salesOp = await this.$spGetSalesOpportunity(salesOpId);
    if (!salesOp) return undefined;

    let projects = await this.$spGetProject(salesOp.Title) || [];
    if (projects.length > 1) {
      throw Error('SalesOpportunity has multiple projects.');
    }
    let project = projects[0] ? await this.$spGetItemFromMetaData(projects[0]) : undefined;

    const files = await this.$spGetOrderFiles(salesOp.Title);

    return { salesOp, project, files, projectMeta: projects[0] };
  }

  ////////////////////////////////////////////
  // FROM LOCAL TO SHARE POINT OPERATIONS
  ////////////////////////////////////////////
  /**
   * Sync local orders to SP.
   * @param force Ignore lastSyncToSp flag.
   * @return {Promise.<void>}
   */
  async $syncOrders(force) {
    const self = this;
    await this.dbConnect();
    const fsLog = new FsLogApi(['syncOrders']);

    return this.orderApi.$list(this.db)
      .then(async x => {
        const orders = x.items
          .filter(o => !o.spOrigin)
          .filter(o => force || o.modified && (o.lastSyncToSp || -1) < o.modified);

        const out = [];
        for (let i = 0, len = orders.length; i < len; i++) {
          const o = orders[i];
          try {
            const res   = await self.$syncOrder(o._id);
            res.orderId = o._id;
            out.push(res);
            fsLog.ok(res);
          } catch (e) {
            e.orderId = o._id;
            out.push(e);
            fsLog.error(e);
          }
        }

        return out;
      });
  }

  /**
   * salesOpToOrder
   * @param salesOp {SalesOpportunity}
   * @param order {Order}
   */
  salesOpToOrder(salesOp, order = {}) {
    order.spId             = salesOp.Title;
    order.spModified       = salesOp.Modified;
    order.spSalesOppID     = salesOp.ID;
    order.spCreated        = salesOp.Created;
    order.spSalesOppStatus = salesOp.Status;
    order.step             = salesOp.PhaseOfSalesOpportunity;
    order.number           = salesOp.OfferNumber;
    order.cost             = salesOp.Value;
  }

  orderUpdateSalesOp(order, salesOp = {}, owner) {
    salesOp.Contact     = order.client.phone;
    salesOp.Value       = order.costVal !== undefined ? order.costVal : null;
    salesOp.Description = order.comment;
    salesOp.SourceId    = order.spSource !== undefined ? order.spSource : null;
    if (order.step === 'S2' || order.step === 'S0') {
      salesOp.PhaseOfSalesOpportunity = order.step;
    }
    /* TODO uncomment this if sync is needed
     if (order._salesManagerOverrideId) {
     salesOp.OpportunityLeaderId = +order._salesManagerOverrideId;
     }
     */

    const distributorField = order.fromNewConfigurator ? 'Distributor' : 'Company';

    // Set distributor
    if (owner.role === 'admin' || owner.role === 'superadmin') {
      if (order.client) {
        salesOp[distributorField] = order.client.name;
      } else {
        throw new Error('order.client.name is empty');
      }
    } else {
      salesOp[distributorField] = owner.meta.spTitle;
    }

    // Set client if applicable
    if (order.fromNewConfigurator) {
      salesOp.Company = order.spCustomerID;
    }

    return salesOp;
  }

  /**
   * orderToSalesOp without ID!
   * @param order {Order}
   * @param salesOp {SalesOpportunity}
   * @param owner {Object}
   */
  orderToSalesOp(order, salesOp = {}, owner) {
    salesOp = this.orderUpdateSalesOp(order, salesOp, owner);

    salesOp.Title = `${order.project.name} (${order.number})`;

    /* TODO uncomment this if sync is needed
     salesOp.OpportunityLeaderId = owner.meta && owner.meta.spAccountManagerId ? (+owner.meta.spAccountManagerId) : null;
     */
    salesOp.Status                  = 'Open';
    salesOp.PhaseOfSalesOpportunity = 'S1';
    salesOp.OfferNumber             = order.number;
    salesOp.Installments            = null;
    salesOp.Receiver                = null;
    salesOp.ProductGroupId          = 3;
    salesOp.ProjectTypeId           = null;
    // salesOp.Attachments              = false;
    // salesOp.ProjectCreated           = false;
    // salesOp.FileSystemObjectType     = 0;
    // salesOp.ServerRedirectedEmbedUri = null;
    // salesOp.ServerRedirectedEmbedUrl = "";
    // salesOp.ContentTypeId            = "0x01006EF774FE86D6CB4CA8C19D623B09080D";
    // salesOp.OpportunityDate          = "2016-11-16T23=00=00Z";
    // salesOp.MaturityDate             = "2016-12-30T23=00=00Z";
    // salesOp.ProjectTypeId            = 2;
    // salesOp.AutoCreateProject        = false;
    return salesOp;
  }

  /**
   * orderToProject without ID!
   * @param order {Order}
   * @param project {Project}
   */
  orderToProject(order, project = {}) {
    project.ProductGroupId = 3;
    return project;
  }

  /**
   * orderToSalesOp without ID!
   * @param order {Order}
   * @param salesOp {SalesOpportunity}
   * @param owner {Object}
   */
  customerFromOrderData(order, spCustomer = {}) {
    spCustomer.Title = (`${order.client.name || ''} ${order.client.phone || ''}`).trim();

    // spCustomer.VatNumber = order.client.vat;
    spCustomer.VatNumber     = order.client.phone; // VatNumber is actually the phone number when customer type is selected
    spCustomer.Phone         = order.client.phone;
    spCustomer.EmailAddress1 = order.client.email;

    if (order.sourceContactInfo) {
      // Use address info from new configurator
      spCustomer.Address    = order.sourceContactInfo.address;
      spCustomer.City       = order.sourceContactInfo.city;
      spCustomer.PostNumber = order.sourceContactInfo.postalCode;
    } else {
      // Fallback to using the generated address string
      spCustomer.Address = order.client.address;
    }

    spCustomer.Country = SP_COUNTRY_CODE_MAP[order.client.countryCode];

    // Static defaults
    spCustomer.SelectType = 'Customer';
    spCustomer.Status     = 'B2C';
    // spCustomer.AccountManagerId; Going to leave this blank for now
    // spCustomer.AccountManagerStringId; Going to leave this blank for now
    spCustomer.Phase    = 'T1';
    spCustomer.SourceId = 4; // B2C Portal

    return spCustomer;
  }

  /**
   * Create new customer in share point.
   */
  async $spCreateCustomer(order) {
    if (DISABLE_SYNC_ORDER) {
      console.warn('Sync order is disabled.');
      return;
    }

    const resCustomer = await this.$spCreateListItem(
      'Companies',
      spListTypeFullName.companies,
      this.customerFromOrderData(order, {})
    );

    /* Example response:
     {
     "__metadata": {
     "id": "e0d3eea8-6876-440e-86b2-26f63a081ee7",
     "uri": "https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid'917747d1-8f27-4298-8828-2dd7a24141b3')/Items(7916)",
     "etag": "\"1\"",
     "type": "SP.Data.CompaniesListItem"
     },
     "FirstUniqueAncestorSecurableObject": {
     "__deferred": {
     "uri": "https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid'917747d1-8f27-4298-8828-2dd7a24141b3')/Items(7916)/FirstUniqueAncestorSecurableObject"
     }
     },
     "RoleAssignments": {
     "__deferred": {
     "uri": "https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid'917747d1-8f27-4298-8828-2dd7a24141b3')/Items(7916)/RoleAssignments"
     }
     },
     "AttachmentFiles": {
     "__deferred": {
     "uri": "https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid'917747d1-8f27-4298-8828-2dd7a24141b3')/Items(7916)/AttachmentFiles"
     }
     },
     "ContentType": {
     "__deferred": {
     "uri": "https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid'917747d1-8f27-4298-8828-2dd7a24141b3')/Items(7916)/ContentType"
     }
     },
     "GetDlpPolicyTip": {
     "__deferred": {
     "uri": "https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid'917747d1-8f27-4298-8828-2dd7a24141b3')/Items(7916)/GetDlpPolicyTip"
     }
     },
     "FieldValuesAsHtml": {
     "__deferred": {
     "uri": "https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid'917747d1-8f27-4298-8828-2dd7a24141b3')/Items(7916)/FieldValuesAsHtml"
     }
     },
     "FieldValuesAsText": {
     "__deferred": {
     "uri": "https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid'917747d1-8f27-4298-8828-2dd7a24141b3')/Items(7916)/FieldValuesAsText"
     }
     },
     "FieldValuesForEdit": {
     "__deferred": {
     "uri": "https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid'917747d1-8f27-4298-8828-2dd7a24141b3')/Items(7916)/FieldValuesForEdit"
     }
     },
     "File": {
     "__deferred": {
     "uri": "https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid'917747d1-8f27-4298-8828-2dd7a24141b3')/Items(7916)/File"
     }
     },
     "Folder": {
     "__deferred": {
     "uri": "https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid'917747d1-8f27-4298-8828-2dd7a24141b3')/Items(7916)/Folder"
     }
     },
     "LikedByInformation": {
     "__deferred": {
     "uri": "https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid'917747d1-8f27-4298-8828-2dd7a24141b3')/Items(7916)/LikedByInformation"
     }
     },
     "ParentList": {
     "__deferred": {
     "uri": "https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid'917747d1-8f27-4298-8828-2dd7a24141b3')/Items(7916)/ParentList"
     }
     },
     "Properties": {
     "__deferred": {
     "uri": "https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid'917747d1-8f27-4298-8828-2dd7a24141b3')/Items(7916)/Properties"
     }
     },
     "Versions": {
     "__deferred": {
     "uri": "https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid'917747d1-8f27-4298-8828-2dd7a24141b3')/Items(7916)/Versions"
     }
     },
     "FileSystemObjectType": 0,
     "Id": 7916,
     "ServerRedirectedEmbedUri": null,
     "ServerRedirectedEmbedUrl": "",
     "ContentTypeId": "0x01005894990510B5004597EDCB95C3EF70DF",
     "Title": "Matej Gorjanc",
     "VatNumber": null,
     "Address": "abcd 1\n1234\naadwa\nsi",
     "Country": "Slovenia",
     "City": null,
     "PostNumber": null,
     "Phone": "12345",
     "Fax": null,
     "EmailAddress1": "matej.gorjanc@kalmia.si",
     "EmailAddress2": null,
     "Website": null,
     "IndustryId": null,
     "Status": "B2C",
     "IntegrationLog": null,
     "AccountsReceivable": null,
     "CompanyWorkflow": null,
     "Province": null,
     "CompanyTypeId": null,
     "SectorId": null,
     "AccountManagerId": null,
     "AccountManagerStringId": null,
     "CompanyCategoryId": null,
     "TransferedToPan": false,
     "SelectType": "Customer",
     "VATPrefix": null,
     "Phase": "T1",
     "kb6gId": null,
     "kb6gStringId": null,
     "ComplianceAssetId": null,
     "SourceId": 4,
     "ID": 7916,
     "Modified": "2019-06-21T13:35:48Z",
     "Created": "2019-06-21T13:35:48Z",
     "AuthorId": 51,
     "EditorId": 51,
     "OData__UIVersionString": "1.0",
     "Attachments": false,
     "GUID": "8b5370d4-e9b8-4fa4-acc9-6f51ef00d46a"
     }
     */
    return { spCustomerData: resCustomer };
  }

  /**
   * List customers in share point for an order.
   */
  async $spGetCustomers(order) {
    // Example request for listing customers:
    // https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists/CompaniesList/items?$filter=Title%20eq%20%27Kalmia%20d.o.o.%20Customer%20Test%27

    const customerData = this.customerFromOrderData(order, {});
    const onItem       = async (acc, x, meta) => {
      console.log(x, meta);
      acc.push(x);
    };
    const { acc, res } = await this.$spGetListReduce(
      spLists.companies + 'List',
      onItem,
      [],
      {
        filters: [`Title eq '${customerData.Title}'`]
      });

    /* Example of an item in response
     {
     "__metadata": {
     "id": "https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid'917747d1-8f27-4298-8828-2dd7a24141b3')/Items(7916)/FieldValuesAsText",
     "uri": "https://mipoo365.sharepoint.com/solutions365/_api/Web/Lists(guid'917747d1-8f27-4298-8828-2dd7a24141b3')/Items(7916)/FieldValuesAsText",
     "type": "SP.FieldStringValues"
     },
     "ContentTypeId": "0x01005894990510B5004597EDCB95C3EF70DF",
     "Title": "Matej Gorjanc",
     "OData__x005f_ModerationComments": "",
     "File_x005f_x0020_x005f_Type": "",
     "VatNumber": "+386 123 456 781",
     "Address": "abcd 11234aadwasi",
     "Country": "Slovenia",
     "City": "",
     "PostNumber": "",
     "Phone": "12345",
     "Fax": "",
     "EmailAddress1": "matej.gorjanc@kalmia.si",
     "EmailAddress2": "",
     "Website": "",
     "Industry": "Public user",
     "Status": "B2C",
     "IntegrationLog": "",
     "AccountsReceivable": "",
     "CompanyWorkflow": "https://mipoo365.sharepoint.com/solutions365/_layouts/15/wrkstat.aspx?List=917747d1-8f27-4298-8828-2dd7a24141b3&WorkflowInstanceName=d307fdec-bf5c-4ad0-97f0-8f46ed63b69d, Transfer Company to Pantheon",
     "Province": "",
     "CompanyType": "Architect",
     "Sector": "Camping",
     "AccountManager": "kalmia",
     "CompanyCategory": "End user ",
     "TransferedToPan": "No",
     "SelectType": "Customer",
     "VATPrefix": "",
     "Phase": "T1",
     "kb6g": "",
     "ComplianceAssetId": "",
     "Source": "Portal - B2C",
     "ID": "7916",
     "Modified": "26. 06. 2019 15:41",
     "Created": "21. 06. 2019 15:35",
     "Author": "kalmia",
     "Editor": "kalmia",
     "OData__x005f_HasCopyDestinations": "",
     "OData__x005f_CopySource": "",
     "owshiddenversion": "7",
     "WorkflowVersion": "1",
     "OData__x005f_UIVersion": "3.584",
     "OData__x005f_UIVersionString": "7.0",
     "Attachments": "False",
     "OData__x005f_ModerationStatus": "Approved",
     "InstanceID": "",
     "Order": "791.600",
     "GUID": "8b5370d4-e9b8-4fa4-acc9-6f51ef00d46a",
     "WorkflowInstanceID": "",
     "FileRef": "/solutions365/Lists/Companies/7916_.000",
     "FileDirRef": "/solutions365/Lists/Companies",
     "Last_x005f_x0020_x005f_Modified": "21. 06. 2019 15:35",
     "Created_x005f_x0020_x005f_Date": "21. 06. 2019 15:35",
     "FSObjType": "0",
     "SortBehavior": "0",
     "FileLeafRef": "7916_.000",
     "UniqueId": "ef6e4d63-86ad-41ea-b568-7b4587be150c",
     "SyncClientId": "",
     "ProgId": "",
     "ScopeId": "{C9C91E38-AD59-4867-9F8D-A42612A6978F}",
     "MetaInfo": "",
     "OData__x005f_Level": "1",
     "OData__x005f_IsCurrentVersion": "Yes",
     "ItemChildCount": "0",
     "FolderChildCount": "0",
     "Restricted": "",
     "OriginatorId": "",
     "NoExecute": "0",
     "ContentVersion": "0",
     "OData__x005f_ComplianceFlags": "",
     "OData__x005f_ComplianceTag": "",
     "OData__x005f_ComplianceTagWrittenTime": "",
     "OData__x005f_ComplianceTagUserId": "",
     "AccessPolicy": "",
     "OData__x005f_VirusStatus": "",
     "OData__x005f_VirusVendorID": "",
     "OData__x005f_VirusInfo": "",
     "AppAuthor": "",
     "AppEditor": "",
     "SMTotalSize": "",
     "SMLastModifiedDate": "2019-06-26T13:41:51Z",
     "SMTotalFileStreamSize": "0",
     "SMTotalFileCount": ""
     }
     */
    return acc;
  }


  /**
   * Create new order in share point.
   * @param order {Order}
   * @param owner {User}
   * @return {Promise.<{salesOp: SalesOpportunity, project: Project}>}
   */
  async $spCreateOrder(order, owner) {
    //const resSo = await this.$spCreateListItemFakeOffer(
    const resSo = await this.$spCreateListItem(
      'Sales opportunities',
      spListTypeFullName.salesOpportunities,
      this.orderToSalesOp(order, {}, owner)
    );

    return { salesOp: resSo, project: undefined };
  }

  /**
   * Update order in share point.
   * @param order {Order}
   * @param spOrder {{salesOp: SalesOpportunity, project: Project, files: SpFile[]}}
   * @param owner {User}
   * @return {Promise.<{salesOp: SalesOpportunity, project: Project}>}
   */
  async $spUpdateOrder(order, spOrder, owner) {
    const orderForSp = this.orderUpdateSalesOp(order, {}, owner);
    console.log('Sending order to SP', orderForSp);
    await this.$spUpdateListItem(
      'Sales opportunities',
      spListTypeFullName.salesOpportunities,
      spOrder.salesOp.ID,
      orderForSp
    );

    // let resP;
    // if (spOrder.project && order.step === 'S3') {
    //   resP = await this.$spUpdateListItem(
    //     spLists.projects,
    //     spListTypeFullName.projects,
    //     spOrder.project.ID,
    //     this.orderToSalesOp(order, {}, owner)
    //   );
    // }
    spOrder.salesOp = this.orderUpdateSalesOp(order, spOrder.salesOp, owner);
    return spOrder;
  }

  /**
   * Upload files to share point & link them to order.
   * @param path
   * @param order
   * @param spOrder
   * @param orderModel
   * @param link
   * @param link.offer
   * @param link.project
   */
  async $spUploadFiles(path, order, spOrder, orderModel, link = {}) {
    let res = [];
    if (!link.offer && !link.project) {
      throw new Error('link param can not be empty obj');
    }
    if (fs.existsSync(path)) {
      const docs = fs.readdirSync(path);
      for (let i in docs) {
        const fileName = docs[i];
        console.log('Uploading ' + fileName);
        try {
          const payload = {
            Title           : fileName,
            Company         : spOrder.salesOp.Company,
            SalesOpportunity: link.offer && spOrder.salesOp ? spOrder.salesOp.Title : null,
            Project         : link.project && spOrder.project ? spOrder.project.Title : null
          };
          if (!link.offer && link.project) {
            delete payload.Company;
            delete payload.SalesOpportunity;
          }

          if (!payload.SalesOpportunity && !payload.Project) {
            throw new Error('SalesOpportunity & Project are null.');
          }

          const uploadStatus = await this.$spUploadFile({
              name: link.offer ? spLists.documents : spLists.documentsProjectLeaders,
              type: link.offer ? spListTypeFullName.documents : spListTypeFullName.documentsProjectLeaders
            },
            path + '/' + fileName,
            payload,
            !!link.project);
          res.push({ resolved: true, value: uploadStatus, file: path + '/' + fileName });
        } catch (e) {
          res.push({ rejected: true, error: e, file: path + '/' + fileName, extraData: e.extraData });
        }
      }
    }
    return res;
  }

  /**
   * Sync specific order from local DB
   * @param orderId
   * @return {Promise.<*>}
   */
  async $syncOrder(orderId) {
    if (DISABLE_SYNC_ORDER) {
      throw Error('Sync order is disabled.');
    }

    const fsLog = new FsLogApi([], { name: 'syncOrder.log' });
    try {
      this.log.info(`Sync started for order ${orderId}`);
      fsLog.info({
        orderId,
        state: 'Sync started'
      });

      await this.dbConnect();
      let created, updated;

      const orderModel = await this.orderApi.$getByFilter(this.db, { _id: orderId });
      /**
       * @type {Order}
       */
      let order        = orderModel.toObject();

      // we don't want do sync imported orders
      if (order.spOrigin) {
        // throw new Error('Order was imported from share point!');
        fsLog.info({
          orderId,
          state: 'Sync ended (order imported from SP)'
        });

        return { skipped: true, spOrigin: true };
      }

      if (order.deleted) {
        if (!order.spDeleted) {
          if (order.spSalesOppID) {
            // TODO check step & remove if step ok
          }
        }

        fsLog.info({
          orderId,
          state: 'Sync ended (order was deleted)'
        });

        return { deleted: true };
      }

      let owner;
      try {
        owner = await this.license.getUser(order.owner);
      } catch (err) {
        fsLog.error({
          orderId,
          order,
          state: 'Could not get owner'
        });

        return reject({ error: err, code: 'can_not_get_owner', orderId: order._id, owner: order.owner });
      }


      // If order is from new configurator, create a customer in sharepoint.
      if (order.fromNewConfigurator) {
        // TODO should we only sync if order does not yet exist in SP? If so check for `spSalesOppID`
        fsLog.info({
          orderId,
          state: 'Order is from new configurator, syncing customer...'
        });

        const existingCustomers = await this.$spGetCustomers(order);

        if (existingCustomers.length) {
          // Customer already exists, skip creation.
          orderModel.spCustomerID = existingCustomers[0].Title;
        } else {
          // Create a customer
          const { spCustomerData } = await this.$spCreateCustomer(order);
          orderModel.spCustomerID  = spCustomerData.Title;
        }

        await orderModel.save();

        order = orderModel.toObject();
      }

      /**
       * @type{{salesOp: SalesOpportunity, project: Project}}
       */
      let spOrder;
      if (!order.spSalesOppID) {
        spOrder = await this.$spCreateOrder(order, owner);

        fsLog.info({
          orderId,
          order,
          spOrder,
          state: 'Creating new order'
        });

        orderModel.spSalesOppID = spOrder.salesOp.ID;
        // save before upload just in case if something unexpected happens
        await orderModel.save();

        created = true;
      } else {
        spOrder = await this.$spGetOrder(order.spSalesOppID);

        fsLog.info({
          orderId,
          order,
          spOrder,
          state: 'Fetched existing order data'
        });

        spOrder = await this.$spUpdateOrder(order, spOrder, owner);

        fsLog.info({
          orderId,
          order,
          spOrder,
          state: 'Updated existing order data'
        });

        updated = true;
      }


      const offerDocsPath   = `${this.orderApi.fs.bucketPath}/${order.id}/sync_offer`;
      const projectDocsPath = `${this.orderApi.fs.bucketPath}/${order.id}/sync_project`;
      // const offerDocsPath   = `${ this.orderApi.fs.bucketPath }/demo/sync_offer`;
      // const projectDocsPath = `${ this.orderApi.fs.bucketPath }/demo/sync_project`;

      // Sales opportunity
      const offerDocsUploadRes = await this.$spUploadFiles(offerDocsPath, order, spOrder, orderModel, { offer: true });

      // Project - Sales opportunity docs
      const projectSalesOpDocsUploadRes = spOrder.project ? await this.$spUploadFiles(offerDocsPath, order, spOrder, orderModel, { project: true }) : [];
      // Project - Project docs
      const projectDocsUploadRes        = spOrder.project ? await this.$spUploadFiles(projectDocsPath, order, spOrder, orderModel, { project: true }) : [];

      fsLog.info({
        orderId,
        order,
        spOrder,
        offerDocsUploadRes,
        projectSalesOpDocsUploadRes,
        projectDocsUploadRes,
        state: 'Documents upload finished'
      });

      offerDocsUploadRes.concat(projectDocsUploadRes).forEach(x => {
        if (x.rejected) {
          x.extraData.file = x.file;
          this.reportError(x.error, x.extraData);
        }
      });

      orderModel.lastSyncToSp = +new Date();
      orderModel.save();

      fsLog.ok({
        created,
        updated,
        orderId,
        salesOpID : (spOrder.salesOp || {}).ID,
        projectID : (spOrder.project || {}).ID,
        filesCount: (spOrder.files || []).length,
        offerDocsUploadRes,
        projectSalesOpDocsUploadRes,
        projectDocsUploadRes
      });

      this.log.info(`Sync for order ${orderId} ENDED. Order was: ${created ? 'created' : ''}${updated ? 'updated' : ''}`);

      return { created, updated };
    } catch (e) {
      this.reportError(e, { code: 'syncOrder', orderId });

      fsLog.error({
        orderId,
        e,
        state: 'Sync ended (unexpected error)'
      });

      e.orderId = orderId;
      fsLog.error(e);
      throw e;
    }
  }

  ////////////////////////////////////////////
  // OTHER
  ////////////////////////////////////////////

  /**
   * Sync all orders
   * @return {Promise}
   */
  async $syncAll() {
    if (this.$syncAllInProgress) {
      throw this.errors.syncInProgress;
    }
    let err, res;
    const output = {};

    this.$syncAllInProgress = true;

    // FROM SP TO LOCAL
    [err, res]              = await to(this.$spSyncCompanies());
    output.$spSyncCompanies = { err, res };
    if (err) console.error(err);

    [err, res]           = await to(this.$spSyncOrders());
    output.$spSyncOrders = { err, res };
    if (err) console.error(err);

    // FROM LOCAL TO SP
    [err, res]         = await to(this.$syncOrders());
    output.$syncOrders = { err, res };
    if (err) console.error(err);

    this.$syncAllInProgress = false;

    return output;
  }

  /**
   * @api {get} /sync/:action/:id Sync order
   * @apiName manualSync
   * @apiOrder Sync
   *
   * @apiParam {String} order id.
   *
   * @apiSuccess {String} status Response status.
   */
  manualSync(req, res) {
    if (req.user.grp !== 'superadmin') {
      return res.send(403);
    }

    const self    = this;
    const actions = {
      $spSyncCompanies: () => {
        return self.$spSyncCompanies();
      },
      $spSyncOrders   : () => {
        return self.$spSyncOrders();
      },
      $syncOrders     : () => {
        return self.$syncOrders();
      },
      $syncOrder      : (x) => {
        return self.$syncOrder(x);
      }
    };

    (actions[req.params.action] || (() => {
      return Promise.reject('unknown action');
    }))(req.params.data)
      .then(r => {
        // res.send(r);
        const fsLog = new FsLogApi(['manualSyncAll']).alertOk(r);
      })
      .catch(e => {
        console.error(e);
        const fsLog = new FsLogApi(['manualSync']).alertError(e);
        // res.send(CircularJSON.stringify(e));
      });

    res.send({ started: true, timestamp: +new Date() });
  };

  async resetConfig() {
    await appConf.set(this.db, confKeys.spOrdersLastItemModifiedDate, undefined);
    await appConf.set(this.db, confKeys.spCompaniesLastItemModified, undefined);
  }

  /**
   * @api {get} /media/ Save a media file
   * @apiName manualSyncAll
   * @apiOrder Sync
   *
   * @apiSuccess {String} status Response status.
   */
  manualSyncAll(req, res) {
    const self = this;
    if (req.user.grp !== 'superadmin') {
      return res.send(403);
    }

    this.resetConfig()
      .then(() => self.$syncAll())
      .then(r => {
        // res.send(CircularJSON.stringify(r));
        const fsLog = new FsLogApi(['manualSyncAll']).alertOk(r);
      })
      .catch(e => {
        console.error(e);
        // res.send(CircularJSON.stringify(e));
        const fsLog = new FsLogApi(['manualSyncAll']).alertError(e);
      });

    res.send({
      started  : true,
      message  : 'Now slowly go over to the coffee machine and grab a cup for yourself. You can monitor progress by refreshing logs.',
      timestamp: +new Date()
    });
  };
}

module.exports.SyncApiCls = SyncApi;

// JSDOC
/**
 * List Provider
 * @callback listProvider
 * @return {Promise}
 */

/**
 * On item callback
 * @callback onItem
 * @param {ProjectType|SalesOpportunity|Project|Company}
 * @return {Promise}
 */

/**
 * @typedef {{lastItemModified: number, items: Object[], rawRes: Object, rawItemsRes: ListResponse }} GetListResponse
 */

/**
 * @typedef  {Object} __metadata
 * @property {String} id
 * @property {String} uri
 * @property {String} type
 */

// JSDOC SP

/**
 * @typedef {Object} Company
 * @property {Object} __metadata
 * @property {String} ContentTypeId
 * @property {String} Title
 * @property {String} OData__x005f_ModerationComments
 * @property {String} File_x005f_x0020_x005f_Type
 * @property {String} VatNumber
 * @property {String} Address
 * @property {String} Country
 * @property {String} City
 * @property {String} PostNumber
 * @property {String} Phone
 * @property {String} Fax
 * @property {String} EmailAddress1
 * @property {String} EmailAddress2
 * @property {String} Website
 * @property {String} Industry
 * @property {String} Status
 * @property {String} IntegrationLog
 * @property {String} AccountsReceivable
 * @property {String} CompanyWorkflow
 * @property {String} Province
 * @property {String} CompanyType
 * @property {String} Sector
 * @property {String} AccountManager
 * @property {String} CompanyCategory
 * @property {String} TransferedToPan
 * @property {String} SelectType
 * @property {String} VATPrefix
 * @property {String} Phase
 * @property {String} kb6g
 * @property {String} ID
 * @property {String} Modified
 * @property {String} Created
 * @property {String} Author
 * @property {String} Editor
 * @property {String} OData__x005f_HasCopyDestinations
 * @property {String} OData__x005f_CopySource
 * @property {String} owshiddenversion
 * @property {String} WorkflowVersion
 * @property {String} OData__x005f_UIVersion
 * @property {String} OData__x005f_UIVersionString
 * @property {String} Attachments
 * @property {String} OData__x005f_ModerationStatus
 * @property {String} InstanceID
 * @property {String} Order
 * @property {String} GUID
 * @property {String} WorkflowInstanceID
 * @property {String} FileRef
 * @property {String} FileDirRef
 * @property {String} Last_x005f_x0020_x005f_Modified
 * @property {String} Created_x005f_x0020_x005f_Date
 * @property {String} FSObjType
 * @property {String} SortBehavior
 * @property {String} FileLeafRef
 * @property {String} UniqueId
 * @property {String} SyncClientId
 * @property {String} ProgId
 * @property {String} ScopeId
 * @property {String} MetaInfo
 * @property {String} OData__x005f_Level
 * @property {String} OData__x005f_IsCurrentVersion
 * @property {String} ItemChildCount
 * @property {String} FolderChildCount
 * @property {String} Restricted
 * @property {String} OriginatorId
 * @property {String} NoExecute
 * @property {String} ContentVersion
 * @property {String} OData__x005f_ComplianceFlags
 * @property {String} OData__x005f_ComplianceTag
 * @property {String} OData__x005f_ComplianceTagWrittenTime
 * @property {String} OData__x005f_ComplianceTagUserId
 * @property {String} AccessPolicy
 * @property {String} OData__x005f_VirusStatus
 * @property {String} OData__x005f_VirusVendorID
 * @property {String} OData__x005f_VirusInfo
 * @property {String} AppAuthor
 * @property {String} AppEditor
 * @property {String} SMTotalSize
 * @property {String} SMLastModifiedDate
 * @property {String} SMTotalFileStreamSize
 * @property {String} SMTotalFileCount
 */

/**
 * @typedef  {Object} Project
 * @property {Object} __metadata
 * @property {String} ContentTypeId
 * @property {String} Title
 * @property {String} OData__x005f_ModerationComments
 * @property {String} File_x005f_x0020_x005f_Type
 * @property {String} Description
 * @property {String} Salesperson
 * @property {String} ProductGroup
 * @property {String} ProjectLeader
 * @property {String} ProjectTeam
 * @property {String} StartDate
 * @property {String} DueDate
 * @property {String} ProjectValue
 * @property {String} Status
 * @property {String} Client
 * @property {String} ClientContact
 * @property {String} ProjectNumber
 * @property {String} DateOfDispatch
 * @property {String} AssembledAndTestedBy
 * @property {String} ID
 * @property {String} Modified
 * @property {String} Created
 * @property {String} Author
 * @property {String} Editor
 * @property {String} OData__x005f_HasCopyDestinations
 * @property {String} OData__x005f_CopySource
 * @property {String} owshiddenversion
 * @property {String} WorkflowVersion
 * @property {String} OData__x005f_UIVersion
 * @property {String} OData__x005f_UIVersionString
 * @property {String} Attachments
 * @property {String} OData__x005f_ModerationStatus
 * @property {String} InstanceID
 * @property {String} Order
 * @property {String} GUID
 * @property {String} WorkflowInstanceID
 * @property {String} FileRef
 * @property {String} FileDirRef
 * @property {String} Last_x005f_x0020_x005f_Modified
 * @property {String} Created_x005f_x0020_x005f_Date
 * @property {String} FSObjType
 * @property {String} SortBehavior
 * @property {String} FileLeafRef
 * @property {String} UniqueId
 * @property {String} SyncClientId
 * @property {String} ProgId
 * @property {String} ScopeId
 * @property {String} MetaInfo
 * @property {String} OData__x005f_Level
 * @property {String} OData__x005f_IsCurrentVersion
 * @property {String} ItemChildCount
 * @property {String} FolderChildCount
 * @property {String} Restricted
 * @property {String} OriginatorId
 * @property {String} NoExecute
 * @property {String} ContentVersion
 * @property {String} OData__x005f_ComplianceFlags
 * @property {String} OData__x005f_ComplianceTag
 * @property {String} OData__x005f_ComplianceTagWrittenTime
 * @property {String} OData__x005f_ComplianceTagUserId
 * @property {String} AccessPolicy
 * @property {String} OData__x005f_VirusStatus
 * @property {String} OData__x005f_VirusVendorID
 * @property {String} OData__x005f_VirusInfo
 * @property {String} AppAuthor
 * @property {String} AppEditor
 * @property {String} SMTotalSize
 * @property {String} SMLastModifiedDate
 * @property {String} SMTotalFileStreamSize
 * @property {String} SMTotalFileCount
 */


/**
 * @typedef  {Object} SalesOpportunity
 * @property {Object} __metadata
 * @property {String} ContentTypeId
 * @property {String} Title
 * @property {String} OData__x005f_ModerationComments
 * @property {String} File_x005f_x0020_x005f_Type
 * @property {String} Company
 * @property {String} Contact
 * @property {String} Value
 * @property {String} OpportunityDate
 * @property {String} MaturityDate
 * @property {String} Description
 * @property {String} OpportunityLeader
 * @property {String} Status
 * @property {String} PhaseOfSalesOpportunity Like S1, S2, S3
 * @property {String} ProductGroup
 * @property {String} ProjectCreated
 * @property {String} SalesOpWF
 * @property {String} ProjectType
 * @property {String} OfferNumber
 * @property {String} Installments
 * @property {String} Receiver
 * @property {String} AutoCreateProject
 * @property {String} ID
 * @property {String} Modified
 * @property {String} Created
 * @property {String} Author
 * @property {String} Editor
 * @property {String} OData__x005f_HasCopyDestinations
 * @property {String} OData__x005f_CopySource
 * @property {String} owshiddenversion
 * @property {String} WorkflowVersion
 * @property {String} OData__x005f_UIVersion
 * @property {String} OData__x005f_UIVersionString
 * @property {String} Attachments
 * @property {String} OData__x005f_ModerationStatus
 * @property {String} InstanceID
 * @property {String} Order
 * @property {String} GUID
 * @property {String} WorkflowInstanceID
 * @property {String} FileRef
 * @property {String} FileDirRef
 * @property {String} Last_x005f_x0020_x005f_Modified
 * @property {String} Created_x005f_x0020_x005f_Date
 * @property {String} FSObjType
 * @property {String} SortBehavior
 * @property {String} FileLeafRef
 * @property {String} UniqueId
 * @property {String} SyncClientId
 * @property {String} ProgId
 * @property {String} ScopeId
 * @property {String} MetaInfo
 * @property {String} OData__x005f_Level
 * @property {String} OData__x005f_IsCurrentVersion
 * @property {String} ItemChildCount
 * @property {String} FolderChildCount
 * @property {String} Restricted
 * @property {String} OriginatorId
 * @property {String} NoExecute
 * @property {String} ContentVersion
 * @property {String} OData__x005f_ComplianceFlags
 * @property {String} OData__x005f_ComplianceTag
 * @property {String} OData__x005f_ComplianceTagWrittenTime
 * @property {String} OData__x005f_ComplianceTagUserId
 * @property {String} AccessPolicy
 * @property {String} OData__x005f_VirusStatus
 * @property {String} OData__x005f_VirusVendorID
 * @property {String} OData__x005f_VirusInfo
 * @property {String} AppAuthor
 * @property {String} AppEditor
 * @property {String} SMTotalSize
 * @property {String} SMLastModifiedDate
 * @property {String} SMTotalFileStreamSize
 * @property {String} SMTotalFileCount
 */

/**
 * @typedef  {Object} ProjectType
 * @property {Object} __metadata
 * @property {String} ContentTypeId
 * @property {String} Title
 * @property {String} OData__x005f_ModerationComments
 * @property {String} File_x005f_x0020_x005f_Type
 * @property {String} ID
 * @property {String} Modified
 * @property {String} Created
 * @property {String} Author
 * @property {String} Editor
 * @property {String} OData__x005f_HasCopyDestinations
 * @property {String} OData__x005f_CopySource
 * @property {String} owshiddenversion
 * @property {String} WorkflowVersion
 * @property {String} OData__x005f_UIVersion
 * @property {String} OData__x005f_UIVersionString
 * @property {String} Attachments
 * @property {String} OData__x005f_ModerationStatus
 * @property {String} InstanceID
 * @property {String} Order
 * @property {String} GUID
 * @property {String} WorkflowInstanceID
 * @property {String} FileRef
 * @property {String} FileDirRef
 * @property {String} Last_x005f_x0020_x005f_Modified
 * @property {String} Created_x005f_x0020_x005f_Date
 * @property {String} FSObjType
 * @property {String} SortBehavior
 * @property {String} FileLeafRef
 * @property {String} UniqueId
 * @property {String} SyncClientId
 * @property {String} ProgId
 * @property {String} ScopeId
 * @property {String} MetaInfo
 * @property {String} OData__x005f_Level
 * @property {String} OData__x005f_IsCurrentVersion
 * @property {String} ItemChildCount
 * @property {String} FolderChildCount
 * @property {String} Restricted
 * @property {String} OriginatorId
 * @property {String} NoExecute
 * @property {String} ContentVersion
 * @property {String} OData__x005f_ComplianceFlags
 * @property {String} OData__x005f_ComplianceTag
 * @property {String} OData__x005f_ComplianceTagWrittenTime
 * @property {String} OData__x005f_ComplianceTagUserId
 * @property {String} AccessPolicy
 * @property {String} OData__x005f_VirusStatus
 * @property {String} OData__x005f_VirusVendorID
 * @property {String} OData__x005f_VirusInfo
 * @property {String} AppAuthor
 * @property {String} AppEditor
 * @property {String} SMTotalSize
 * @property {String} SMLastModifiedDate
 * @property {String} SMTotalFileStreamSize
 * @property {String} SMTotalFileCount
 */

/**
 * @typedef  {Object} ListResponse
 * @property {Object} __metadata
 * @property {Object} FirstUniqueAncestorSecurableObject
 * @property {Object} RoleAssignments
 * @property {Object} Activities
 * @property {Object} ContentTypes
 * @property {Object} CreatablesInfo
 * @property {Object} DefaultView
 * @property {Object} DescriptionResource
 * @property {Object} EventReceivers
 * @property {Object} Fields
 * @property {Object} Forms
 * @property {Object} InformationRightsManagementSettings
 * @property {Object} Items
 * @property {Object} ParentWeb
 * @property {Object} RootFolder
 * @property {Object} Subscriptions
 * @property {Object} TitleResource
 * @property {Object} UserCustomActions
 * @property {Object} Views
 * @property {Object} WorkflowAssociations
 * @property {Boolean} AllowContentTypes
 * @property {Number} BaseTemplate
 * @property {Number} BaseType
 * @property {Boolean} ContentTypesEnabled
 * @property {Boolean} CrawlNonDefaultViews
 * @property {String} Created
 * @property {Object} CurrentChangeToken
 * @property {Object} CustomActionElements
 * @property {String} DefaultContentApprovalWorkflowId
 * @property {Boolean} DefaultItemOpenUseListSetting
 * @property {String} Description
 * @property {String} Direction
 * @property {Object} DocumentTemplateUrl
 * @property {Number} DraftVersionVisibility
 * @property {Boolean} EnableAttachments
 * @property {Boolean} EnableFolderCreation
 * @property {Boolean} EnableMinorVersions
 * @property {Boolean} EnableModeration
 * @property {Boolean} EnableVersioning
 * @property {String} EntityTypeName
 * @property {Boolean} ExemptFromBlockDownloadOfNonViewableFiles
 * @property {Boolean} FileSavePostProcessingEnabled
 * @property {Boolean} ForceCheckout
 * @property {Boolean} HasExternalDataSource
 * @property {Boolean} Hidden
 * @property {String} Id
 * @property {Object} ImagePath
 * @property {String} ImageUrl
 * @property {Object[]} Items
 * @property {Boolean} IrmEnabled
 * @property {Boolean} IrmExpire
 * @property {Boolean} IrmReject
 * @property {Boolean} IsApplicationList
 * @property {Boolean} IsCatalog
 * @property {Boolean} IsPrivate
 * @property {Number} ItemCount
 * @property {String} LastItemDeletedDate
 * @property {String} LastItemModifiedDate
 * @property {String} LastItemUserModifiedDate
 * @property {Number} ListExperienceOptions
 * @property {String} ListItemEntityTypeFullName
 * @property {Number} MajorVersionLimit
 * @property {Number} MajorWithMinorVersionsLimit
 * @property {Boolean} MultipleDataList
 * @property {Boolean} NoCrawl
 * @property {Object} ParentWebPath
 * @property {String} ParentWebUrl
 * @property {Boolean} ParserDisabled
 * @property {Boolean} ServerTemplateCanCreateFolders
 * @property {String} TemplateFeatureId
 * @property {String} Title
 */
