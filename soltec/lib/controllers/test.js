var debug       = require('debug')('procedures'),
    log         = require('../../shared/log')(module),
    c           = require('../configuration'),
    request     = require('request'),
    async       = require('async'),
    q           = require('q'),
    mongoose    = require('mongoose');

// Test connectivity
/**
 * @api {get} /test Test connection
 * @apiName Test
 * @apiGroup Test
 *
 * @apiSuccess {String} db Database connection status.
 * @apiSuccess {String} auth AUTH api connection status.
 * @apiSuccess {String} worker WORKER api connection status.
 */
exports.connectivity = function(req, res) {
  var result = {};
  // API connectivity
  var apiPromise = q.defer();
  var requests = {
    auth: c.get('license') + 'v1/version',
  };

  async.forEachOf(requests, function(url, urlDescription, callback) {
    request({url: url}, function (err, response, version) {
      if (!err && response.statusCode === 200) {
        result[urlDescription] = 'ok';
        callback();
      } else {
        result[urlDescription] = 'error';
        callback(true);
      }
    });
  }, function(err) {
    if (!err) {
      apiPromise.resolve();
    } else {
      apiPromise.reject();
    }
  });

  // DB connectivity
  var dbPromise = q.defer();
  var db = mongoose.createConnection(c.get('mongo:clients') + 'kb_kalmia' + '?authSource=admin');
  db.on('error', function() {
    result.db = 'error';
    dbPromise.reject();
    db.close();
  });
  db.once('open', function() {
    result.db = 'ok';
    dbPromise.resolve();
    db.close();
  });
  // Check if all connectivity test passed
  q.all([apiPromise.promise, dbPromise.promise]).then(function() {
    res.send(result);
  }).catch(function() {
    res.statusCode = 404;
    res.send(result);
  });
};
