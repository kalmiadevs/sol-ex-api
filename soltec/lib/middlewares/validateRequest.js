var jwt           = require('jwt-simple'),
    mongoose      = require('mongoose'),
    softDelete    = require('mongoose-softdelete'),
    leanVirtuals  = require('mongoose-lean-virtuals'),
    historical    = require('historical'),
    paginate      = require('mongoose-paginate');
    DataTable     = require('mongoose-datatable'),
    glob          = require('glob'),
    log           = require('../../shared/log')(module),
    c             = require('../configuration'),
    path          = require('path'),
    q             = require('q'),
    request       = require('request');

mongoose.set('debug', true);

exports.validatePublicRequest = function(req, res, next) {
  log.info('================== HEADERS ==================');
  log.info(req.headers);
  log.info('================== ======= ==================');

  // Public requests need to have aid (account id) query parameter
  if (!req.query.aid) {
    res.status(401);
    res.json({
      'status'  : 401,
      'message' : 'Invalid account id'
    });
    return;
  }
  // Get database name from account id
  var options = {
    method: 'GET',
    url     : c.get('authApi') + '/db/' + req.query.aid
  };
  request(options, function (err, response, dbName) {
    // Make connection to the database
    return setConnection(req, res, dbName, next);
  });
};

exports.validateRequest = function(req, res, next) {

  // We skip the token outh for [OPTIONS] requests.
  if (req.method === 'OPTIONS') {
    next();
  }

  log.info('================== HEADERS ==================');
  log.info(req.headers);
  if (!req.headers['x-access-token']) {
    log.error('Houston, we have a problem!');
  }
  log.info('================== ======= ==================');

  var apiCall = req.url.split('/')[2];
  // We skip the token auth for test request.
  if (apiCall === 'test') {
    return next();
  }
  // We skip the token auth for public requests
  if (req.url.startsWith('/v1/public') || req.url.startsWith('/public')) {
    return next();
  }

  // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
  var token = (req.body && req.body.access_token) ||
               (req.query && req.query.access_token) ||
               req.headers['x-access-token'];
  // jscs:enable requireCamelCaseOrUpperCaseIdentifiers

  if (token) {
    try {
      var decoded = jwt.decode(token, c.get('secret'));

      if (decoded.exp <= Date.now()) {
        res.status(401);
        res.json({
          'status'  : 401,
          'message' : 'Token Expired'
        });
        return;
      }

      // Check if token from another api (unstable, stable)
      // TODO(Marko): Remove decoded.iss !== c.get('secret') this was to prevent forcing users to logout
      // because of our typo where iss was secret instead of api
      /*log.info('Issuer');
      log.info(decoded.iss, c.get('api'));
      if (decoded.iss !== c.get('api')) {
        res.status(401);
        res.json({
          'status'  : 401,
          'message' : 'Invalid token'
        });
        return;
      }*/

      req.user  = decoded;
      req.token = token;

      var options = {
        method: 'GET',
        url     : c.get('authApi') + '/authenticated/' + req.user.uid,
        headers : {'x-access-token': req.token}
      };

      request(options, function (err, response, authenticated) {
        try {
          authenticated = JSON.parse(authenticated);
          if (!err) {
              if (!authenticated.auth) {
                  if (response.statusCode === 401) {
                      res.status(401);
                      res.json({
                          'status': 401,
                          'message': 'Token expired'
                      });
                      return;
                  } else {
                      res.status(500);
                      res.json({
                          'status': 500,
                          'message': 'Oops something went wrong',
                          'error': err
                      });
                      return;
                  }
              } else {
                  return setConnection(req, res, decoded.db, next);
              }
          } else {
              log.error(err);
              res.status(500);
              res.json({
                  'status': 500,
                  'message': 'Oops something went wrong',
                  'error': err
              });
              return;
          }
        } catch (err) {
          log.error(err);
          res.status(500);
          res.json({
              'status': 500,
              'message': 'Oops something went wrong',
              'error': err
          });
          return;
        }
      });
    }
    catch (err) {
      res.status(500);
      res.json({
        'status': 500,
        'message': 'Oops something went wrong',
        'error': err
      });
      return;
    }
  }
  else {
    res.status(401);
    res.json({
      'status': 401,
      'message': 'Invalid Token or Key'
    });
    return;
  }
};

// Create a dynamic DB connection and save it to global variable
function setConnection(req, res, dbName, next) {
  try {
    var db;
    if (global.App.clientDbConn[dbName] === undefined) {
      db = mongoose.createConnection(c.get('mongo:clients') + dbName + '?authSource=admin');

      db.on('error', console.error.bind(console, 'connection error:'));

      db.once('open', function (callback) {
        log.info('Connection to DB ' + dbName);
        global.App.clientDbConn[dbName] = db;
      });
    }
    mongoose.connection = global.App.activeDb = global.App.clientDbConn[dbName];

    modelsInit(req, next, dbName, db);

  }
  catch (e) {
    console.log(e);
  }
}

// Init models if not already
function modelsInit(req, next, dbName, db) {
  log.debug('Check if models for this user are already compiled');

  if (global.App.clientModels[dbName] === undefined) {
    log.debug('Not compiled. Compiling models...');

    glob('./lib/models/**/*.js', function (err, files) {
      if (err) {
        log.error(err);
        return true;
      }

      if (files && files.length > 0) {
        var clientDb = {};
        for (var i in files) {
          if (i !== undefined) {
            var filename = files[i].replace(/^.*[\\\/]/, '');
            var fullname = filename.substr(0, filename.lastIndexOf('.'));
            clientDb[fullname] = require(path.resolve(files[i]))(mongoose, db, [softDelete, DataTable.init, leanVirtuals, historical, paginate]);
          }
        }
        global.App.clientModels[dbName] = clientDb;
      }

      req.db = global.App.clientModels[dbName];
      next();
    });
  }
  else {
    log.debug('Already compiled. Just setting the active connection.');
    req.db = global.App.clientModels[dbName];
    next();
  }
}
