module.exports = function (mongoose, dbconn, plugins) {

  let schema = new mongoose.Schema({
    owner: {type: mongoose.Schema.Types.ObjectId, ref: 'Users'},

    title: {type: String},
    content: {type: String},

    birth: {type: Date}
  }, {
    timestamps: true
  });

  // Ensure virtual fields are serialised.
  schema.set('toJSON', {virtuals: true});
  schema.set('toObject', {virtuals: true});

  // Apply plugins
  for (let i = 0; i < plugins.length; i++) {
    schema.plugin(plugins[i]);
  }

  return dbconn.model('Articles', schema, 'articles');
};
