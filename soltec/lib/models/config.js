module.exports = function (mongoose, dbconn, plugins) {

  let schema = new mongoose.Schema({
    //_id: {type: String},
    key: {type: String},
    val    : {}
  }, {
    timestamps: false,
    // _id: false
  });

  // Apply plugins
  for (let i = 0; i < plugins.length; i++) {
    schema.plugin(plugins[i]);
  }

  return dbconn.model('Config', schema, 'config');
};
