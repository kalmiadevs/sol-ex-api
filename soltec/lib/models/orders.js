module.exports = function (mongoose, dbconn, plugins) {

  let specifications = new mongoose.Schema({
    // motorPosition: {
    //   a: {type: Boolean},
    //   b: {type: Boolean},
    // },
    motorPos: { type: String },

    waterExit    : {
      a1    : { type: Boolean },
      b1    : { type: Boolean },
      a2    : { type: Boolean },
      b2    : { type: Boolean },
      a3    : { type: Boolean },
      b3    : { type: Boolean },
      a4    : { type: Boolean },
      b4    : { type: Boolean },
      custom: { type: Boolean }
    },
    powerSupply  : {
      p1: { type: Boolean },
      p2: { type: Boolean },
      p3: { type: Boolean },
      p4: { type: Boolean }
    },
    wallFixations: {
      p1p3: { type: Boolean },
      p2p4: { type: Boolean },
      p1p2: { type: Boolean },
      p3p4: { type: Boolean }
    },

    mountingWallType           : { type: String },
    mountingGroundType         : { type: String },
    mountingGroundQuantity     : { type: Number },
    mountingGroundColorStandard: { type: Boolean },

    bladesOpeningDirection: { type: String },

    ledLights  : { type: Boolean },
    ledQuantity: { type: Number },
    ledLength  : { type: Number },
    ledType    : { type: String },

    ledPositions            : [{ type: Number }],
    customLedPositionEnabled: { type: Boolean },
    customLedPosition       : { type: Number }
  });

  let positions = new mongoose.Schema({
    p1p2: { type: Boolean },
    p3p4: { type: Boolean },
    p1p3: { type: Boolean },
    p2p4: { type: Boolean }
  });

  let poleData = new mongoose.Schema({
    p1      : { type: Boolean },
    p2      : { type: Boolean },
    p3      : { type: Boolean },
    p4      : { type: Boolean },
    p1y     : { type: Number },
    p2y     : { type: Number },
    p3y     : { type: Number },
    p4y     : { type: Number },
    p1height: { type: Number },
    p2height: { type: Number },
    p3height: { type: Number },
    p4height: { type: Number }
  });

  const additionalPoleData = new mongoose.Schema({
    position: { type: String },
    x       : { type: Number },
    height  : { type: Number }
  });

  const slidingPanelGroup = new mongoose.Schema({
    type         : { type: String },
    size         : { type: Number },
    panelQuantity: { type: Number },
    placement    : [{ type: Number }],
    arrowLeft    : { type: String },
    arrowRight   : { type: String }
  });

  const glassPanelGroup = new mongoose.Schema({
    guides       : { type: Number },
    size         : { type: Number },
    panelQuantity: { type: Number },
    placement    : [{ type: Number }],
    arrowLeft    : { type: String },
    arrowRight   : { type: String },
    locker       : { type: Boolean }
  });


  let options = new mongoose.Schema({
    blindsPosition              : positions,
    blindsFabric                : { type: String },
    blindsCustomFabric          : { type: Boolean },
    blindsCustomFabricID        : { type: String },
    blindsFabricSoltis          : { type: Boolean },
    blindsFabricSoltisCode      : { type: String },
    blindsFullScreen            : { type: Boolean },
    blindsGuideColorMatchesFrame: { type: Boolean },
    blindsGuideColorCustomID    : { type: Number },
    blindsFinishType            : { type: String },
    blindsGuideType             : { type: String },
    blindsMotorType             : { type: String },
    blindsPanelSize1            : { type: Number },
    blindsPanelSize2            : { type: Number },

    slidingPanelPosition     : positions,
    slidingPanelOnLength     : slidingPanelGroup,
    slidingPanelOnWidth      : slidingPanelGroup,
    slidingPanelMaterial     : { type: String },
    slidingPanelColor        : { type: Number },
    slidingPanelCustomColor  : { type: Boolean },
    slidingPanelCustomColorID: { type: Number },
    slidingPanelFinishType   : { type: String },

    glassPanelsPosition: positions,
    glassPanelsOnLength: glassPanelGroup,
    glassPanelsOnWidth : glassPanelGroup
  });

  let accessories = new mongoose.Schema({
    // LED Lights
    betaQuantity: { type: Number },
    ledStrip    : { type: Number },

    // Remote controller
    additionalRemoteControl: { type: Number },

    // Heaters
    heaterQuantity          : { type: Number },
    heaterAnthraciteQuantity: { type: Number },

    // Audio
    bluetoothSystem: { type: Number },
    speakers       : { type: Number },
    rcAudioSystem  : { type: Number },

    // Sensors
    windSensorQuantity       : { type: Number },
    rainSensorQuantity       : { type: Number },
    temperatureSensorQuantity: { type: Number },
    presenceSensorQuantity   : { type: Number },
    externalAntenna          : { type: Number },

    // Somfy
    somfyMotorQuantity: { type: Number },

    somfyRTST1TBQuantity : { type: Number },
    somfyRTST4TBQuantity : { type: Number },
    somfyRTST16TBQuantity: { type: Number },

    somfyBridge1Quantity: { type: Number },
    somfyBridge7Quantity: { type: Number },

    somfyRTST1Quantity : { type: Number },
    somfyRTST4Quantity : { type: Number },
    somfyRTST16Quantity: { type: Number },

    doubleMotor: { type: Number },

    // Transport
    transportBox1: { type: Number },
    transportBox2: { type: Number },
    transportBox3: { type: Number },

    // Extra
    sparePartsKitSL: { type: Number },
    merbenitGlue   : { type: Number },
    butylTapeRoll  : { type: Number }
  });

  let product = new mongoose.Schema({
    pergolaType     : { type: String },
    installationType: { type: Number },

    pergolaQuantity: { type: Number },

    width : { type: Number },
    length: { type: Number },
    height: { type: Number },

    numberOfPoles          : { type: Number },
    numberOfAdditionalPoles: { type: Number },
    poleData               : poleData,
    customPoleHeight       : { type: Boolean },
    additionalPoleData     : [additionalPoleData],

    nonStandardColorEnabled: { type: Boolean },
    structureColor         : { type: Number },
    bladesColor            : { type: Number },
    structureFinishType    : { type: String },
    bladesFinishType       : { type: String },

    specifications: specifications,
    options       : options,
    accessories   : accessories
  });

  let documents = new mongoose.Schema({
    type    : { type: String },
    path    : { type: String },
    name    : { type: String },
    size    : { type: Number },
    noPublic: { type: Boolean },
    meta    : {}
  });

  let schema = new mongoose.Schema({
    owner             : { type: mongoose.Schema.Types.ObjectId, ref: 'Users' }, // who created order
    _originalOwnerId  : { type: mongoose.Schema.Types.ObjectId, ref: 'Users' }, // If distributor was changed, store the original owner id here
    _originalOwnerName: { type: String }, // If distributor was changed, store the original owner name here

    _state                   : { type: String },
    _public                  : { type: Boolean }, // Determines whether the order is public (can be transferred to another sales manager) - owner must be a distributor!
    _salesManagerOverrideId  : { type: mongoose.Schema.Types.ObjectId, ref: 'Users' }, // Override for what sales manager can see this order.
    _originalSalesManagerName: { type: String }, // Original name of the sales manager for public orders
    _originalSalesManagerId  : { type: mongoose.Schema.Types.ObjectId, ref: 'Users' }, // Original id of the sales manager for public orders

    number       : { type: String },
    projectNumber: { type: String },
    discount     : { type: String },
    discountVal  : { type: Number },
    transport    : { type: String },
    packaging    : { type: String },

    cost   : { type: String },
    costVal: { type: Number },
    step   : { type: String },
    status : { type: String },

    client              : {
      name       : { type: String },
      address    : { type: String },
      phone      : { type: String },
      email      : { type: String },
      vat        : { type: String },
      countryCode: { type: String } // Used for new configurator to store country code
    },
    deliverySameAsClient: { type: Boolean, required: true },

    sourceContactInfo: { type: Object }, // Source contact info object from new configurator

    delivery: {
      name   : { type: String },
      address: { type: String },
      phone  : { type: String },
      email  : { type: String }
    },

    deliveryCost: { type: Number },
    vatRate     : { type: Number },

    product: product,

    project: {
      name       : { type: String },
      date       : { type: Date },
      description: { type: String },

      // dateOfDispatch: {type: Date},
      // dueDate       : {type: Date},
      // startDate     : {type: Date},

      requestedDeliveryDate: { type: Date }
    },

    comment: { type: String },

    terms: {
      terms: { type: String },
      agree: { type: Boolean }
    },

    spSyncTimestamp : { type: Number },
    spModified      : { type: String },
    spSalesOppID    : { type: String },
    spProjectID     : { type: String },
    spCustomerID    : { type: String }, // Only applicable for orders created from the new configurator.
    spSalesOppStatus: { type: String },
    spOrigin        : { type: Boolean },
    spSource        : { type: Number },
    spDueDate       : { type: String },
    spFiles         : [{}], // {file: string, name: string, modified: string}[]
    birth           : { type: Date }, // we don't use createdAt from mongo since we wan't preserve it for orders created via SP

    // flag if we already send S2||S3 proforma email
    proformaEmailConfirmation: { type: Boolean },

    lastSyncToSp: { type: Number },
    modified    : { type: Number }, // timestamp when obj was modified (used for SP sync). we don't use mongo property with similar reason than for birth

    documents: [documents],

    languageOverride: { type: String },
    discountOverride: { type: Number },

    skipCustomerEmails: { type: Number },

    fromNewConfigurator: { type: Boolean },
    assemblyFee        : { type: Number },

    distributor : { type: String }, // sorting and filtering
    salesManager: { type: String }, // sorting and filtering

    transaction: {
      id    : { type: String },
      date  : { type: Date },
      amount: { type: Number },
      status: { type: String }
    }

    // its already  in mongoose ;)  version: {type: Number, default: 0}
  }, {
    timestamps: true
  });

  // Ensure virtual fields are serialised.
  schema.set('toJSON', { virtuals: true });
  schema.set('toObject', { virtuals: true });

  // Apply plugins
  for (let i = 0; i < plugins.length; i++) {
    schema.plugin(plugins[i]);
  }

  return dbconn.model('Orders', schema, 'orders');
};
