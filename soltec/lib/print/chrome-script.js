var HeadlessChrome = require('simple-headless-chrome');
var Config         = require('../const');
var c              = require('../configuration');

var license = Config.LICENSE_CONF;

var browser = new HeadlessChrome({
  headless: true,
  launchChrome: false,
  chrome: {
    host: 'localhost',
    port: 9222,
    remote: true,
  },
  browser: {
    loadPageTimeout: 180000,
    loadSelectorInterval: 500,
    loadSelectorTimeout: 180000,
    browserLog: true
  }
});

async function navigate (orderId, token, path, authorName) {
  console.log('navigate');
  return new Promise(async(resolve, reject) => {
    try {
      // token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1MTYzMDU3ODYxMzksImFjYyI6IjU5YjdhY2MxYzNlZWU3YjcwMDgzY2Q1NiIsInN1YiI6InNvbHRlY0BrYWxtaWEuc2kiLCJ1aWQiOiI1OWI3YWNjMmMzZWVlN2I3MDA4M2NkNTciLCJkYiI6ImthbF9jdXN0b21lcl9zb2x0ZWMiLCJncnAiOiJzdXBlcmFkbWluIiwicGVyIjp7ImtiX2FkbWluIjo3fSwiaXNzIjoiaHR0cHM6Ly9zb2wtYXBpLmthbG1pYS5zaSJ9.M681oKtwJUsZrH85icuvtpsU8P4yTINYLcT8BNJ5Y7A";
      await browser.init()
      console.log('browser init');

      const mainTab = await browser.newTab({ privateTab: false })

      // Navigate to a URL
      await mainTab.goTo(c.get('webApp') + '/user/login')
      console.log(c.get('webApp') + '/user/login')
      // await mainTab.goTo('https://soltec.kalmia.si/user/login')

      await mainTab.injectScript(" \
        localStorage.setItem('ng2-ui-auth_token', '" + token + "');\
      ");

      await mainTab.goTo(c.get('webApp') + '/order/' + orderId + '/print?author=' + encodeURIComponent(authorName));
      // await mainTab.goTo('https://soltec.kalmia.si/order/' + orderId + '/print')
      console.log('Order print page');

      // Remove top bar & dev warning
      await mainTab.evaluateAsync(function() {
        var topBar = document.getElementById('project-container');
        if (topBar) {
          topBar.removeChild(topBar.children[0]);
        }
        var devWarning = document.getElementById('devWarningNotice');
        if (devWarning) {
          devWarning.remove();
        }
      });

      // Remove dev warning
      await mainTab.evaluateAsync(function() {

      });
      
      console.log('Top bar removed');

      await mainTab.waitForSelectorToLoad('#pergolaDisplayLarge', 0);

      await mainTab.wait(1500);
      
      // Take a screenshot
      const file = await mainTab.savePdf(path, {
        landscape: false,
        paperWidth: 8.3, // A4 size in inches
        paperHeight: 11.7, // A4 size in inches
        scale: 1.0,
        marginTop: 0.4, // Inches
        marginRight: 0.38, // Inches
        marginBottom: 0.4, // Inches
        marginLeft: 0.3, // Inches
        printBackground: true,
      });

      console.log('Order print');

      mainTab.close();
      console.log('Tab closed');

      // Close the browser
      await browser.close()

      resolve(file);

    } catch (err) {
      console.log('ERROR!', err)
      reject(err);
    }
  })
}


exports.navigate = navigate;
