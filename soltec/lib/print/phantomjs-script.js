var system = require('system');
var args = system.args;
var page = require('webpage').create();

/*
// FOR QUICK TESTING
args = [
  "/home/tilen/Kalmia/Soltec/KalSoltec API/soltec/lib/print/phantomjs-script.js",
  "http://localhost:4301/order/5a3bd32d2c213f453ba25f54/print",
  "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1MTQ1NDEzMDQwODMsImFjYyI6IjVhMzc4NzRkOTg2MmQ5NmUyY2VlYjhjMCIsInN1YiI6InNvbHRlY0BrYWxtaWEuc2kiLCJ1aWQiOiI1YTM3ODc0ZDk4NjJkOTZlMmNlZWI4YzEiLCJkYiI6ImthbF9jdXN0b21lcl9zb2x0ZWMiLCJncnAiOiJzdXBlcmFkbWluIiwicGVyIjp7ImtiX2FkbWluIjo3fSwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo1MDAxL3YxIn0.Ryb8FxY9-Mq8XOwDvcr9eGQNhvKf1YetKD4n2tnUSxQ",
  "/tmp/SOLTEC_PRINT_TEST.pdf"
];
*/

var url = args[1];
var token = args[2];
var savePath = args[3];

console.log('url:' + url);
console.log('savePath:' + savePath);

page.setContent("", url); // doesn't actually open any page

page.evaluate(function(token) {
  localStorage.setItem("ng2-ui-auth_token", token);
}, token);

page.paperSize = {
  format: 'A4',
  orientation: 'portrait',

  width: (1200 ) + 'px', // 1200 so we don't fall into mobile mode
  height: (1698) + 'px',

  margin: '1.5cm'
};

page.customHeaders = {
  "x-access-token": token,
  "IS-AUTO-GENERATOR": 'TRUE'
};

var componentCallBackTimeOut;
var componentCallCount = 0; // we expect 6 calls
// var componentInitCount = 0; // TODO we can make better logic to figure out when ready

page.onCallback = function(data) {
  console.log('Callback: ' + JSON.stringify(data));
  if (data.component === "OrderComponent" && data.type === "ngAfterViewChecked") {
    componentCallCount++;
    console.log('Component call count: ' + componentCallCount);
    // if (componentCallCount < 5) {
    //  return;
    // }
    if (componentCallBackTimeOut) {
      clearTimeout(componentCallBackTimeOut);
      console.log('Render timeout cleared.');
    }
    console.log('Render timeout set.');
    componentCallBackTimeOut = setTimeout(function() {
      console.log('Rendering...');
      page.render(savePath);
      phantom.exit(0);
    }, 1000 * 5);
  }
};

page.open(url, function(status) {
  console.log('status: ' + status);
  if(status === "success") {
    setTimeout(function() {
      phantom.exit(2);
    }, 1000 * 90);
  } else {
    phantom.exit(1);
  }
});
