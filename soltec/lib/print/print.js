const path         = require('path'),
      FsLogApi     = require('../../shared/abstract/fs-log-api'),
      LicenseApi   = require('../../shared/sdk/license'),
      Config       = require('../const'),
      c            = require('../configuration');

const licenseConf = Config.LICENSE_CONF;
var chromeScript = require('./chrome-script');

async function webOrderToPdf(orderId, savePath, authorName) {
  const license = new LicenseApi(c.get('authApi'));
  license.setAuth(licenseConf.email, licenseConf.password);
  await license.checkIfAuth();

  return new Promise(async(resolve, reject) => {
    const fsLog = new FsLogApi([], {name: 'print.log'});

    let file;
    try {
      file = await chromeScript.navigate(orderId, license.token, savePath, authorName);
      console.log(file);
    } catch (e) {
      console.error('Error generating order snapshot', e);
      return reject(e);
    }

    fsLog.log({orderId, savePath, file});
    resolve(file);
  })
}

exports.webOrderToPdf = webOrderToPdf;
