const path         = require('path'),
      childProcess = require('child_process'),
      phantomjs    = require('phantomjs-prebuilt'),
      binPath      = phantomjs.path,
      FsLogApi     = require('../../shared/abstract/fs-log-api'),
      LicenseApi   = require('../../shared/sdk/license'),
      Config       = require('../const'),
      c            = require('../configuration');

const licenseConf = Config.LICENSE_CONF;
const license     = new LicenseApi(c.get('authApi'));

async function webOrderToPdf(orderId, savePath) {
  license.setAuth(licenseConf.email, licenseConf.password);
  await license.checkIfAuth();

  const childArgs = [
    path.join(__dirname, 'phantomjs-script.js'),
    `${c.get('appBaseUrl')}/order/${ orderId }/print`,
    license.token,
    savePath
  ];

  return new Promise((resolve, reject) => {
    const fsLog = new FsLogApi([], {name: 'print.log'});

    childProcess.execFile(binPath, childArgs, function(err, stdout, stderr) {
      if (err) {
        fsLog.error({orderId, savePath, stderr, err, stdout});
        reject(err);
      } else {
        fsLog.log({orderId, savePath, stdout});
        resolve(stdout);
      }
    })
  })
}

// Win debug:
// webOrderToPdf('5a47d3de61d9ca2ed80158cb', 'E:\\WORK\\partnerportal-api\\soltec\\lib\\print\\debug.pdf').then(console.info).catch(console.error);
// Linux debug:
// webOrderToPdf('5a3d0e28f600127606c44b41', '/tmp/debug.pdf').then(console.info).catch(console.error);

exports.webOrderToPdf = webOrderToPdf;
