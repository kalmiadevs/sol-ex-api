var mongoose = require('mongoose');
var c = require('../lib/configuration');
var _ = require('underscore');

// Print instructions
console.log('This script will add closed status.');

var license = mongoose.createConnection(c.get('mongo:license') + '?authSource=admin');
license.once('open', function() {
  license.accounts  = require('../lib/models/license/accounts')(mongoose, license, []);
  license.accounts.find().exec(function(err, accounts) {
    _.each(accounts, function(account) {
      console.log('Updating case status for', account.company);
      updateForDB(account.database);
    });
  });
});

function updateForDB(databaseName) {
  // Open db connection
  var db = mongoose.createConnection(c.get('mongo:clients') + databaseName + '?authSource=admin');
  db.once('open', function () {
    // Load models
    db.cases = require('../lib/models/cases')(mongoose, db, []);
    db.procedures = require('../lib/models/procedure')(mongoose, db, []);
    db.cases
      .find({})
      .exec(function (err, cases) {
        if (!err) {
          console.log('Number of cases', cases.length);
          var i = 0;
          _.each(cases, function (c) {
            c.status = 'closed';
            c.save(function (err) {
              if (!err) {
                i++;
                console.log('Done case', c.name, i);
              }
            });
          });
        }
      });
  });
}
