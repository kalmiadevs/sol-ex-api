const adminApi = require('../lib/controllers/admin').adminApi;
const SM = require('../lib/const').SALES_MANAGERS;

adminApi.$createSalesManagers(SM)
  .then(x => {
    console.log(x);
    process.exit(0)
  })
  .catch(e => {
    console.error(e);
    process.exit(0)
  });