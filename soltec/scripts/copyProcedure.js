/********************************************************************************************************
This script will copy procedure to new API from old/different API - it will do everything automatically
It will:  a) Copy codelists (if they exists, find a new name)
          b) Copy categories (if category name already exists, use that one)
          c) Copy procedure (only if procedure with that name doesnt exist already)
          d) Copy documents
          e) Copy tasks
Run the script with nodejs ./copyProcedure.js, but first fill the data below (tokens, apis, ids ...)
********************************************************************************************************/

/*************************   FILL THIS DATA HERE   *****************************************************/
//jscs: disable maximumLineLength
var fromToken     = '', // jshint ignore:line
    toToken       = '', //jshint ignore:line
    procId        = '',
    fromApi       = 'https://api.mightyfields.com/api/v1/',
    toApi         = 'https://api.mightyfields.com/api/v1/',
    toUserId      = '',
    copyDocsTasks = true;
/*******************************************************************************************************/

var request     = require('request'),
    _           = require('underscore'),
    async       = require('async');

// Get all codelists from procedure form or change all codelist ids
function getChangeCodelists(procedure, swapCodelists) {
  var codelists = [];
  _.each(procedure.forms, function(form) {
    _.each(form.sections, function(section) {
      _.each(section.rows, function(row) {
        _.each(row.fields, function(field) {
          if (field.dataSource) {
            if (swapCodelists) {
              // Also check if we successfully saved new codelist
              if (swapCodelists[field.dataSource.codelist]) {
                field.dataSource.codelist = swapCodelists[field.dataSource.codelist];
              } else {
                delete field.dataSource;
              }
            } else {
              codelists.push(field.dataSource.codelist);
            }
          }
          if (field.properties && field.properties.default && swapCodelists) {
            if (swapCodelists[field.properties.default]) {
              field.properties.default = swapCodelists[field.properties.default];
            } else {
              delete field.properties;
            }
          }
          // Check if table
          if (field.rows) {
            _.each(field.rows, function(tRow) {
              _.each(tRow.fields, function(tField) {
                if (tField.dataSource) {
                  if (swapCodelists) {
                    // Also check if we successfully saved new codelist
                    if (swapCodelists[tField.dataSource.codelist]) {
                      tField.dataSource.codelist = swapCodelists[tField.dataSource.codelist];
                    } else {
                      delete tField.dataSource;
                    }
                  } else {
                    codelists.push(tField.dataSource.codelist);
                  }
                }
              });
            });
          }
        });
      });
    });
  });

  if (swapCodelists) {
    return procedure;
  }
  return _.uniq(codelists);
}

var options = {
  headers : {'x-access-token': fromToken},
  url : fromApi + 'procedures/' + procId,
  method : 'GET',
  json : true
};

// Get procedure
var procedure;
var swapCodelists = {};
var swapDocuments = {};
request(options, function (err, proc) {
  if (!err && proc.body.procedure) {
    procedure = proc.body.procedure;

    async.parallel({
      categories: function(callback) {
        // Get categories
        var categories = [];
        async.each(procedure.categories, function(category, clbk) {
          options.url = fromApi + 'categories/' + category;

          request(options, function (err, c) {
            if (!err) {
              if (c.body.category) {
                categories.push(c.body.category);
              }
              clbk();
            } else {
              clbk('Could not get category');
            }
          });

        }, function(err) {
          callback(err, categories);
        });
      },
      tasks: function(callback) {
        // Get tasks
        var tasks = [];
        async.each(procedure.tasks, function(task, clbk) {
          options.url = fromApi + 'tasks/' + task._id;

          request(options, function (err, t) {
            if (!err) {
              if (t.body.task) {
                tasks.push(t.body.task);
              }
              clbk();
            } else {
              clbk('Could not get task');
            }
          });

        }, function(err) {
          callback(err, tasks);
        });
      },
      documents: function(callback) {
        // Get documents
        var documents = [];
        async.each(procedure.documents, function(document, clbk) {
          options.url = fromApi + 'documents/' + document._id;

          request(options, function (err, d) {
            if (!err) {
              if (d.body.document) {
                documents.push(d.body.document);
              }
              clbk();
            } else {
              clbk('Could not get document');
            }
          });

        }, function(err) {
          callback(err, documents);
        });
      },
      codelists: function(callback) {
        // Get all codelist ids
        var codelists = {};
        codelists.codelists = getChangeCodelists(procedure.published);
        // Get all codeitems
        codelists.codeitems = [];
        async.each(codelists.codelists, function(codelist, clbk) {
          options.url = fromApi + 'codelists/' + codelist + '/codeitems';

          request(options, function (err, c) {
            if (!err) {
              codelists.codeitems.push(c.body);
              clbk();
            } else {
              clbk('Could not get codeitem');
            }
          });

        }, function(err) {
          codelists.codeitems = _.flatten(codelists.codeitems);
          // Get all codelists information
          async.each(codelists.codelists, function(c, clbk2) {
            options.url = fromApi + 'codelists/' + c;
            codelists.codelistInfo = [];

            request(options, function (err, c) {
              if (!err) {
                if (c.body.codelist) {
                  codelists.codelistInfo.push(c.body.codelist);
                }
                clbk2();
              } else {
                clbk2('Could not get codelist info');
              }
            });
          }, function(err) {
            callback(err, codelists);
          });
        });

      },
      // Check if procedure name already exists in toApi
      procedureNameValid: function(callback) {
        var options = {
          headers : {'x-access-token': toToken},
          url : toApi + 'procedurename/valid/',
          method : 'POST',
          json : true,
          body : {
            label : procedure.label
          }
        };
        request(options, function(err, s) {
          if (s.body.error) {
            callback(err, false);
          } else {
            callback(err, true);
          }
        });
      },
      // To check if any of the categories already exist in toApi
      allCategories: function(callback) {
        var options = {
          headers : {'x-access-token': toToken},
          url : toApi + 'categories/',
          method : 'GET',
          json : true
        };
        request(options, function(err, cat) {
          callback(err, cat.body);
        });
      },
      // To check if any of the codelists already exist in toApi
      allCodelists: function(callback) {
        var options = {
          headers : {'x-access-token': toToken},
          url : toApi + 'codelists/',
          method : 'GET',
          json : true
        };
        request(options, function(err, codelists) {
          if (!_.isEmpty(codelists.body)) {
            callback(err, _.sortBy(codelists.body, 'label'));
          } else {
            callback(err, null);
          }
        });
      }
    }, function(err, r) {
      if (!err) {

        if (!r.procedureNameValid) {
          console.log('Procedure with that name already exits! Stopping ...');
          return;
        }

        // Enter codelists in new API
        async.each(r.codelists.codelistInfo, function(codelist, clbk) {

          // Check if codelist name already exists - add new name
          var nameExists = _.findWhere(r.allCodelists, {label: codelist.label});
          var i = 2;
          var oldName = codelist.label;
          while (nameExists) {
            codelist.label = oldName + ' - ' + i;
            nameExists = _.findWhere(r.allCodelists, {label: codelist.label});
            i++;
          }

          var options = {
            headers : {'x-access-token': toToken},
            url : toApi + 'codelists/',
            method : 'POST',
            json : true,
            body : {
              label : codelist.label,
              description : codelist.description,
              type : codelist.type,
              template : codelist.template
            }
          };

          request(options, function (err, c) {
            if (!err) {
              if (c.body.codelist) {
                swapCodelists[codelist._id] = c.body.codelist._id;
                console.log('Saving codelist: ' + codelist.label + ' done');
              }
              clbk();
            } else {
              clbk('Error saving codelist: ' + codelist.label);
            }
          });
        }, function(err) {
          // Done saving codelists
          // Start saving codeitems
          async.eachSeries(r.codelists.codeitems, function(codeitem, clbk2) {

            var options = {
              headers : {'x-access-token': toToken},
              url : toApi + 'codelists/' + swapCodelists[codeitem.codelist] + '/codeitems',
              method : 'POST',
              json : true,
              body : {
                name : codeitem.name,
                value : codeitem.value
              }
            };

            request(options, function (err, c) {
              if (!err) {
                if (c.body.codeitem) {
                  console.log('Saving codeitem: ' + codeitem.name + ' done');
                }
                clbk2();
              } else {
                clbk2('Error saving codeitem: ' + codeitem.name + ' - ', err);
              }
            });

          }, function(err) {
            // First change all ids to new ones in procedure
            procedure.published = getChangeCodelists(procedure.published, swapCodelists);

            procedure.categories = [];
            // Find if category already exists
            _.each(r.categories, function(category) {
              var exists = _.findWhere(r.allCategories, {label: category.label});
              if (exists) {
                procedure.categories.push(exists._id);
                category.skip = true;
              }
            });
            // Add categories
            async.eachSeries(r.categories, function(category, clbk3) {
              if (category.skip) {
                clbk3();
              } else {
                var options = {
                  headers : {'x-access-token': toToken},
                  url : toApi + 'categories/',
                  method : 'POST',
                  json : true,
                  body : {
                    label : category.label
                  }
                };

                request(options, function (err, p) {
                  if (!err) {
                    if (p.body.category) {
                      procedure.categories.push(p.body.category._id);
                      console.log('Category saved');
                    }
                    clbk3();
                  } else {
                    console.log('Error saving category: ', err);
                    clbk3('Error saving category');
                  }
                });
              }

            }, function(err) {
              // Add procedure
              procedure.users = [toUserId];
              procedure.forms = procedure.published.forms;

              var options = {
                headers : {'x-access-token': toToken},
                url : toApi + 'procedures/',
                method : 'POST',
                json : true,
                body : {
                  procedure : procedure
                }
              };

              request(options, function (err, p) {
                if (!err && p.body.procedure) {
                  procedure._id = p.body.procedure._id;
                  console.log('Procedure saved');

                  // Publish procedure
                  options.method = 'GET';
                  options.url = toApi + 'procedures/' + procedure._id + '/publish';
                  request(options, function (err, p) {
                    if (!err) {
                      console.log('Procedure published');
                    } else {
                      console.log('Error publishing procedure: ', err);
                    }
                  });

                  if (copyDocsTasks) {
                    // Add documents
                    async.each(r.documents, function(doc, clbk4) {
                      var options = {
                        headers : {'x-access-token': toToken},
                        url : toApi + 'documents/',
                        method : 'POST',
                        json : true,
                        body : {
                          label : doc.label,
                          description : doc.description,
                          content : doc.content,
                          procedure : procedure._id,
                          user : toUserId
                        }
                      };

                      request(options, function (err, d) {
                        if (!err) {
                          if (d.body.document) {
                            swapDocuments[doc._id] = d.body.document._id;
                            console.log('Document saved');
                          }
                          clbk4();
                        } else {
                          console.log('Error saving document: ', err);
                          clbk4('Error saving document');
                        }
                      });
                    }, function(err) {

                      // Add Tasks
                      async.each(r.tasks, function(task, clbk5) {

                        // Swap documents with new ones
                        if (task.content && task.content.attachments) {
                          var newAttachments = [];
                          _.each(task.content.attachments, function(attachment) {
                            newAttachments.push(swapDocuments[attachment]);
                          });
                          task.content.attachments = newAttachments;
                        }

                        var options = {
                          headers : {'x-access-token': toToken},
                          url : toApi + 'tasks/',
                          method : 'POST',
                          json : true,
                          body : {
                            label : task.label,
                            description : task.description,
                            content : task.content,
                            procedure : procedure._id,
                            type : task.type
                          }
                        };

                        request(options, function (err, t) {
                          if (!err) {
                            console.log('Task saved');
                            clbk5();
                          } else {
                            console.log('Error saving task: ', err);
                            clbk5('Error saving task');
                          }
                        });

                      }, function(err) {
                        if (err) {
                          console.log(err);
                        }
                        console.log('FINISHED');
                      });
                    });

                  } else {
                    console.log('FINISHED');
                  }

                } else {
                  console.log('Error saving procedure: ', err);
                }
              });
            });

          });
        });
      } else {
        console.log(err);
      }
    });

  } else {
    console.log('Could not get procedure');
  }
});
