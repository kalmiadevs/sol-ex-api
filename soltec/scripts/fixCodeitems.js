var mongoose = require('mongoose');
var c = require('../lib/configuration');
var _ = require('underscore');

// Print instructions
console.log('This script will create codeitems.');

var license = mongoose.createConnection(c.get('mongo:license') + '?authSource=admin');
license.once('open', function() {
  license.accounts  = require('../lib/models/license/accounts')(mongoose, license, []);
  license.accounts.find().exec(function(err, accounts) {
    _.each(accounts, function(account) {
      console.log('Creating codeitems for', account.company);
      updateForDB(account.database);
    });
  });
});

function updateForDB(databaseName) {
  // Open db connection
  var db = mongoose.createConnection(c.get('mongo:clients') + databaseName + '?authSource=admin');
  db.once('open', function () {
    // Load models
    db.codelists = require('../lib/models/codelists')(mongoose, db, []);
    db.codeitems = require('../lib/models/codeitems')(mongoose, db, []);
    db.codelists
      .find()
      .exec(function (err, codelists) {
        _.each(codelists, function (c) {
          console.log('Creating codeitems for:', c.label);
          if (c.codeitems && c.codeitems.length > 0) {
            var count = 0;
            _.each(c.codeitems, function (ci) {
              var codeitem = new db.codeitems({
                _id        : ci._id,
                name       : ci.name,
                value      : ci.value,
                type       : c.type,
                codelist   : c._id
              });

              codeitem.save(function (err) {
                if (!err) {
                  count ++;

                  if (count === c.codeitems.length) {
                    c.codeitems = undefined;
                    c.size = count;
                    c.save(function (err) {
                      if (!err) {
                        console.log('Finished creating codeitems for', c.label);
                      }
                    });
                  }
                }
              });
            });
          } else if (c.codeitems) {
            c.codeitems = undefined;
            c.size = 0;
            c.save(function (err) {
              if (!err) {
                console.log('Finished creating codeitems for', c.label);
              }
            });
          }
        });
      });
  });
}
