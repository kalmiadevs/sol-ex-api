var mongoose = require('mongoose');
var c = require('../lib/configuration');
var _ = require('underscore');

// Print instructions
console.log('This script will fix image sources.');

// Get static url and add https:// if there is none
var staticUrl = c.get('static');
if (!staticUrl.startsWith('http')) {
  staticUrl = 'https://' + staticUrl;
}

var license = mongoose.createConnection(c.get('mongo:license') + '?authSource=admin');
license.once('open', function() {
  license.accounts  = require('../lib/models/license/accounts')(mongoose, license, []);
  license.accounts.find().exec(function(err, accounts) {
    _.each(accounts, function(account) {
      console.log('Updating image sources for', account.company);
      updateForDB(account.database, account._id);
    });
  });
});

function updateForDB(databaseName, aid) {
  // Open db connection
  var db = mongoose.createConnection(c.get('mongo:clients') + databaseName + '?authSource=admin');
  db.once('open', function () {
    // Load models
    db.cases = require('../lib/models/cases')(mongoose, db, []);
    db.procedures = require('../lib/models/procedure')(mongoose, db, []);
    db.cases
      .find()
      .exec(function (err, cases) {
        _.each(cases, function(ca) {
          var cont = ca.content;
          _.each(cont, function(val) {
            if (val.value instanceof Array) {
              // Fix source for normal image
              if (val.value[0] && val.value[0].name) {
                if (val.value[0].source) {
                  for (var i = 0; i < val.value.length; i++) {
                    if (!val.value[i].source.startsWith('http')) {
                      val.value[i].source = staticUrl + '/v1/image/' + aid + '/' + val.value[i].id + '/image';
                    }
                  }
                }
              // Fix in table
              } else {
                for (var j = 0; j < val.value.length; j++) {
                  for (var k in val.value[j]) {
                    if (k && val.value[j][k].Type === 'file') {
                      if (val.value[j][k].Value[0] && val.value[j][k].Value[0].source) {
                        for (var l = 0; l < val.value[j][k].Value.length; l++) {
                          if (!val.value[j][k].Value[l].source.startsWith('http')) {
                            val.value[j][k].Value[l].source = staticUrl + '/v1/image/' + aid + '/' + val.value[j][k].Value[l].id + '/image';
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          });
          ca.content = cont;
          ca.markModified('content');
          ca.save();
        });
      });
  });
}
