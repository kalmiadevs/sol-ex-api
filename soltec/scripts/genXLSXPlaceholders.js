"use strict";
const _       = require('lodash'),
      order   = require('../lib/controllers/order').orderApi;

let schema = order.entitySchema;

let placeholders = [];

function getChildren(arr, parentPlaceholder = '') {
  arr.forEach((x) => {
    let placeholder = parentPlaceholder + '.' + x.key;
    if (x.schema._inner.children) {
      getChildren(x.schema._inner.children, placeholder);
    } else {
      placeholders.push(placeholder);
    }
  })
}
getChildren(schema._inner.children);

_.map(placeholders, (x) => {
  let val = '${' + x.substring(1) + '}';
  console.log(val);
});
