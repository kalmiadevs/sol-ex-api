var client;
var collections = { };

var mongoose      = require('mongoose'),
    log           = require('../shared/log')(module),
    c             = require('../lib/configuration');

try {
  var db;
      db = mongoose.createConnection(c.get('mongo:license'));

      db.on('error', console.error.bind(console, 'connection error:'));
      
      db.on('connected', function() {
        log.info('==> Mongo DB connection open');
      });

      db.once('open', function () {
        log.info('==> Connection to DB license');
      });
}
catch (e) {
  console.log(e);
}