const LicenseApi = require('../shared/sdk/license'),
    c = require('../lib/configuration'),
    Config = require('../lib/const'),
    generator     = require('password-generator'),
    promptly = require('promptly');

(async () => {
    const licenseConf = Config.LICENSE_CONF;

    const license = new LicenseApi(c.get('authApi'));
    license.setAuth(licenseConf.email, licenseConf.password);

    console.log('====== Reset User Password ======');
    let uid = await promptly.prompt('User ID: ');
    uid = uid.trim();

    console.log('Getting user data...');
    let user;
    try {
        user = await license.getUser(uid);
    } catch (e) {
        console.error('Can\'t find user with this ID!');
        process.exit(1);
    }

    console.log('Found user: ');
    console.log(user);

    const answer = await promptly.confirm('Are you sure you want to reset this user\'s password? (y/n) ');
    if (!answer) {
        console.log('Aborted.');
        process.exit(1);
    } else {
        const newPassword = generator(12, false);
        console.log('New password: ' + newPassword);

        user.password = newPassword;
        user.newPassword = user.password;
        user.confirmNewPassword = user.password;

        console.log('Updating user data...');
        newUserData = await license.updateUser(user, true);

        if (newUserData) {
            console.log('Done. Have a nice day :).');
        } else {
            console.error('Error: License server did not return new user data, the password may not have been updated.')
        }

        process.exit(0);
    }
})();
