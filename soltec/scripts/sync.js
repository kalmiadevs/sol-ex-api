console.log('Changing icwd to soltec <root> dir.');
process.chdir('../');
console.log('Script changed cwd to: ' + process.cwd());
console.log('Note: YOU MUST HAVE LICENSE SERVER RUNNING!');
console.log('Starting sync ...');

const orderApi            = require('../lib/controllers/order').orderApi;

let action;
let limit;
let id;

function help() {
  console.log('\n================');
  console.log('Note: License server must be online!');
  console.log('\n================\nUsage:');
  console.log('spSyncCompanies    - pull companies from share point');
  console.log('spSyncOrders       - pull orders from share point');
  console.log('syncOrders         - push orders to share point');
  console.log('syncOrder          - push specific order to share point');
  console.log('--help             - this help');
  console.log('--limit            - iteration limit');
  console.log('--id               - id');
  console.log('======================');
  console.log('Example usage:');
  console.log('node spSyncCompanies');
  console.log('node spSyncCompanies --limit=10');
  console.log('node syncOrders');
  console.log('node syncOrder --id=<order id>');
  console.log('======================');
  process.exit(0)
}

process.argv.slice(2).forEach(x => {
  if (x.startsWith('--limit=')) {
    limit = + x.slice('--limit='.length);
  }
  if (x.startsWith('--id=')) {
    id = x.slice('--id='.length);
  }
  if (x === ('--help')) {
    help();
  }
  if (x === ('spSyncCompanies')) action = '$spSyncCompanies';
  if (x === ('spSyncOrders')) action = '$spSyncOrders';
  if (x === ('syncOrders')) action = '$syncOrders';
  if (x === ('syncOrder')) action = '$syncOrder';
});

const self = orderApi.syncApi;

// FOR DEBUG:
// self.skipAccountManagerCheck = true;
// self.skipPhaseCheck          = true;

const actions = {
  $spSyncCompanies: (x) => { return self.$spSyncCompanies(x) },
  $spSyncOrders   : (x) => { return self.$spSyncOrders(x) },
  $syncOrders     : (x) => { return self.$syncOrders(x) },
  $syncOrder      : (x) => { return self.$syncOrder(x) },
};

(actions[action] || (() => { return Promise.reject('Unknown action. Use --help for list of commands.') }))(limit || id)
  .then(r => {
    console.log(r);
    process.exit(0)
  })
  .catch(e => {
    console.error(e);
    console.info('Did you check if LICENSE SERVER IS RUNNING?');
    help();
  });