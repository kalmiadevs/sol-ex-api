console.log('Changing icwd to soltec <root> dir.');
process.chdir('../');
console.log('Script changed cwd to: ' + process.cwd());
console.log('Note: YOU MUST HAVE LICENSE SERVER RUNNING!');
console.log('Starting sync ...');

const orderApi = require('../lib/controllers/order').orderApi;
const self = orderApi.syncApi;
console.log('ORDER ID: ' + process.argv[2]);
self.$syncOrder(process.argv[2])
    .then(r => {
        console.log(r);
        process.exit(0)
    })
    .catch(e => {
        console.error(e);
        console.info('Did you check if LICENSE SERVER IS RUNNING?');
    });
