const log = require('../log'),
  Raven = require('raven'),
  _ = require('lodash'),
  FsLogApi = require('./fs-log-api'),
  CircularJSON = require('circular-json'),
  hasRaven = require('../store/raven').isInstalled;

/**
 * This contains all common operations.
 * List of features:
 *  - error handling
 *  - logging
 *  - permission check
 *
 * @type {module.BaseApi}
 */
module.exports = class BaseApi {

  constructor(options = {
    module         : module,
    niceServiceName: 'unknown',
  }) {
    this.log = log(options.module || module);
    this.niceServiceName = options.niceServiceName || 'BaseApi';

    /**
     * @typedef {{resStatusCode: number, resCode: string, resMessage: string}} errorRes
     */
    /**
     * List of possible errors
     * @type { Object.<string, errorRes> }
     */
    this.errors = {
      unknownError: {
        resStatusCode: 500,
        resCode      : 'unknown_error',
        resMessage   : `Unknown error happened for ${ options.niceServiceName }.`
      }
    };

    /**
     * List of used permissions.
     * @type {{}}
     */
    this.permissions = {};
  }

  addErrors(errors) {
    this.errors = _.extend(this.errors, errors);
  }

  /**
   *
   * @param req
   * @param key
   * @param mask
   * @return {boolean}
   */
  hasPermission(req, key, mask) {
    return !!(req.user.permissions[key] & mask)
  }

  /**
   *
   * @param req
   * @param key
   * @return {boolean}
   */
  canRead(req, key) {
    return this.hasPermission(req, key, 0b100);
  }

  /**
   *
   * @param req
   * @param key
   * @return {boolean}
   */
  canWrite(req, key) {
    return this.hasPermission(req, key, 0b010);
  }

  /**
   *
   * @param req
   * @param key
   * @return {boolean}
   */
  canDelete(req, key) {
    return this.hasPermission(req, key, 0b001);
  }

  reportInfo(data) {
    console.info(data);
    new FsLogApi([this.niceServiceName], {name: 'info.log'}).error(data);
  }

  reportWarning(data) {
    console.warn(data);
    new FsLogApi([this.niceServiceName], {name: 'warn.log'}).error(data);
    Raven.captureException(new Error(data), {
      level: 'warning',
    })
  }

  reportError(e, extra) {
    // Debug log for developers (prints nice stack)
    console.error(e);
    console.log('Extra data:', extra);

    // Winston log
    this.log.error(`Error in ${ this.niceServiceName } service`, e.message);

    new FsLogApi([this.niceServiceName], {name: 'error.log'}).error({message: (e || {}).message, error: e, extra});

    // Raven log
    if (hasRaven()) {
      e.extraData = {
        niceServiceName: this.niceServiceName,
      };
      _.extend(e.extraData, extra || {});

      if (e.message || e.stack) {
        try {
          Raven.captureException(e);
        } catch (re) {
          new FsLogApi([this.niceServiceName], {name: 'error.log'}).error(re);
          Raven.captureException(new Error(re.message));
        }
      } else {
        const tmpE = new Error(`Object error ${ +new Date() }.${ e.statusCode ? ' STATUS CODE' + e.statusCode : '' }`);
        tmpE.extraData = CircularJSON.stringify(e);
        Raven.captureException(tmpE);
      }
    }
  }

  /**
   * Send preset error message or create generic one & log server error.
   * @param req
   * @param res
   * @param e
   */
  handleError(req, res, e) {
    res.statusCode = e.resStatusCode || 500;
    res.send({
      errorCode   : e.resCode || this.errors.unknownError.resCode,
      errorMessage: e.resMessage || (e.resCode ? `Error code: ${ e.resCode }` : this.errors.unknownError.resMessage)
    });

    if (res.statusCode === 500) {
      // Winston log
      this.log.error(
        `${
          !e.resStatusCode
            ?
            `/!\\ Unhandled error in ${ this.niceServiceName } service /!\\`
            :
            `Internal error in ${ this.niceServiceName } service`
          }
Request info: %j
Error message: %s`,
        {
          originalMethod: req.originalMethod,
          originalUrl   : req.originalUrl,
          UID           : req.user && req.user.uid,
          ACC           : req.user && req.user.acc,
          DB            : req.user && req.user.db
        },
        e.message
      );

      // Debug log for developers (prints nice stack)
      console.error(e);

      // Raven log
      if (hasRaven()) {
        e.extraData = {
          originalMethod : req.originalMethod,
          originalUrl    : req.originalUrl,
          UID            : req.user && req.user.uid,
          ACC            : req.user && req.user.acc,
          DB             : req.user && req.user.db,
          niceServiceName: this.niceServiceName
        };
        Raven.captureException(e);
      }
    }
  }
};

