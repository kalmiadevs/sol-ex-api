'use strict';

const _        = require('lodash'),
      Joi      = require('joi'),
      BaseApi  = require('./base-api'),
      log      = require('../log')(module),
      sanitize = require('mongo-sanitize');

/**
 * Abstract entity.
 *
 * A few guides & tips:
 * - functions are written in CRUD order (create, get, list, update, remove)
 * - When you have reject in Promise you can also add into error following parameters:
 *    { resStatusCode: number, resCode: string, resMessage: string }
 *    This params will be used when responding back to client (default behaviour).
 *
 *
 * Example usage, for using generic node collection:
 *
 * class OrderApi extends EntityApi {
 *  constructor() {
 *    super({
 *      entityCollectionName: 'nodes',
 *      entityName          : 'order',
 *      entitiesName        : 'orders',
 *      entitySchema        : undefined
 *    });
 *  }
 * }
 *
 */
module.exports = class EntityApi extends BaseApi {

  /**
   * EntityApi constructor. Please pass correct options.
   * @param options
   * @param options.entityCollectionName string Name of collection used for this class.
   * @param options.entityName string Mostly used for nice error messages & logs. Should be lowercase (instead of spaces use '-').
   * @param options.entitiesName string Mostly used for nice error messages & logs. Should be lowercase (instead of spaces use '-').
   * @param options.entitySchema Joi Used for entity schema validation.
   */
  constructor(options = {
    entityCollectionName: 'entity',
    entityName          : 'entity',
    entitiesName        : 'entities',
    entitySchema        : Joi.object().keys({ id: Joi.string().required() }),
    module              : module
  }) {
    super(options);

    this.entityCollectionName = options.entityCollectionName;
    this.entityName           = options.entityName;
    this.entitiesName         = options.entitiesName;
    this.entitySchema         = options.entitySchema;
  }


  /* ===VALIDATORS=== */

  /**
   * Check if object has valid structure.
   * It's not this function job to check if payload is semantically valid.
   * @param obj
   * @return {{error: Object, value: Object}}
   */
  validateSchema(obj) {
    if (!this.entitySchema) {
      return { error: null, value: obj };
    }
    return Joi.validate(obj, this.entitySchema, { stripUnknown: true });
  }


  /* ===CONVERTERS=== */

  setObjToExistingModel(obj, model) {
    _.forOwn(obj, (value, key) => {
      model[key] = value;
    });
  }

  /**https://www.google.si/search?q=service+locator+anti+pattern&gws_rd=cr&ei=BVWUWaHLHsbNjwSZmbj4Dw
   * Convert model to object ready to send to client.
   * It also strips any parameters that shouldn't be sent to client.
   * @param m Mongoose.model
   */
  modelToResObj(m) {
    return this.objToResObj(m.toObject());
  }

  /**
   * Strip any parameters that shouldn't be sent to client.
   * @param obj
   */
  objToResObj(obj) {
    return this.validateSchema(obj).value;
  }

  /**
   * Check if object has valid structure.
   * It's not this function job to check if payload is semantically valid.
   * @param obj
   * @return {boolean}
   */
  isSchemaValid(obj) {
    return this.validateSchema(obj).error === null;
  }

  getResErrorForInvalidSchema(obj, err = {}) {
    return {
      schemaError  : err,
      resStatusCode: 400,
      resCode      : `${this.entityName}_invalid_schema`,
      resMessage   : `Invalid object for ${ this.entityName }. ${ err.message }`
    };
  }

  populateFilterObject(o, data) {
    data = sanitize(JSON.parse(data));
    if (!_.isEmpty(data)) {
      _.each(_.toPairs(data), function (x) {
        let val;
        if (_.isString(x[1])) {
          val = { '$regex': x[1], '$options': 'i' };
        } else {
          if (x[1].from && x[1].to) {
            if (_.isString(x[1].from) && _.isString(x[1].to)) {
              val = { $gte: new Date(x[1].from), $lte: new Date(x[1].to) };
            } else {
              val = { $gte: x[1].from, $lte: x[1].to };
            }
          } else {
            val = x[1];
          }
        }
        o.push({ [x[0]]: val });
      });
    }
  }

  /* ===DB CRUD=== */

  /**
   * Return collection from req.db obj.
   * @param db
   * @return {*}
   */
  collection(db) {
    return db[this.entityCollectionName];
  }

  /**
   * Create entity.
   * @param db
   * @param x {Object<>}
   * @return {Promise.<Mongoose.model>}
   */
  $create(db, x) {
    return new Promise((resolve, reject) => {
      const item = this.collection(db)(x);
      item.save((err, model) => {
        if (err) {
          reject(err);
        } else {
          resolve(model);
        }
      });
    });
  }

  /**
   * Get specific entity.
   * @param db
   * @param id string
   * @return {Promise.<Mongoose.model>}
   */
  $get(db, id) {
    return new Promise((resolve, reject) => {
      let filter = { _id: id, deleted: false };
      this.collection(db).findOne(filter, (error, entity) => {
        if (!error) {
          resolve(entity);
        } else {
          reject(error);
        }
      });
    });
  }

  $getByFilter(db, filter) {
    return new Promise((resolve, reject) => {
      this.collection(db).findOne(filter, (error, entity) => {
        if (!error) {
          resolve(entity);
        } else {
          reject(error);
        }
      });
    });
  }

  /**
   * Get all entities.
   * @param db
   * @param options
   * @return {Promise.<Object<{items: Mongoose.model[],total,limit,offset,page,pages}>>}
   */
  $list(db, options = { noLean: false, filter: {} }) {
    return new Promise((resolve, reject) => {
      options.filter.deleted = options.filter.deleted || false;

      if ((options.offset !== undefined || options.page !== undefined) && options.limit) {
        // Paginated query
        let paginationOptions = {
          sort      : { [options.sort]: (options.sortOrder ? options.sortOrder : 'asc') },
          lean      : !options.noLean,
          leanWithId: true,
          limit     : parseInt(options.limit)
        };
        if (options.offset) paginationOptions.offset = parseInt(options.offset);
        if (options.page) paginationOptions.page = parseInt(options.page);

        this.collection(db).paginate(options.filter, paginationOptions)
          .then(function (result) {
            [result.items, result.docs] = [result.docs, result.items];
            resolve(result);
          })
          .catch(function (err) {
            reject(err);
          });
      } else {
        // Standard query
        let op = this.collection(db).find(options.filter);

        if (options.sort) op = op.sort({ [options.sort]: (options.sortOrder ? options.sortOrder : 'asc') });

        op = options.noLean ? op : op.lean({ virtuals: true });
        op.exec(function (err, models) {
          if (err) {
            reject(err);
          } else {
            resolve({ items: models });
          }
        });
      }
    });
  }

  /**
   * Update entity.
   * @param db
   * @param x {Object<>}
   * @return {Promise.<Mongoose.model[]>}
   */
  $update(db, x) {
    return new Promise((resolve, reject) => {
      this.$get(db, x.id)
        .then((entity) => {
          this.setObjToExistingModel(x, entity);
          entity.save((error, model) => {
            if (error) {
              reject(error);
            } else {
              resolve(model);
            }
          });
        })
        .catch(reject);
    });
  }

  /**
   * Delete specific entity.
   * @param db
   * @param id string
   * @return {Promise}
   */
  $remove(db, id) {
    return new Promise((resolve, reject) => {
      this.$get(db, id)
        .then((entity) => {
          entity.softdelete((error) => {
            if (error) {
              reject(error);
            } else {
              resolve(entity);
            }
          });
        })
        .catch(reject);
    });
  }


  /* ===API CRUD=== */

  //region CREATE
  /**
   * Create entity in collection.
   * @param req
   * @param req.db
   * @param req.body
   * @param res
   */
  async create(req, res) {
    const result = this.validateSchema(req.body);
    if (result.error) {
      return this.hook_createReqFail(req, res, this.getResErrorForInvalidSchema(req.body, result.error));
    }
    delete result.value.id;

    try {
      await this.hook_beforeCreate(req, res, result.value);
      const model = await this.$create(req.db, result.value);
      this.hook_createReqSuccess(req, res, model);
    } catch (e) {
      this.hook_createReqFail(req, res, e);
    }
  };

  hook_beforeCreate(req, res, entity) { return Promise.resolve(); }

  /**
   * Hook for create API success.
   * @param req Express req object.
   * @param res Express res object.
   * @param model Mongoose.model Entity model.
   */
  hook_createReqSuccess(req, res, model) {
    res.send({ [this.entityName]: this.modelToResObj(model), id: model._id });
  }

  /**
   * Hook for create API fail.
   * Hook must make sure to send HTTP response!
   * @param req Express req object.
   * @param res Express res object.
   * @param e Promise error
   * @param e.resStatusCode number
   * @param e.resCode string
   * @param e.resMessage string
   */
  hook_createReqFail(req, res, e) {
    e.resCode    = e.resCode || `${this.entityName}_create_fail`;
    e.resMessage = e.resMessage || `Can't create new ${this.entityName}.`;
    this.handleError(req, res, e);
  }

  //endregion

  //region READ - get by id
  /**
   * Get entity from collection.
   * @param req
   * @param req.db
   * @param req.params.id
   * @param res
   */
  get(req, res) {
    const self = this;

    this.$get(req.db, req.params.id)
      .then(function (data) {
        self.hook_getReqSuccess(req, res, data);
      })
      .catch(function (e) {
        self.hook_getReqFail(req, res, e);
      });
  };

  /**
   * Hook for get API success.
   * @param req Express req object.
   * @param res Express res object.
   * @param model Mongoose.model Entity model.
   */
  hook_getReqSuccess(req, res, model) {
    res.send({ [this.entityName]: this.modelToResObj(model) });
  }

  /**
   * Hook for get API fail.
   * Hook must make sure to send HTTP response!
   * @param req Express req object.
   * @param res Express res object.
   * @param e Promise error
   * @param e.resStatusCode number
   * @param e.resCode string
   * @param e.resMessage string
   */
  hook_getReqFail(req, res, e) {
    e.resCode    = e.resCode || `${this.entityName}_get_fail`;
    e.resMessage = e.resMessage || `Can't get ${this.entityName}.`;
    this.handleError(req, res, e);
  }

  //endregion

  //region READ - get all
  /**
   * Get entities from collection.
   * @param req
   * @param req.db
   * @param req.headers
   * @param req.params
   * @param req.query
   * @param res
   */
  async list(req, res) {
    const self = this;

    let filter = {};
    if (req.query.filter) {
      try {
        filter.$and = [];
        this.populateFilterObject(filter.$and, req.query.filter);
      } catch (e) {
        return self.hook_listReqFail(req, res, e);
      }
    }
    if (req.query.search) {
      try {
        filter.$or = [];
        this.populateFilterObject(filter.$or, req.query.search);
      } catch (e) {
        return self.hook_listReqFail(req, res, e);
      }
    }
    const options = {
      filter   : filter,
      offset   : req.query.offset,
      page     : req.query.page,
      limit    : req.query.limit,
      sort     : req.query.sort,
      sortOrder: req.query.sortOrder
    };

    await this.hook_beforeList(req, res, options);

    this.$list(req.db, options)
      .then(function (result) {
        self.hook_listReqSuccess(req, res, result);
      })
      .catch(function (e) {
        self.hook_listReqFail(req, res, e);
      });
  }

  hook_beforeList(req, res, options) {}

  /**
   * Hook for list API success.
   * @param req Express req object.
   * @param res Express res object.
   * @param result
   * @param result.items
   * @param result.total
   * @param result.limit
   * @param result.offset
   * @param result.page
   * @param result.pages
   */
  hook_listReqSuccess(req, res, result) {
    res.send({
      [this.entitiesName]: result.items.map(x => this.objToResObj(x)),
      total              : result.total,
      limit              : result.limit,
      offset             : result.offset,
      page               : result.page,
      pages              : result.pages,
      data               : result.data
    });
  }

  /**
   * Hook for list API fail.
   * Hook must make sure to send HTTP response!
   * @param req Express req object.
   * @param res Express res object.
   * @param e Promise error
   * @param e.resStatusCode number
   * @param e.resCode string
   * @param e.resMessage string
   */
  hook_listReqFail(req, res, e) {
    e.resCode    = e.resCode || `${this.entityName}_list_fail`;
    e.resMessage = e.resMessage || `Can't get ${this.entityName}.`;
    this.handleError(req, res, e);
  }

  //endregion

  //region UPDATE
  /**
   * Update entity from collection.
   * @param req
   * @param req.db
   * @param req.body
   * @param res
   */
  async update(req, res) {
    const self = this;

    const result = this.validateSchema(req.body);
    if (result.error) {
      return self.hook_createReqFail(req, res, this.getResErrorForInvalidSchema(req.body, result.error));
    }
    try {
      await this.hook_beforeUpdate(req, res, result.value);
      const model = await this.$update(req.db, result.value);
      self.hook_updateReqSuccess(req, res, model);
    } catch (e) {
      self.hook_updateReqFail(req, res, e);
    }
  }

  async hook_beforeUpdate(req, res, entity) { return Promise.resolve(); }


  /**
   * Hook for update API success.
   * @param req Express req object.
   * @param res Express res object.
   * @param model Mongoose.model Entity model.
   */
  hook_updateReqSuccess(req, res, model) {
    res.send({ [this.entityName]: this.modelToResObj(model) });
  }

  /**
   * Hook for update API fail.
   * Hook must make sure to send HTTP response!
   * @param req Express req object.
   * @param res Express res object.
   * @param e Promise error
   * @param e.resStatusCode number
   * @param e.resCode string
   * @param e.resMessage string
   */
  hook_updateReqFail(req, res, e) {
    e.resCode    = e.resCode || `${this.entityName}_update_fail`;
    e.resMessage = e.resMessage || `Can't update ${this.entityName}.`;
    this.handleError(req, res, e);
  }

  //endregion

  //region DELETE
  /**
   * Get entity from collection.
   * @param req
   * @param req.db
   * @param req.params.id
   * @param res
   */
  remove(req, res) {
    const self = this;

    this.$remove(req.db, req.params.id)
      .then(function (model) {
        self.hook_removeReqSuccess(req, res, model._doc._id);
      })
      .catch(function (e) {
        self.hook_removeReqFail(req, res, e);
      });
  };

  /**
   * Hook for remove API success.
   * @param req Express req object.
   * @param res Express res object.
   * @param id  Entity id.
   */
  hook_removeReqSuccess(req, res, id) {
    res.send({ id: id });
  }

  /**
   * Hook for remove API fail.
   * Hook must make sure to send HTTP response!
   * @param req Express req object.
   * @param res Express res object.
   * @param e Promise error
   * @param e.resStatusCode number
   * @param e.resCode string
   * @param e.resMessage string
   */
  hook_removeReqFail(req, res, e) {
    e.resCode    = e.resCode || `${this.entityName}_remove_fail`;
    e.resMessage = e.resMessage || `Can't remove ${this.entityName}.`;
    this.handleError(req, res, e);
  }

  //endregion


  //region AGGREGATE
  /**
   * Hook for aggregate API success.
   * @param req Express req object.
   * @param res Express res object.
   * @param stats Aggregated data.
   */
  hook_aggregateReqSuccess(req, res, stats) {
    res.send({ stats: stats });
  }

  /**
   * Hook for remove API fail.
   * Hook must make sure to send HTTP response!
   * @param req Express req object.
   * @param res Express res object.
   * @param e Promise error
   * @param e.resStatusCode number
   * @param e.resCode string
   * @param e.resMessage string
   */
  hook_aggregateReqFail(req, res, e) {
    e.resCode    = e.resCode || `${this.entityName}_aggregate_fail`;
    e.resMessage = e.resMessage || `Can't aggregate ${this.entityName}.`;
    this.handleError(req, res, e);
  }

  //endregion
};
