// 'use strict';
const fs               = require('fs-extra'),
      path             = require('path'),
      find             = require('find'),
      CircularJSON     = require('circular-json'),
      _                = require('lodash');

/**
 *  * n = new log()
 * n.log()
 * n.error()
 * n.info()
 *
 *
 *
 *
 * danger
 *
 * __header
 * __footer
 * __alert
 *
 *
 * @type {module.FsLogApi}
 */
module.exports = class FsLogApi {

  /**
   * Log service. Saves logs into bucket /logs/:category
   * @param bucket Example: sync or [sync, articles]
   * @param options {{prefix, postfix}|{name}|string}
   */
  constructor(bucket = [], options = {}, postfix = '.log') {
    this.basePath = path.resolve(process.cwd(), 'storage/logs/');
    if (_.isString(options)) {
      options = {
        prefix: options,
        postfix: postfix,
      }
    }

    options.prefix  = options.prefix || '';
    options.postfix = options.postfix || '.log';

    const dateObj    = new Date();

    const month      = dateObj.getUTCMonth() + 1; //months from 1-12
    const day        = dateObj.getUTCDate();
    const year       = dateObj.getUTCFullYear();

    let hour = ('' + dateObj.getUTCHours()).padStart(2, "0");
    let min  = ('' + dateObj.getUTCMinutes()).padStart(2, "0");
    let sec  = ('' + dateObj.getUTCSeconds()).padStart(2, "0");
    let mili = ('' + dateObj.getUTCMilliseconds()).padStart(3, "0");

    const folderName = `${year}-${month}-${day}`;
    if (options.name) {
      this.name = options.name;
    } else {
      this.name = `${options.prefix}${hour}-${min}-${sec}-${mili}${options.postfix}`;
    }

    if (!_.isArray(bucket)) {
      bucket = [bucket];
    }

    bucket.unshift(folderName);
    bucket = bucket.join('/');

    this.bucketPath = path.resolve(this.basePath, bucket);
    this.path = this.bucketPath + '/' + this.name;

    this.logStream = undefined;
  };

  /**
   *
   * @return {module.FsLogApi}
   */
  toStream() {
    fs.ensureFileSync(this.path);
    this.logStream = fs.createWriteStream(this.path, {'flags': 'a'});
    return this;
  }

  /**
   *
   * @param msg
   * @return {module.FsLogApi}
   */
  end(msg) {
    if (this.logStream) {
      this.logStream.end(msg || "");
    }
    return this;
  }

  log(data) {
    if (_.isString(data)) { data = { __message: data } }
    data.__type = 'log';
    return this.add(data);
  }

  info(data) {
    if (_.isString(data)) { data = { __message: data } }
    data.__type = 'info';
    return this.add(data);
  }

  warning(data) {
    if (_.isString(data)) { data = { __message: data } }
    data.__type = 'warning';
    return this.add(data);
  }

  error(data) {
    if (_.isString(data)) { data = { __message: data } }
    data.__type = 'danger';
    return this.add(data);
  }

  danger(data) {
    if (_.isString(data)) { data = { __message: data } }
    data.__type = 'danger';
    return this.add(data);
  }

  ok(data) {
    if (_.isString(data)) { data = { __message: data } }
    data.__type = 'success';
    return this.add(data);
  }

  success(data) {
    if (_.isString(data)) { data = { __message: data } }
    data.__type = 'success';
    return this.add(data);
  }

  alert(data) {
    if (_.isString(data)) { data = { __message: data } }
    data.__alert = true;
    return this.add(data);
  }

  alertOk(data) {
    if (_.isString(data)) { data = { __message: data } }
    data.__type = 'success';
    return this.alert(data);
  }

  alertInfo(data) {
    if (_.isString(data)) { data = { __message: data } }
    data.__type = 'info';
    return this.alert(data);
  }

  alertSuccess(data) {
    if (_.isString(data)) { data = { __message: data } }
    data.__type = 'success';
    return this.alert(data);
  }

  alertError(data) {
    if (_.isString(data)) { data = { __message: data } }
    data.__type = 'danger';
    return this.alert(data);
  }

  add(data) {
    console.log(data);
    data.__timestamp = new Date();
    try {
      // data = JSON.stringify(data);
      if ((data.constructor ? data.constructor.name : '?') === 'StatusCodeError') {
        data.__url = (data.options || {}).url;
        delete data.options;
        delete data.response;
        delete data.__proto__;
      }

      data = CircularJSON.stringify(data);
    } catch(e) {
      data = { error: e.message, errorInLog: true };
      data = JSON.stringify(data);
    }
    data = data + '\n';
    if (this.logStream) {
      this.logStream.write(data);
    } else {
      fs.ensureFileSync(this.path);
      fs.appendFile(this.path, data, function (err) {
        if (err) {
          console.error(err);
        }
      });
    }
    return this;
  }
};
