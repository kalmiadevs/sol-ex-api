const _         = require('lodash'),
      BaseApi   = require('./base-api');


module.exports = class TestApi extends BaseApi {

  constructor(options = {
    niceServiceName: 'testService',
    module         : module
  }) {
    options.module          = options.module || module;
    options.niceServiceName = options.niceServiceName || 'test service';
    super(options);
  }


  status(req, res) {
    res.send({
      online: true
    });
  }

  connectivity(req, res) {

  };

};
