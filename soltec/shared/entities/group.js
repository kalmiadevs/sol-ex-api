var debug        = require('debug')('group_svc'),
    _            = require('lodash'),
    errors       = require('./../error'),
    handleReject = require('./../errorHandler'),
    Q            = require('q');

"use strict";

/**
 * CRUD for groups.
 * @param c
 * @param log
 * @return {{}}
 */
module.exports = function (c, log) {
  var module = {};

  /**
   * Key used for this api calls
   * @type {string}
   */
  var apiKey = 'role';

  /**
   * Create group
   * @param req
   * @param group {Object}
   * @return {Q.Promise}
   */
  module.create = function (req, group) {
    var def = Q.defer();
    delete group.id;

    var options = {
      url    : c.get('authApi') + '/' + apiKey + '/' + req.user.acc,
      method : 'POST',
      form   : group
    };

    req.authRequestJson(options)
      .then(function (x) {
        x.role.id = x.role._id;
        def.resolve(x.role);
      })
      .catch(function (e) {
        if (e.json) {
          if (e.json.code === 11000) {
            return handleReject(req, def, errors.group_already_exists)(e, 400);
          }
        }

        handleReject(req, def, errors.group_create_fail)(e);
      });

    return def.promise;
  };

  /**
   * Get group
   * @param req
   * @param gid  {string}
   * @return {Q.Promise}
   */
  module.get = function (req, gid) {
    var def = Q.defer();

    // var options = {
    //   url: c.get('authApi') + '/' + apiKey + '/' + req.user.acc // + '/' + gid
    // };

    // TODO just ugly hack
    module.getAll(req)
      .then(function (groups) {
        def.resolve(_.find(groups, function(o) { return o.id === gid; }));
      })
      .catch(function (e) {
        def.reject(e);
      });

    // req.authRequestJson(options)
    //   .then(function (x) {
    //     x.id = x._id;
    //     def.resolve(x);
    //   })
    //   .catch(handleReject(req, def, errors.group_get_fail));

    return def.promise;
  };

  /**
   * Get groups
   * @param req
   * @return {Q.Promise}
   */
  module.getAll = function (req) {
    var def = Q.defer();

    var options = {
      url: c.get('authApi') + '/' + apiKey + '/' + req.user.acc,
    };

    req.authRequestJson(options)
      .then(function (x) {
        _.forEach(x, function (y) {
          y.id = y._id;
        });
        def.resolve(x);
      })
      .catch(handleReject(req, def, errors.group_get_all_fail));

    return def.promise;
  };

  /**
   * Update group
   * @param req
   * @param group {Object}
   * @param group.id
   * @return {Q.Promise}
   */
  module.update = function (req, group) {
    var def = Q.defer();

    var options = {
      url     : c.get('authApi') + '/' + apiKey + '/' + req.user.acc + '/' + group.id,
      method  : 'PUT',
      form    : group
    };

    req.authRequestJson(options, { forwardError: true })
      .then(function (x) {
        x.role.id = x.role._id;

        // TODO why we can't save number into db?
        x.role.permissions = _.mapValues(x.role.permissions, (x) => {
          return + x;
        });
        def.resolve(x.role);
      })
      .catch((e) => {
        handleReject(req, def, errors.group_update_fail)(e)
      });

    return def.promise;
  };

  /**
   * Delete group
   * @param req
   * @param gid {string}
   * @return {Q.Promise}
   */
  module.delete = function (req, gid) {
    var def = Q.defer();

    var options = {
      url     : c.get('authApi') + '/' + apiKey + '/' + req.user.acc + '/' + gid,
      method  : 'DELETE',
    };

    req.authRequestJson(options, { forwardError: true })
      .then(function (x) {
        def.resolve(x);
      })
      .catch(handleReject(req, def, errors.group_delete_fail));
    
    return def.promise;
  };

  return module;
};