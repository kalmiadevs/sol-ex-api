var debug        = require('debug')('node_svc'),
    _            = require('lodash'),
    errors       = require('./../error'),
    handleReject = require('./../errorHandler'),
    Q            = require('q');

"use strict";

/* ============================================
 * NOT FOR DIRECT USE!
 * This is intended for other services like
 * kalPass to import and use, so they don't need
 * to reinvent stuff.
 * ============================================ */

/**
 * CRUD for nodes.
 * @param c
 * @param log
 * @return {{}}
 */
module.exports = function (c, log) {
  var module = {};

  /**
   * Key used for this api calls
   * @type {string}
   */
  var apiKey = 'nodes';
  
  /**
   * Create node
   * @param req
   * @param node {Object}
   * @return {Q.Promise}
   */
  module.create = function (req, node) {
    var def = Q.defer();
    delete node.id;

    var options = {
      url    : c.get('kbApi') + '/' + apiKey,
      method : 'POST',
      form   : node
    };

    options.form.meta = {
      project : c.get('nodeProjectId')
    };

    req.authRequestJson(options)
      .then(function (x) {
        x.node.id = x.node._id;
        def.resolve(x.node);
      })
      .catch(handleReject(req, def, errors.node_create_fail));

    return def.promise;
  };

  /**
   * Get node
   * @param req
   * @param nid  {string}
   * @return {Q.Promise}
   */
  module.get = function (req, nid) {
    var def = Q.defer();

    var options = {
      url: c.get('kbApi') + '/' + apiKey + '/' + nid
    };

    req.authRequestJson(options)
      .then(function (x) {
        x.node.id = x.node._id;
        def.resolve(x.node);
      })
      .catch(handleReject(req, def, errors.node_get_fail));

    return def.promise;
  };

  /**
   * Get nodes
   * @param req
   * @param filters
   * @return {Q.Promise}
   */
  module.getAll = function (req, filters) {
    var def = Q.defer();

    var f = filters || {};
    if (f.meta) {
      f.meta.push({name: 'project', value: c.get('nodeProjectId')});
    } else {
      f.meta = [
        {name: 'project', value: c.get('nodeProjectId')}
      ]
    }

    var options = {
      url: c.get('kbApi') + '/' + apiKey,
      headers : { 'filters' : JSON.stringify(f) }
    };

    req.authRequestJson(options)
      .then(function (x) {
        x = _.forEach(x, function (y) {
          y.id = y._id;
        });
        def.resolve(x);
      })
      .catch(handleReject(req, def, errors.node_get_all_fail));

    return def.promise;
  };

  /**
   * Update node
   * @param req
   * @param node {Object}
   * @param node.id
   * @return {Q.Promise}
   */
  module.update = function (req, node) {
    var def = Q.defer();

    var options = {
      url     : c.get('kbApi') + '/' + apiKey + '/' + node.id,
      method  : 'PUT',
      form    : node
    };

    options.form.meta = {
      project : c.get('nodeProjectId')
    };

    req.authRequestJson(options, { forwardError: true })
      .then(function (x) {
        x.node.id = x.node._id;
        def.resolve(x.node);
      })
      .catch(handleReject(req, def, errors.node_update_fail));

    return def.promise;
  };

  /**
   * Delete node
   * @param req
   * @param nid {string}
   * @return {Q.Promise}
   */
  module.delete = function (req, nid) {
    var def = Q.defer();

    var options = {
      url     : c.get('kbApi') + '/' + apiKey +'/' + nid,
      method  : 'DELETE'
    };

    req.authRequestJson(options, { forwardError: true })
      .then(function (x) {
        def.resolve(x);
      })
      .catch(handleReject(req, def, errors.node_delete_fail));
    
    return def.promise;
  };

  return module;
};