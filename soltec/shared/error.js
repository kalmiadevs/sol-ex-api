var _           = require('lodash');

var genericErrors = {
  'already_exists': ''
};

var errors = {
  'group_get_fail': 'Failed to get group',
  'group_get_all_fail': 'Failed to get groups',
  'group_create_fail': 'Failed to create group',
  'group_update_fail': 'Failed to update group',
  'group_delete_fail': 'Failed to delete group',

  'node_get_fail': 'Failed to get node',
  'node_get_all_fail': 'Failed to get nodes',
  'node_create_fail': 'Failed to create node',
  'node_update_fail': 'Failed to update node',
  'node_delete_fail': 'Failed to delete node',

  'node_handle_groups_fail': '',

  'user_get_fail': 'Failed to get user',
  'user_get_all_fail': 'Failed to get users',
  'user_create_fail': 'Failed to create user',
  'user_update_fail': 'Failed to update user',
  'user_delete_fail': 'Failed to delete user',
  'users_batch_keyrings_update': 'users_batch_keyrings_update',

  // IDMS responses
  'user_already_exists': 'User already exists',
  'group_already_exists': 'Group already exists',

};

/**
 * @type {Object.<string, {code: string, msg: string}>}
 */
module.exports = _.mapValues(errors, function(val, key) {
  return { code: key, msg: val };
});