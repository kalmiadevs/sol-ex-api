let errors       = require('./error');

/**
 * Helper for handling error responses.
 * Note: this should be used only for entities!
 * @param req
 * @param def
 * @param error
 * @return {Function}
 */
module.exports = function handleReject(req, def, error) {
  let errorCode = error.code || error;
  return function(e, statusCode) {
    console.error(e);

    def.reject({
      errorCode   : errorCode,
      errorMessage: errors[errorCode].msg,
      error: e,
      finish: function () {
        req.res.statusCode = statusCode || 500;
        req.res.send({
          errorCode   : errorCode,
          errorMessage: errors[errorCode].msg,
        });
      }
    });
  }
};