var Q       = require('q'),
    request = require('request');

/**
 * Modifies original 'req' object.
 * @param log
 * @return {*}
 */
module.exports = function (log) {
  "use strict";

  module.addRequestWrapper = function (req, res, next) {

    /**
     * Make request
     * @param reqOptions
     * @param options Optional options
     * @param options.forwardError boolean
     * @param options.disableErrorJson boolean
     * @param options.forwardBodyError boolean
     * @param options.json boolean
     * @param options.errorHandler (error, response, body) => boolean
     * @return {Promise}
     */
    req.authRequest = function (reqOptions, options) {
      "use strict";

      options = options || {};

      reqOptions.headers = reqOptions.headers || {};
      reqOptions.headers['x-access-token'] = req.token;
      var def            = Q.defer();

      request(reqOptions, function (error, response, body) {

        // error handling party
        if (error || response.statusCode !== 200) {
          let json = undefined;

          if (!options.disableErrorJson) {
            try {
              json = JSON.parse(body);
            } catch (e) { }
          }

          // TODO fin fun

          return def.reject({
            error: error,
            response: response,
            body: body,
            json: json
          });

          /*
           if (options.forwardBodyError) {
           res.statusCode = response.statusCode;
           res.send(body);

           return def.reject({
           res: {
           error: error, response: response, body: body
           },
           forwardedBodyError: true
           });
           }

           // if caller wan'ts to handle error
           if (options.errorHandler) {

           // if caller failed to handle error or wan't us to handle it
           if (options.errorHandler(error, response, body)) {
           return def.reject({
           res: {
           error: error, response: response, body: body
           },
           handled: true
           });
           }
           }

           res.statusCode = 500;
           res.send({error: 'Server error'});

           log.error('Internal error(%d): %s', res.statusCode, (error ? error.message : body));

           return def.reject({
           res: {
           error: error, response: response, body: body
           }
           });*/
        }

        if (options.json) {
          try {
            body = JSON.parse(body);
          } catch (e) {
            res.statusCode = 500;
            res.send({error: 'Server error'});

            log.error('Internal error(%d): %s', res.statusCode, (error ? error.message : body));

            return def.reject({
              res: {
                error: error, response: response, body: body
              },
              jsonError: e
            });
          }
          return def.resolve(body, response);
        }

        def.resolve(body, response);
      });

      return def.promise;
    };

    /**
     * Make json request
     * @param reqOptions
     * @param options Optional options
     * @param options.forwardBodyError boolean
     * @param options.errorHandler (error, response, body) => boolean
     * @return {Promise}
     */
    req.authRequestJson = function (reqOptions, options) {
      "use strict";

      options      = options || {};
      options.json = true;

      return req.authRequest(reqOptions, options);
    };

    return next();
  };

  return module;
};