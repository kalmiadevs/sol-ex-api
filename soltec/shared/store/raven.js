const Raven = require('raven');
"use strict";

let installed = false;

module.exports.isInstalled = () => {
   return installed;
};

module.exports.config = (conf, options = {}) => {
  if (conf) {
    Raven.config(conf, options).install();
    installed = true;
  }
};


module.exports.Raven = Raven;
