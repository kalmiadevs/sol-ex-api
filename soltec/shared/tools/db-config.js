"use strict";

module.exports.get = (db, key, def) => {
  return new Promise((resolve, reject) => {
    db['config'].findOne({ key: key  }, (error, entity) => {
      if (!error) {
        resolve(entity === null ? def : entity.val);
      } else {
        console.error(error);
        resolve(def);
      }
    });
  });
};

module.exports.set = (db, key, val) => {
  return new Promise((resolve, reject) => {
    db['config'].findOne({ key: key  }, (error, entity) => {
      if (error) {
        reject(error);
      }

      if (entity === null) {
        entity = db['config']({key, val});
      }

      if (val) {
          entity.val = val;
          entity.save((error, entity) => {
              if (!error) {
                  resolve();
              } else {
                  console.error(error);
                  reject();
              }
          });
      } else {
          entity.remove((error) => {
              if (!error) {
                  resolve();
              } else {
                  console.error(error);
                  reject();
              }
          });
      }
    });
  });
};
