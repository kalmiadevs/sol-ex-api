const mongoose   = require('mongoose'),
      softDelete = require('mongoose-softdelete'),
      historical    = require('historical'),
      c          = require('../../lib/configuration'),
      fs         = require('fs-extra'),
      path       = require('path'),
      DataTable  = require('mongoose-datatable');

"use strict";

/**
 * Get client collection (db) as promise.
 * @param dbName
 * @return {Promise}
 */
function getClientConnection(dbName) {
  const uri = c.get('mongo:clients');
  if (!uri) {
    return Promise.reject('Missing configuration: mongo:clients');
  }
  return getConnection(uri + dbName + '?authSource=admin');
}

/**
 *
 * @param dbName
 * @param subModelsPath
 * @return {*}
 */
function getClientConnectionAndInitModels(dbName, subModelsPath) {
  const uri = c.get('mongo:clients');
  if (!uri) {
    return Promise.reject('Missing configuration: mongo:clients');
  }
  return getConnectionAndInitModels(uri + dbName + '?authSource=admin', subModelsPath);
}

/**
 * Get connection to db as promise.
 * @param uri Connection string.
 * @param options Connection string.
 * @return {Promise}
 */
function getConnection(uri) {
  return new Promise((resolve, reject) => {
    const db = mongoose.createConnection(uri);
    db.on('error', reject);
    db.once('open', () => {
      resolve(db);
    });
  });
}

/**
 *
 * @param uri
 * @param subModelsPath You can specify sub dir path inside /lib/models if you don't wan't to process root models.
 * @return {Promise}
 */
function getConnectionAndInitModels(uri, subModelsPath = '') {
  return new Promise((resolve, reject) => {
    getConnection(uri)
      .then(db => {
        modelsInit(db, subModelsPath)
          .then(resolve)
          .catch(reject)
      })
      .catch(reject);
  });
}

/**
 * Process models for db as promise.
 * Note: This won't cache models. There is no check if they were already processed.
 * If you need cache functionality you can create new function beside this one.
 *
 * @param db
 * @param subModelsPath You can specify sub dir path inside /lib/models if you don't wan't to process root models.
 * @return {Promise}
 */
function modelsInit(db, subModelsPath = '') {
  const modelsPath = path.resolve(process.cwd(), path.join('lib/models/', subModelsPath));
  return new Promise((resolve, reject) => {
    fs.readdir(modelsPath, (err, files) => {
      if (err) {
        return reject(err);
      }

      const out = files.reduce((acc, f) => {
        const rFilePath = path.join('../../lib/models/', subModelsPath, f);
        const aFilePath = path.join(modelsPath, f);
        const stat      = fs.lstatSync(aFilePath);

        if (stat.isDirectory()) {
          return acc;
        }

        const modelName = f.substr(0, f.lastIndexOf('.'));
        acc[modelName] = require(aFilePath)(mongoose, db, [softDelete, DataTable.init, historical]);

        return acc;
      }, {});

      resolve(out);
    });
  });
}


// EXPORTS
module.exports.getClientConnection              = getClientConnection;
module.exports.getClientConnectionAndInitModels = getClientConnectionAndInitModels;
module.exports.getConnection                    = getConnection;
module.exports.modelsInit                       = modelsInit;