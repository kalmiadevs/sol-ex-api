# Application client storage

This folder contains buckets similar to Amazon S3. All content should be put inside buckets.
No content should be put directly into the root of this folder (besides this file). Bucket inside bucket is allowed.
Bucket in this context is just folder inside folder `storage`.


You should make use of `fs-api.js` since it proves CRUD for files and security layer.


Known buckets:
- templates
- orders
- media
- logs