var chakram = require('chakram');

// Get authentication token and user id
module.exports = function(config) {
  before(function() {
    var options = {
      body: {
        email: config.testUserEmail,
        password: config.testUserPassword
      }
    };
    return chakram.request('POST', config.baseAuthUrl + '/login', options)
    .then(function(response) {
      // Set token and user id in config object
      config.authToken = response.body.token;
      config.userId = response.body.user._id;
    });
  });
}
