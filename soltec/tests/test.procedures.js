var chakram = require('chakram');
var expect = chakram.expect;
var fs = require('fs');
var auth = require('./auth');

// Load config
var config = JSON.parse(fs.readFileSync(__dirname + '/config.json', 'utf8'));
// Get auth token and user id and save them in config object
auth(config);

// Test procedures/user/:uid
describe('procedures/user/:uid', function() {

  // Make call and save response before running tests
  var response;
  before(function() {
    var options = {
      headers: {
        'x-access-token': config.authToken
      }
    };
    return chakram.request('GET', config.baseApiUrl + '/api/v1/procedures/user/' + config.userId, options)
    .then(function(res) {
      response = res;
    });
  });

  describe('User', function() {
    it('should not be able to list procedures if not authenticated', function () {
      return chakram.request('GET', config.baseApiUrl + '/api/v1/procedures/user/' + config.userId)
      .then(function(response) {
        expect(response).to.have.status(401);
      });
    });
    it('should be able to list procedures if authenticated', function () {
      expect(response).to.have.status(200);
    });
  });

  describe('User procedures', function() {
    it('should be grouped in categories', function () {
      expect(response.body).to.have.property('categories');
      expect(response.body.categories).to.be.a('array');
    });
  });

  describe('Each category', function() {
    it('should contain procedures', function () {
      expect(response.body).to.have.property('categories');
      var categories = response.body.categories;
      for (var i = 0; i < categories.length; i++) {
        expect(categories[i]).to.have.property('procedures').with.length.above(0);
      }
    });
  });
});
