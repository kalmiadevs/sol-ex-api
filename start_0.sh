#!/usr/bin/env bash

cd ./chrome
docker build -t chrome-sol .
docker run -d --privileged -p 9222:9222 chrome-sol


cd ../mongo
docker build -t mongo-sol .
mkdir data
docker run -d -v "`pwd`/data":/data/db -p 27017:27017 mongo-sol

cd ..
