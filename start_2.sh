#!/usr/bin/env bash

cd ./base/license
npm install

cd shared
npm install

cd ..
node app.js

cd ../..
