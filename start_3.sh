#!/usr/bin/env bash

cd ./soltec
npm install

cd shared
npm install

cd ..

node app.js

cd ../..
